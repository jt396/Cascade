/*
===============================================================================

	XOF
	===
	File	:	XOF_Clip.hpp
	Desc	:	Basic representation of an animation clip.

===============================================================================
*/
#ifndef XOF_CLIP_HPP
#define XOF_CLIP_HPP


#include "Platform/XOF_Platform.hpp"
#include "Animation/XOF_Pose.hpp"


struct AnimationSample {
	AnimationSample() : jointPoses( nullptr ) {}
	~AnimationSample() { if ( jointPoses ) { delete [] jointPoses; } }
	
	JointPose *jointPoses;
};

struct AnimationClip {
	AnimationClip() : skeleton( nullptr ), samples( nullptr ) {}
	~AnimationClip() { if( skeleton ) { skeleton = nullptr; } if( samples ) { delete [] samples; } }

	Skeleton		*skeleton;
	float			framesPerSecond;
	U32				frameCount;
	AnimationSample	*samples;	// alloate frameCount + 1 instances if looping else frameCount
	bool			isLooping;
};


#endif // XOF_CLIP_HPP
