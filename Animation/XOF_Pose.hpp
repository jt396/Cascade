/*
===============================================================================

	XOF
	===
	File	:	XOF_Pose.hpp
	Desc	:	Basic animation pose representation for use in skeletal animation.
				Skeletal-animation is done by having the skeleton rapidly
				iteratre through a collection of poses (collectively known as
				a clip). A pose for the whole skeleton is therefore the
				summation of its joint poses.

===============================================================================
*/
#ifndef XOF_POSE_HPP
#define XOF_POSE_HPP


#include <glm/gtc/quaternion.hpp>
#include <glm/vec3.hpp>

#include "Platform/XOF_Platform.hpp"
#include "Animation/XOF_Skeleton.hpp"


struct JointPose {
	glm::fquat	rotation;
	double		tempRotationTime;
	glm::vec3	translation;
	double		tempTranslationTime;
	double		scale; // uniform-scale
};

struct SkeletonPose {
	Skeleton	*skeleton;		// skeleton + num of joints
	JointPose	*localJointPoses;
	glm::mat4x4	globalPose;
};


#endif // XOF_POSE_HPP
