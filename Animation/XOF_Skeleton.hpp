/*
===============================================================================

	XOF
	===
	File	:	XOF_Skeleton.hpp
	Desc	:	Basic skeleton representation for use in skeletal animation.

===============================================================================
*/
#ifndef XOF_SKELETON_HPP
#define XOF_SKELETON_HPP


#include <glm/mat4x3.hpp>

#include "Platform/XOF_Platform.hpp"


static const U8 XOF_ROOT_JOINT = 0XFF;


struct Joint {
    glm::mat4x4	tempOffset;			// temporary to help with assimp->xof transition?
                                    // describes transformation from mesh space to bone space in bind pose
    glm::mat4x4 tempFinalTransform; // another temporary to help with assimp->xof?
    glm::mat4x4	inverseBindPose;	// what about bindPoseMatrix?
    std::string	name;               // a hashed string would be faster
    U8			parentIndex;
};

struct Skeleton {
			Skeleton() : joints( nullptr ) {}
			~Skeleton() { if( joints ) { delete [] joints; } }

	U32		jointCount;
	Joint	*joints;
			// name/UID?
};


#endif // XOF_SKELETON_HPP
