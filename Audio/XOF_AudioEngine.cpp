/*
===============================================================================

	XOF
	===
	File	:	XOF_AudioEngine.cpp
	Desc	:	Audio equivalent of the renderer, processes audio requests.

===============================================================================
*/
#include "XOF_AudioEngine.hpp"
#include "XOF_SoundEffect.hpp"
#include "XOF_Music.hpp"


bool AudioEngine::Startup() { 
	//mAudioManager = AudioManager::Get();
	return true; 
}

void AudioEngine::Shutdown() { 
	mAudioRequests.clear(); 
}

void AudioEngine::SubmitAudioRequest( AudioRequest& request ) {
    XOF_ASSERT( IsValidAudioRequest( request.type ) );
    // TODO: - Does this work as expected?
	mAudioRequests.push_back( std::move( request ) );
}

void AudioEngine::Update() {
    for( const AudioRequest& req : mAudioRequests ) {
        switch( req.type ) {
            case AudioRequestType::playMusic: req.soundEffect->Play(); break;
            case AudioRequestType::playSoundEffect: req.music->Play(); break;
            // TODO: STOP_MUSIC etc.
        }
	}
	mAudioRequests.clear();
}
