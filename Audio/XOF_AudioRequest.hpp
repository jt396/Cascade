/*
===============================================================================

	XOF
	===
	File	:	XOF_AudioRequest.hpp
	Desc	:	Allows game objects to make requests of the audio system,
				used to implement basic batched upating. Objects submit
				requests then the game loop tells the audio system to 
				processes requests, calling the appropriate audio functions.

				AUDIO API AGNOSTIC. API SPECIFIC FLAGS ARRAY TO BE ADDED
				TO RENDERER CLASS/IMPLEMENTATION.

===============================================================================
*/
#ifndef XOF_AUDIO_REQUEST_HPP
#define XOF_AUDIO_REQUEST_HPP


#include "Platform/XOF_Platform.hpp"
class Music;
class SoundEffect;


enum class AudioRequestType : U32 {
	// Music
    playMusic = 0,
    pauseMuse,
    stopMusic,
    restartMusic,
	// Sound FX
    playSoundEffect,
    pauseSoundEffect,
    stopSoundEffect,
    restartSoundEffect,
    invalid,
	// etc...
};

inline constexpr bool IsValidAudioRequest( AudioRequestType requestType ) {
    return requestType >= AudioRequestType::playMusic &&
           requestType <= AudioRequestType::restartSoundEffect;
}
inline constexpr bool IsSoundEffectAudioRequest( AudioRequestType requestType ) {
    return requestType >= AudioRequestType::playSoundEffect &&
           requestType <= AudioRequestType::restartSoundEffect;
}


struct AudioRequest {
    AudioRequest() : type{AudioRequestType::invalid},
                     music{nullptr}, soundEffect{nullptr}, volume{0.f} {}

    AudioRequest( AudioRequestType type, Music *music,
                  float volume, const std::string& audioFileName ) {
        XOF_ASSERT( IsValidAudioRequest( type ) );
        this->type = type;
        this->music = music;
		this->volume = volume;
		this->audioFileName = audioFileName;
	}

    AudioRequest( AudioRequestType type, SoundEffect *soundEffect,
                  float volume, const std::string& audioFileName ) {
        XOF_ASSERT( IsValidAudioRequest( type ) );
        this->type = type;
        this->soundEffect = soundEffect;
        this->volume = volume;
        this->audioFileName = audioFileName;
    }

	AudioRequest( AudioRequest&& rhs ) {
        XOF_ASSERT( IsValidAudioRequest( type ) );
        type = rhs.type;

        if( IsSoundEffectAudioRequest( rhs.type ) ) {
            soundEffect = rhs.soundEffect;
        } else {
            music = rhs.music;
		}

        volume = rhs.volume;
        audioFileName = rhs.audioFileName;
	}

    AudioRequest( AudioRequest& ) = delete;
    AudioRequest& operator=( AudioRequest& ) = delete;
    AudioRequest& operator=( AudioRequest&& ) = delete;

    // ---

    AudioRequestType	type;

	union {
        Music			*music;
        SoundEffect		*soundEffect;
	};

    float				volume;
    std::string			audioFileName;
};


#endif // XOF_AUDIO_REQUEST_HPP
