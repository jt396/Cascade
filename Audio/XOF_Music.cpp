/*
===============================================================================

	XOF
	===
	File	:	XOF_Music.cpp
	Desc	:	Basic music class, intended for background music streamed in
				from memory.

===============================================================================
*/
#include "XOF_Music.hpp"


Music::Music() {}

Music::~Music() {}

bool Music::Load( ResourceDescriptor *desc )  {
    std::string fullName( desc->directory.CStr() );

    if( mIsLoaded = mMusic.openFromFile( fullName.append( desc->fileName.CStr() ) ) ) {
        mMusic.setVolume( 100.f );
    }

    return mIsLoaded;
}
