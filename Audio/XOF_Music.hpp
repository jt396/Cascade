/*
===============================================================================

	XOF
	===
	File	:	XOF_Music.hpp
	Desc	:	Basic music class, intended for background music streamed in
				from memory.

===============================================================================
*/
#ifndef XOF_MUSIC_HPP
#define XOF_MUSIC_HPP


#include <SFML/Audio.hpp>

#include "Resources/CAS_Resource.hpp"


class Music : public Resource {
public:
                    Music();
                    ~Music();

    bool            Load( ResourceDescriptor *desc ) override;

    inline void     Play();
    inline void     Pause();
    inline void     Stop();
    inline void     Restart();

    inline F32      GetDuration() const;

    virtual CHAR*   GetName() const override { return (CHAR*)musicName; }

private:
                    XOF_REFLECTED_VARIABLE()
    MSCRSRC         musicName[32];

    sf::Music       mMusic;
};


void Music::Play() {
    mMusic.play();
}

void Music::Pause() {
    mMusic.pause();
}

void Music::Stop() {
    mMusic.stop();
}

void Music::Restart() {
    mMusic.stop();
    mMusic.play();
}

F32 Music::GetDuration() const {
    return mMusic.getDuration().asSeconds();
}


#endif // XOF_MUSIC_HPP
