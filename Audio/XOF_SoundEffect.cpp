/*
===============================================================================

	XOF
	===
	File	:	XOF_SoundEffect.cpp
	Desc	:	Describes a basic sound effect.

===============================================================================
*/
#include "XOF_SoundEffect.hpp"


SoundEffect::SoundEffect() {}

SoundEffect::~SoundEffect() {}

bool SoundEffect::Load( ResourceDescriptor *desc ) {
    std::string fullName( desc->directory.CStr() );

    if( mIsLoaded = mBuffer.loadFromFile( fullName.append( desc->fileName.CStr() ) ) ) {
        mSound.setBuffer( mBuffer );
        mSound.setVolume( 100.f );
    }

    return mIsLoaded;
}
