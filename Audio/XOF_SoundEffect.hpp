/*
===============================================================================

	XOF
	===
	File	:	XOF_SoundEffect.hpp
	Desc	:	Describes a basic sound effect.

===============================================================================
*/
#ifndef XOF_SOUND_EFFECT_HPP
#define XOF_SOUND_EFFECT_HPP


#include <SFML/Audio.hpp>

#include "Resources/CAS_Resource.hpp"


XOF_REFLECTED_CLASS_BEGIN()
class SoundEffect : public Resource {
public:
                    SoundEffect();
                    ~SoundEffect();

    bool            Load( ResourceDescriptor *desc ) override;

    inline void     Play();
    inline void     Pause();
    inline void     Stop();
    inline void     Restart();

    virtual CHAR*   GetName() const override { return (CHAR*)soundEffectName; }

private:
                    XOF_REFLECTED_VARIABLE()
    SNDFXRSRC       soundEffectName[32];

    sf::SoundBuffer mBuffer;
    sf::Sound       mSound;
};
XOF_REFLECTED_CLASS_END()


void SoundEffect::Play() {
    mSound.play();
}

void SoundEffect::Pause() {
    mSound.pause();
}

void SoundEffect::Stop() {
    mSound.stop();
}

void SoundEffect::Restart() {
    mSound.stop();
    mSound.play();
}


#endif // XOF_SOUND_EFFECT_HPP
