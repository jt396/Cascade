# Automatically generated
TEMPLATE = lib
CONFIG += staticlib
#CONFIG += console
#QT += core gui
CONFIG += c++11
#CONFIG -= app_bundle
#CONFIG -= qt


QMAKE_LFLAGS += /ignore:4099 # ignore PDB warnings on Windows
greaterThan(QT_MAJOR_VERSION, 4): QT += widgets # SFML widget will NOT work without this

INCLUDEPATH += $$PWD

# Trying to get Cascade running on Laptop
INCLUDEPATH += "C:\Program Files (x86)\Windows Kits\10\Include"

# GLM
INCLUDEPATH += "$$PWD/ThirdParty/glm"
DEPENDPATH += "$$PWD/ThirdParty/glm"

# SFML
INCLUDEPATH += "$$PWD/ThirdParty/SFML/include"
DEPENDPATH += "$$PWD/ThirdParty/SFML/include"
LIBS += -L"$$PWD/ThirdParty/SFML/lib"
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

# GLEW
INCLUDEPATH += "$$PWD/ThirdParty/glew-1.13.0/include"
DEPENDPATH += "$$PWD/ThirdParty/glew-1.13.0/include"
LIBS += -L"$$PWD/ThirdParty/glew-1.13.0/lib/Release/Win32"
CONFIG(debug, debug|release): LIBS += -lglew32

INCLUDEPATH += "$$PWD/ThirdParty/tinyObj"
# ASSIMP
INCLUDEPATH += "$$PWD/ThirdParty/assimp-3.1.1-win-binaries/include"
DEPENDPATH += "$$PWD/ThirdParty/assimp-3.1.1-win-binaries/include"
LIBS += -L"$$PWD/ThirdParty/assimp-3.1.1-win-binaries/lib/lib32"
CONFIG(debug, debug|release): LIBS += -lassimp

# Dont look in stb.* when generating warnings
#QMAKE_CXXFLAGS += -isystem "$$PWD/ThirdParty/stb"

# OTHER
INCLUDEPATH += "$$PWD/ThirdParty/smb"
#LIBS += -L"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib"
CONFIG(debug, debug|release): LIBS += -lOpenGL32


# Automatically generated
SOURCES += \
    Core/XOF_LoggingManager.cpp \
    Resources/XOF_ResourceManager.cpp \
    Core/XOF_CRC32.cpp \
    Core/XOF_DynamicPoolAllocator.cpp \
    Core/XOF_Tokenizer.cpp \
    Rendering/stb_image.cpp \
    Rendering/XOF_Camera.cpp \
    Rendering/XOF_FirstPersonCamera.cpp \
    Rendering/XOF_Frustum.cpp \
    Rendering/XOF_GPUState.cpp \
    Rendering/XOF_Material.cpp \
    Rendering/XOF_Mesh.cpp \
    Rendering/XOF_Renderer.cpp \
    Rendering/XOF_Shader.cpp \
    Rendering/XOF_Texture.cpp \
    Rendering/XOF_ThirdPersonCamera.cpp \
    GameplayFoundations/XOF_Component.cpp \
    GameplayFoundations/XOF_Entity.cpp \
    GameplayFoundations/XOF_ReflectionManager.cpp \
    Audio/XOF_AudioEngine.cpp \
    Audio/XOF_Music.cpp \
    Audio/XOF_SoundEffect.cpp \
    HIDs/XOF_Keyboard_SFML.cpp \
    HIDs/XOF_Mouse_SFML.cpp \
    Tests/XOF_Game.cpp \
    Platform/XOF_PlatformInfo.cpp \
    Platform/XOF_Timer_Windows.cpp \
    Collision&Physics/XOF_AABB.cpp \
    Collision&Physics/XOF_Sphere.cpp \
    GameplayFoundations/CAS_Components.cpp \
    Collision&Physics/CAS_BoundingVolume.cpp \
    Resources/CAS_ResourceDescriptors.cpp \
    Rendering/CAS_CubeMap.cpp

HEADERS += \
    Core/XOF_LoggingManager.hpp \
    Resources/XOF_ResourceManager.hpp \
    Core/XOF_CRC32.hpp \
    Core/XOF_Assert.hpp \
    Core/XOF_DynamicPoolAllocator.hpp \
    Core/XOF_Tokenizer.hpp \
    Rendering/stb_image.h \
    Rendering/XOF_Camera.hpp \
    Rendering/XOF_FirstPersonCamera.hpp \
    Rendering/XOF_Frustum.hpp \
    Rendering/XOF_GPUState.hpp \
    Rendering/XOF_Lights.hpp \
    Rendering/XOF_Material.hpp \
    Rendering/XOF_Mesh.hpp \
    Rendering/XOF_Renderer.hpp \
    Rendering/XOF_RenderRequest.hpp \
    Rendering/XOF_Shader.hpp \
    Rendering/XOF_StaticCamera.hpp \
    Rendering/XOF_Texture.hpp \
    Rendering/XOF_ThirdPersonCamera.hpp \
    Rendering/XOF_Transform.hpp \
    Rendering/XOF_Vertex.hpp \
    Animation/XOF_Clip.hpp \
    Animation/XOF_Pose.hpp \
    Animation/XOF_Skeleton.hpp \
    GameplayFoundations/XOF_Component.hpp \
    GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp \
    GameplayFoundations/XOF_Entity.hpp \
    GameplayFoundations/XOF_EntityManager.hpp \
    GameplayFoundations/XOF_Event.hpp \
    GameplayFoundations/XOF_ReflectionMacros.hpp \
    GameplayFoundations/XOF_ReflectionManager.hpp \
    Audio/XOF_AudioEngine.hpp \
    Audio/XOF_AudioRequest.hpp \
    Audio/XOF_Music.hpp \
    Audio/XOF_SoundEffect.hpp \
    HIDs/XOF_Keyboard_I.hpp \
    HIDs/XOF_Keyboard_SFML.hpp \
    HIDs/XOF_Mouse_I.hpp \
    HIDs/XOF_Mouse_SFML.hpp \
    Tests/EnginePrep.hpp \
    Tests/TempEvents.hpp \
    Tests/XOF_Game.hpp \
    Platform/XOF_Platform.hpp \
    Platform/XOF_PlatformInfo.hpp \
    Platform/XOF_Timer.hpp \
    Platform/XOF_Timer_Windows.hpp \
    Collision&Physics/XOF_CollisionWorld.hpp \
    Collision&Physics/XOF_Ray.hpp \
    Collision&Physics/XOF_Sphere.hpp \
    Core/XOF_MathCommon.hpp \
    GameplayFoundations/CAS_Components.hpp \
    Collision&Physics/XOF_AABB.hpp \
    Collision&Physics/CAS_BoundingVolume.hpp \
    Resources/CAS_Resource.hpp \
    Resources/CAS_ResourceTypes.hpp \
    Resources/CAS_ResourceDescriptors.hpp \
    Core/XOF_Strings.hpp \
    Rendering/CAS_CubeMap.hpp \
    GameplayFoundations/CAS_World.hpp
