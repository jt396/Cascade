#ifndef XOF_BOUNDING_VOLUME_HPP
#define XOF_BOUNDING_VOLUME_HPP


#include <glm/vec3.hpp>
#include <glm/mat4x4.hpp>

#include "Platform/XOF_Platform.hpp"
#include "Rendering/XOF_Mesh.hpp"

#include "XOF_Ray.hpp"

class Material;


class BoundingVolume {
public:
#if defined( EDITOR_OR_DEBUG )
    struct VisualRepresentation {
        Mesh* mesh{nullptr};
        Material* material{nullptr};
    };
#endif
                                    BoundingVolume();
    virtual                         ~BoundingVolume();

    virtual void                    ScaleToFit( const Mesh::Dimensions& meshDimensions ) = 0;
    virtual void                    Transform( const glm::vec3& translation, const glm::mat4x4 *rotationAndScale ) = 0;

    virtual bool                    IsIntersected( const Ray& r ) = 0;
    inline bool                     IsHit() const;

#if defined( EDITOR_OR_DEBUG )
    inline VisualRepresentation*    GetVisualRepresentation() const;
    inline virtual glm::mat4x4      GetVisualRepresentationTransform() const = 0;
#endif

protected:
    bool                            mIsHit;
#if defined( EDITOR_OR_DEBUG )
    VisualRepresentation            mVisualRepresentation;
#endif
};


inline bool BoundingVolume::IsHit() const {
    return mIsHit;
}


#if defined( EDITOR_OR_DEBUG )
inline BoundingVolume::VisualRepresentation* BoundingVolume::GetVisualRepresentation() const {
    return const_cast<BoundingVolume::VisualRepresentation*>( &mVisualRepresentation );
}
#endif


#endif // XOF_BOUNDING_VOLUME_HPP
