/*
===============================================================================

	XOF
	===
	File:	XOF_AABB.cpp
	Desc:	Basic AABB (axis-aligned bounding box).
				
===============================================================================
*/
#include <algorithm>

#include "XOF_AABB.hpp"
#include "Resources/XOF_ResourceManager.hpp"


AABB::AABB() : mCenter( 0.f ), mOriginalCenter( 0.f ), mHalfExtents( 0.f ), mOriginalHalfExtents( 0.f ) {}

AABB::AABB( const glm::vec3 &center, const glm::vec3 &halfExtents ) : mCenter( center ), mHalfExtents( halfExtents ) {}

AABB::AABB( const Mesh::Dimensions& meshDimensions ) {
    ScaleToFit( meshDimensions );
}

AABB::~AABB() {}

void AABB::ScaleToFit( const Mesh::Dimensions& meshDimensions ) {
    mOriginalCenter = mCenter = ( meshDimensions.min + meshDimensions.max ) / 2.f;
    mOriginalHalfExtents = mHalfExtents = ( meshDimensions.max - meshDimensions.min ) / 2.f;
}

// Real Time Collision Detection (page 86)
// NOTE: PROBABLY GOING TO NEED TO PASS IN ANOTHER AABB TO HANDLE ALL ROTATION CASES
void AABB::Transform( const glm::vec3& translation, const glm::mat4x4 *rotationAndScale ) {
    // center-radius representation
    if( rotationAndScale ) {
        glm::mat3x3 rotateAndScale( *rotationAndScale );
        for( U32 i=0; i<3; ++i ) {
            mCenter[i] = translation[i];
            mHalfExtents[i] = 0.f;
            for( U32 j=0; j<3; ++j ) {
                mCenter[i] += std::abs( rotateAndScale[i][j] ) * mOriginalCenter[j];
                mHalfExtents[i] += std::abs( rotateAndScale[i][j] ) * mOriginalHalfExtents[j];
            }
        }
    } else {
        mCenter += translation;
    }
}

bool AABB::IsIntersected( const Ray& r ) {
    // 0 = min, 1 = max
    glm::vec3 bounds[2] { mCenter - mHalfExtents, mCenter + mHalfExtents };

    static const float EPSILON = 0.1f;
    float tmin = 0.0f;
    float tmax = std::numeric_limits<float>::max();

    for( U32 i=0; i<3; ++i ) {
        if( std::abs( r.direction[i] ) < EPSILON ) {
            if( r.origin[i] < bounds[0][i] || r.origin[i] > bounds[1][i] ) {
                return mIsHit = false;
            }
        } else {
            float ood = 1.f / r.direction[i];
            float t1 = ( bounds[0][i] - r.origin[i] ) * ood;
            float t2 = ( bounds[1][i] - r.origin[i] ) * ood;

            if( t1 > t2 ) {
                std::swap( t1, t2 );
            }

            tmin = std::max( tmin, t1 );
            tmax = std::min( tmax, t2 );

            if( tmin > tmax ) {
                return mIsHit = false;
            }
        }
    }

    return mIsHit = true;
}
