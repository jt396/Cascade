#ifndef XOF_AABB_HPP
#define XOF_AABB_HPP


#include <glm/gtx/transform.hpp>

#include "CAS_BoundingVolume.hpp"


class AABB : public BoundingVolume {
public:
                                AABB();
                                AABB( const glm::vec3 &center, const glm::vec3 &halfExtents );
                                AABB( const Mesh::Dimensions& meshDimensions );
                                ~AABB();

    virtual void                ScaleToFit( const Mesh::Dimensions& meshDimensions ) override;
    virtual void                Transform( const glm::vec3& translation, const glm::mat4x4 *rotationAndScale ) override;

    virtual bool                IsIntersected( const Ray& r ) override;

#if defined( EDITOR_OR_DEBUG )
    virtual inline glm::mat4x4  GetVisualRepresentationTransform() const override;
#endif

//private:
    glm::vec3					mCenter;
    glm::vec3					mOriginalCenter; // ?
    glm::vec3					mHalfExtents;
    glm::vec3					mOriginalHalfExtents; // ?
};


#if defined( EDITOR_OR_DEBUG )
inline glm::mat4x4 AABB::GetVisualRepresentationTransform() const  {
    return glm::translate( mCenter ) * glm::scale( mHalfExtents );
}
#endif


#endif // XOF_AABB_HPP
