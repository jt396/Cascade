/*
===============================================================================

	XOF
	===
	File:	XOF_AABB.hpp
	Desc:	Basic AABB (axis-aligned bounding box).
				
===============================================================================
*/
#ifndef XOF_AABB_HPP
#define XOF_AABB_HPP


#include "../Platform/XOF_Platform.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Mesh.hpp"
class ResourceManager;


struct AABB {
						// CORE
	glm::vec3			center;
	glm::vec3			originalCenter; // ?
	glm::vec3			halfExtents;
	glm::vec3			originalHalfExtents; // ?
						// DEBUGGING
	Mesh				*visualRepresentation;
	bool				isHit;

						// CORE
						AABB();
						AABB( glm::vec3 &centerPoint, glm::vec3 &bounds, const Mesh *visualRepresentation_ );

	void				Transform( const glm::vec3& translation, const glm::mat4x4 *rotationAndScale );

						// DEBUGGING
	inline glm::mat4	GetVisualRepresentationTransform() const;
};


inline glm::mat4 AABB::GetVisualRepresentationTransform() const {
	XOF_ASSERT( visualRepresentation );
	return	glm::translate( center ) * glm::scale( halfExtents );
}


// AABB Generation
void GenerateAABB( AABB& aabb, const Mesh::MeshDimensions& meshDimensions, ResourceManager *rm = nullptr  );


#endif // XOF_AABB_HPP