#ifndef XOF_COLLISION_WORLD_HPP
#define XOF_COLLISION_WORLD_HPP


#include <algorithm>

// TODO: Replace with collision volume component? Probably just add it?
#include "XOF_Ray.hpp"
#include "XOF_AABB.hpp"
#include "XOF_Sphere.hpp"


class CollisionWorld {
public:
	// ...
private:
	// ...
};


float SquaredDistanceFromPointToAABB( const glm::vec3& point, const AABB& aabb ) {
	// Reconstruct AABB min/max
	glm::vec3 bounds[2] { aabb.center - aabb.halfExtents, aabb.center + aabb.halfExtents };

	// Count any excess along the main axis
	float squaredDistance = 0.f;
	for( U32 i=0; i<3; ++i ) {
		float v = point[i];
		if( v < bounds[0][i] ) {
			squaredDistance += ( bounds[0][i] - v ) * ( bounds[0][i] - v );
		}
		if( v > bounds[1][i] ) {
			squaredDistance += ( v - bounds[1][i] ) * ( v - bounds[1][i] );
		}
	}

	return squaredDistance;
}


// AABB ------------------------------------------------------------------------------------------------------------------------------------------------- AABB

// Real Time Collision Detection (page 180/181)
bool TestForIntersection( const Ray &r, AABB& aabb ) {
	// 0 = min, 1 = max
	glm::vec3 bounds[2] { aabb.center - aabb.halfExtents, aabb.center + aabb.halfExtents };

	static const float EPSILON = 0.1f;
	float tmin = 0.0f;
	float tmax = std::numeric_limits<float>::max();

	for( U32 i=0; i<3; ++i ) {
		if( std::abs( r.direction[i] ) < EPSILON ) {
			if( r.origin[i] < bounds[0][i] || r.origin[i] > bounds[1][i] ) {
				return aabb.isHit = false;
			}
		} else {
			float ood = 1.f / r.direction[i];
			float t1 = ( bounds[0][i] - r.origin[i] ) * ood;
			float t2 = ( bounds[1][i] - r.origin[i] ) * ood;

			if( t1 > t2 ) {
				std::swap( t1, t2 );
			}

			tmin = std::max( tmin, t1 );
			tmax = std::min( tmax, t2 );

			if( tmin > tmax ) { 
				return aabb.isHit = false;
			}
		}
	}

	return aabb.isHit = true;
}

bool TestForIntersection( const Ray &r, AABBN& aabb ) {
	// 0 = min, 1 = max
	glm::vec3 bounds[2] { aabb.center - aabb.halfExtents, aabb.center + aabb.halfExtents };

	static const float EPSILON = 0.1f;
	float tmin = 0.0f;
	float tmax = std::numeric_limits<float>::max();

	for( U32 i=0; i<3; ++i ) {
		if( std::abs( r.direction[i] ) < EPSILON ) {
			if( r.origin[i] < bounds[0][i] || r.origin[i] > bounds[1][i] ) {
				return false;
			}
		} else {
			float ood = 1.f / r.direction[i];
			float t1 = ( bounds[0][i] - r.origin[i] ) * ood;
			float t2 = ( bounds[1][i] - r.origin[i] ) * ood;

			if( t1 > t2 ) {
				std::swap( t1, t2 );
			}

			tmin = std::max( tmin, t1 );
			tmax = std::min( tmax, t2 );

			if( tmin > tmax ) { 
				return false;
			}
		}
	}

	return true;
}

bool TestForCollision( AABB &a, AABB &b ) {
	// center-radius representation, Real Time Collision Detection (page 80)
	// switching y and z cases around since tests on the xz plane are more common
	return ( a.isHit = b.isHit = !( ( abs( a.center.x - b.center.x ) > ( a.halfExtents.x + b.halfExtents.x ) ) ||
									( abs( a.center.z - b.center.z ) > ( a.halfExtents.z + b.halfExtents.z ) ) ||
									( abs( a.center.y - b.center.y ) > ( a.halfExtents.y + b.halfExtents.y ) ) ) );
}

// SPHERE ------------------------------------------------------------------------------------------------------------------------------------------------- SPHERE
// ...

// MIXED ------------------------------------------------------------------------------------------------------------------------------------------------- MIXED
bool TestForCollision( const Sphere& sphere, const AABB& aabb ) {
	float sqDistFromSphereCenterToAABB = SquaredDistanceFromPointToAABB( sphere.center, aabb );
	return sqDistFromSphereCenterToAABB <= ( sphere.radius * sphere.radius );
}


#endif // XOF_COLLISION_WORLD_HPP
