/*
===============================================================================

	XOF
	===
	File:	XOF_Ray.hpp
	Desc:	Basic ray, used for picking etc...
			http://antongerdelan.net/opengl/raycasting.html
				
===============================================================================
*/
#ifndef XOF_RAY_HPP
#define XOF_RAY_HPP


#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "Platform/XOF_Platform.hpp"


struct Ray {
                Ray() { memset( this, 0x00, sizeof( Ray ) ); }

                Ray( const glm::vec3& origin, const glm::vec3& direction )
                    : origin( origin ), direction( direction ) {}

	glm::vec3	origin;
	glm::vec3	direction;
};

#if 0
#include "../Rendering/XOF_Camera.hpp"
struct Renderer::BufferDimensions;
void CreatePickingRay( const glm::vec2 &mouseViewSpacePos, const Camera *camera, const Renderer::BufferDimensions& screenDimensions, float forwards, Ray& resultingRay ) {
	// Into 3D NDC space
	float x = ( 2.f * mouseViewSpacePos.x ) / screenDimensions.mBufferWidth - 1.f;
	float y = -( ( 2.f * mouseViewSpacePos.y ) / screenDimensions.mBufferHeight - 1.f );
	float z = 1.f; // not strictly necessary yet
	glm::vec3 rayNDS( x, y, z );

	// Into 4D Homogeneous Clip space
	// NOTE: No need to reverse perspective divide here since this is a ray with no intrinsic depth
	glm::vec4 rayCLIP( rayNDS.x, rayNDS.y, forwards, 1.f );

	// Into 4D Camera Coordinates
	// Normally, you use a projection matrix to go from eye/camera space to clip space.
	// In this instance however we're going the other way, so we invert this matrix.
	glm::vec4 rayEYE( glm::inverse( camera->GetProjectionMatrix() ) * rayCLIP );
	// zw part = "forwards(FOWARDS)" and "not a point (0.f)"
	rayEYE = glm::vec4( rayEYE.x, rayEYE.y, forwards, 0.f );

	// Into 4D World Coordinates
	// -1 for forwards means the ray isn't normalised, normalise the vector here
	glm::vec3 rayWORLD( glm::inverse( camera->GetViewMatrix() ) * rayEYE );
	glm::normalize( rayWORLD );

	//Ray ray( gCamera.GetPosition(), rayWORLD, gCamera.GetPosition().z, gCamera.GetPosition().z + 10.f );
	resultingRay.origin = camera->GetPosition();
	resultingRay.direction = glm::normalize( rayWORLD );
}
#endif

#endif // XOF_RAY_HPP
