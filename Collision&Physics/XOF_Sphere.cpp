/*
===============================================================================

	XOF
	===
	File:	XOF_Sphere.cpp
	Desc:	Sphere collision/bounding volume.
				
===============================================================================
*/
#include <algorithm>

#include "XOF_Sphere.hpp"
#include "Resources/XOF_ResourceManager.hpp"


Sphere::Sphere() { 
	memset( this, 0x00, sizeof( Sphere ) ); 
    radius = 1.f;
#if defined( EDITOR_OR_DEBUG )
    visualRepresentation = (Mesh*)ResourceManager::Get()->GetResource<Mesh>( "UNIT_SPHERE" );
#endif
}

Sphere::Sphere( const glm::vec3& center_, float radius_, const Mesh *visualRepresentation_ ) 
    : center( center_ ), radius( radius_ ), visualRepresentation( const_cast<Mesh*> ( visualRepresentation_ ) ) {
#if defined( EDITOR_OR_DEBUG )
    visualRepresentation = (Mesh*)ResourceManager::Get()->GetResource<Mesh>( "UNIT_SPHERE" );
#endif
}

#ifndef IGNORE
// Real Time Collision Detection (pages 177-179) 
// Intersect ray r = p + td where ||d|| = 1 with sphere s
// Setup and solve as a quadratic in t
bool Sphere::IsHit( const Ray& r ) {
	glm::vec3 m = r.origin - center;
	float c = glm::dot( m, m ) - radius * radius;

	// Intersection if there's at least one real root
	if( c <= 0.f ) {
		return isHit = true;
	}

	// Early exit if ray origin outside sphere and the ray is pointing away from sphere
	float b = glm::dot( m, r.direction );
	if( b > 0.f ) {
		return isHit = false;
	}

	// A negative discriminant corresponds to ray missing sphere
	float disc = b * b - c;
	if( disc < 0.f ) {
		return isHit = false;
	}

	return isHit = true;
}

// Real Time Collision Detection (page 88)
bool Sphere::IsHit( const Sphere& s ) { 
	float distBetweenCenters = glm::length( center - s.center );
	float sumOfRadii = radius + s.radius;

	return isHit = ( distBetweenCenters < sumOfRadii ); 
}
#endif


// ---
// Sphere generation - Simple
void GenerateSimpleSphere( Sphere& sphere, const glm::vec3& aabbMin, const glm::vec3& aabbMax, ResourceManager *rm ) {
	sphere.center = ( aabbMin + aabbMax ) / 2.f;
	
	glm::vec3 temp( ( aabbMax - aabbMin ) / 2.f );
	temp = glm::abs( temp );
	sphere.radius = std::max( std::max( temp.x, temp.y ), temp.z );

#ifdef XOF_DEBUG
    sphere.visualRepresentation = reinterpret_cast<Mesh*>( rm->GetResource<Mesh>( "XOF_UNIT_SPHERE" ) );
#endif
}


// Sphere generation - Ritter
static void MostSeparatedPointsOnAABB( U32& min, U32& max, glm::vec3 *points, U32 pointCount ) {
	// Get most extreme points along primal axis
	U32 minx = 0, maxx = 0, miny = 0, maxy = 0, minz = 0, maxz = 0;
	for( U32 i=1; i<pointCount; ++i ) {
		if( points[i].x < points[minx].x ) minx = i;
		if( points[i].x > points[maxx].x ) maxx = i;
		if( points[i].y < points[miny].y ) miny = i;
		if( points[i].y > points[maxy].y ) maxy = i;
		if( points[i].z < points[minz].z ) minz = i;
		if( points[i].z > points[maxz].z ) maxz = i;
	}

	// Compute squared distances for the three pairs of points
	float distXsq = glm::dot( points[maxx] - points[minx], points[maxx] - points[minx] );
	float distYsq = glm::dot( points[maxy] - points[miny], points[maxy] - points[miny] );
	float distZsq = glm::dot( points[maxz] - points[minz], points[maxz] - points[minz] );

	// Pick the pair (min, max) of points most distant
	min = minx;
	max = maxx;

	if( distYsq > distXsq && distYsq > distZsq ) {
		max = maxy;
		min = miny;
	}

	if( distZsq > distXsq && distZsq > distYsq ) {
		max = maxz;
		min = minz;
	}
}

static void SphereFromDistantPoints( Sphere& sphere, glm::vec3 *points, U32 pointCount ) {
	// Find the most separated point pair defining the encompassing AABB
	U32 min, max;
	MostSeparatedPointsOnAABB( min, max, points, pointCount );

	// Setup sphere to encompass these two points
	sphere.center = ( points[min] + points[max] ) / 2.f;
	sphere.radius = glm::dot( points[max] - sphere.center, points[max] - sphere.center );
	sphere.radius = std::sqrtf( sphere.radius );
}

// Update the sphere if necessary to encompass the point
static void SphereOfSphereAndPoint( Sphere &sphere, glm::vec3& point ) {
	// Comute squared distance between point and sphere center
	glm::vec3 d = point - sphere.center;
	float d2 = glm::dot( d, d );

	// Only update the sphere if the point is outside of it
	if( d2 > ( sphere.radius * sphere.radius ) ) {
		float dist = std::sqrtf( d2 );
		float newRadius = ( sphere.radius + dist ) / 2.f;
		float k = ( newRadius - sphere.radius ) / dist;
		
		sphere.radius = newRadius;
		sphere.center += d * k;
	}
}

void GenerateRitterSphere( Sphere& sphere, const glm::vec3& aabbMin, const glm::vec3& aabbMax ) {
	// Reconstruct the 8 points of the AABB
	U32 pointCount = 8;
	glm::vec3 aabbPoints[] = {
		// front face
		{aabbMin.x, aabbMax.y, aabbMin.z}, {aabbMax.x, aabbMax.y, aabbMin.z},
		{aabbMax.x, aabbMin.y, aabbMin.z}, {aabbMin.x, aabbMin.y, aabbMin.z},
		// back face
		{aabbMin.x, aabbMax.y, aabbMax.z}, {aabbMax.x, aabbMax.y, aabbMax.z},
		{aabbMax.x, aabbMin.y, aabbMax.z}, {aabbMin.x, aabbMin.y, aabbMax.z}
	};

	// Pass 1/2: Get sphere encompassing two approximately most distant points
	SphereFromDistantPoints( sphere, aabbPoints, pointCount );

	// Pass 2/2: Grow sphere to include all points
	for( U32 i=0; i<pointCount; ++i ) {
		SphereOfSphereAndPoint( sphere, aabbPoints[i] );
	}
}
