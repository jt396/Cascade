/*
===============================================================================

	XOF
	===
	File:	XOF_Sphere.cpp
	Desc:	Sphere collision/bounding volume.
				
===============================================================================
*/
#ifndef XOF_SPHERE_HPP
#define XOF_SPHERE_HPP


#include "Platform/XOF_Platform.hpp"
#include "Rendering/XOF_Transform.hpp"

class ResourceManager;
class Mesh;


struct Sphere {
						// CORE
	glm::vec3			center;
	float				radius;
						// DEBUGGING
	Mesh				*visualRepresentation;
	bool				isHit;

						// CORE
						Sphere();
						Sphere( const glm::vec3& center_, float radius_, const Mesh *visualRepresentation_ );

    //virtual void        ScaleToFit( const Mesh::MeshDimensions& meshDimensions ) override {}

	//bool				IsHit( const Ray& r );
	//bool				IsHit( const Sphere& s );

						// DEBUGGING
	inline glm::mat4	GetVisualRepresentationTransform() const;
	inline void			Transform( const glm::vec3& translation /*, scale? */ );
};


inline glm::mat4 Sphere::GetVisualRepresentationTransform() const {
	XOF_ASSERT( visualRepresentation );
	return	glm::translate( center ) * glm::scale( glm::vec3( radius ) );
}

inline void Sphere::Transform( const glm::vec3& translation ) {
	center += translation;
}


// Sphere generation
void GenerateSimpleSphere( Sphere& sphere, const glm::vec3& aabbMin, const glm::vec3& aabbMax, ResourceManager *rm );
void GenerateRitterSphere( Sphere& sphere, const glm::vec3& aabbMin, const glm::vec3& aabbMax );


#endif // XOF_SPHERE_HPP
