#include "XOF_CRC32.hpp"


U32 CalculateCRC32( U32 crc, const void *buf, size_t size ) {
    const U8 *p;

    p = reinterpret_cast<const U8*>( buf );
    crc = crc ^ ~0U;

    while( size-- ) {
        crc = crc32_tab[( crc ^ *p++ ) & 0xFF] ^ ( crc >> 8 );
    }

    return crc ^ ~0U;
}
