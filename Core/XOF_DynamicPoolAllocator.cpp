/*
===============================================================================

	XOF
	===
	File	:	XOF_DynamicPoolAllocator.hpp
	Desc	:	Will use pre-allocated memory if any is made available;
				otherwise it will request more memory from the system

===============================================================================
*/

#include "Core/XOF_Assert.hpp"

#include "XOF_DynamicPoolAllocator.hpp"


DynamicPoolAllocator::DynamicPoolAllocator() {}

DynamicPoolAllocator::DynamicPoolAllocator( U32 elementSize, U32 elementCount, U8 elementAlignment ) {
	Configure( elementSize, elementCount, elementAlignment );
}

DynamicPoolAllocator::~DynamicPoolAllocator() {
	if( mPoolHead ) {
		Clear();
	}
}

void DynamicPoolAllocator::Configure( U32 elementSize, U32 elementCount, U8 elementAlignment ) {
	mElementSize = ( elementSize > sizeof( PoolElement ) ) ? elementSize : sizeof( PoolElement );
	mAlignment = elementAlignment;

	// setup x initial elements - starting with the mPoolHead
	mPoolHead  = reinterpret_cast<PoolElement*>( InternalAllocate() ); 
	mPoolHead->next = nullptr;

	// additional elements, if numberOf > 1
	for( U32 i=0; i<( elementCount - 1 ); ++i ) {
		PoolElement *temp = reinterpret_cast<PoolElement*>( InternalAllocate() );
		temp->next = mPoolHead;
		mPoolHead = temp;
	}
}

void* DynamicPoolAllocator::Allocate() {
	--mNumberOfAvailableElements;
	// static pool version would assert this to ensure we're not out of memory 
	if( mPoolHead ) {
		PoolElement *temp = mPoolHead;
	    mPoolHead = mPoolHead->next;
		return reinterpret_cast<void*>( temp );
	} else {
	    return InternalAllocate();
	}
}

void* DynamicPoolAllocator::InternalAllocate() {
	// dynamically allocate memory for another element
	size_t  temp = reinterpret_cast<size_t>( operator new( mElementSize + mAlignment ) ); 

	// get the misalignment - mask = alignment-1
	size_t misalignment = ( temp & ( mAlignment - 1 ) );
	// get the adjustment needed
	size_t adjustment = mAlignment - misalignment;
	// get the newly aligned address
	temp += adjustment;

	// store adjustment info in preceeding byte
	uintptr_t *p = reinterpret_cast<uintptr_t*>( temp - 1 );
	*p = adjustment;

	++mTotalAllocationsMade;
	++mNumberOfAvailableElements;

	return reinterpret_cast<void*>( temp );
}

void DynamicPoolAllocator::Deallocate( void *putBack ) {
	PoolElement *temp = reinterpret_cast<PoolElement*>( putBack );
	temp->next = mPoolHead;
	mPoolHead = temp;

	++mNumberOfAvailableElements;
}

void DynamicPoolAllocator::Clear() {
	XOF_ASSERT( mNumberOfAvailableElements == mTotalAllocationsMade );

	U8 *toFree = nullptr;
	PoolElement *temp = nullptr;

	while( mPoolHead != nullptr ) {
	    temp = mPoolHead;
	    mPoolHead = mPoolHead->next;
 	    toFree = reinterpret_cast<U8*>( temp );
	    toFree = ( toFree - ( *( toFree - 1 ) ) );

	    operator delete( reinterpret_cast<void*>( toFree ) );
	}

	mPoolHead = nullptr;
	mTotalAllocationsMade = 0;
	mNumberOfAvailableElements = 0;
}
