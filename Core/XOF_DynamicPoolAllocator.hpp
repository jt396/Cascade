/*
===============================================================================

	XOF
	===
	File	:	XOF_DynamicPoolAllocator.hpp
	Desc	:	Will use pre-allocated memory if any is made available;
				otherwise it will request more memory from the system

===============================================================================
*/
#ifndef XOF_DYNAMIC_POOL_ALLOCATOR_HPP
#define XOF_DYNAMIC_POOL_ALLOCATOR_HPP


#include "Platform/XOF_Platform.hpp"


class DynamicPoolAllocator {
public:
					DynamicPoolAllocator();
					DynamicPoolAllocator( U32 elementSize, U32 elementCount = 8, U8 elementAlignment = 1 );
					~DynamicPoolAllocator();

	void			Configure( U32 elementSize, U32 elementCount = 8, U8 elementAlignment = 1 );

	void*			Allocate();
	void			Deallocate( void *putBack );
					// Hand back dynamically allocated memory
	void			Clear();

					// Construct & destruct- don't use on POD or void
					template<typename T, typename ...TArgs>
	void			Construct( T *posInMem, TArgs&&... args );
					template<typename T>
	void 			Destruct( T *toDestruct );

					// Again, don't use on POD or void
					template<typename T, typename ...TArgs>
	T*				AllocateAndConstruct( TArgs&&... args );
					template<typename T>
	void			DestructAndDeallocate( T *toDestruct );
					template<typename T>
	void			DestructAndDeallocate( void *toDestruct );

private:
					// Stays in 'linked list' when free, memory holds data when in use 
					// and is 'recycled' to hold address of next element when put back in pool
					struct PoolElement {
						PoolElement *next;
					};

					// Aligned and unaligned allocation - dynamically allocate memory for pool elements
	void*			InternalAllocate();

	U16				PADDING;
	U16				mAlignment;

	U16				mTotalAllocationsMade;
	U16				mNumberOfAvailableElements;
	U32				mElementSize;
			
	PoolElement		*mPoolHead;
};


template<typename T, typename ...TArgs>
void DynamicPoolAllocator::Construct( T *posInMem, TArgs&&... args ) {
	new( reinterpret_cast<void*>( posInMem ) ) T( std::forward<TArgs>( args )... );
}

template<typename T>
void DynamicPoolAllocator::Destruct( T *toDestruct ) {
	toDestruct->~T();
}

template<typename T, typename ...TArgs>
T* DynamicPoolAllocator::AllocateAndConstruct( TArgs&&... args ) {
	T *p = reinterpret_cast<T*>( Allocate() );
	Construct<T>( p, std::forward<TArgs>( args )... );
	return p;
}

template<typename T>
void DynamicPoolAllocator::DestructAndDeallocate( void *toDestruct ) {
	T *tp = reinterpret_cast<T*>( toDestruct );
	Destruct<T>( tp );
	Deallocate( tp );
}
template<typename T>
void DynamicPoolAllocator::DestructAndDeallocate( T *toDestruct ) {
	Destruct<T>( toDestruct );
	Deallocate( toDestruct );
}


#endif // XOF_DYNAMIC_POOL_ALLOCTOR_HPP
