#include <assert.h>
#include <cstdio>
#include <ctime>
#include <cstring>

#include "XOF_LoggingManager.hpp"


static U32 gChannelToMaskMapping[Underlying( LoggingChannels::count )];


LoggingManager* LoggingManager::Get() {
    static LoggingManager instance;
    return &instance;
}

bool LoggingManager::Startup( const LoggingManagerConfig& config ) {
    // Map the log channels to the relevant bit-masks
    gChannelToMaskMapping[Underlying( LoggingChannels::rendering )] = Underlying( LoggingChannelBits::rendering );
    gChannelToMaskMapping[Underlying( LoggingChannels::audio )] = Underlying( LoggingChannelBits::audio );
    gChannelToMaskMapping[Underlying( LoggingChannels::core )] = Underlying( LoggingChannelBits::core );
    gChannelToMaskMapping[Underlying( LoggingChannels::collisionAndPhysics )] = Underlying( LoggingChannelBits::collisionAndPhysics );
    gChannelToMaskMapping[Underlying( LoggingChannels::animation )] = Underlying( LoggingChannelBits::animation );
    gChannelToMaskMapping[Underlying( LoggingChannels::resources )] = Underlying( LoggingChannelBits::resources );
    gChannelToMaskMapping[Underlying( LoggingChannels::playerIO )] = Underlying( LoggingChannelBits::playerIO );
    gChannelToMaskMapping[Underlying( LoggingChannels::platform )] = Underlying( LoggingChannelBits::platform );
    gChannelToMaskMapping[Underlying( LoggingChannels::worldEditor )] = Underlying( LoggingChannelBits::worldEditor );

    // Now configure the logger itself
    mVerbosity = config.verbosity;

    // If we're using per channel logging then each logger is configured individually for specific messages.
    // Otherwise, entry 0 is used for all logging messages.
    mPerChannelLogging = config.perChannelLogging;
    for( const auto& channelConfig : config.channelConfigs ) {
        if( IsValidChannelConfig( channelConfig.mask ) ) {
            mLoggers.push_back( channelConfig );
        }
    }

    if( config.logToFile ) {
        const char directory[] = "..//LOG_FILES//";
		const unsigned int directorySize = sizeof( directory ) - 1;
		const char fileExtension[] = ".txt";
		const unsigned int fileExtensionSize = sizeof( fileExtension ) - 1;

		const time_t now = time( 0 );

		char buffer[128];
		// Set directory
		memcpy( buffer, directory, directorySize );

		const char *dateString = ctime( &now );
		unsigned int dateSize = 0;

		char *temp = const_cast<char*>( dateString );
		while( *temp != '\0' && *temp != '\n' ) {
			// fstream::open() doesn't like colons
			if( *temp == ':' ) { *temp = '-'; }
			++dateSize;
			++temp;
		}
		// Add date to be used as the actual file-name
		memcpy( &buffer[directorySize], dateString, dateSize );
		// Lastly add the file extension
		memcpy( &buffer[directorySize + dateSize], fileExtension, fileExtensionSize );
		buffer[directorySize + dateSize + fileExtensionSize] = '\0';

		mLogFile.open( buffer, std::fstream::out | std::fstream::app );
		if( !mLogFile.is_open() ) {
			return false;
		}
    }

    return true;
}

void LoggingManager::Shutdown() {
    if( mLogFile.is_open() ) {
        mLogFile.close();
    }
}

void LoggingManager::LogMessage( LoggingVerbosity verbosity, LoggingChannels channel, const CHAR *msg, ... ) {
    // Only log the message if the verbosity is high enough and it's from an enabled channel
    if( verbosity >= mVerbosity ) {
        const U32 channelIndex = mPerChannelLogging? Underlying( channel ) : 0;
        if( Underlying( mLoggers[channelIndex].mask ) & gChannelToMaskMapping[Underlying( channel )] ) {
            // Format the message
            const U32 maxMessageLength = 1024;
            CHAR buffer[maxMessageLength] {'\0'};

            va_list argList;
            va_start( argList, msg );
            vsnprintf( buffer, maxMessageLength, msg, argList );
            va_end( argList );

            mLoggers[channelIndex].loggingFunction( buffer );

            if( mLogFile.is_open() ) {
                mLogFile << buffer << '\n';
				// Force the log file to flush after every message, we don't want to lose any contents should the engine crash
				mLogFile.flush();
            }
        }
    }
}
