#ifndef XOF_LOGGINGMANAGER_HPP
#define XOF_LOGGINGMANAGER_HPP


#include <functional>
#include <cstdarg>
#include <fstream>
#include <vector>

#include "Platform/XOF_Platform.hpp"


enum class LoggingVerbosity : U32 {
    high = 0,   // Low priority
    medium,     // Medium priority
    low         // High priority
};

enum class LoggingChannels : U32 {
    rendering = 0,
    audio,
    core,
    collisionAndPhysics,
    animation,
    resources,
    playerIO,
    platform,
    worldEditor,
    count
};

enum class LoggingChannelBits : U32 {
    rendering           = 0x01,
    audio               = 0x02,
    core                = 0x04,
    collisionAndPhysics = 0x08,
    animation           = 0x10,
    resources           = 0x20,
    playerIO            = 0x40,
    platform            = 0x80,
    worldEditor         = 0x100,
    invalid             = 0x200
};

struct LoggingChannelDesc {
    LoggingChannelBits                  mask{LoggingChannelBits::invalid};
    std::function<void( const CHAR* )>  loggingFunction{nullptr};
};
inline constexpr bool IsValidChannelConfig( LoggingChannelBits configMask ) {
    return configMask != LoggingChannelBits::invalid;
}

// TODO: Add log file with flush-threshold?
struct LoggingManagerConfig {
    LoggingVerbosity                verbosity;
    std::vector<LoggingChannelDesc> channelConfigs;
    bool                            perChannelLogging;
    bool                            logToFile;
};


class LoggingManager {
public:
    static LoggingManager*  Get();

    bool                            Startup( const LoggingManagerConfig& config );
    void                            Shutdown();

    inline LoggingVerbosity         GetVerbosity() const;
    inline void                     SetVerbosity( LoggingVerbosity verbosity );

    inline bool                     IsUsingPerChannelLogging() const;

    void                            LogMessage( LoggingVerbosity verbosity, LoggingChannels channel, const CHAR *msg, ... );

private:
    LoggingVerbosity                mVerbosity;

    bool                            mPerChannelLogging;
    std::vector<LoggingChannelDesc> mLoggers;
    std::fstream                    mLogFile;
};


LoggingVerbosity LoggingManager::GetVerbosity() const {
    return mVerbosity;
}

void LoggingManager::SetVerbosity( LoggingVerbosity verbosity ) {
    if( verbosity == mVerbosity ) {
        return;
    }

    const CHAR* levels[] { "HIGH", "MEDIUM", "LOW" };
    LogMessage( LoggingVerbosity::low, LoggingChannels::core,
                "Logging verbosity changed from: %s to %s",
                levels[Underlying( mVerbosity )],
                levels[Underlying( verbosity )] );

    mVerbosity = verbosity;
}

inline bool LoggingManager::IsUsingPerChannelLogging() const {
    return mPerChannelLogging;
}

// Create new message combining FILE,FUNCTION,LINE and msg, then pass that into LogMessage()?
#define XOF_LOG_MESSAGE( verbosity, channels, msg, ... ) LoggingManager::Get()->LogMessage( verbosity, channels, msg, __VA_ARGS__ );


#endif // XOF_LOGGINGMANAGER_HPP
