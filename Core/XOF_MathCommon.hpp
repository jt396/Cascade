/*
===============================================================================

	XOF
	===
	File	:	XOF_MathCommon.hpp
	Desc	:	Commonly used math values and functions.

===============================================================================
*/
#ifndef XOF_MATH_COMMON_HPP
#define XOF_MATH_COMMON_HPP


#include "Platform/XOF_Platform.hpp"


static const float XOF_PI_OVER_TWO = 3.14f / 2.f;


// Abs function done using basic template meta-programming, increases
// compilation time but has the potential to reduce run-time load?
template<typename T>
T Abs( T value ) {
	if( value < 0 ) { 
		return -value;
	}
	return value;
}

#endif // XOF_MATH_COMMON_HPP
