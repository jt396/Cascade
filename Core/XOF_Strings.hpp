#ifndef XOF_STRINGS_HPP
#define XOF_STRINGS_HPP


#include <cstdlib>
#include <utility>

#include "Platform/XOF_Platform.hpp"



// Select the smallest possible type still capable of holding the string' length (preference: uchar -> ushort -> uint)
template<size_t val>
using StringSizeType = typename std::conditional<(val <= 255), U8, typename std::conditional<(val <= 65535), U16, U32>::type>::type;

using StringEntry = std::pair<const char*, size_t>;


template<size_t size>
class FixedLengthString {
public:
                                FixedLengthString() {}

                                FixedLengthString( const CHAR *str ) {
                                    Set( str );
                                }

                                //FixedLengthString( const FixedLengthString& ); assert other.size <= this.size
                                //operator=( const FixedLengthString& ); assert other.size <= this.size

    FixedLengthString<size>&	operator=( const CHAR *str ) {
                                    Set( str );
                                    return *this;
                                }

                                ~FixedLengthString() {}

    inline CHAR*				CStr() const { return const_cast<CHAR*>( mBuffer ); }

    inline StringSizeType<size>	GetLength() const { return mLength; }
    inline StringSizeType<size> GetSize() const { return size; }

    inline bool					IsEmpty() const { return mLength == 0; }
    inline void 				Clear() { mLength = 0; }

                                // Go forwards (1)
    inline StringEntry			FirstOf( const char *character ) const {
                                    return Find( 0, 1, character );
                                }

                                // Go backwards (-1)
    inline StringEntry			LastOf( const char *character ) const {
                                    return Find( mLength-1, -1, character );
                                }

                                // TEMP - Use move semantics
    FixedLengthString<size>     SubString( size_t start, size_t end ) {
                                    CHAR temp[size] = {'\0'}; // do something about this

                                    CHAR *p = &mBuffer[start];
                                    StringSizeType<size> index = start;
                                    while( p != &mBuffer[end] ) {
                                        temp[index] = *p;
                                        ++p;
                                        ++index;
                                    }

                                    FixedLengthString<size> s{temp};
                                    return s;
                                }

    void                        Append( const CHAR *msg ) {
                                    // Do we have space?
                                    XOF_ASSERT( mLength != size );

                                    CHAR *p = (CHAR*)msg;
                                    while( *p != '\0' && &mBuffer[mLength] != &mBuffer[size-1] ) {
                                        mBuffer[mLength++] = *p++;
                                    }
                                    mBuffer[mLength] = '\0';

                                    // Did we get through all of the message we tried to add?
                                    XOF_ASSERT( *p == '\0' );
                                }

private:
    CHAR 						mBuffer[size];
    StringSizeType<size>		mLength{0u};

    void						Set( const CHAR *str ) {
                                    while( str[mLength] != '\0' && mLength < size ) {
                                        mBuffer[mLength] = str[mLength];
                                        ++mLength;
                                    }
                                    XOF_ASSERT( mLength < size );
                                    mBuffer[mLength] = '\0';
                                }

    StringEntry					Find( size_t start, size_t direction, const CHAR *character ) const {
                                    CHAR *p = const_cast<CHAR*>( &mBuffer[start] );
                                    const size_t endIndex = ( direction == 1 ) ? ( mLength - 1 ) : 0;

                                    while( *p != *character && p != &mBuffer[endIndex] ) {
                                        p += direction;
                                    }

                                    size_t val = std::max<size_t>( (size_t)&mBuffer[endIndex], (size_t)p ) -
                                                    std::min<size_t>( (size_t)&mBuffer[endIndex], (size_t)p );

                                    return ( p != &mBuffer[endIndex] ) ? StringEntry( p, val ) : StringEntry( nullptr, '\0' );
                                }
};


#endif // XOF_STRINGS_HPP
