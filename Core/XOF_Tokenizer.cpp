/*
===============================================================================

    XOF
    ===
    File	:	XOF_Tokenizer.hpp
    Desc	:	A basic tokenizer, primarily written to parse .obj files.
                A tokenizer takes a string and breaks it down into smaller strings called tokens,
                various delimiters can be used to help identify tokens.

===============================================================================
*/


#include <algorithm>

#include "XOF_Tokenizer.hpp"


Tokenizer::Tokenizer() {
    ReleaseBuffer();
}

Tokenizer::~Tokenizer() {
    ReleaseBuffer();
}

void Tokenizer::ReleaseBuffer() {
    // stop pointing to the buffer, doesn't actually free it, that's the job of the calling code
    mBuffer = nullptr;
    mBufferSize = 0;
    mCurrentTokenStartIndex = 0;
    mCurrentTokenEndIndex = 0;
}

U32 Tokenizer::GetNextToken( CHAR *buffer, U32 bufferSize, const CHAR *delimiters, U32 delimiterCount ) {
	// have we reached the end of the buffer?
    if( mCurrentTokenEndIndex == mBufferSize ) {
		return false;
	}

    // will return true while there are still tokens to be read
    I8 *outgoingBuffer = reinterpret_cast<CHAR*>( buffer );

	// don't start on a delimiter
    while( IsDelimiter( &mBuffer[mCurrentTokenStartIndex], delimiters, delimiterCount ) ) {
        ++mCurrentTokenStartIndex;
	}

    mCurrentTokenEndIndex = mCurrentTokenStartIndex;

	// parse a token (stop when a delimiter is found)
    U32 i = 0;
    while( ( mCurrentTokenEndIndex < mBufferSize ) && ( IsValid( &mBuffer[mCurrentTokenEndIndex] ) ) &&
           ( !IsDelimiter( &mBuffer[mCurrentTokenEndIndex], delimiters, delimiterCount ) ) ) {
        ++mCurrentTokenEndIndex;
        ++i;
    }

    U32 tokenSize = std::max<unsigned int>( mCurrentTokenEndIndex - mCurrentTokenStartIndex, 1u );

    if( outgoingBuffer ) {
        memcpy( reinterpret_cast<void*>( outgoingBuffer ),
                reinterpret_cast<void*>( &mBuffer[mCurrentTokenStartIndex] ),
                std::min<unsigned int>( tokenSize, bufferSize ) );
    }

    // prepare for next token
    mCurrentTokenStartIndex = ( mCurrentTokenEndIndex + 1 );

    return tokenSize;
}
