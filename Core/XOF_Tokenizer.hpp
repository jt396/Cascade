/*
===============================================================================

    XOF
    ===
    File	:	XOF_Tokenizer.hpp
    Desc	:	A basic tokenizer, primarily written to parse .obj files.
                A tokenizer takes a string and breaks it down into smaller strings called tokens,
                various delimiters can be used to help identify tokens.

===============================================================================
*/
#ifndef RT_TOKENIZER_HPP
#define RT_TOKENIZER_HPP


#include <cstring> // memset

#include "Platform/XOF_Platform.hpp"


class Tokenizer {
public:
                Tokenizer();
                ~Tokenizer();

    inline void	SetBuffer( CHAR *buffer, UINT bufferSize );
    inline void	ResetBuffer();
                // Stop pointing to the buffer
    void        ReleaseBuffer();

    U32         GetNextToken( CHAR *buffer, U32 bufferSize, const CHAR *delimiters, UINT delimiterCount );
    inline U32  ClearAndGetNextToken( CHAR *buffer, UINT bufferSize, const CHAR *delimiters, UINT delimiterCount );

    inline U32  GetCurrentLineLength() const;
    inline U32  GetCurrentLine( CHAR *buffer, U32 bufferLength ) const;

    inline bool IsBufferEmpty() const;

private:
                // Checks wether *c is an accepted character
    inline bool IsValid( const CHAR *c ) const;
    inline bool	IsDelimiter( const CHAR *c, const CHAR *delimiters, UINT delimiterCount ) const;

                // Can be from a file or any other source
    CHAR        *mBuffer;
    UINT        mBufferSize;
    U32         mCurrentTokenStartIndex;
    U32         mCurrentTokenEndIndex;
};


inline void Tokenizer::SetBuffer( CHAR *buffer, UINT bufferSize ) {
    mBuffer = buffer;
    mBufferSize = bufferSize;
}

inline void Tokenizer::ResetBuffer() {
    // prepare to start reading from the beginning
    mCurrentTokenStartIndex = 0;
    mCurrentTokenEndIndex = 0;
}

inline U32 Tokenizer::ClearAndGetNextToken( CHAR *buffer, UINT bufferSize, const CHAR *delimiters, U32 delimiterCount ) {
     memset( buffer, 0x00, bufferSize );
     return GetNextToken( buffer, bufferSize, delimiters, delimiterCount );
}

inline bool Tokenizer::IsValid( const CHAR *c ) const {
    // is *c a valid character? (ascii/unicode etc. depending on whatever validRangeStart + validRangeEnd are set to)
    U32 i = static_cast<U32>( *c );
    return ( ( i > 32 ) && ( i < 127 ) );
}

inline bool Tokenizer::IsDelimiter( const CHAR *c, const CHAR *delimiters, U32 delimiterCount ) const {
    for( U32 i=0; i<delimiterCount; ++i ) {
        if ( *c == delimiters[i] ) {
            return true;
        }
    }
    return false;
}

inline U32 Tokenizer::GetCurrentLineLength() const {
    U32 lineLength = 0;
    CHAR *p = &mBuffer[mCurrentTokenStartIndex];
    while( *p != '\n' && *p != '\r' ) {
        ++lineLength;
        ++p;
    }
    return lineLength;
}

inline U32 Tokenizer::GetCurrentLine( CHAR *buffer, U32 bufferLength ) const {
    U32 lineLength = 0;
    CHAR *p = &mBuffer[mCurrentTokenStartIndex];
    while( ( *p != '\n' && *p != '\r' ) && ( lineLength != bufferLength ) ) {
        buffer[lineLength] = *p;
        ++lineLength;
        ++p;
    }
    return lineLength;
}


inline bool Tokenizer::IsBufferEmpty() const {
    return mCurrentTokenEndIndex >= mBufferSize;
}


#endif // XOF_TOKENIZER_HPP
