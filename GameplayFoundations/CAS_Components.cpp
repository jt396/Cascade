/*
===============================================================================

    CASCADE
    =======
    File	:	CAS_Components.cpp
    Desc	:   Default components for use in scene construction.

===============================================================================
*/

#include "GameplayFoundations/XOF_Entity.hpp"
#include "GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp"
#include "Rendering/XOF_Mesh.hpp"
#include "Rendering/XOF_Material.hpp"
#include "Rendering/XOF_Camera.hpp"

#include "CAS_Components.hpp"

// Renderable
Renderable::Renderable( const std::string& name ) : Component( name ) {
    mTypeID = CAS_COMPONENTS::RENDERABLE;
    SET_TYPE_STRING( "Renderable" );
}

Renderable::Renderable( const std::string& name, Mesh *mesh, Material *material, Camera *camera ) :
    Component( name ), mMesh( mesh ), mMaterial( material ), mCamera( camera ) {
    mTypeID = CAS_COMPONENTS::RENDERABLE;
    SET_TYPE_STRING( "Renderable" );
}

Renderable::Renderable( Mesh *mesh, Material *material, Camera *camera ) :
    mMesh( mesh ), mMaterial( material ), mCamera( camera ) {
    mTypeID = CAS_COMPONENTS::RENDERABLE;
    SET_TYPE_STRING( "Renderable" );
}

Renderable::~Renderable() {}
// ---


// Transformable
Transformable::Transformable() {
    mTypeID = CAS_COMPONENTS::TRANSFORMABLE;
    SET_TYPE_STRING( "Transformable" );
}

Transformable::Transformable( const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale ) {
    mTypeID = CAS_COMPONENTS::TRANSFORMABLE;
    SET_TYPE_STRING( "Transformable" );
    mTransform.translation = translation;
    mTransform.rotation = rotation;
    mTransform.scale = scale;
}

Transformable::Transformable( const std::string& name ) : Component( name ) {
    mTypeID = CAS_COMPONENTS::TRANSFORMABLE;
    SET_TYPE_STRING( "Transformable" );
}

Transformable::Transformable( const std::string& name, const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale ) : Component( name ) {
    mTypeID = CAS_COMPONENTS::TRANSFORMABLE;
    SET_TYPE_STRING( "Transformable" );
    mTransform.translation = translation;
    mTransform.rotation = rotation;
    mTransform.scale = scale;
}

Transformable::~Transformable() {}
// ---

PointLightSource::PointLightSource() {
    mTypeID = CAS_COMPONENTS::POINT_LIGHT;
    SET_TYPE_STRING( "PointLightSource" );
    mLight.color = glm::vec3( 1.f, 2.f, 3.f );
    mLight.diffuseIntensity = 0.75f;
    mLight.ambientIntensity = 0.25f;
}

PointLightSource::PointLightSource( const std::string& name ) : Component( name ) {
    mTypeID = CAS_COMPONENTS::POINT_LIGHT;
    SET_TYPE_STRING( "PointLightSource" );
    mLight.color = glm::vec3( 1.f, 2.f, 3.f );
    mLight.diffuseIntensity = 0.75f;
    mLight.ambientIntensity = 0.25f;
}
// ---


// Pass-through logic
PassThrough::PassThrough( Entity *entity, EngineRuntime *engine  ) {
    XOF_ASSERT( entity && engine );

    mTypeID = CAS_COMPONENTS::PASS_THROUGH_LOGIC;
    SET_TYPE_STRING( "PassThrough" );

    mEngine = engine;

    mRenderable = reinterpret_cast<Renderable*>( entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );
    mTransform = reinterpret_cast<Transformable*>( entity->GetDataComponent( CAS_COMPONENTS::TRANSFORMABLE ) );
    XOF_ASSERT( mRenderable && mTransform );

    // TODO: Store off original/initial translation, rotation and scale for correct Reset()
}

void PassThrough::Update( float dt ) {
    // TEMP - Added to test editor simulation control buttons
    glm::vec3 rot = mTransform->GetTransform().rotation + glm::vec3(0.f, 0.001f * dt, 0.f);
    //mTransform->SetRotation( rot.x, rot.y, rot.z );
    //if( dt ) mTransform->GetTransform().rotation = mTransform->GetTransform().rotation % 360.f;
    mTransform->Rotate( 0.f, 0.001f * dt, 0.f );
    // ---
#if 1
    RenderRequest rr( XOF_RENDER_REQUEST_TYPE::RENDER_MESH, mRenderable->GetMaterial(),
        mRenderable->GetMesh(), &mTransform->GetTransform(), mRenderable->GetCamera() );

    // Set WVP matrix - BROKEN HERE
    glm::mat4x4 MVP = mRenderable->GetCamera()->GetProjectionMatrix() *
        //mRenderable->GetCamera()->GetOrthographicProjectionMatrix() *
        mRenderable->GetCamera()->GetViewMatrix() * mTransform->GetModelToWorldMatrix();

    // TODO: Create a simpler/more minimalist way of creating shader args
    const size_t nameLength = 32;

    std::string rrpn( "wvp" );
    rr.shaderArgs[0].type = XOF_SHADER_PARAM_TYPE::MAT4x4F;
    memset( rr.shaderArgs[0].name, 0x00, nameLength );
    memcpy( rr.shaderArgs[0].name, rrpn.c_str(), rrpn.length() );
    memcpy( (void*)&(rr.shaderArgs[0].valueAsFloat), &MVP, sizeof( glm::mat4x4 ) );

    if( TEMP_HACK_LIGHT ) {
        PointLightSource *tempHackLightEmitter = (PointLightSource*)TEMP_HACK_LIGHT->GetComponent( CAS_COMPONENTS::POINT_LIGHT );
        XOF_ASSERT( tempHackLightEmitter );
        Transformable *tempHackLightTransformable = (Transformable*)TEMP_HACK_LIGHT->GetComponent( CAS_COMPONENTS::TRANSFORMABLE );
        XOF_ASSERT( tempHackLightTransformable );

        glm::mat4x4 WORLD = mTransform->GetModelToWorldMatrix();//tempHackLightTransformable->GetModelToWorldMatrix();
        rrpn = "world";
        rr.shaderArgs[1].type = XOF_SHADER_PARAM_TYPE::MAT4x4F;
        memset( rr.shaderArgs[1].name, 0x00, nameLength );
        memcpy( rr.shaderArgs[1].name, rrpn.c_str(), rrpn.length() );
        memcpy( (void*)&(rr.shaderArgs[1].valueAsFloat), &WORLD, sizeof( glm::mat4x4 ) );

        // ---
        glm::vec3 color( tempHackLightEmitter->GetColor() );
        rrpn = "gPointLight.base.color";
        rr.shaderArgs[2].type = XOF_SHADER_PARAM_TYPE::VEC3F;
        memset( rr.shaderArgs[2].name, 0x00, nameLength );
        memcpy( rr.shaderArgs[2].name, rrpn.c_str(), rrpn.length() );
        memcpy( (void*)&(rr.shaderArgs[2].valueAsVec3f), &color, sizeof( glm::vec3 ) );

        float ambient = tempHackLightEmitter->GetAmbientIntensity();
        rrpn = "gPointLight.base.ambientIntensity";
        rr.shaderArgs[3].type = XOF_SHADER_PARAM_TYPE::FLOAT_;
        memset( rr.shaderArgs[3].name, 0x00, nameLength );
        memcpy( rr.shaderArgs[3].name, rrpn.c_str(), rrpn.length() );
        memcpy( (void*)&(rr.shaderArgs[3].valueAsFloat), &ambient, sizeof( F32 ) );

        float diffuse = tempHackLightEmitter->GetDiffuseIntensity();
        rrpn = "gPointLight.base.diffuseIntensity";
        rr.shaderArgs[4].type = XOF_SHADER_PARAM_TYPE::FLOAT_;
        memset( rr.shaderArgs[4].name, 0x00, nameLength );
        memcpy( rr.shaderArgs[4].name, rrpn.c_str(), rrpn.length() );
        memcpy( (void*)&(rr.shaderArgs[4].valueAsFloat), &diffuse, sizeof( F32 ) );

        glm::vec3 pos( tempHackLightTransformable->GetTransform().translation );
        rrpn = "gPointLight.position";
        rr.shaderArgs[5].type = XOF_SHADER_PARAM_TYPE::VEC3F;
        memset( rr.shaderArgs[5].name, 0x00, nameLength );
        memcpy( rr.shaderArgs[5].name, rrpn.c_str(), rrpn.length() );
        memcpy( (void*)&(rr.shaderArgs[5].valueAsVec3f), &pos, sizeof( glm::vec3 ) );
    }

    mEngine->renderer->SubmitRenderRequest( rr );
#endif
}

void PassThrough::HandleEvent( const Event *e ) {
    XOF_UNUSED_PARAMETER( e );
}

void PassThrough::Reset() {
    mTransform->GetTransform().rotation = glm::vec3( 0.f );
}

// ---

// Skybox/Skydome logic
SkyBoxLogic::SkyBoxLogic( Entity *entity, EngineRuntime *engine  ) {
    XOF_ASSERT( entity && engine );

    mTypeID = CAS_COMPONENTS::SKYBOX_LOGIC;
    SET_TYPE_STRING( "PassThrough" );

    mEngine = engine;

    mRenderable = reinterpret_cast<Renderable*>( entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );
    mTransform = reinterpret_cast<Transformable*>( entity->GetDataComponent( CAS_COMPONENTS::TRANSFORMABLE ) );
    XOF_ASSERT( mRenderable && mTransform );
}

void SkyBoxLogic::Update( float dt ) {
    XOF_UNUSED_PARAMETER( dt );
#if 1
    RenderRequest rr( XOF_RENDER_REQUEST_TYPE::RENDER_MESH, mRenderable->GetMaterial(),
        mRenderable->GetMesh(), &mTransform->GetTransform(), mRenderable->GetCamera() );

    // NOTE: Skybox/Skydome needs different culling and depth-test function
    rr.depthTest = GL_LEQUAL;
    rr.cullingMode = GL_FRONT;

    // Set WVP matrix - BROKEN HERE
    const Transform& transform = mTransform->GetTransform();

    const glm::mat4 translationMatrix = glm::translate( mRenderable->GetCamera()->GetPosition() );

    // NOTE: Quarternions would be preferable (Applied in inverse order (X, Y then Z))
    const glm::mat4 rotationMatrix = glm::rotate( transform.rotation.z, glm::vec3( 0.f, 0.f, 1.f ) ) *
                                     glm::rotate( transform.rotation.y, glm::vec3( 0.f, 1.f, 0.f ) ) *
                                     glm::rotate( transform.rotation.x, glm::vec3( 1.f, 0.f, 0.f ) );

    const glm::mat4 scaleMatrix = glm::scale( transform.scale );

    const glm::mat4x4 transformMatrix = translationMatrix * rotationMatrix * scaleMatrix;

    const glm::mat4x4 MVP = mRenderable->GetCamera()->GetProjectionMatrix() *
                            mRenderable->GetCamera()->GetViewMatrix() *
                            transformMatrix;

    // TODO: Create a simpler/more minimalist way of creating shader args
    const size_t nameLength = 32;

    std::string rrpn( "wvp" );
    rr.shaderArgs[0].type = XOF_SHADER_PARAM_TYPE::MAT4x4F;
    memset( rr.shaderArgs[0].name, 0x00, nameLength );
    memcpy( rr.shaderArgs[0].name, rrpn.c_str(), rrpn.length() );
    memcpy( (void*)&(rr.shaderArgs[0].valueAsFloat), &MVP, sizeof( glm::mat4x4 ) );

    mEngine->renderer->SubmitRenderRequest( rr );
#endif
}

void SkyBoxLogic::HandleEvent( const Event *e ) {
    XOF_UNUSED_PARAMETER( e );
}

void SkyBoxLogic::Reset() {}
