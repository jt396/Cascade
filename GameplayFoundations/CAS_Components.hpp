/*
===============================================================================

    CASCADE
    =======
    File	:	CAS_Components.hpp
    Desc	:   Default components for use in scene construction.

===============================================================================
*/
#ifndef CAS_COMPONENTS_HPP
#define CAS_COMPONENTS_HPP


#include "XOF_Component.hpp"
#include "Rendering/XOF_Transform.hpp"
#include "Rendering/XOF_Lights.hpp"

class Mesh;
class Material;
class Camera;
struct Event;
struct EngineRuntime;


#define SET_TYPE_STRING( name )                       \
    do {                                              \
    memset( mTypeString, 0x00, 32 );                  \
    memcpy( mTypeString, name, sizeof( name ) - 1 );  \
    } while( 0 )


enum CAS_COMPONENTS {
    RENDERABLE = 0,
    TRANSFORMABLE,
    BOUNDING_VOLUME,
    POINT_LIGHT,
    PASS_THROUGH_LOGIC,
    SKYBOX_LOGIC,
    CAS_COMPONENTS_COUNT,
};


// Renderable
XOF_REFLECTED_CLASS_BEGIN()
class Renderable : public Component {
public:
                        Renderable() = delete;
                        Renderable( const std::string& name );
                        Renderable( const std::string& name, Mesh *mesh, Material *material, Camera *camera );
                        Renderable( Mesh *mesh, Material *material, Camera *camera );
                        ~Renderable() override;

                        // SetMesh, SetMaterial
    inline Mesh*        GetMesh() const;
    inline void         SetMesh( Mesh *newMesh );

    inline Material*    GetMaterial();
    inline Material*    InspectMaterial() const;
    inline void         SetMaterial( Material *newMaterial );

    inline void         SetCamera( const Camera *camera ) { mCamera = const_cast<Camera*>( camera ); }
    inline Camera*      GetCamera() const;

//private:
                        XOF_REFLECTED_VARIABLE()
    Mesh*               mMesh;
                        XOF_REFLECTED_VARIABLE()
    Material*           mMaterial;
    Camera*             mCamera;
};
XOF_REFLECTED_CLASS_END()

inline Mesh* Renderable::GetMesh() const {
    return mMesh;
}
inline void Renderable::SetMesh( Mesh *newMesh ) {
   mMesh = newMesh;
}

inline Material* Renderable::GetMaterial() {
    return mMaterial;
}
inline Material* Renderable::InspectMaterial() const {
    return mMaterial;
}
inline void Renderable::SetMaterial( Material *newMaterial ) {
    mMaterial = newMaterial;
}

inline Camera* Renderable::GetCamera() const {
    return mCamera;
}
// ---


// Transformable
XOF_REFLECTED_CLASS_BEGIN()
class Transformable : public Component {
public:
                        Transformable();
                        Transformable( const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale );
                        Transformable( const std::string& name );
                        Transformable( const std::string& name, const glm::vec3& translation, const glm::vec3& rotation, const glm::vec3& scale );
                        ~Transformable();

    inline void         SetRotation( float x, float y, float z ) { mTransform.rotation = glm::vec3( x, y, z); }

    inline void         Translate( float x, float y, float z );
    inline void         Rotate( float x, float y, float z );
    inline void         Scale( float x, float y, float z );
    inline void         SetScale( float x, float y, float z );

    inline Transform&	GetTransform();
    inline glm::mat4x4	GetModelToWorldMatrix();

//private:
                        XOF_REFLECTED_VARIABLE()
    Transform           mTransform;
};
XOF_REFLECTED_CLASS_END()

inline void Transformable::Translate( float x, float y, float z ) {
    mTransform.translation += glm::vec3( x, y, z );
}
inline void Transformable::Rotate( float x, float y, float z ) {
    mTransform.rotation += glm::vec3( x, y, z );
}
inline void Transformable::SetScale( float x, float y, float z ) {
    mTransform.scale = glm::vec3( x, y, z );
}
inline void Transformable::Scale( float x, float y, float z ) {
    mTransform.scale += glm::vec3( x, y, z );
}
inline Transform& Transformable::GetTransform() {
    return mTransform;
}
inline glm::mat4x4 Transformable::GetModelToWorldMatrix() {
    return mTransform.GetModelToWorldMatrix();
}
// ---

// Point light
XOF_REFLECTED_CLASS_BEGIN()
class PointLightSource : public Component {
public:
                PointLightSource();
                PointLightSource( const std::string& name );

    float       GetAmbientIntensity() const { return mLight.ambientIntensity; }
    float       GetDiffuseIntensity() const { return mLight.diffuseIntensity; }
    glm::vec3   GetColor() const { return mLight.color; }

private:
                XOF_REFLECTED_VARIABLE()
    PointLight  mLight;
};
XOF_REFLECTED_CLASS_END()
// ---

// Pass-through logic (just submit a render request/tick-over logic).
class PassThrough : public Component {
public:
                    PassThrough() = delete;
                    PassThrough( Entity *entity, EngineRuntime *engine  );

    virtual void	Update( float dt ) override;
    virtual void	HandleEvent( const Event *e ) override;

    virtual void    Reset() override;

    Entity*         TEMP_HACK_LIGHT{nullptr};

private:
    Renderable*     mRenderable;
    Transformable*  mTransform;
    EngineRuntime*  mEngine;
};

// Skybox/Skydome logic; pretty much the same as passthough bar differences in how we configure the render-request
class SkyBoxLogic : public Component {
public:
                    SkyBoxLogic() = delete;
                    SkyBoxLogic( Entity *entity, EngineRuntime *engine  );

    virtual void	Update( float dt ) override;
    virtual void	HandleEvent( const Event *e ) override;

    virtual void    Reset() override;

    Entity*         TEMP_HACK_LIGHT{nullptr};

private:
    Renderable*     mRenderable;
    Transformable*  mTransform;
    EngineRuntime*  mEngine;
};
// ---


#endif // CAS_COMPONENTS_HPP
