
#include "Core/XOF_CRC32.hpp"

#include "XOF_Component.hpp"

static const U32 XOF_COMPONENT_TYPE_UNASSIGNED = 0xDEADDEAD;
static const U32 XOF_ASCII_INTEGER_OFFSET = 48;


unsigned int Component::smComponentCount = 0;


Component::Component() {
    AssignDefaultName();
    mTypeID = XOF_COMPONENT_TYPE_UNASSIGNED;
}

Component::Component( const std::string& name ) {
    SetName( name );
    mTypeID = XOF_COMPONENT_TYPE_UNASSIGNED;
}

Component::~Component() {
    Component::smComponentCount--;
}

void Component::Update( F32 dt ) { XOF_UNUSED_PARAMETER( dt ); }

void Component::HandleEvent( const Event *e ) { XOF_UNUSED_PARAMETER( e ); }

void Component::Reset() {}

void Component::SetName( const std::string& name ) {
    memset( mName, 0x00, XOF_COMPONENT_MAX_NAME_LENGTH );
    memcpy( mName, name.c_str(), name.length() );
    mUniqueID = CalculateCRC32( 0, mName, name.length() ); //XOF_COMPONENT_MAX_NAME_LENGTH );
}

void Component::AssignDefaultName() {
    memset( mName, 0x00, XOF_COMPONENT_MAX_NAME_LENGTH );

    std::string name( "Component_" );
    name.append( std::to_string( Component::smComponentCount ) );

    memcpy( &mName, name.c_str(), name.length() );
    mUniqueID = CalculateCRC32( 0, mName, name.length() );// XOF_COMPONENT_MAX_NAME_LENGTH );

    Component::smComponentCount++;
}
