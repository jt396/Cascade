#ifndef XOF_COMPONENT_HPP
#define XOF_COMPONENT_HPP


#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"

class Entity;
struct Event;


static const U32 XOF_COMPONENT_MAX_NAME_LENGTH = 32;


XOF_REFLECTED_CLASS_BEGIN()
class Component {
public:
    friend class            Entity;

                            Component();
                            Component( const std::string& name );
                            Component( const Component &lc ) = delete;
                            Component( Component &&lc ) = delete;

    Component&              operator=( const Component &e ) = delete;
    Component&              operator=( Component &&e ) = delete;

    virtual					~Component();

    virtual	void			Update( F32 dt );
    virtual	void			HandleEvent( const Event *e );

    inline U32              GetType() const;
    inline bool				IsType( U32 componentTypeID ) const;

    void					SetName( const std::string& name );
    inline const CHAR*		GetName() const;
    inline U32              GetUniqueID() const;

    virtual void            Reset();

                            // FOR WORLD EDITOR ENTITY/COMPONENT LISTING
    inline const CHAR*      DEBUG_GetTypeAsString() const;

protected:
    U32                     mTypeID;
                            // FOR WORLD EDITOR ENTITY/COMPONENT LISTING
    CHAR                    mTypeString[32];

private:
    static U32              smComponentCount;

                            XOF_REFLECTED_VARIABLE()
    CHAR                    mName[32];
    U32                     mUniqueID;

    void					AssignDefaultName();
};
XOF_REFLECTED_CLASS_END()

//using DataComponent = Component;
//using LogicComponent = Component;

// INLINES
inline U32 Component::GetType() const {
    return mTypeID;
}

inline bool Component::IsType( U32 LogicComponentTypeID ) const {
    return ( mTypeID == LogicComponentTypeID );
}

inline const CHAR* Component::GetName() const {
    return const_cast<CHAR*>( &mName[0] );
}

inline U32 Component::GetUniqueID() const {
    return mUniqueID;
}

inline const CHAR* Component::DEBUG_GetTypeAsString() const {
    return const_cast<CHAR*>( &mTypeString[0] );
}


#endif // XOF_COMPONENT_HPP
