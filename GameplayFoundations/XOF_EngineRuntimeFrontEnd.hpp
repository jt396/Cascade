/*
===============================================================================

	XOF
	===
    File	:	XOF_EngineRuntimeFrontEnd.hpp
    Desc	:	Provides a  front-end for game objects to use
                when they need to interact with the engine runtime.

===============================================================================
*/
#ifndef XOF_ENGINE_RUNTIME_FRONT_END_HPP
#define XOF_ENGINE_RUNTIME_FRONT_END_HPP


#include "Core/XOF_CRC32.hpp"
//
#include "GameplayFoundations/XOF_ReflectionManager.hpp"
//
#include "Resources/XOF_ResourceManager.hpp"
#include "Rendering/XOF_Renderer.hpp"
#include "Audio/XOF_AudioEngine.hpp"
// Change these to interfaces later on
#include "HIDs/XOF_Keyboard_SFML.hpp"
#include "HIDs/XOF_Mouse_SFML.hpp"
//
#include "GameplayFoundations/XOF_EntityManager.hpp"
//
#include "Core/XOF_LoggingManager.hpp"

struct EngineRuntime {
    EngineRuntime() {
        memset( this, 0x00, sizeof( this ) );
        // TEMP?
        reflectionManager = ReflectionManager::Get();
        reflectionManager->Startup();
        // Game/demo will provide this file list to the Startup() function eventually...
        // NOTE: If class B uses an instance of class/struct A as a member, then class/struct A must be handled FIRST
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Resources\\CAS_Resource.hpp" );
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Mesh.hpp" );
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Transform.hpp" );

        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Texture.hpp" );

        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Shader.hpp" );
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Material.hpp" );

        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\Rendering\\XOF_Lights.hpp" );

        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\GameplayFoundations\\XOF_Entity.hpp" );
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\GameplayFoundations\\XOF_Component.hpp" );
        reflectionManager->QueueSourceFileForParsing( "C:\\Users\\Jamie\\Desktop\\Cascade\\GameplayFoundations\\CAS_Components.hpp" );

        reflectionManager->ParseQueuedSourceFiles();

        loggingManager = LoggingManager::Get();
    }

    ~EngineRuntime() {
        loggingManager->Shutdown();
    }

    // TODO: Add start up function (queue files for reflection, configure etc. then call start up)...

    ReflectionManager               *reflectionManager;

    ResourceManager                 *resourceManager;
    std::unique_ptr<Renderer>       renderer;
    std::unique_ptr<AudioEngine>    audio;

    struct InputDevices {
        Keyboard                    keyboard;
        Mouse                       mouse;
        //Gamepad                   gamepad[n];
    };
    InputDevices                    inputDevices;

    TempEntityManager               entityManager;

    LoggingManager*                 loggingManager;
};


#endif // XOF_ENGINE_RUNTIME_FRONT_END_HPP
