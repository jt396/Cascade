/*
===============================================================================

    XOF
    ===
    File	:	XOF_Entity.cpp
    Desc	:	Part of the game object/entity system. Components (data and logic)
                are attached to an entity and together make up a game object/entity.

===============================================================================
*/

#include <string>

#include "../Core/XOF_CRC32.hpp"

#include "XOF_Entity.hpp"


static const unsigned int XOF_ASCII_INTEGER_OFFSET = 48;


U32 Entity::smEntitiesCreatedCount = 0;
U32 Entity::smEntitiesActiveCount = 0;


// NOTE: This handling of entityCount will need revising when
// multi-threading/concurrency is added to the engine
Entity::Entity() {
    AssignDefaultName();

    Entity::smEntitiesCreatedCount++;
    Entity::smEntitiesActiveCount++;

    mIsVisible = true;
}

Entity::Entity( const std::string &name ) {
    SetName( name );

    Entity::smEntitiesCreatedCount++;
    Entity::smEntitiesActiveCount++;

    mIsVisible = true;
}

Entity::~Entity() {
    mDataComponents.clear();
    mLogicComponents.clear();
    Entity::smEntitiesActiveCount--;
}

void Entity::Update( F32 dt ) {
    // TODO: Add an emtpy update function and set a function pointer
    // to call the appropriate update function depending on visibility?
    if( mIsVisible ) {
        for( auto &component : mLogicComponents ) {
            component.get()->Update( dt );
        }
    }
}

void Entity::HandleEvent( const Event *e ) {
    for( auto &component : mLogicComponents ) {
        component.get()->HandleEvent( e );
    }
}

void Entity::RegisterInterestInEvent( U32 eventID ) {
    mRegisteredEventBitField[eventID / XOF_INTEGER_BIT_WIDTH] |= ( 1 << ( eventID % XOF_INTEGER_BIT_WIDTH ) );
}

void Entity::SetName( const std::string &name ) {
    memset( mName, 0x00, XOF_ENTITY_MAX_NAME_LENGTH );
    memcpy( mName, name.c_str(), name.length() );
    mUniqueID = CalculateCRC32( 0, mName, name.length() ); //XOF_ENTITY_MAX_NAME_LENGTH );
}

bool Entity::HasDataComponent( U32 dataComponentTypeID, const CHAR *name ) const {
    return ( GetDataComponent( dataComponentTypeID, name ) != nullptr );
}

Component* Entity::GetDataComponent( U32 typeID, const CHAR *name ) const {
    unsigned int requestedCRC32 = 0;
    if( name ) {
        std::string test( name );
        requestedCRC32 = CalculateCRC32( 0, name, test.length() );
    }

    if( mDataComponentBitField[typeID / XOF_INTEGER_BIT_WIDTH] & ( 1 << ( typeID % XOF_INTEGER_BIT_WIDTH ) ) ) {
        for( auto &component : mDataComponents ) {
            // Get the data component first by type, then refine it by name if one is provided
            if( component.get()->IsType( typeID ) ) {
                if( !name || ( name && ( component.get()->GetUniqueID() == requestedCRC32 ) ) ) {
                    return component.get();
                }
            }
        }
    }

    return nullptr;
}

bool Entity::HasDataComponent( const CHAR *typeString, const CHAR *name ) const {
    return ( GetDataComponent( typeString, name ) != nullptr );
}

Component* Entity::GetDataComponent( const CHAR *typeString, const CHAR *name ) const {
    unsigned int requestedCRC32 = 0;
    if( name ) {
        std::string test( name );
        requestedCRC32 = CalculateCRC32( 0, name, test.length() );
    }

    //if( mDataComponentBitField[typeID / XOF_INTEGER_BIT_WIDTH] & ( 1 << ( typeID % XOF_INTEGER_BIT_WIDTH ) ) ) {
        for( auto &component : mDataComponents ) {
            // Get the data component first by type, then refine it by name if one is provided
            if( std::strcmp( component.get()->DEBUG_GetTypeAsString(), typeString ) == 0 ) {
                if( !name || ( name && ( component.get()->GetUniqueID() == requestedCRC32 ) ) ) {
                    return component.get();
                }
            }
        }
    //}

    return nullptr;
}

bool Entity::HasLogicComponent( U32 typeID ) const {
    return ( GetLogicComponent( typeID ) != nullptr );
}

Component* Entity::GetLogicComponent( U32 typeID ) const {
    //if( mLogicComponentBitField[typeID / XOF_INTEGER_BIT_WIDTH] & ( 1 << ( typeID % XOF_INTEGER_BIT_WIDTH ) ) ) {
        for( auto &component : mLogicComponents ) {
            // Since we're only allowing one of each logic type we don't need to consider the name
            if( component.get()->IsType( typeID ) ) {
                return component.get();
            }
        }
    //}
    return nullptr;
}

// Generic versions, will search both data and logic components
bool Entity::HasComponent( U32 typeID, const CHAR *name ) const {
    return HasDataComponent( typeID, name ) || HasLogicComponent( typeID );
}

Component* Entity::GetComponent( U32 typeID, const CHAR *name ) const {
    Component *data = GetDataComponent( typeID, name );
    if( data ) {
        return data;
    }

    Component *logic = GetLogicComponent( typeID /*, name*/ );
    if( logic ) {
        return logic;
    }

    return nullptr;
}

Component* Entity::GetComponentByName( const CHAR *name ) const {
    unsigned int requestedCRC32 = 0;
    if( name ) {
        std::string test( name );
        requestedCRC32 = CalculateCRC32( 0, name, test.length() );
    }

    // Check data components
    for( auto &component : mDataComponents ) {
        if( component.get()->GetUniqueID() == requestedCRC32 ) {
            return component.get();
        }
    }

    // Check logic components
    for( auto &component : mLogicComponents ) {
        if( component.get()->GetUniqueID() == requestedCRC32 ) {
            return component.get();
        }
    }

    return nullptr;
}

void Entity::Reset() {
    for( auto &component : mDataComponents ) {
        component.get()->Reset();
    }

    for( auto &component : mLogicComponents ) {
        component.get()->Reset();
    }
}

U32 Entity::GetEntitiesCreatedCount() {
    return Entity::smEntitiesCreatedCount;
}

U32 Entity::GetEntitiesActiveCount() {
    return Entity::smEntitiesActiveCount;
}

void Entity::AssignDefaultName() {
    memset( mName, 0x00, XOF_ENTITY_MAX_NAME_LENGTH );

    std::string name( "Entity_" );
    name.append( std::to_string( Entity::smEntitiesCreatedCount ) );

    memcpy( &mName, name.c_str(), name.length() );
    mUniqueID = CalculateCRC32( 0, mName, name.length() );// XOF_ENTITY_MAX_NAME_LENGTH );
}
