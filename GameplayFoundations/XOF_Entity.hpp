/*
===============================================================================

    XOF
    ===
    File	:	XOF_Entity.hpp
    Desc	:	Part of the game object/entity system. Components (data and logic)
		are attached to an entity and together make up a game object/entity.

		NOTE: Handling of static component/entity count values will
		need to be reworked once multi-threading/concurrency is
		introduced.

===============================================================================
*/
#ifndef XOF_ENTITY_HPP
#define XOF_ENTITY_HPP


#include <vector>
#include <memory>

#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"

#include "XOF_Component.hpp"

struct Event;


// Should/will be replaced with actual values based on the number of component and event types
XOF_REFLECTED_ARRAY_SIZE()
static const U32 XOF_GAME_EVENT_TYPE_COUNT = 1; // max( GAME_EVENT_TYPE::COUNT / ( sizeof( int ) * 8 ), 1 )
XOF_REFLECTED_ARRAY_SIZE()
static const U32 XOF_DATA_COMPONENT_TYPE_COUNT = 1; // GAME_DATA_COMPONENT_TYPE::COUNT / ( sizeof( int ) * 8 )


XOF_REFLECTED_ARRAY_SIZE()
#define XOF_ENTITY_MAX_NAME_LENGTH 32


using ComponentUnqPtrVector = std::vector<std::unique_ptr<Component>>;


XOF_REFLECTED_CLASS_BEGIN()
class Entity {
public:
				Entity();
				Entity( const std::string &name );
				Entity( const Entity &e ) = delete;
				Entity( Entity &&e ) = delete;

    Entity&			operator=( const Entity &e ) = delete;
    Entity&			operator=( Entity &&e ) = delete;

				~Entity();

    void			Update( F32 dt );
    void			HandleEvent( const Event *e );
    void			RegisterInterestInEvent( U32 eventID );
    inline const U32*           GetRegisteredEvents() const;

    void			SetName( const std::string &name );
    inline const CHAR*		GetName() const;
    inline U32                  GetUniqueID() const;

    inline void                 SetVisibility( bool isVisible );
    inline bool                 IsVisible() const;

				template<typename ComponentType, typename ...ComponentCtorArgs>
    void                        AddDataComponent( ComponentCtorArgs&&... args );

				// Returns the first data component of the specified type if no name is supplied,
				// otherwise we search for the exact data component type with the specified name
    bool                        HasDataComponent( U32 typeID, const CHAR *name = nullptr ) const;
    Component*                  GetDataComponent( U32 typeID, const CHAR *name = nullptr ) const;

    bool                        HasDataComponent( const CHAR *typeString, const CHAR *name = nullptr ) const;
    Component*                  GetDataComponent( const CHAR *typeString, const CHAR *name = nullptr ) const;

				template<typename ComponentType, typename ...ComponentCtorArgs>
    void			AddLogicComponent( ComponentCtorArgs&&... args );
				// Only allowed one logic component of any given type,
				// so a name field isn't really needed here?
    bool			HasLogicComponent( U32 typeID ) const;
    Component*                  GetLogicComponent( U32 typeID ) const;

				// Generic versions, will search both data and logic components
    bool                        HasComponent( U32 typeID, const CHAR *name = nullptr ) const;
    Component*                  GetComponent( U32 typeID, const CHAR *name = nullptr ) const;



    Component*                  GetComponentByName( const CHAR *name ) const;

    void                        Reset();

				// FOR WORLD EDITOR ENTITY/COMPONENT LISTING
    inline ComponentUnqPtrVector&   DEBUG_GetDataComponents() const;
    inline ComponentUnqPtrVector&   DEBUG_GetLogicComponents() const;

    static U32                  GetEntitiesCreatedCount();
    static U32                  GetEntitiesActiveCount();

private:
				// Keeps track of how many times an entity has been created - never decrements
    static U32                  smEntitiesCreatedCount;
				// Increments/decrements on entity construction/destruction
    static U32          	smEntitiesActiveCount;

    CHAR                        mType[32];

				XOF_REFLECTED_VARIABLE()
    CHAR        		mName[32];

    U32         		mUniqueID;

    U32         		mRegisteredEventBitField[1];

    bool                        mIsVisible;
    CHAR                        padding[3]; // Reflection parsing won't work correctly with unpadded bools/alignment - look into this

    U32         		mDataComponentBitField[1];
    U32                         mLogicComponentBitField[1];

    ComponentUnqPtrVector       mDataComponents;
    ComponentUnqPtrVector       mLogicComponents;

    void			AssignDefaultName();
};
XOF_REFLECTED_CLASS_END()


// TEMP? Could use a custom allocator here? - Create components via entity manager, then simply attach to entities
static const unsigned int XOF_INTEGER_BIT_WIDTH = sizeof( unsigned int ) * 8;

template<typename ComponentType, typename ...ComponentCtorArgs>
void Entity::AddDataComponent( ComponentCtorArgs&&... args ) {
    std::unique_ptr<Component> component( new ComponentType( std::forward<ComponentCtorArgs>( args )... ) );

    U32 componentID = component.get()->GetType();
    mDataComponentBitField[componentID / XOF_INTEGER_BIT_WIDTH] |= ( 1 << ( componentID % XOF_INTEGER_BIT_WIDTH ) );

    mDataComponents.emplace_back( std::move( component ) );
}

template<typename ComponentType, typename ...ComponentCtorArgs>
void Entity::AddLogicComponent( ComponentCtorArgs&&... args ) {
    std::unique_ptr<Component> component( new ComponentType( std::forward<ComponentCtorArgs>( args )... ) );
    mLogicComponents.emplace_back( std::move( component ) );
}


// inlines
inline const U32* Entity::GetRegisteredEvents() const {
    return &mRegisteredEventBitField[0];
}

inline const CHAR* Entity::GetName() const {
    return &mName[0];
}

inline U32 Entity::GetUniqueID() const {
    return mUniqueID;
}

inline void Entity::SetVisibility( bool isVisible ) {
    mIsVisible = isVisible;
}

inline bool Entity::IsVisible() const {
    return mIsVisible;
}

inline ComponentUnqPtrVector& Entity::DEBUG_GetDataComponents() const {
    return const_cast<ComponentUnqPtrVector&>( mDataComponents );
}

inline ComponentUnqPtrVector& Entity::DEBUG_GetLogicComponents() const {
    return const_cast<ComponentUnqPtrVector&>( mLogicComponents );
}

#endif // XOF_ENTITY_HPP
