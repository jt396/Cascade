#ifndef XOF_ENTITYMANAGER_HPP
#define XOF_ENTITYMANAGER_HPP


#include "Platform/XOF_Platform.hpp"
#include "Core/XOF_DynamicPoolAllocator.hpp"
#include "Core/XOF_CRC32.hpp"

#include "XOF_Entity.hpp"

class Entity;
class Component;


struct TempEntityManager {
    TempEntityManager() { mEntities.resize( 1 ); }

    std::vector<std::unique_ptr<Entity>> mEntities;

    Entity* GetEntityTEMP() {
        for( const std::unique_ptr<Entity> &entity : mEntities ) {
            if( entity.get() ) {
                return entity.get();
            }
        }
        return nullptr;
    }
    Entity* GetEntity( std::string& name ) {
        U32 entityID = CalculateCRC32( 0, name.c_str(), name.length() );

        for( const std::unique_ptr<Entity> &entity : mEntities ) {
            if( entity.get() && entity.get()->GetUniqueID() == entityID ) {
                return entity.get();
            }
        }

        return nullptr;
    }
};


// Use this later
#if 0
struct EntityManagerComponentPoolConfig {
            // startingSize * allocationSize = number of initial free elements in pool
    U32     startingSize;
    U32     allocationSize;
    U16     maxAllocations;
    U8      alignment;
            // can the pool grow?
    bool    isDynamic;
};


class EntityManager {
public:
    static EntityManager*   Get();

    bool                    StatUp();
    void                    ShutDown();

    bool                    SetupComponentPools( const EntityManagerComponentPoolConfig *poolConfigs, U32 poolConfigCount );

    Entity*                 CreateEntity( const std::string *entityName = nullptr );
    Entity*                 GetEntity( const std::string& entityName, U32 entityID );
    void                    DestroyEntity( const std::string& componentName, U32 componentID, bool destroyComponents );
    void                    DestroyEntity( Entity *entity, bool destroyComponents );
    void                    DestroyAllEntities( bool destroyComponents );
    inline U32              GetEntityCount() const;

                            // Handles both data and logic components?
                            template<typename ComponentType, typename ...ComponentCtorArgs>
    Component*              CreateComponent( ComponentCtorArgs&&... args );
                            template<typename ComponentType, typename ...ComponentCtorArgs>
    Component*              CreateComponent( const std::string *componentName, ComponentCtorArgs&&... args );
                            // Either use a string-name or (preferably) a hash
    Component*              GetComponent( const std::string& componentName, U32 componentID );
    void                    DestroyComponent( const std::string& componentName, U32 componentID );
    void                    DestroyComponent( Component *component );
    void                    DestroyAllComponents();
    inline U32              GetComponentCount() const;

private:
    DynamicPoolAllocator    mEntityPool; // is this the right type?

    DynamicPoolAllocator    *mComponentPools;
    U32                     mComponentPoolCount;

    U32                     mEntityCount;
    U32                     mComponentCount;
};


// inlines
inline U32 EntityManager::GetEntityCount() const {
    return mEntityCount;
}

inline U32 EntityManager::GetComponentCount() const {
    return mComponentCount;
}
#endif


#endif // XOF_ENTITYMANAGER_HPP
