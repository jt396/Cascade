/*
===============================================================================

	XOF
	===
	File	:	XOF_Event.hpp
	Desc	:	The base for all events in the game.

===============================================================================
*/
#ifndef XOF_EVENT_HPP
#define XOF_EVENT_HPP


#include "Platform/XOF_Platform.hpp"


// Generic event argument
enum class XOF_EVENT_ARG_TYPE : U32 {
    XEAT_SIGNED_INT = 0,
    XEAT_UNSIGNED_INT,
	XEAT_FLOAT,
	XEAT_VEC3F,
    XEAT_32_CHAR_ARRAY,
    XEAT_VOID_POINTER,
    XEAT_BOOL,
	EVENT_ARG_TYPE_COUNT,
};


struct EventArg {
	XOF_EVENT_ARG_TYPE	type;

	union {
        I32             asI32;
		U32				asU32;
		float			asFloat;
                        // GLM vec3 has unallowed operations, so we can't include it in a union directly
		float			asVec3f[3];
        CHAR            as32CharArray[32];
        void*           asVoidPtr;
        bool            asBool;
	};
};
// Entities have the option/ability to 'reply' to events (picking rays etc.)
using EventResponse = EventArg;


struct Event {
    static const U8 MAX_EVENT_ARGS = 16;
    static const U8 MAX_EVENT_REPLIES = 16;

	U32				type;
	EventArg		arguments[MAX_EVENT_ARGS];
    EventResponse   replies[MAX_EVENT_REPLIES];
    U8              replyCount{0};
};


#endif // XOF_EVENT_HPP
