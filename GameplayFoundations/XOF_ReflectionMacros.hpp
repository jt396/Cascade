#ifndef XOF_REFLECTION_MACROS_HPP
#define XOF_REFLECTION_MACROS_HPP


#define XOF_REFLECTED_ARRAY_SIZE()  /* Does nothing atm */
#define XOF_REFLECTED_CLASS_BEGIN() /* Does nothing atm */
#define XOF_REFLECTED_CLASS_END()   /* Does nothing atm */
#define XOF_REFLECTED_STRUCT()      /* Does nothing atm */
#define XOF_REFLECTED_VARIABLE()    /* Does nothing atm */
#define XOF_REFLECTED_FUNCTION()    /* Does nothing atm */


#endif // XOF_REFLECTION_MACROS_HPP
