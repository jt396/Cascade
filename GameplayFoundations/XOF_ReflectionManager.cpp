#include <iostream>
#include <memory>
#include <algorithm>
#include <vector>

#include <GLM/vec3.hpp>

#include "Rendering/XOF_Lights.hpp"
#include "Rendering/XOF_Mesh.hpp"
#include "Rendering/XOF_Transform.hpp"
#include "Rendering/XOF_Material.hpp"

#include "XOF_ReflectionManager.hpp"
// ---


std::map<U32, std::string> mBaseClasses; // TODO: Have as temp global in cpp file


ReflectedVariable* ReflectedClass::GetMemberVariable( const CHAR *name, U32 nameLength, U32 nameHash ) {
    // Get CRC32
    if( nameHash == 0 ) {
        nameHash = CalculateCRC32( 0, name, nameLength );
    }

    // Check base classes
    for( U32 i=0; i<baseClassCount; ++i ) {
        ReflectedVariable *variable;
        if( ( variable = baseClasses[i]->GetMemberVariable( name, nameLength, nameHash ) ) ) {
            return variable;
        }
    }

    // Check variables of this class
    for( U32 i=0; i<reflectedVariableCount; ++i ) {
        ReflectedVariable *variable = &memberVariables[i];
        if( variable->nameAsHash == nameHash ) {
            return variable;
        }
        if( variable->isClassInstance ) {
            if( variable->classInstance->GetMemberVariable( name, nameLength, nameHash ) ) {
                return variable;
            }
        }
    }

    return nullptr;
}
// ---

ReflectionManager* ReflectionManager::Get() {
    static ReflectionManager instance;
    return &instance;
}

bool ReflectionManager::Startup() {
	// POD types
    XOF_ADD_REFLECTED_TYPE( U8 );
    XOF_ADD_REFLECTED_TYPE( I8 );
    XOF_ADD_REFLECTED_TYPE( CHAR );
    XOF_ADD_REFLECTED_TYPE( MSHRSRC );
    XOF_ADD_REFLECTED_TYPE( TXTRRSRC );
    XOF_ADD_REFLECTED_TYPE( SHDRRSRC );
    XOF_ADD_REFLECTED_TYPE( SNDFXRSRC );
    XOF_ADD_REFLECTED_TYPE( MSCRSRC );
    XOF_ADD_REFLECTED_TYPE( bool );
    XOF_ADD_REFLECTED_TYPE( U16 );
    XOF_ADD_REFLECTED_TYPE( I16 );
    XOF_ADD_REFLECTED_TYPE( U32 );
    XOF_ADD_REFLECTED_TYPE( UINT );
    XOF_ADD_REFLECTED_TYPE( I32 );
    XOF_ADD_REFLECTED_TYPE( INT );
    XOF_ADD_REFLECTED_TYPE( F32 );
	// More 'complex' types
	XOF_ADD_REFLECTED_TYPE( std::string );
    XOF_ADD_REFLECTED_TYPE( glm::vec3 );
    XOF_ADD_REFLECTED_CLASS_TYPE( Mesh );
    XOF_ADD_REFLECTED_CLASS_TYPE( Transform );
    XOF_ADD_REFLECTED_CLASS_TYPE( Texture );
    XOF_ADD_REFLECTED_CLASS_TYPE( Shader );
    XOF_ADD_REFLECTED_CLASS_TYPE( Material );
    //XOF_ADD_REFLECTED_CLASS_TYPE( BaseLight );
    XOF_ADD_REFLECTED_CLASS_TYPE( PointLight );
    XOF_ADD_REFLECTED_TEMPLATE_TYPE( std::vector<U32> );
    XOF_ADD_REFLECTED_TEMPLATE_TYPE( std::unique_ptr<U32> );
    // Done
    return true;
}

void ReflectionManager::Shutdown() {}

void ReflectionManager::QueueSourceFileForParsing( const CHAR *fileName ){
    mSourceFilesQueuedForParsing.push_back( fileName );
}

bool ReflectionManager::ParseQueuedSourceFiles() {
    for( auto& sourceFile : mSourceFilesQueuedForParsing ) {
        if( !ParseSourceFileForReflectionData( sourceFile.c_str() ) ) {
            std::cerr << "ReflectionManager::ParseQueuedSourceFiles(): failed to parse source file: " << sourceFile << std::endl;
            return false;
        }
    }

	// The memory offset of reflected variables must account for the additional size introduced by any base-classes
	for( auto& reflectedClass : mReflectedClasses ) {
		auto res = mBaseClasses.find( reflectedClass.first );
		if( res != mBaseClasses.end() ) {
            ReflectedClass *rc = &reflectedClass.second;
			std::string baseClasses( res->second );
			U32 charactersCounted = 0;
			while( charactersCounted != baseClasses.length() && rc->baseClassCount != XOF_MAX_BASE_CLASSES ) {
				// Get the base classes
				mTokenizer.SetBuffer( const_cast<CHAR*>( baseClasses.c_str() ), baseClasses.length() );

				CHAR className[XOF_REFLECTED_CLASS_NAME_LEN] = {'\0'};
				U32 baseClassNameLength = mTokenizer.GetNextToken( className, XOF_REFLECTED_CLASS_NAME_LEN, ",", 1 );
				charactersCounted += baseClassNameLength + 1; // +1 for the separating character

                ReflectedClass *baseClass = &mReflectedClasses.find( CalculateCRC32( 0, className, baseClassNameLength ) )->second;

                for( U32 i=0; i<rc->reflectedVariableCount; ++i ) {
                    rc->memberVariables[i].memoryOffset += baseClass->size;
				}
				rc->baseClasses[rc->baseClassCount] = baseClass;
				rc->baseClassCount++;

                mTokenizer.ReleaseBuffer();
			}
		}
	}

    // Setup any class/struct instance member variables
    for( auto& reflectedClass : mReflectedClasses ) {
        ReflectedClass *rc = &reflectedClass.second;
        for( U32 i=0; i<rc->reflectedVariableCount; ++i ) {
            ReflectedVariable *rv = &rc->memberVariables[i];
            if( rv->isClassInstance ) {
                rv->classInstance = &mReflectedClasses.find( rv->typeAsHash )->second;
            }
        }
    }

	mBaseClasses.clear();
    mSourceFilesQueuedForParsing.clear();
    return true;
}

ReflectedClass* ReflectionManager::GetReflectedClass( const CHAR *reflectedClassName ) { 
	std::string className( reflectedClassName );
    auto itr = mReflectedClasses.find( CalculateCRC32( 0, className.c_str(), className.length() ) );
    return ( itr != mReflectedClasses.end() ) ? &itr->second : nullptr;
}

bool ReflectionManager::ParseSourceFileForReflectionData( const CHAR *fileName ) {
    FILE *f = fopen( fileName, "rb" );
    if( !f ) {
		std::cerr << "ReflectionManager::ParseSourceFileForReflectionData(): Failed to open file: "
			<< fileName << " for parsing" << std::endl;
        return false;
    }

    // Read in source file contents
    fseek( f, 0, SEEK_END );
    LONG fileSize = ftell( f );
    fseek( f, 0, SEEK_SET );

    std::unique_ptr<CHAR> sourceFileContents( new CHAR[fileSize + 1] );
    fread( sourceFileContents.get(), fileSize, 1, f );
    fclose( f );
    sourceFileContents.get()[fileSize] = '\0';

#if 0
    // other approaches
    std::ifstream ifs( fileName );
    std::string sourceFileContents;
    sourceFileContents.assign( std::istreambuf_iterator<char>( ifs ), std::istreambuf_iterator<char>() );

    ifs.seekg( 0, is.end );
    CHAR *sourceFileContents = new CHAR[ifs.tellg()];
    ifs.seekg( 0, is.beg );
#endif

    // Parse for reflection data
    mTokenizer.SetBuffer( sourceFileContents.get(), fileSize );

    ReflectedClass *rc = nullptr;

    const U32 tokenMaxSize = 128;
    CHAR token[tokenMaxSize];
    CHAR delimiters[] = { ' ', '\n', '\t', '\r', '<' }; // '\n' for unix, '\r' for mac, '\n' + '\r' on windows
	const U32 delimiterCount = sizeof( delimiters );

    bool wasLastTypeTemplate = false;

    while( !mTokenizer.IsBufferEmpty() ) {
		memset( token, 0x00, tokenMaxSize );
        U32 tokenLength = mTokenizer.ClearAndGetNextToken( token, tokenMaxSize, delimiters, delimiterCount );
#if 0
        if( strcmp( token, "XOF_REFLECTED_ARRAY_SIZE()" ) == 0 ) {
            ParseReflectedArraySize();
        } else
#endif
        if( strcmp( token, "XOF_REFLECTED_CLASS_BEGIN()" ) == 0 ) {
            rc = ParseReflectedClass();
        } else if( strcmp( token, "XOF_REFLECTED_VARIABLE()" ) == 0 ) {
            ParseReflectedVariable( rc );
        } else if( strcmp( token, "XOF_REFLECTED_CLASS_END()" ) == 0 ) {
            // Done
            rc = nullptr;
        } else if( rc ) {
            // Get the current line in the source file
            U32 lineLength = mTokenizer.GetCurrentLineLength();
            if( lineLength == 0 ) {
                continue;
            }
            std::unique_ptr<CHAR> currentLine( new CHAR[lineLength] );
            mTokenizer.GetCurrentLine( currentLine.get(), lineLength );

            // Static variables don't add to the instance size, so skip them
            for( U32 i=0; i<2; ++i ) {
                if( strcmp( token, "static" ) == 0 || strcmp( token, "const" ) == 0 ) {
                    tokenLength = mTokenizer.ClearAndGetNextToken( token, tokenMaxSize, delimiters, delimiterCount );
                }
            }

            // Check for structs/classes defined within a reflected class.
            // These struct/class declarations should not add to the reflected class size themselves.
            // if(token == struct) inStruct = true; continue
            // else if(inStruct && token == } && token+1 == ;) inStruct = false; continue?

			// TODO: Go backward
            // Check the whole line to see if it's actually a function
            U32 i=0;
            for( i; i<lineLength; ++i ) {
                if( currentLine.get()[i] == '(' || currentLine.get()[i] == ')' ) {
                    break;
                }
            }
            if( i != lineLength ) {
                continue;
            }

            // Check for arrays and get the size if one's present
            U32 elementCount = 1;
            for( i=0; i<lineLength; ++i ) {
                if( currentLine.get()[i] == '[' ) {
                    CHAR *p = &currentLine.get()[i+1];
                    while( *p != ';' && *p != '\0' ) {
                        U32 currentDimension = 0;
                        U32 currentDimensionScalar = 1;
                        while( *p != ']' ) {
                            currentDimension *= currentDimensionScalar;
                            currentDimension += (*p - 48);
                            currentDimensionScalar *= 10;
                            ++p;
                        }
                        elementCount *= currentDimension;
                        if( *(++p) == '[' ) {
                            // Dealing with a multidimensional array
                            ++p;
                        }
                    }
                    break;
                }
            }

            // Check the token for raw pointers
            bool isRawPointer = false;
            for( i=0; i<tokenLength; ++i ) {
                if( token[i] == '*' ) {
                    isRawPointer = true;
                    tokenLength = i;
                    break;
                }
            }

            // Is it actually a variable type?
            auto typeInfo = mReflectedTypes.find( CalculateCRC32( 0, token, ( tokenLength ) ) );
            if( typeInfo != mReflectedTypes.end() ) {
                if( wasLastTypeTemplate ) {
                    // Templates within templates don't themselves contribute to the class size
                    wasLastTypeTemplate = false;
                    continue;
                }
                rc->size += ( isRawPointer ? sizeof( U32* ) : typeInfo->second->size ) * elementCount;
                wasLastTypeTemplate = typeInfo->second->isTemplated;
            }

            // TEMP - Need a better way at handling template<template<>> situations,
            // what if a type simply isn't reflected?
            if( typeInfo == mReflectedTypes.end() && isRawPointer ) {
                rc->size += sizeof( U32* ) * elementCount;
            }
        }
    } // while( !mTokenizer.IsBufferEmpty() )

    mTokenizer.ReleaseBuffer();
    return true;
}

void ReflectionManager::ParseReflectedArraySize() {}

ReflectedClass* ReflectionManager::ParseReflectedClass() {
    CHAR delimiters[] = { ' ', '\n', '\t', '\r' };
	const U32 delimiterCount = sizeof( delimiters );

    // Skip the class keyword
    mTokenizer.ClearAndGetNextToken( nullptr, 0, delimiters, delimiterCount );

    // Get the class name
    ReflectedClass rc;

	CHAR className[XOF_REFLECTED_CLASS_NAME_LEN];
	U32 nameLength = mTokenizer.ClearAndGetNextToken( className, XOF_REFLECTED_CLASS_NAME_LEN, delimiters, delimiterCount );
	rc.nameAsHash = CalculateCRC32( 0, className, nameLength );
#if defined( XOF_DEBUG )
    memcpy( rc.nameAsString, className, XOF_REFLECTED_CLASS_NAME_LEN );
#endif

    auto pair = mReflectedClasses.emplace( std::pair<U32, ReflectedClass>( rc.nameAsHash, rc ) );

    // Check for inhertiance/base classes
	CHAR buffer[64] = {'\0'};
    mTokenizer.GetNextToken( buffer, 64, delimiters, delimiterCount );
    if( strcmp( buffer, ":" ) == 0 ) {
        ParseInheritance( &rc );
    }

    return pair.second ? &pair.first->second : nullptr;
}

void ReflectionManager::ParseInheritance( ReflectedClass *rc ) {
	const U32 bufferSize = 64;
	CHAR buffer[bufferSize] = {'\0'};

	CHAR delimiters[] = { ' ', ',', '\n', '\t', '\r' };
	const U32 delimiterCount = sizeof( delimiters );

    // Skip the first access level
    mTokenizer.GetNextToken( buffer, bufferSize, delimiters, delimiterCount );

	U32 i = 0;
	std::string baseClasses;
    do {
        // Get the name
        mTokenizer.GetNextToken( buffer, bufferSize, delimiters, delimiterCount );
		baseClasses.append( buffer );
		// Add a separator to the base class names so they can be easily parsed again later
		baseClasses.append( "," );

        // Skip remaining access levels
		memset( buffer, 0x00, bufferSize );
        mTokenizer.GetNextToken( buffer, bufferSize, delimiters, delimiterCount );
    } while( ( strcmp( buffer, "{" ) != 0 ) && ( ++i < XOF_MAX_BASE_CLASSES ) );

	mBaseClasses.emplace( std::pair<U32, std::string>( rc->nameAsHash, baseClasses ) );
}

void ReflectionManager::ParseReflectedVariable( ReflectedClass *rc ) {
    ReflectedVariable *rv = &( rc->memberVariables[rc->reflectedVariableCount] );

    const U32 tokenMaxSize = 128;
    CHAR token[tokenMaxSize];
    CHAR delimiters[] = { ' ', '\n', ';', '\t', '<' };
    const U32 delimiterCount = sizeof( delimiters );

    U32 tokenLength = mTokenizer.ClearAndGetNextToken( token, tokenMaxSize, delimiters, delimiterCount );
    // Skip over static and/or const if present and get the next token
    U32 i;
    for( i=0; i<2; ++i ) {
        if( strcmp( token, "static" ) == 0 ) {
            tokenLength = mTokenizer.ClearAndGetNextToken( token, tokenMaxSize, delimiters, delimiterCount );
        }
        if( strcmp( token, "const" ) == 0 ) {
            rv->isConst = true;
            tokenLength = mTokenizer.ClearAndGetNextToken( token, tokenMaxSize, delimiters, delimiterCount );
        }
    }

    // Handle raw pointers - TODO: Handle pointer to pointer etc.
    for( i=0; i<tokenLength; ++i ) {
        if( token[i] == '*') {
            rv->isRawPointer = true;
            tokenLength = i;
            break;
        }
    }

    // Get/Set the variable type
    U32 varTypeLength;
    CHAR varType[XOF_REFLECTED_VAR_TYPE_LEN];

    auto type = mReflectedTypes.find( CalculateCRC32( 0, token, tokenLength ) );
    if( type != mReflectedTypes.end() ) {
        memcpy( varType, token, tokenLength );
        varTypeLength = tokenLength;
    } else {
        varTypeLength = mTokenizer.ClearAndGetNextToken( varType, XOF_REFLECTED_VAR_TYPE_LEN, delimiters, delimiterCount );
        // Check again for raw pointers
        for( i=0; i<varTypeLength; ++i ) {
            if( varType[i] == '*' ) {
                rv->isRawPointer = true;
                varTypeLength = i;
                break;
            }
        }
    }

    auto typeInfo = mReflectedTypes.find( CalculateCRC32( 0, varType, varTypeLength ) );
    // TODO: We could let this slide if the variable is a pointer, since that's just sizeof(int)?
    XOF_ASSERT( typeInfo != mReflectedTypes.end() );
    rv->typeAsHash = CalculateCRC32( 0, varType, varTypeLength );

    // Get the variable name
    CHAR varName[XOF_REFLECTED_VAR_NAME_LEN];
    U32 varNameLength = mTokenizer.ClearAndGetNextToken( varName, XOF_REFLECTED_VAR_NAME_LEN, delimiters, delimiterCount );

    // Check for arrays and get the size if one's present
    rv->count = 1;
    for( i=0; i<varNameLength; ++i ) {
        if( varName[i] == '[' ) {
            CHAR *p = &varName[i+1];
            while( *p != ';' && *p != '\0' ) {
                U32 currentDimension = 0;
                U32 currentDimensionScalar = 1;
                while( *p != ']' ) {
                    currentDimension *= currentDimensionScalar;
                    currentDimension += (*p - 48);
                    currentDimensionScalar *= 10;
                    ++p;
                }
                rv->count *= currentDimension;
                if( *(++p) == '[' ) {
                    // Dealing with a multidimensional array
                    ++p;
                }
            }
            break;
        }
    }
    varNameLength = i;
    rv->nameAsHash = CalculateCRC32( 0, varName, varNameLength );
    rv->memoryOffset = rc->size;

    // Check if it's actually a member instance of a class/struct
    rv->isClassInstance = typeInfo->second->isClass;

    // Update the class
    rc->size += ( rv->isRawPointer ? sizeof( U32* ) : typeInfo->second->size ) * rv->count;
    rc->reflectedVariableCount++;

#if defined( XOF_DEBUG )
    memcpy( rv->typeAsString, varType, varTypeLength );
    memcpy( rv->nameAsString, varName, varNameLength );
#endif
}
