#ifndef XOF_REFLECTIONMANAGER_HPP
#define XOF_REFLECTIONMANAGER_HPP


#include <functional>
#include <vector>
#include <map>
#include <string>
#include <type_traits>
#include <cstdarg>

#include "Core/XOF_Tokenizer.hpp"
#include "Core/XOF_CRC32.hpp"

#include "Rendering/XOF_Mesh.hpp"
#include "ThirdParty/JSON/json.hpp"

#include "XOF_ReflectionMacros.hpp"

// TEMP
#define XOF_DEBUG 1


struct ReflectedVariable;
struct ReflectedClass;
class Entity;

struct ReflectedTypeInfo {
            ReflectedTypeInfo( size_t _size, bool _isTemplated, bool _isClass ) :
                size{_size}, isTemplated{_isTemplated}, isClass{_isClass} {}

    size_t  size;
    bool    isTemplated;
    bool   	isClass;

    std::function<bool( Entity*, CHAR*, void*, void*, void* )>      Set;
    std::function<void*( Entity*, CHAR*, void*, void*, void* )>     Get;

    // Variable to serialize, JSON representation and the base adddress
    std::function<bool( const CHAR*, void*, const CHAR*, const nlohmann::json& )>    SerializeIn;
};

// TODO: Add external variants?
#define XOF_ADD_REFLECTED_TYPE( type )                                                                                      \
    do {                                                                                                                    \
        ReflectedTypeInfo *reflectedType = new ReflectedTypeInfo( sizeof( type ), false, false );                           \
        mReflectedTypes.insert( std::make_pair( CalculateCRC32( 0, #type, sizeof( #type ) - 1 ), reflectedType ) );         \
    } while( 0 )

#define XOF_ADD_REFLECTED_TEMPLATE_TYPE( type )                                                         \
    do {                                                                                                \
        mTokenizer.ReleaseBuffer();                                                                     \
        CHAR buffer[128] = {'\0'};                                                                      \
        mTokenizer.SetBuffer( (CHAR*)#type, sizeof( #type ) );                                          \
        U32 length = mTokenizer.GetNextToken( buffer, 128, "<", 1 );                                    \
                                                                                                        \
        ReflectedTypeInfo *reflectedType = new ReflectedTypeInfo( sizeof( type ), true, false );        \
        mReflectedTypes.insert( std::make_pair( CalculateCRC32( 0, buffer, length ), reflectedType ) ); \
    } while( 0 )

// Only necessary for classes instances of which are likely to be used as member variables in other classes/structs
#define XOF_ADD_REFLECTED_CLASS_TYPE( type )                                                                        \
    do {                                                                                                            \
        ReflectedTypeInfo *reflectedType = new ReflectedTypeInfo( sizeof( type ), false, true );                    \
        mReflectedTypes.insert( std::make_pair( CalculateCRC32( 0, #type, sizeof( #type ) - 1 ), reflectedType ) ); \
    } while( 0 )

#define XOF_REFLECTED_VAR_NAME_LEN      32
#define XOF_REFLECTED_VAR_TYPE_LEN      16
#define XOF_REFLECTED_CLASS_NAME_LEN    32


struct ReflectedVariable {
                    ReflectedVariable() { memset( this, 0x00, sizeof( ReflectedVariable ) ); }

    U32     		nameAsHash;
    U32     		typeAsHash;
    size_t  		memoryOffset;
    bool    		isConst;
    bool    		isRawPointer;
                    // for member classes (i.e transform instance in a transform component)
    bool    		isClassInstance;
    ReflectedClass* classInstance;
                    // 1 for a single variable, > 1 for arrays
    U8      		count;
#if defined( XOF_DEBUG )
    CHAR    		nameAsString[XOF_REFLECTED_VAR_NAME_LEN];
    CHAR    		typeAsString[XOF_REFLECTED_VAR_TYPE_LEN];
#endif
};


static const U32 XOF_MAX_REFLECTED_VARIABLES = 8;
static const U32 XOF_MAX_BASE_CLASSES = 2;

struct ReflectedClass {
                        ReflectedClass() { memset( this, 0x00, sizeof( ReflectedClass ) ); }

    ReflectedVariable*  GetMemberVariable( const CHAR *name, U32 nameLength, U32 crc32 = 0 );
    //ReflectedVariable*  GetMemberVariable( U32 crc32 = 0 );
    //ReflectedVariable*  GetMemberVariable( FixedLengthString<N>& name );

    U32                 nameAsHash;

    ReflectedVariable   memberVariables[XOF_MAX_REFLECTED_VARIABLES];
    U8                  reflectedVariableCount;
    ReflectedClass*     baseClasses[XOF_MAX_BASE_CLASSES];
    U8					baseClassCount;
    size_t              size;

#if defined( XOF_DEBUG )
    CHAR                nameAsString[XOF_REFLECTED_CLASS_NAME_LEN];
#endif
};


class ReflectionManager {
public:
    static ReflectionManager*           Get();

    using ReflectedTypeStore            = std::map<U32, ReflectedTypeInfo*>;
    using ReflectedClassStore           = std::map<U32, ReflectedClass>;

    bool                                Startup();
    void                                Shutdown();

    void                                QueueSourceFileForParsing( const CHAR *fileName );
    bool                                ParseQueuedSourceFiles();

    ReflectedClass*                     GetReflectedClass( const CHAR *reflectedClassName );
    ReflectedClassStore&                GetReflectedClasses() const { return const_cast<ReflectedClassStore&>( mReflectedClasses ); }

                                        template<typename T>
    bool                                SetReflectedVariable( const CHAR *reflectedClassName, void *classInstance, const CHAR *reflectedVariableName, const void *value );

    inline ReflectedTypeInfo*           GetReflectedTypeInfo( U32 typeHash ) const;

private:
    ReflectedTypeStore                  mReflectedTypes;
    ReflectedClassStore                 mReflectedClasses;

    std::vector<std::string>            mSourceFilesQueuedForParsing;
    Tokenizer                           mTokenizer;

    bool                                ParseSourceFileForReflectionData( const CHAR *fileName );
    void                                ParseReflectedArraySize();
    ReflectedClass*                     ParseReflectedClass();
    void                                ParseInheritance( ReflectedClass *rc );
    void                                ParseReflectedVariable( ReflectedClass *rc );
};


// Templates & inline
template<typename T>
bool ReflectionManager::SetReflectedVariable( const CHAR *reflectedClassName, void *classInstance, const CHAR *reflectedVariableName, const void *value ) {
    std::string className( reflectedClassName );
    auto itr = mReflectedClasses.find( CalculateCRC32( 0, className.c_str(), className.length() ) );

    // Couldn't find the class
    if( itr == mReflectedClasses.end() ) {
        std::cerr << "ReflectionManager::SetReflectedVariable(): Couldn't find class: "
            << reflectedClassName << std::endl;
        return false;
    }

    ReflectedClass *rc = &itr->second;

    std::string varName( reflectedVariableName );
    ReflectedVariable *rv = rc->GetMemberVariable( varName.c_str(), varName.length() );

    // Couldn't find the varaible
    if( !rv ) {
        std::cerr << "ReflectionManager::SetReflectedVariable(): Couldn't find variable: "
            << reflectedVariableName << " in class: " << reflectedClassName << std::endl;
        return false;
    }

    size_t totalMemoryOffset = rv->memoryOffset;
    if( rv->isClassInstance && (!rv->isRawPointer) ) {
        ReflectedVariable *var = rv->classInstance->GetMemberVariable( varName.c_str(), varName.length() );
        totalMemoryOffset += var->memoryOffset;
    }

    // Update the member variable
    const T* newValue = reinterpret_cast<const T*>( std::is_pointer<T>::value ? &value : value );
    T* variable = reinterpret_cast<T*>( reinterpret_cast<size_t>( classInstance ) + totalMemoryOffset );
    for( U32 i=0; i<rv->count; ++i ) {
        *variable++ = *newValue++;
    }

    return true;
}

inline ReflectedTypeInfo* ReflectionManager::GetReflectedTypeInfo( U32 typeHash ) const {
    auto typeInfo = mReflectedTypes.find( typeHash );
    XOF_ASSERT( typeInfo != mReflectedTypes.end() );
    return const_cast<ReflectedTypeInfo*>( typeInfo->second );
}


#endif // XOF_REFLECTIONMANAGER_HPP
