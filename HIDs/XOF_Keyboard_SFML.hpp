/*
===============================================================================

	XOF
	===
	File	:	XOF_Keyboard_SFLM.hpp
	Desc	:	SFML implementation of basic keyboard interface.

===============================================================================
*/
#ifndef XOF_KEYBOARD_SFML
#define XOF_KEYBOARD_SFML


#include <SFML/Window/Keyboard.hpp>

#include "XOF_Keyboard_I.hpp"


// implementation specific button/key/control codes
static const U8 KeyboardKeyCodes[XOF_KEYBOARD::KEY_COUNT] =  {
	// esc
	sf::Keyboard::Escape, 

	// function keys
	sf::Keyboard::F1, sf::Keyboard::F2, sf::Keyboard::F3, sf::Keyboard::F4, sf::Keyboard::F5, sf::Keyboard::F6,
	sf::Keyboard::F7, sf::Keyboard::F8, sf::Keyboard::F9, sf::Keyboard::F10, sf::Keyboard::F11, sf::Keyboard::F12, 

	// numerical keys
	sf::Keyboard::Num0, sf::Keyboard::Num1, sf::Keyboard::Num2, sf::Keyboard::Num3, sf::Keyboard::Num4,
	sf::Keyboard::Num5, sf::Keyboard::Num6, sf::Keyboard::Num7, sf::Keyboard::Num8, sf::Keyboard::Num9,

	// alphabetic keys
	sf::Keyboard::A, sf::Keyboard::B, sf::Keyboard::C, sf::Keyboard::D, sf::Keyboard::E, sf::Keyboard::F, sf::Keyboard::G, sf::Keyboard::H, sf::Keyboard::I, sf::Keyboard::J, sf::Keyboard::K, sf::Keyboard::L, sf::Keyboard::M, 
	sf::Keyboard::N, sf::Keyboard::O, sf::Keyboard::P, sf::Keyboard::Q, sf::Keyboard::R, sf::Keyboard::S, sf::Keyboard::T, sf::Keyboard::U, sf::Keyboard::V, sf::Keyboard::W, sf::Keyboard::X, sf::Keyboard::Y, sf::Keyboard::Z,

	// numpad
	sf::Keyboard::Numpad0, sf::Keyboard::Numpad1, sf::Keyboard::Numpad2, sf::Keyboard::Numpad3, sf::Keyboard::Numpad4,
	sf::Keyboard::Numpad5, sf::Keyboard::Numpad6, sf::Keyboard::Numpad7, sf::Keyboard::Numpad8, sf::Keyboard::Numpad9,
	0, 0, 0, 0, 0, 0, 0,

	// arrows
	sf::Keyboard::Up, sf::Keyboard::Down, sf::Keyboard::Left, sf::Keyboard::Right, 

	// 'special' cluster 1
	0, 0, 0,

	// 'special' cluster 2
	sf::Keyboard::Insert, sf::Keyboard::Home, 0, sf::Keyboard::Delete, 0, 0,

	// left & right variant keys
	sf::Keyboard::LShift, sf::Keyboard::RShift, sf::Keyboard::LControl, sf::Keyboard::RControl, 0, 0, sf::Keyboard::LSystem, sf::Keyboard::RSystem,

	// cluster 3
	0, 0,
	0, 0,
	sf::Keyboard::Space, 0,

	// cluster 4
	0, 0,
	0, 0,

	// OEM
	0, 0, 0, 0, 0, 0, 0, 0,
};

class Keyboard : public KeyboardI {
public:
            Keyboard();
            ~Keyboard();

	void	Update();

	bool	IsKeyUp( U32 key );
	bool	IsKeyDown( U32 key );

private:
    //      ...
};

#endif // XOF_KEYBOARD_SFML
