/*
===============================================================================

	XOF
	===
	File	:	XOF_Mouse_SFLM.cpp
	Desc	:	SFML implementation of basic mouse interface.

===============================================================================
*/
#include "XOF_Mouse_SFML.hpp"


#if defined window
	#undef window
	#define window sf::Window*
#endif


Mouse::Mouse() {
	MouseI::mWindow = nullptr;
}

Mouse::~Mouse() {}

void Mouse::SetWindow( const void *relativeTo ) {
	mWindow = const_cast<void*>( relativeTo );
}

void Mouse::Update() {}

bool Mouse::IsButtonUp( U32 button ) const {
	return !sf::Mouse::isButtonPressed( static_cast<sf::Mouse::Button>( MouseButtonCodes[button] ) );
}

bool Mouse::IsButtonDown( U32 button ) const {
	return sf::Mouse::isButtonPressed( static_cast<sf::Mouse::Button>( MouseButtonCodes[button] ) );
}

glm::vec2 Mouse::GetRelativePosition() const {
	if( mWindow ) {
		sf::Vector2i relativeMousePosition = sf::Mouse::getPosition( 
			const_cast<sf::Window&>( *reinterpret_cast<sf::Window*>( mWindow ) ) );
		return  glm::vec2( relativeMousePosition.x, relativeMousePosition.y );
	} else {
		return glm::vec2( -1.f, -1.f );
	}
}

void Mouse::SetPosition( U32 x, U32 y ) {
	sf::Mouse::setPosition( sf::Vector2i( x, y ), 
		const_cast<sf::Window&>( *reinterpret_cast<sf::Window*>( mWindow ) ) );
}
