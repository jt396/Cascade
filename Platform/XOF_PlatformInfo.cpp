/*
===============================================================================

	XOF
	===
	File	:	XOF_PlatformInfo.cpp
	Desc	:	Get more info on the platform

===============================================================================
*/
#include "XOF_PlatformInfo.hpp"
#include <SFML/OpenGL.hpp>


void CPUIDQuery( I32 query, CPUDesc& result ) {
#if XOF_PLATFORM == XOF_PLATFORM_WINDOWS
	__cpuid( (I32*)result.registers, query );
#elif XOF_PLATFORM == XOF_PLATFORM_LINUX
	#error Add Linux CPUID support!
#else
	std::cout << "TODO: Resolve CPU Vendor query" << std::endl; 
#endif
}

void GetPlatformInfo() {
	CPUDesc cpuDesc;
	// Query 0: CPU Vendor
	CPUIDQuery( 0, cpuDesc );

	cpuDesc.vendor =  reinterpret_cast<CHAR*>( &cpuDesc.registers[1] );
	*reinterpret_cast<U32*>(&cpuDesc.vendor[0] + 4) = cpuDesc.registers[3];
	*reinterpret_cast<U32*>(&cpuDesc.vendor[0] + 8) = cpuDesc.registers[2];

	std::cout << "CPU: " << cpuDesc.vendor << std::endl;
	std::cout << "GPU: " << glGetString( GL_RENDERER ) << std::endl;
}