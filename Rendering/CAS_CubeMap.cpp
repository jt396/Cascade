/*
===============================================================================

    XOF
    ===
    File	:	XOF_CubeMap.hpp
    Desc	:	TEMP representation of a cubemap for use in skyboxes.

===============================================================================
*/

#include "stb_image.h"
#include "Resources/XOF_ResourceManager.hpp"

#include "CAS_CubeMap.hpp"

#define XOF_UNREFERENCED_PARAMETER( param ) (void)param
#define TEMP_ASSERT_NO_GL_ERROR() assert( glGetError == GL_NO_ERROR )


CubeMap::CubeMap() {
    mIsLoaded = false;
}

CubeMap::~CubeMap() {
    glDeleteTextures( 1, &mTexture );
}

bool CubeMap::Load( ResourceDescriptor *desc ) {
    bool mIsLoaded = false;

    // generate the GL specifics
    glGenTextures( 1, &mTexture );
    assert( glGetError() == GL_NO_ERROR );
    glBindTexture( GL_TEXTURE_CUBE_MAP, mTexture );
    assert( glGetError() == GL_NO_ERROR );

    // load the actual image files
    const GLenum cubeSides[] {GL_TEXTURE_CUBE_MAP_POSITIVE_X, GL_TEXTURE_CUBE_MAP_NEGATIVE_X,
                              GL_TEXTURE_CUBE_MAP_POSITIVE_Y, GL_TEXTURE_CUBE_MAP_NEGATIVE_Y,
                              GL_TEXTURE_CUBE_MAP_POSITIVE_Z, GL_TEXTURE_CUBE_MAP_NEGATIVE_Z};

    const std::string cubeSideNames[] { "left", "right", "top", "bottom", "front", "back" };

    const std::string baseName( desc->fileName.CStr() );
    const std::string directory( desc->directory.CStr() );
    const std::string baseNameAndDirectory( directory + baseName );

    for( auto i=0; i<6; ++i ) {
        const std::string fileName( baseNameAndDirectory + "_" + cubeSideNames[i] + ".jpg" );

        // load face i
        I32 componentCount;
        GLvoid *imageData = reinterpret_cast<GLvoid*>( stbi_load( fileName.c_str(), &mWidth, &mHeight, &componentCount, 4 ) );
        assert( imageData );

        // set the data
        glTexImage2D( cubeSides[i], 0, GL_RGB, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, imageData );
        assert( glGetError() == GL_NO_ERROR );

        // set other image properties
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri( GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE );
        assert( glGetError() == GL_NO_ERROR );

        // free data and continue to next cube face
        stbi_image_free( imageData );
    }

    memset( texName, 0x00, 32 );
    std::string name( "cubeMapTest" );//desc->fileName.CStr() );
    memcpy( texName, name.c_str(), std::min<INT>( name.length(), 32 ) );
    mUID = CalculateCRC32( 0, name.c_str(), std::min<INT>( name.length(), 32 ) );

    mIsLoaded = true;
    return mIsLoaded;
}
