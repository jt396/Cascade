/*
===============================================================================

    XOF
    ===
    File	:	XOF_CubeMap.hpp
    Desc	:	TEMP representation of a cubemap for use in skyboxes.

===============================================================================
*/
#ifndef CAS_CUBEMAP_HPP
#define CAS_CUBEMAP_HPP


#include <GL/glew.h>
#if defined( EDITOR_OR_DEBUG )
#include <string>
#endif

#include "Resources/CAS_Resource.hpp"


XOF_REFLECTED_CLASS_BEGIN()
class CubeMap : public Resource {
public:
    friend class            ResourceManager;

                            CubeMap();
                            ~CubeMap();

    virtual bool            Load( ResourceDescriptor *desc ) override;

    //inline XOF_TEXTURE_TYPE GetTextureType() const;

    inline I32              GetWidth() const;
    inline I32              GetHeight() const;
    //inline U32            GetPixel( U32 x, U32 y ) const;

    inline void             Bind( U32 unit );

    virtual CHAR*           GetName() const override { return (CHAR*)texName; }

private:
                            XOF_REFLECTED_VARIABLE()
    TXTRRSRC                texName[32];

    U32                     mTexture;
    I32                     mWidth;
    I32                     mHeight;
    U32*                    mRawImageData;

    //XOF_TEXTURE_TYPE      mType;
};
XOF_REFLECTED_CLASS_END()


// Inlines
/*
inline XOF_TEXTURE_TYPE CubeMap::GetTextureType() const {
    return mType;
}
*/
inline I32 CubeMap::GetWidth() const {
    return 0;
}

inline I32 CubeMap::GetHeight() const {
    return 0;
}
/*
inline U32 CubeMap::GetPixel( U32 x, U32 y ) const {
    return mRawImageData[x + y * mWidth];
}
*/
inline void CubeMap::Bind( U32 unit ) {
    //XOF_ASSERT( ( unit >= 0 ) && ( unit <= 31 ) )
    //glActiveTexture( GL_TEXTURE0 + unit );
    glActiveTexture( unit );
    glBindTexture( GL_TEXTURE_CUBE_MAP, mTexture );
}


#endif // CAS_CUBEMAP_HPP
