/*
===============================================================================

	XOF
	===
	File	:	XOF_Camera.cpp
	Desc	:	Base class for different camera types.

===============================================================================
*/
#include "XOF_Camera.hpp"


Camera::Camera() {}

Camera::Camera( const glm::vec3 &pos, const glm::vec3 &lookAt,	
				float screenWidth, float screenHeight, float aspectRatio, float zNear, float zFar ) {
    XOF_UNUSED_PARAMETER( lookAt );
	Setup( pos, mLookAt, screenWidth, screenHeight, aspectRatio, zNear, zFar );
}

void Camera::Setup( const glm::vec3 &pos, const glm::vec3 &lookAt,
					float screenWidth, float screenHeight,
					float aspectRatio, float zNear, float zFar ) {
	mPos		= pos;
	mLookAt		= lookAt;
	mUp			= glm::vec3( 0.f, 1.f, 0.f );
	mRight		= glm::cross( mUp, mLookAt );

	mScreenWidth  = screenWidth;
	mScreenHeight = screenHeight;

    mFov		 = mScreenWidth / mScreenHeight;
	mNearPlane	 = zNear;
	mFarPlane	 = zFar;
	mAspectRatio = aspectRatio;
}
