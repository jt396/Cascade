/*
===============================================================================

	XOF
	===
	File	:	XOF_Camera.hpp
	Desc	:	Base class for different camera types.

===============================================================================
*/
#ifndef XOF_CAMERA_HPP
#define XOF_CAMERA_HPP


#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtx/transform.hpp>

#include "XOF_Frustum.hpp"


class Camera {
public:
								Camera();
								Camera( const glm::vec3 &pos, const glm::vec3 &lookAt, 
										float screenHeight, float screenWidth, 
										float aspectRatio, float zNear, float zFar );

    virtual void                Setup( const glm::vec3 &pos, const glm::vec3 &lookAt,
									   float screenWidth, float screenHeight,
								       float aspectRatio, float zNear, float zFar );

	inline const glm::vec3&		GetPosition() const;
	inline const glm::vec3&		GetViewDirection() const;
	inline const glm::vec3&		GetRight() const;
	inline virtual glm::mat4	GetViewMatrix() const = 0;
	inline glm::mat4			GetProjectionMatrix() const;
    inline glm::mat4            GetOrthographicProjectionMatrix() const;

	inline void					Resize( float newWidth, float newHeight );

//protected:
	glm::vec3					mPos;
	glm::vec3					mLookAt;
	glm::vec3					mUp;
	glm::vec3					mRight;

								// Projection params
	float						mFov;
	float						mNearPlane;
	float						mFarPlane;
	float						mAspectRatio;

	float						mScreenWidth;
	float						mScreenHeight;

								// For frustum culling
	Frustum						mFrustum;

	//virtual void				ConstructFrustum( const Sphere& s ) = 0;
};


//using StaticCamera = Camera;


inline const glm::vec3& Camera::GetPosition() const {
	return mPos;
}

inline const glm::vec3& Camera::GetViewDirection() const {
	return mLookAt;
}

inline const glm::vec3& Camera::GetRight() const {
	return mRight;
}

//inline glm::mat4 Camera::GetViewMatrix() const {
//	return glm::lookAt( mPos, mLookAt, mUp );
//}

inline glm::mat4 Camera::GetProjectionMatrix() const {
    return glm::perspectiveFov<float>( glm::radians( 75.f ), mScreenWidth, mScreenHeight, mNearPlane, mFarPlane );
}

inline glm::mat4 Camera::GetOrthographicProjectionMatrix() const {
    return glm::ortho( 0.f, 1.f, 0.f, 1.f );
}

void Camera::Resize( float newWidth, float newHeight ) {
	mScreenWidth = newWidth;
	mScreenHeight = newHeight;
}


#endif // XOF_CAMERA_HPP
