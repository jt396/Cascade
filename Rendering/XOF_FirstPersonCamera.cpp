/*
===============================================================================

	XOF
	===
	File	:	XOF_FirstPersonCamera.cpp
	Desc	:	Basic first-person camera.

===============================================================================
*/

#include "Core/XOF_MathCommon.hpp"
#include "XOF_FirstPersonCamera.hpp"


FirstPersonCamera::FirstPersonCamera() {}

FirstPersonCamera::FirstPersonCamera( const glm::vec3 &pos, const glm::vec3 &forward,
										float screenWidth, float screenHeight,
										float aspectRatio, float zNear, float zFar ) {
    Setup( pos, forward, screenWidth, screenHeight, aspectRatio, zNear, zFar );
	//ConstructFrustum();
    mYaw = 0.f;
    mPitch = 0.f;
}
#if 1
void FirstPersonCamera::Setup( const glm::vec3 &pos, const glm::vec3 &lookAt,
       float screenWidth, float screenHeight,
       float aspectRatio, float zNear, float zFar ) {
    std::cout << "FirstPersonCamera::Setup()" << std::endl;
    Camera::Setup( pos, lookAt, screenWidth, screenHeight, aspectRatio, zNear, zFar );
    mYaw = 0.f;
    mPitch = 0.f;
}
#endif

void FirstPersonCamera::Translate( float x, float y, float z ) {
    glm::vec3 translation( x, y, z );

    translation = glm::mat3( glm::rotate( mYaw, glm::vec3( 0.f, 1.f, 0.f ) ) ) * translation;
    //translation = glm::mat3( glm::rotate( mPitch, mRight ) ) * translation;

    mPos += translation;

    //ConstructFrustum();
}

void FirstPersonCamera::Pitch( float amount ) {
    mPitch += amount;
    mLookAt = glm::mat3( glm::rotate( amount, mRight ) ) * mLookAt;
    mUp = glm::mat3( glm::rotate( amount, mRight ) ) * mUp;

    //ConstructFrustum();
}

void FirstPersonCamera::Yaw( float amount ) {
    mYaw += amount;
    mLookAt = glm::mat3( glm::rotate( amount, glm::vec3( 0.f, 1.f, 0.f ) ) ) * mLookAt;
    mRight = glm::mat3( glm::rotate( amount, glm::vec3( 0.f, 1.f, 0.f ) ) ) * mRight;

	//ConstructFrustum();
}

// Currently not working, causes camera to flip upside-down?
void FirstPersonCamera::SphericalPitchAndYaw( float pitch, float yaw ) {
	mPitch = pitch;
	mYaw = yaw;
	mLookAt = glm::vec3( cos( mPitch ) * sin( mYaw ), sin( mPitch ), cos( mPitch ) * cos( mYaw ) );
	mRight = glm::vec3( sin( mYaw - XOF_PI_OVER_TWO ), 0.f, cos( mYaw - XOF_PI_OVER_TWO ) );
	mUp = glm::cross( mLookAt, mRight );

	//ConstructFrustum();
}

#ifdef INCLUDE
bool FirstPersonCamera::TEMP_AABBFrustumIntersection( const AABB& aabb ) {
	for( U32 i=0; i<XOF_FRUSTUM_PLANES::FRUSTUM_PANE_COUNT; ++i ) {
		float r = aabb.halfExtents.x * std::abs( mFrustum.planes[i].a ) + 
			aabb.halfExtents.y * std::abs( mFrustum.planes[i].b ) + 
			aabb.halfExtents.z * std::abs( mFrustum.planes[i].c );

		float s = ( mFrustum.planes[i].a * aabb.center.x + 
			mFrustum.planes[i].b * aabb.center.y + 
			mFrustum.planes[i].c * aabb.center.z ) - mFrustum.planes[i].d;

		if( !( std::abs( s ) <= r ) ) {
			std::cout << "PLANE: " << i << std::endl;
			return false;
		}
	}

	return true;
}

bool FirstPersonCamera::TEMP_SphereFrustumIntersection( const Sphere& sphere ) {
	// Test the sphere against each of the six planes
	for( U32 i=0; i<XOF_FRUSTUM_PLANES::FRUSTUM_PANE_COUNT; ++i ) {
		float result = ( mFrustum.planes[i].a * sphere.center.x +
			mFrustum.planes[i].b * sphere.center.y + 
			mFrustum.planes[i].c * sphere.center.z ) + 
			mFrustum.planes[i].d;

		if( result < -sphere.radius ) {
			std::cout << "PLANE: " << i << std::endl;
			std::cout << "D: " << result << "R: " << -sphere.radius << std::endl;
			return false;
		}
	}

	return true;
}

void FirstPersonCamera::ConstructFrustum( const glm::mat4x4& objTransform ) {
	glm::mat4x4 clipMatrix( GetProjectionMatrix() * GetViewMatrix() * objTransform );

	mFrustum.planes[XOF_FRUSTUM_PLANES::LEFT_].a = clipMatrix[3][0] + clipMatrix[0][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::LEFT_].b = clipMatrix[3][1] + clipMatrix[0][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::LEFT_].c = clipMatrix[3][2] + clipMatrix[0][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::LEFT_].d = clipMatrix[3][3] + clipMatrix[0][3];

	mFrustum.planes[XOF_FRUSTUM_PLANES::RIGHT_].a = clipMatrix[3][0] - clipMatrix[0][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::RIGHT_].b = clipMatrix[3][1] - clipMatrix[0][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::RIGHT_].c = clipMatrix[3][2] - clipMatrix[0][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::RIGHT_].d = clipMatrix[3][3] - clipMatrix[0][3];

	mFrustum.planes[XOF_FRUSTUM_PLANES::TOP].a = clipMatrix[3][0] - clipMatrix[1][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::TOP].b = clipMatrix[3][1] - clipMatrix[1][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::TOP].c = clipMatrix[3][2] - clipMatrix[1][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::TOP].d = clipMatrix[3][3] - clipMatrix[1][3];

	mFrustum.planes[XOF_FRUSTUM_PLANES::BOTTOM].a = clipMatrix[3][0] + clipMatrix[1][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::BOTTOM].b = clipMatrix[3][1] + clipMatrix[1][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::BOTTOM].c = clipMatrix[3][2] + clipMatrix[1][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::BOTTOM].d = clipMatrix[3][3] + clipMatrix[1][3];

	mFrustum.planes[XOF_FRUSTUM_PLANES::NEAR_].a = clipMatrix[3][0] + clipMatrix[2][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::NEAR_].b = clipMatrix[3][1] + clipMatrix[2][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::NEAR_].c = clipMatrix[3][2] + clipMatrix[2][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::NEAR_].d = clipMatrix[3][3] + clipMatrix[2][3];

	mFrustum.planes[XOF_FRUSTUM_PLANES::FAR_].a = clipMatrix[3][0] - clipMatrix[2][0];
	mFrustum.planes[XOF_FRUSTUM_PLANES::FAR_].b = clipMatrix[3][1] - clipMatrix[2][1];
	mFrustum.planes[XOF_FRUSTUM_PLANES::FAR_].c = clipMatrix[3][2] - clipMatrix[2][2];
	mFrustum.planes[XOF_FRUSTUM_PLANES::FAR_].d = clipMatrix[3][3] - clipMatrix[2][3];

	NormalizeFrustumPlanes( mFrustum );
}
#endif
