/*
===============================================================================

	XOF
	===
	File	:	XOF_FirstPersonCamera.hpp
	Desc	:	Basic first-person camera.

===============================================================================
*/
#ifndef XOF_FIRST_PERSON_CAMERA_HPP
#define XOF_FIRST_PERSON_CAMERA_HPP


#include "Collision&Physics/XOF_AABB.hpp"
#include "Collision&Physics/XOF_Sphere.hpp"

#include "XOF_Camera.hpp"


class FirstPersonCamera : public Camera {
public:
								FirstPersonCamera();
								FirstPersonCamera( const glm::vec3 &pos, const glm::vec3 &target, 
												   float screenHeight, float screenWidth, 
												   float aspectRatio, float zNear, float zFar );
#if 1
    virtual void                Setup( const glm::vec3 &pos, const glm::vec3 &lookAt,
                                       float screenWidth, float screenHeight,
                                       float aspectRatio, float zNear, float zFar ) override;
#endif
								// Vector/Matrix-based
	void						Translate( float x, float y, float z );
	void						Pitch( float amount );
	void						Yaw( float amount );
								// Spherical-coordinates-based (added for mouse control)
	inline void					SphericalTranslate( const glm::vec3& translation );
	void						SphericalPitchAndYaw( float pitch, float yaw );

	inline virtual glm::mat4	GetViewMatrix() const override;

	//bool						TEMP_AABBFrustumIntersection( const AABB& aabb );
	//bool						TEMP_SphereFrustumIntersection( const Sphere& sphere );

//private:
	float						mYaw;
	float						mPitch;

	void						ConstructFrustum( const glm::mat4x4& objTransform  );
};


void FirstPersonCamera::SphericalTranslate( const glm::vec3& translation ) {
	mPos += translation;

	//ConstructFrustum();
}

inline glm::mat4 FirstPersonCamera::GetViewMatrix() const {
	return glm::lookAt( mPos, mPos + mLookAt, mUp );
}


#endif // XOF_FIRST_PERSON_CAMERA_HPP
