/*
===============================================================================

	XOF
	===
	File	:	XOF_Frustum.hpp
	Desc	:	Defines a frustum for use in culling etc...

===============================================================================
*/
#include "XOF_Frustum.hpp"


void NormalizeFrustumPlanes( Frustum& frustum )  {
	for( U32 i=0; i<XOF_FRUSTUM_PLANES::FRUSTUM_PANE_COUNT; ++i ) {
		float mag = std::sqrtf( 
			frustum.planes[i].a * frustum.planes[i].a + 
			frustum.planes[i].b * frustum.planes[i].b + 
			frustum.planes[i].c * frustum.planes[i].c );

		frustum.planes[i].a /= mag;
		frustum.planes[i].b /= mag;
		frustum.planes[i].c /= mag;
		frustum.planes[i].d /= mag;
	}
}