/*
===============================================================================

	XOF
	===
	File	:	XOF_Frustum.hpp
	Desc	:	Defines a frustum for use in culling etc...

===============================================================================
*/
#ifndef XOF_FRUSTUM_HPP
#define XOF_FRUSTUM_HPP


#include "Platform/XOF_Platform.hpp"


enum XOF_FRUSTUM_PLANES {
	LEFT_ = 0,
	RIGHT_,
	TOP,
	BOTTOM,
	NEAR_,
	FAR_,
	FRUSTUM_PANE_COUNT,
};

enum XOF_FRUSTUM_PLANE_POINT_CLASSIFICATION {
	NEGATIVE = 0,	// on the negative half-space of the plane
	POSITIVE,		// on the positive half-space of the plane
	ON_PLANE,		// n(x-p) = 0
};


struct Frustum {
	// Based on the plane equation: ax + by + cz + d = 0
	struct FrustumPlane {
		float a, b, c, d;
	};

	FrustumPlane	planes[XOF_FRUSTUM_PLANES::FRUSTUM_PANE_COUNT];

					Frustum() { memset( this, 0x00, sizeof( Frustum ) ); }
};


void NormalizeFrustumPlanes( Frustum& frustum );


#endif // XOF_FRUSTUM_HPP
