/*
===============================================================================

	XOF
	===
	File	:	XOF_GPUState.cpp
	Desc	:	Part of the material system, describes what GPU state settings
				to enfore when drawing a particular mesh/surface.

				GRAPHICS API AGNOSTIC. API SPECIFIC FLAGS ARRAY TO BE ADDED
				TO RENDERER CLASS/IMPLEMENTATION.

===============================================================================
*/
#include "XOF_GPUState.hpp"


GPUState::GPUState() { 
	Reset();
}

GPUState::GPUState( const GPUState& src ) {
	mEnabledFlags			= src.mEnabledFlags;
	mHighestEnabledFlag		= src.mHighestEnabledFlag;
	mDisabledFlags			= src.mDisabledFlags;
	mHighestDisabledFlag	= src.mHighestDisabledFlag;
}

// Other functions inlined in header