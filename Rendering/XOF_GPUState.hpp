/*
===============================================================================

	XOF
	===
	File	:	XOF_GPUState.hpp
	Desc	:	Part of the material system, describes what GPU state settings
				to enfore when drawing a particular mesh/surface.

				GRAPHICS API AGNOSTIC. API SPECIFIC FLAGS ARRAY TO BE ADDED
				TO RENDERER CLASS/IMPLEMENTATION.

===============================================================================
*/
#ifndef XOF_GPU_STATE_HPP
#define XOF_GPU_STATE_HPP


#include "Platform/XOF_Platform.hpp"


enum XOF_GPU_STATE_FLAGS {
	CW_WINDING	= ( 1 << 0 ),
	CCW_WINDING = ( 1 << 1 ),
	CULLING		= ( 1 << 2 ),
	CULL_BACK	= ( 1 << 3 ),
	CULL_FRONT	= ( 1 << 4 ),
	DEPTH_TEST	= ( 1 << 5 ),
	DEPTH_CLAMP = ( 1 << 6 ),
	// These last two need glPolygonMode
	SOLID_FILL	= ( 1 << 7 ),
	WIREFRAME	= ( 1 << 8 ),
};


// highestEnabled/Disabled flag fields are automatically updated
class GPUState {
public:
	GPUState();
	GPUState( const GPUState& src );

	inline void AddToEnabledFlags( U8 flag );
	inline void AddToDisabledFlags( U8 flag );

	inline U16	GetEnabledFlags() const;
	inline U16	GetDisabledFlags() const;
	inline U8	GetHighestEnabledFlag() const;
	inline U8	GetHighestDisabledFlag() const;

	inline void Reset();

private:
	U16 mEnabledFlags;
	U16 mDisabledFlags;
	U8	mHighestEnabledFlag;
	U8	mHighestDisabledFlag;
};


inline void GPUState::AddToEnabledFlags( U8 flag ) {
	mEnabledFlags |= flag;
	if( flag > mHighestEnabledFlag ) {
		mHighestEnabledFlag = flag;
	}
}

inline void GPUState::AddToDisabledFlags( U8 flag ) {
	mDisabledFlags |= flag;
	if( flag > mHighestDisabledFlag ) {
		mHighestDisabledFlag = flag;
	}
}

inline U16 GPUState::GetEnabledFlags() const {
	return mEnabledFlags;
}

inline U16 GPUState::GetDisabledFlags() const {
	return mDisabledFlags;
}

inline U8 GPUState::GetHighestEnabledFlag() const {
	return mHighestEnabledFlag;
}

inline U8 GPUState::GetHighestDisabledFlag() const {
	return mHighestDisabledFlag;
}

inline void GPUState::Reset() {
	mEnabledFlags = mDisabledFlags = 0;
	mHighestEnabledFlag = mHighestDisabledFlag = 0;
}
						  

using UniqueGPUStatePtr = std::unique_ptr<GPUState>;


#endif // XOF_GPU_STATE_HPP
