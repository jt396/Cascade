/*
===============================================================================

	XOF
	===
	File	:	XOF_Lights.cpp
	Desc	:	Defines different types of light.

===============================================================================
*/
#ifndef XOF_LIGHTS_HPP
#define XOF_LIGHTS_HPP


#include <glm/vec3.hpp>

#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"


XOF_REFLECTED_CLASS_BEGIN()
struct PointLight {
                XOF_REFLECTED_VARIABLE()
    glm::vec3	color;
                XOF_REFLECTED_VARIABLE()
    F32 		ambientIntensity;
                XOF_REFLECTED_VARIABLE()
    F32 		diffuseIntensity;
};
XOF_REFLECTED_CLASS_END()


struct BaseLight {
	// specular colour is often the same as diffuse colour, so
	// use the one colour vector for both
	glm::vec3	color;
    F32 		ambientIntensity;
    F32 		diffuseIntensity;
};



struct DirectionalLight : public BaseLight {
    glm::vec3	direction;
};

struct SpotLight : public PointLight {
	glm::vec3	direction;
	float		cutoff;
};

/*
 * 	struct Attenuation {
        float constant;
        float linear;
        float exponential;
    };
    Attenuation attenuation;*/

#endif // XOF_LIGHTS_HPP
