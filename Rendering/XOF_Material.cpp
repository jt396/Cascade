/*
===============================================================================

    XOF
    ===
    File	:	XOF_MaterialManager.hpp
    Desc	:	Manage the matrials used by objects in the game.

===============================================================================
*/


#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <algorithm>

#include "Core/XOF_LoggingManager.hpp"
#include "Resources/CAS_ResourceDescriptors.hpp"

#include "XOF_Material.hpp"


Material::Material() {
    // Junk values have the potential to cause crashes during operations utilising reflection.
    // Initialising these members to null is a simple way to avoid such issues.
    memset( reinterpret_cast<void*>( &mTextures[0][0] ), 0x00, sizeof( Texture* ) * 2 * 4 );
    // TODO: Remove
    mTextureCount[XOF_TEXTURE_TYPE::DIFFUSE] = 0;
    mTextureCount[XOF_TEXTURE_TYPE::BUMP] = 0;

    memset( reinterpret_cast<void*>( &mCubeMaps[0] ), 0x00, sizeof( CubeMap* ) * 4 );
    mCubeMapCount = 0;
}

Material::~Material() {}

bool Material::Load( ResourceDescriptor *desc ) {
    // TODO: Fix this...
    if( (std::strcmp( desc->fileName.CStr(), "CASCADE_ERROR_MATERIAL" ) == 0) ||
        (std::strcmp( desc->fileName.CStr(), "RESONANCE_SHADER_LIGHT_VISUAL_REPRESENTATION.mtl" ) == 0) ) {
        return true;
    }

    MaterialDescriptor *materialDesc = reinterpret_cast<MaterialDescriptor*>( desc );

    std::string textureDir( "C:/Users/Jamie/Desktop/Cascade/Resources/Textures/" );

    // This is horrible
    std::string newName( "C:/Users/Jamie/Desktop/Cascade/Resources/Meshes/" );
    newName.append( desc->fileName.SubString( 0, desc->fileName.LastOf( "." ).second ).CStr() );
    newName.append( ".obj" );

    // Load in the scene via assimp
    Assimp::Importer importer;
    const aiScene *scene = importer.ReadFile( newName.c_str(), aiProcess_FlipUVs );

    if( !scene ) {
        return false;
    }

    // Get texture-related info
    for( U32 i=0; i<scene->mNumMaterials; ++i ) {
        const aiMaterial *material = scene->mMaterials[i];

        if( material->GetTextureCount( aiTextureType_DIFFUSE ) > 0 ) {
            aiString path;
            if( material->GetTexture( aiTextureType_DIFFUSE, 0, &path ) == AI_SUCCESS ) {
                TextureDescriptor *texture = &materialDesc->textures[XOF_TEXTURE_TYPE::DIFFUSE]
                                                                     [mTextureCount[XOF_TEXTURE_TYPE::DIFFUSE]];

                texture->fileName = path.C_Str();
                texture->directory = textureDir.c_str();
                texture->type = XOF_TEXTURE_TYPE::DIFFUSE;

                mTextureCount[XOF_TEXTURE_TYPE::DIFFUSE]++;
            } else {
                XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                                 "Material::Load( %s ) -> %s failed to load!", desc->fileName.CStr(), path.C_Str() );
                return false;
            }
        }

        // Normal maps: so apparently assimp loads and stores normal maps under textureType_HEIGHT... what...
        const auto heightMapCount = material->GetTextureCount( aiTextureType_HEIGHT );
        if( heightMapCount > 0 ) {
            aiString path;
            if( material->GetTexture( aiTextureType_HEIGHT, 0, &path ) == AI_SUCCESS ) {
                TextureDescriptor *texture = &materialDesc->textures[XOF_TEXTURE_TYPE::BUMP]
                                                                     [mTextureCount[XOF_TEXTURE_TYPE::BUMP]];

                texture->fileName = path.C_Str();
                texture->directory = textureDir.c_str();
                texture->type = XOF_TEXTURE_TYPE::BUMP;

                mTextureCount[XOF_TEXTURE_TYPE::BUMP]++;
            } else {
                XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                                 "Material::Load( %s ) -> %s failed to load!", desc->fileName.CStr(), path.C_Str() );
                return false;
            }
        }
    } // for( U32 i=0; i<scene->mNumMaterials; ++i )

    return true;
}

void Material::SetTexture( XOF_TEXTURE_TYPE type, U32 index, Texture *texture ) {
    mTextures[type][index] = texture;
}

Texture* Material::GetTexture( XOF_TEXTURE_TYPE type, U32 index ) const {
    return mTextures[type][index];
}

TextureInfo Material::GetTextureInfo( U32 textureID ) const {
    TextureInfo texInfo;
    texInfo.type = XOF_TEXTURE_TYPE::INVALID;
    texInfo.index = -1;
    texInfo.ptr = nullptr;

    for( U32 i=0; i<XOF_TEXTURE_TYPE::TEXTURE_TYPE_COUNT; ++i ) {
        for( U32 j=0; j<mTextureCount[i]; ++j ) {
            if( mTextures[i][j]->GetUID() == textureID ) {
                texInfo.type = mTextures[i][j]->GetTextureType();
                texInfo.index = j;
                texInfo.ptr = mTextures[i][j];
                break;
            }
        }
    }

    return texInfo;
}
#if defined( EDITOR_OR_DEBUG )
TextureInfo Material::GetTextureInfo( const std::string& textureStringID ) const {
    // Could just call the above version with the CRC32 of the passed in string ID.
    // Would need to get all string handling to use the same approach so the correct/same
    // hash is always produced.
    TextureInfo texInfo;
    texInfo.type = XOF_TEXTURE_TYPE::INVALID;
    texInfo.index = -1;
    texInfo.ptr = nullptr;

    for( U32 i=0; i<XOF_TEXTURE_TYPE::TEXTURE_TYPE_COUNT; ++i ) {
        for( U32 j=0; j<mTextureCount[i]; ++j ) {
            if( strcmp( mTextures[i][j]->GetName(), textureStringID.c_str() ) == 0 ) {
                texInfo.type = mTextures[i][j]->GetTextureType();
                texInfo.index = j;
                texInfo.ptr = mTextures[i][j];
                break;
            }
        }
    }

    return texInfo;
}
#endif

U8 Material::GetTextureCount( XOF_TEXTURE_TYPE type ) const {
    return mTextureCount[type];
}

void Material::SetCubemap( U32 index, CubeMap *cubemap ) {
    mCubeMaps[index] = cubemap;
}

CubeMap* Material::GetCubeMap( U32 index ) const {
    return mCubeMaps[index];
}

void Material::SetShader( Shader *shader ) {
    mShader = shader;
}

Shader* Material::GetShader() {
    return mShader;
}

GPUState* Material::GetGPUState() const {
    return mGPUState;
}

float Material::GetSpecularExponent() const {
    return mSpecularExponent;
}

float Material::GetSpecularColour() const {
    return mSpecularColour;
}
