/*
===============================================================================

    XOF
    ===
    File	:	XOF_Material.hpp
    Desc	:	Describe visual properties for some mesh.

===============================================================================
*/
#ifndef XOF_MATERIAL_HPP
#define XOF_MATERIAL_HPP


#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"

#include "XOF_Texture.hpp"
#include "XOF_Shader.hpp"
#include "CAS_CubeMap.hpp"
#include "XOF_GPUState.hpp"


// TODO: Move this inside material class once reflection system can handle nested structs/classes.
struct TextureInfo {
    XOF_TEXTURE_TYPE    type;
    size_t              index;
    const Texture       *ptr;
};


XOF_REFLECTED_CLASS_BEGIN()
class Material : public Resource {
public:
    friend class		ResourceManager;

                        Material();
                        ~Material();

    virtual bool        Load( ResourceDescriptor *desc ) override;

    void    			SetTexture( XOF_TEXTURE_TYPE type, U32 index, Texture *texture );
    Texture*			GetTexture( XOF_TEXTURE_TYPE type, U32 index ) const;

    TextureInfo         GetTextureInfo( U32 textureID ) const;
#if defined( EDITOR_OR_DEBUG )
    TextureInfo         GetTextureInfo( const std::string& textureStringID ) const;
#endif

    U8					GetTextureCount( XOF_TEXTURE_TYPE type ) const;

    void    			SetCubemap( U32 index, CubeMap *cubemap );
    CubeMap*			GetCubeMap( U32 index ) const;

    void                SetShader( Shader *shader );
    Shader*				GetShader();

    GPUState*			GetGPUState() const;

    float				GetSpecularExponent() const;
    float				GetSpecularColour() const;

    virtual CHAR*       GetName() const override { return (CHAR*)mName.c_str(); }

private:
#if defined( EDITOR_OR_DEBUG )
    std::string			mName;
#endif

                        XOF_REFLECTED_VARIABLE()
    Texture*			mTextures[2][4];

                        XOF_REFLECTED_VARIABLE()
    Shader*				mShader {nullptr};

    GPUState*			mGPUState {nullptr};

    U8					mTextureCount[2];
    float				mSpecularExponent;
    float				mSpecularColour;

    CubeMap*            mCubeMaps[4];
    U8                  mCubeMapCount;
};
XOF_REFLECTED_CLASS_END()


#endif // XOF_MATERIAL_HPP
