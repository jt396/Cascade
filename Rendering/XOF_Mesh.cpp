/*
===============================================================================

	XOF
	===
	File	:	XOF_Mesh.cpp
	Desc	:	Represents a mesh, be it from a file or manually created.

===============================================================================
*/

#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <algorithm>

#include "Resources/XOF_ResourceManager.hpp"
#include "Core/XOF_LoggingManager.hpp"

#include "XOF_Mesh.hpp"


Mesh::Mesh() {}
Mesh::~Mesh() {}

bool Mesh::InitFromScene( const aiScene *scene, ResourceDescriptor *fileDesc ) {
	mSubMeshes.resize( scene->mNumMeshes );

	std::vector<glm::vec3> normals;
    std::vector<glm::vec3> tangents;
	std::vector<glm::vec2> texCoords;

	U32 vertexCount = 0;
	U32 indexCount = 0;

	for( U32 i=0; i<scene->mNumMeshes; ++i ) {
		mSubMeshes[i].baseVertex = vertexCount;
		mSubMeshes[i].baseIndex = indexCount;
		mSubMeshes[i].indexCount = scene->mMeshes[i]->mNumFaces * 3;

        size_t subMeshNameLength = scene->mRootNode->mChildren[i]->mName.length;
        memcpy( mSubMeshes[i].name, scene->mRootNode->mChildren[i]->mName.C_Str(),
                ( subMeshNameLength > 32 ) ? 32 : subMeshNameLength );

        mSubMeshes[i].materialIndex = scene->mMeshes[i]->mMaterialIndex - 1;
#if 0
		// Set material index
		switch( fileDesc->meshType ) {
			case XOF_MESH_TYPE::OBJ: mSubMeshes[i].materialIndex = 
										scene->mMeshes[i]->mMaterialIndex - 1; 
										break;

			case XOF_MESH_TYPE::MD5: mSubMeshes[i].materialIndex = 
										scene->mMeshes[i]->mMaterialIndex > 0 ? 
										scene->mMeshes[i]->mMaterialIndex : 0; 
										break;
		}
#endif
		vertexCount += scene->mMeshes[i]->mNumVertices;
		indexCount += mSubMeshes[i].indexCount;
	}

    mVertexData.reserve( vertexCount );
    mIndexData.reserve( indexCount );

	normals.reserve( vertexCount );
    tangents.reserve( vertexCount );
	texCoords.reserve( vertexCount );

	// Init the submeshes
    U32 childNodeCount = scene->mRootNode->mNumChildren;
    for( U32 i=0; i<childNodeCount; ++i) {
        aiNode *childNode = scene->mRootNode->mChildren[i];
        // TODO: May need to account for the other meshes 'belonging' to this node?
        InitSubMesh( scene->mMeshes[childNode->mMeshes[0]], normals, tangents, texCoords );
	}

	// Generate and populate VBOs for the VAO
	glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::POSITION] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( mVertexData[0] ) * mVertexData.size(), &mVertexData[0], GL_STATIC_DRAW );
	glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::POSITION );
	glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0 );

    if( normals.size() > 0 ) {
        glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::NORMAL] );
        glBufferData( GL_ARRAY_BUFFER, sizeof( normals[0] ) * normals.size(), &normals[0], GL_STATIC_DRAW );
        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::NORMAL );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    } else {
        glDisableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::NORMAL );
    }

    if( tangents.size() > 0 ) {
        glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::TANGENT] );
        glBufferData( GL_ARRAY_BUFFER, sizeof( tangents[0] ) * tangents.size(), &tangents[0], GL_STATIC_DRAW );
        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TANGENT );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::TANGENT, 3, GL_FLOAT, GL_FALSE, 0, 0 );
    } else {
        glDisableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TANGENT );
    }

    if( texCoords.size() > 0 ) {
        glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::TEX_COORDS] );
        glBufferData( GL_ARRAY_BUFFER, sizeof( texCoords[0] ) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW );
        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TEX_COORDS );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::TEX_COORDS, 2, GL_FLOAT, GL_FALSE, 0, 0 );
    } else {
        glDisableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TEX_COORDS );
    }

	glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::INDEX] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( mIndexData[0] ) * mIndexData.size(), &mIndexData[0], GL_STATIC_DRAW );

    const MeshDescriptor *meshDesc = reinterpret_cast<MeshDescriptor*>( fileDesc );
    if( !meshDesc->retainVertexData ) {
        mVertexData.clear();
    }
    if( !meshDesc->retainIndexData ) {
        mIndexData.clear();
    }

    const GLuint errorCode = glGetError();
    if( errorCode != GL_NO_ERROR ) {
        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                         "Mesh::InitFromScene(%s) -> generted GL_ERROR: %u", fileDesc->fileName.CStr(), errorCode );
    }
	return errorCode == GL_NO_ERROR;
}

void Mesh::InitSubMesh( const aiMesh *mesh,
                        std::vector<glm::vec3>& normals,
                        std::vector<glm::vec3>& tangents,
                        std::vector<glm::vec2>& texCoords) {

	const aiVector3D zero3D( 0.f, 0.f, 0.f );

	// Populate vertex attribute vectors
    for( U32 i=0; i<mesh->mNumVertices; ++i ) {
        const aiVector3D *texCoordinates = mesh->HasTextureCoords( 0 ) ?
            &mesh->mTextureCoords[0][i] : &zero3D;

		// Get min and max extents for dimensions and bounding box creation
        if( mesh->mVertices[i].x > mDimensions.max.x ) {
            mDimensions.max.x = mesh->mVertices[i].x;
		}
        if( mesh->mVertices[i].y > mDimensions.max.y ) {
            mDimensions.max.y = mesh->mVertices[i].y;
		}
        if( mesh->mVertices[i].z > mDimensions.max.z ) {
            mDimensions.max.z = mesh->mVertices[i].z;
		}
        if( mesh->mVertices[i].x < mDimensions.min.x ) {
            mDimensions.min.x = mesh->mVertices[i].x;
		}
        if( mesh->mVertices[i].y < mDimensions.min.y ) {
            mDimensions.min.y = mesh->mVertices[i].y;
		}
        if( mesh->mVertices[i].z < mDimensions.min.z ) {
            mDimensions.min.z = mesh->mVertices[i].z;
		}
		
        mVertexData.push_back( glm::vec3( mesh->mVertices[i].x, mesh->mVertices[i].y, mesh->mVertices[i].z ) );

        if( mesh->mNormals ) {
            normals.push_back( glm::vec3( mesh->mNormals[i].x, mesh->mNormals[i].y, mesh->mNormals[i].z ) );
        }
        if( mesh->mTextureCoords ) {
            texCoords.push_back( glm::vec2( texCoordinates->x, texCoordinates->y ) );
        }
        if( mesh->mTangents ) {
            tangents.push_back( glm::vec3( mesh->mTangents[i].x, mesh->mTangents[i].y, mesh->mTangents[i].z ) );
        }
	}

	// These dimensions will change for each submesh that makes up the entire mesh, so check before potentially updating
	if( ( mDimensions.max.x - mDimensions.min.x ) > mDimensions.sizeAlongX ) {
		mDimensions.sizeAlongX = mDimensions.max.x - mDimensions.min.x;
	}
	if( ( mDimensions.max.y - mDimensions.min.y ) > mDimensions.sizeAlongY ) {
		mDimensions.sizeAlongY = mDimensions.max.y - mDimensions.min.y;
	}
	if( ( mDimensions.max.z - mDimensions.min.z ) > mDimensions.sizeAlongZ ) {
		mDimensions.sizeAlongZ = mDimensions.max.z - mDimensions.min.z;
	}

	// Populate index buffer
    for( U32 i=0; i<mesh->mNumFaces; ++i ) {
		//XOF_ASSERT( mesh->mFaces[i].mNumIndices == 3 );
        mIndexData.push_back( mesh->mFaces[i].mIndices[0] );
        mIndexData.push_back( mesh->mFaces[i].mIndices[1] );
        mIndexData.push_back( mesh->mFaces[i].mIndices[2] );
	}
}

Mesh::SubMesh::SubMesh() {
	indexCount = -1;
	baseVertex = -1;
	baseIndex  = -1;
	materialIndex = -1;
    memset( name, 0x00, 32 );
}

Mesh::SubMesh::~SubMesh() {}

bool Mesh::Load( ResourceDescriptor *desc ) {
    MeshDescriptor *meshDesc = reinterpret_cast<MeshDescriptor*>( desc );

	// Create VAO
	glGenVertexArrays( 1, &mVAO );
	glBindVertexArray( mVAO );

	// Create the buffers for the vertex attributes
	glGenBuffers( XOF_VERTEX_ATTRIBUTES::VERTEX_ATTRIBUTES_COUNT, mBuffers );

    if( meshDesc->loadFromAssimpFile ) {
        std::string newName( meshDesc->directory.CStr() );
        newName = newName.append( meshDesc->fileName.CStr() );

        Assimp::Importer importer;
        const U32 flags = aiProcess_Triangulate | aiProcess_GenSmoothNormals |
                          aiProcess_FlipUVs | aiProcess_FlipWindingOrder |
                          aiProcess_CalcTangentSpace;

        const aiScene *scene = importer.ReadFile( newName.c_str(), flags  );
        mIsLoaded = scene? InitFromScene( scene, meshDesc ) : false;
    } else {
        // Generate and populate VBOs for the VAO
        glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::POSITION] );
        glBufferData( GL_ARRAY_BUFFER, sizeof( mVertexData[0] ) * mVertexData.size(), &mVertexData[0], GL_STATIC_DRAW );
        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::POSITION );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        XOF_ASSERT( glGetError() == GL_NO_ERROR );

        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::NORMAL );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0 );
        XOF_ASSERT( glGetError() == GL_NO_ERROR );

        glDisableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TANGENT );
        XOF_ASSERT( glGetError() == GL_NO_ERROR );

        glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TEX_COORDS );
        glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::TEX_COORDS, 2, GL_FLOAT, GL_FALSE, 0, 0 );
        XOF_ASSERT( glGetError() == GL_NO_ERROR );

        GLint error = glGetError();
        XOF_ASSERT( error == GL_NO_ERROR );

        mIsLoaded = true;
    }

	glBindVertexArray( 0 );

    memset( meshName, 0x00, 64 );
    memcpy( meshName, meshDesc->fileName.CStr(), meshDesc->fileName.GetLength() );
    mUID = CalculateCRC32( 0, meshDesc->fileName.CStr(), meshDesc->fileName.GetLength() );

    return mIsLoaded;
}
