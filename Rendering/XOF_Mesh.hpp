/*
===============================================================================

	XOF
	===
	File	:	XOF_Mesh.hpp
	Desc	:	Represents a mesh, be it from a file or manually created.

===============================================================================
*/
#ifndef XOF_MESH_HPP
#define XOF_MESH_HPP


#include <GL/glew.h>
#include <vector>

#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"
#include "XOF_Vertex.hpp"
#include "XOF_Material.hpp"


struct aiScene;
struct aiMesh;


XOF_REFLECTED_CLASS_BEGIN()
class Mesh : public Resource {
public:
	friend class ResourceManager;

#if defined( EDITOR_OR_DEBUG )
                                                XOF_REFLECTED_VARIABLE()
    MSHRSRC                                     meshName[64];
#endif
    U32                                         mUID;

    bool                                        mIsLoaded;

    // A whole mesh is built up as a collection of submeshes
    struct SubMesh {
                    SubMesh();
                    ~SubMesh();

        U32         baseVertex;
        U32         baseIndex;
        U32         indexCount;
        U32         materialIndex;
        CHAR        name[32];
    };

    struct Dimensions {
        float       sizeAlongX;
        float       sizeAlongY;
        float       sizeAlongZ;
        glm::vec3   min;
        glm::vec3   max;
	};

                                                Mesh();
                                                ~Mesh();

                                                // Not const since loading from an assimp file will result
                                                // in populating info about the material
    virtual bool                                Load( ResourceDescriptor *desc ) override;

                                                // TEMP? It's GL specific
    inline GLuint                               GetVAO() const;

    inline const std::vector<Mesh::SubMesh>&    GetSubMeshData() const;
    inline U32                                  GetSubMeshCount() const;

    inline const Mesh::Dimensions&              GetDimensions() const;

    inline const std::vector<glm::vec3>&        GetRetainedVertexData() const;
    inline const std::vector<U32>&              GetRetainedIndexData() const;
    inline void                                 FreeRetainedData();

    virtual CHAR*                               GetName() const override { return (CHAR*)meshName; }

//private:
                                                // Optionally retainable data
    std::vector<glm::vec3>                      mVertexData;
    std::vector<U32>                            mIndexData;

    Dimensions                                  mDimensions;

    std::vector<Mesh::SubMesh>                  mSubMeshes;

    GLuint                                      mVAO;
    GLuint                                      mBuffers[XOF_VERTEX_ATTRIBUTES::VERTEX_ATTRIBUTES_COUNT];

    bool                                        InitFromScene( const aiScene *scene, ResourceDescriptor *fileDesc );
    void                       					InitSubMesh( const aiMesh *mesh, std::vector<glm::vec3>& normals, std::vector<glm::vec3>& tangents, std::vector<glm::vec2>& texCoords );

    bool                                        InitMaterials( const aiScene *scene, ResourceDescriptor *fileDesc );
};
XOF_REFLECTED_CLASS_END()


inline GLuint Mesh::GetVAO() const {
    return mVAO;
}

inline const std::vector<Mesh::SubMesh>& Mesh::GetSubMeshData() const {
    return mSubMeshes;
}

inline U32 Mesh::GetSubMeshCount() const {
    return mSubMeshes.size();
}

inline const Mesh::Dimensions& Mesh::GetDimensions() const {
    return mDimensions;
}

inline const std::vector<glm::vec3>& Mesh::GetRetainedVertexData() const {
    return mVertexData;
}

inline const std::vector<U32>& Mesh::GetRetainedIndexData() const {
    return mIndexData;
}

inline void Mesh::FreeRetainedData() {
    mVertexData.clear();
    mIndexData.clear();
}


#endif // XOF_MESH_HPP
