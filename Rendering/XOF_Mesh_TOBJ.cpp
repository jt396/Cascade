/*
===============================================================================

	XOF
	===
	File	:	XOF_Mesh.cpp
	Desc	:	Represents a mesh, be it from a file or manually created.

===============================================================================
*/
#include "XOF_Mesh.hpp"
#include "../Resources/XOF_ResourceManager.hpp"

#define TINYOBJLOADER_IMPLEMENTATION
#include <tiny_obj_loader.h>
#include <unordered_map>


Mesh::Mesh() {}
Mesh::~Mesh() {}

Mesh::SubMesh::SubMesh() {
	indexCount = -1;
	baseVertex = -1;
	baseIndex  = -1;
	materialIndex = -1;
}

Mesh::SubMesh::~SubMesh() {}

bool Mesh::Load( MeshDescNEW *desc ) {
    bool isLoaded = false;

	// Create VAO
	glGenVertexArrays( 1, &mVAO );
	glBindVertexArray( mVAO );

	// Create the buffers for the vertex attributes
	glGenBuffers( XOF_VERTEX_ATTRIBUTES::VERTEX_ATTRIBUTES_COUNT, mBuffers );

    // ----------------------------------------------------------------------------------------------------------------------
    tinyobj::attrib_t attributes;
    std::vector<tinyobj::shape_t> shapes;
    std::vector<tinyobj::material_t> materials;

    std::string error;
    if ( !tinyobj::LoadObj( &attributes, &shapes, &materials, &error, desc->fileName, "C:/Users/Jamie/Desktop/XOF/Resources/" ) ) {
        std::cerr << "MESH FAILED TO LOAD: " << error << std::endl;
        return isLoaded;
    }

    std::unordered_map<Vertex, unsigned int> uniqueVertices;

    std::vector<U32> indices; indices.resize( 1 );
    std::vector<Vertex> vertices; vertices.resize( 1 );

    mSubMeshes.resize( shapes.size() );

    std::vector<glm::vec3> tempPositions;
    std::vector<glm::vec3> tempNormals;
    std::vector<glm::vec2> tempTexCoords;

    U32 i = 0;
    U32 indexCount = 0;
    for( const auto& shape : shapes ) {
        indexCount = indices.size() - 1;
        for( const auto& index : shape.mesh.indices ) {
            Vertex v;

            v.pos = {
                attributes.vertices[3 * index.vertex_index + 0],
                attributes.vertices[3 * index.vertex_index + 1],
                attributes.vertices[3 * index.vertex_index + 2]
            };
            tempPositions.push_back( v.pos );

            if( attributes.normals.size() > 0 ) {
                v.normal = {
                    attributes.normals[3 * index.normal_index + 0],
                    attributes.normals[3 * index.normal_index + 1],
                    attributes.normals[3 * index.normal_index + 2]
                };
                tempNormals.push_back( v.normal );
            } else {
                tempNormals.push_back( glm::vec3( 0.f ) );
            }

            if( attributes.texcoords.size() > 0 ) {
                v.texCoords = {
                    attributes.texcoords[2 * index.texcoord_index + 0],
                    attributes.texcoords[2 * index.texcoord_index + 1]
                };
                tempTexCoords.push_back( v.texCoords );
            } else {
                tempTexCoords.push_back( glm::vec2( 0.f ) );
            }

            if( uniqueVertices.count( v ) == 0 ) {
                uniqueVertices[v] = mVertexData.size();
                mVertexData.push_back( v );
            }

            indices.push_back( uniqueVertices[v] );
        }
        // Configure corresponding submesh
        mSubMeshes[i].baseVertex = mVertexData.size();
        mSubMeshes[i].baseIndex = indexCount;
        mSubMeshes[i].indexCount = uniqueVertices.size() - indexCount;
        mSubMeshes[i].materialIndex = shape.mesh.material_ids[0]; //?
    }

    // load in textures for the material
    for( auto& material : materials ) {
        if (!(material.diffuse_texname.empty())) {
            U32 diffuseCount = desc->material.textureCount[XOF_TEXTURE_TYPE::DIFFUSE];

            memcpy( desc->material.textures[XOF_TEXTURE_TYPE::DIFFUSE][diffuseCount].fileNameAsArray,
                    material.diffuse_texname.c_str(), material.diffuse_texname.length() );

            desc->material.textureCount[XOF_TEXTURE_TYPE::DIFFUSE]++;
        }
        if (!(material.normal_texname.empty())) {
            U32 bumpCount = desc->material.textureCount[XOF_TEXTURE_TYPE::BUMP];

            memcpy( desc->material.textures[XOF_TEXTURE_TYPE::BUMP][bumpCount].fileNameAsArray,
                    material.normal_texname.c_str(), material.normal_texname.length() );

            desc->material.textureCount[XOF_TEXTURE_TYPE::BUMP]++;
        }
    }
    //desc->material.textures

    int stop = 1;
    // ----------------------------------------------------------------------------------------------------------------------
#if 1
    // Generate and populate VBOs for the VAO
    glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::POSITION] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( tempPositions[0] ) * tempPositions.size(), &tempPositions[0], GL_STATIC_DRAW );
    glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::POSITION );
    glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::POSITION, 3, GL_FLOAT, GL_FALSE, 0, 0 );

    glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::NORMAL] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( tempNormals[0] ) * tempNormals.size(), &tempNormals[0], GL_STATIC_DRAW );
    glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::NORMAL );
    glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0 );

    glBindBuffer( GL_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::TEX_COORDS] );
    glBufferData( GL_ARRAY_BUFFER, sizeof( tempTexCoords[0] ) * tempTexCoords.size(), &tempTexCoords[0], GL_STATIC_DRAW );
    glEnableVertexAttribArray( XOF_VERTEX_ATTRIBUTES::TEX_COORDS );
    glVertexAttribPointer( XOF_VERTEX_ATTRIBUTES::TEX_COORDS, 2, GL_FLOAT, GL_FALSE, 0, 0 );

    glBindBuffer( GL_ELEMENT_ARRAY_BUFFER, mBuffers[XOF_VERTEX_ATTRIBUTES::INDEX] );
    glBufferData( GL_ELEMENT_ARRAY_BUFFER, sizeof( indices[0] ) * indices.size(), &indices[0], GL_STATIC_DRAW );

    U32 errorCode = glGetError();
    return errorCode == GL_NO_ERROR;
#endif
	glBindVertexArray( 0 );

    return ( isLoaded = true );
}
