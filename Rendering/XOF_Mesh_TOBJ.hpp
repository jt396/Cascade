/*
===============================================================================

	XOF
	===
	File	:	XOF_Mesh.hpp
	Desc	:	Represents a mesh, be it from a file or manually created.

===============================================================================
*/
#ifndef XOF_MESH_HPP
#define XOF_MESH_HPP


#include "../Platform/XOF_Platform.hpp"
#include "XOF_Vertex.hpp"
#include <GL/glew.h>
#include <vector>


struct MeshDesc;
struct MeshDescNEW;
struct aiScene;
struct aiMesh;


enum XOF_MESH_TYPE {
	OBJ,
	MD5,
	MESH_TYPE_COUNT,
};


class Mesh {
private:
	struct SubMesh;
	
public:
	friend class ResourceManager;

	struct MeshDimensions {
        float       sizeAlongX;
        float       sizeAlongY;
        float       sizeAlongZ;
        glm::vec3   min;
        glm::vec3   max;
	};

                                                Mesh();
                                                ~Mesh();

                                                // TEMP? It's GL specific
    inline GLuint                               GetVAO() const;

    inline const std::vector<Mesh::SubMesh>&    GetSubMeshData() const;
    inline U32                                  GetSubMeshCount() const;

    inline const Mesh::MeshDimensions&          GetDimensions() const;

    inline const std::vector<Vertex>&           GetRetainedData() const;
    inline void                                 FreeRetainedData();

private:
    // A whole mesh is built up as a collection of submeshes
    struct SubMesh {
                SubMesh();
                ~SubMesh();

        U32		baseVertex;
        U32		baseIndex;
        U32		indexCount;
        U32		materialIndex;
    };


    std::vector<Vertex>             mVertexData;

	MeshDimensions					mDimensions;

    std::vector<Mesh::SubMesh>      mSubMeshes;

    GLuint                          mVAO;
    GLuint                          mBuffers[XOF_VERTEX_ATTRIBUTES::VERTEX_ATTRIBUTES_COUNT];

                                    // Not const since loading will result in populating info about the material
    bool                            Load( MeshDescNEW *desc );

    void        					ConfigureSubMesh( U32 meshIndex, std::vector<glm::vec3>& positions, std::vector<glm::vec3>& normals,
                                                      std::vector<glm::vec2>& texCoords, std::vector<U32>& indices );

};


inline GLuint Mesh::GetVAO() const {
    return mVAO;
}

inline const std::vector<Mesh::SubMesh>& Mesh::GetSubMeshData() const {
    return mSubMeshes;
}

inline U32 Mesh::GetSubMeshCount() const {
    return mSubMeshes.size();
}

inline const Mesh::MeshDimensions& Mesh::GetDimensions() const {
    return mDimensions;
}

inline const std::vector<Vertex>& Mesh::GetRetainedData() const {
    return mVertexData;
}

inline void Mesh::FreeRetainedData() {
    mVertexData.clear();
}


#endif // XOF_MESH_HPP
