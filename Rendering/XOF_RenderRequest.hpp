/*
===============================================================================

	XOF
	===
	File	:	XOF_RenderRequest.hpp
	Desc	:	Allows game objects to make requests of the renderer,
				used to implement basic batched upating. Objects submit
				requests then the game loop tells the renderer to render
				a frame, wherein it processes requests, calling the appropriate
				draw functions.

				GRAPHICS API AGNOSTIC. API SPECIFIC FLAGS ARRAY TO BE ADDED
				TO RENDERER CLASS/IMPLEMENTATION.

===============================================================================
*/
#ifndef XOF_RENDER_REQUEST_HPP
#define XOF_RENDER_REQUEST_HPP


#include "Platform/XOF_Platform.hpp"
#include "XOF_Camera.hpp"
#include "XOF_Mesh.hpp"
#include "XOF_Transform.hpp"
#include "XOF_Material.hpp"


enum XOF_RENDER_REQUEST_TYPE {
	RENDER_MESH = 0,
	RENDER_SPRITE,
	INVALID_RENDERING_REQUEST,
	// RENDER STRING etc...
};


// Material pointer non-const since we will be setting shader variables
struct RenderRequest {
	XOF_RENDER_REQUEST_TYPE			type;
	
	Material						*material;

	struct ShaderParamArg {
		XOF_SHADER_PARAM_TYPE		type;

        CHAR						name[64];

        union {
            F32                     valueAsFloat;
            F32                     valueAsVec3f[3];
            F32                     valueAsVec4f[4];
            F32                     valueAsMatrix4x4f[16];
            bool                    valueAsBool;
            U32                     valueAsTexture;
        };
                                    ShaderParamArg() {
                                        memset( this, 0x00, sizeof( ShaderParamArg ) );
										type = XOF_SHADER_PARAM_TYPE::UNUSED_SHADER_PARAM; 
									}
	};
	ShaderParamArg					shaderArgs[XOF_MAX_PARAMS_PER_SHADER];

    union {
        const Mesh          		*mesh;
        //const Sprite              *sprite;
        //const BitmapFontString    *string;
    };

	const Transform					*transform;
	const Camera					*camera;

    // TEMP
    GLint                          depthTest{GL_LESS};
    GLint                          cullingMode{GL_BACK};

// ---

	// Default ctor, for use in request container only
	RenderRequest() {
		memset( this, 0x00, sizeof( RenderRequest ) );
		type = XOF_RENDER_REQUEST_TYPE::INVALID_RENDERING_REQUEST;
	}

	// When SHADER_PARAM system is implemented we can make these pointers const;
	RenderRequest( XOF_RENDER_REQUEST_TYPE type_, Material *material_, const Mesh *mesh_, 
		const Transform *transform_, const Camera *camera_ ) {
		XOF_ASSERT( type_ >= 0 && type_ < XOF_RENDER_REQUEST_TYPE::INVALID_RENDERING_REQUEST );

        //memset( this, 0x00, sizeof( RenderRequest ) );

		type = type_;
		material = material_;

		switch( type ) {
			case XOF_RENDER_REQUEST_TYPE::RENDER_MESH: mesh = mesh_; break;
			// ...
		}

		transform = transform_;
		camera = camera_;
	}

	// Steal the data from rhs
	RenderRequest( RenderRequest&& rhs ) {
		XOF_ASSERT( rhs.type >= 0 && rhs.type < XOF_RENDER_REQUEST_TYPE::INVALID_RENDERING_REQUEST )
		
		type = rhs.type;
		material = rhs.material;
		
		memcpy( shaderArgs, rhs.shaderArgs, sizeof( ShaderParamArg ) * XOF_MAX_PARAMS_PER_SHADER );

		switch( type ) {
			case XOF_RENDER_REQUEST_TYPE::RENDER_MESH: mesh = rhs.mesh; break;
			// ...
		}

		transform = rhs.transform;
		camera = rhs.camera;

        depthTest = rhs.depthTest;
        cullingMode = rhs.cullingMode;

		memset( &rhs, 0x00, sizeof( RenderRequest ) );
		rhs.type = XOF_RENDER_REQUEST_TYPE::INVALID_RENDERING_REQUEST;
	}
};


#endif // XOF_RENDER_REQUEST_HPP
