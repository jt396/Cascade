/*
===============================================================================

	XOF0
	===
	File	:	XOF_GraphicsDevice_OpenGL.cpp
	Desc	:	OpenGL implementation of the GraphicsDevice interface.

				glEnable( GL_FRAMEBUFFER_SRGB ); undefined error/issues
				were resolved by getting glew (use this in future, never
				rely on Microsoft's implementation - ever.)

===============================================================================
*/

#include <GL/glew.h>
#include <SFML/OpenGL.hpp>
#include <GLM/gtc/type_ptr.hpp>

#include "Core/XOF_LoggingManager.hpp"
#include "XOF_Vertex.hpp"

#include "XOF_Renderer.hpp"


// Must correspond to those in XOF_GPUState.hpp
static U32 XOF_GL_GPU_STATE_FLAGS[] = {
	GL_CW,
	GL_CCW,
	GL_CULL_FACE,
	GL_BACK,
	GL_FRONT,
	GL_DEPTH_TEST,
	GL_DEPTH_CLAMP,
	// etc..?
};


bool Renderer::StartUp() {
	GLenum status = glewInit();
	if( status != GLEW_OK ) {
        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::rendering, "Renderer::Startup() -> Failed!" );
		return false;
	}

    glClearColor( 0.f, 0.f, 0.f, 1.f );

    glFrontFace( GL_CW );
    glCullFace( GL_BACK );
    glEnable( GL_CULL_FACE );

    glEnable( GL_DEPTH_TEST );
    glEnable( GL_DEPTH_CLAMP );
    glDepthFunc( GL_LESS );

	// Transparency
    //glEnable( GL_BLEND );
    //glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA );

    XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::rendering, "Renderer::Startup() -> Succeeded!" );

	return true;
}

void Renderer::ShutDown() {
	for( U32 i=0; i<XOF_VERTEX_ATTRIBUTES::VERTEX_ATTRIBUTES_COUNT; ++i ) {
		glDisableVertexAttribArray( i );
	}
}

void Renderer::SubmitRenderRequest( RenderRequest& request ) {
	mRenderRequests.push_back( std::move( request ) );
}

void Renderer::RenderFrame() {
	for( RenderRequest& req : mRenderRequests ) {
		//SetGPUState( req.material->GetGPUState() );
		req.material->GetShader()->Bind();

		U32 i = 0;
        Shader *shader = req.material->GetShader();
		while( req.shaderArgs[i].type != XOF_SHADER_PARAM_TYPE::UNUSED_SHADER_PARAM ) {
            glm::vec3 ffs0 = glm::make_vec3( req.shaderArgs[i].valueAsVec3f );
            glm::vec4 ffs1 = glm::make_vec4( req.shaderArgs[i].valueAsVec4f );
            glm::mat4 ffs2 = glm::make_mat4( req.shaderArgs[i].valueAsMatrix4x4f );

            if( shader->HasUniform( req.shaderArgs[i].name ) ) {
                switch( req.shaderArgs[i].type ) {
                    case XOF_SHADER_PARAM_TYPE::FLOAT_:
                        shader->SetUniform_float( req.shaderArgs[i].name, req.shaderArgs[i].valueAsFloat );
                        break;
                    case XOF_SHADER_PARAM_TYPE::VEC3F:
                        shader->SetUniform_vec3( req.shaderArgs[i].name, ffs0 );
                        break;
                    case XOF_SHADER_PARAM_TYPE::VEC4F:
                        shader->SetUniform_vec4( req.shaderArgs[i].name, ffs1 );
                        break;
                    case XOF_SHADER_PARAM_TYPE::MAT4x4F:
                        shader->SetUniform_mat4x4( req.shaderArgs[i].name, ffs2 );
                        break;
                    case XOF_SHADER_PARAM_TYPE::BOOL_:
                        shader->SetUniform_bool( req.shaderArgs[i].name, req.shaderArgs[i].valueAsBool );
                        break;
                }
            }
			++i;
		}

		switch( req.type ) {
            case XOF_RENDER_REQUEST_TYPE::RENDER_MESH: {
                // TEMP ---
                GLint oldDepthTest;
                glGetIntegerv( GL_DEPTH_FUNC, &oldDepthTest );
                glDepthFunc( req.depthTest );

                GLint oldCullMode;
                glGetIntegerv( GL_CULL_FACE_MODE, &oldCullMode );
                glCullFace( req.cullingMode );
                // TEMP ---

                DrawMesh( req.mesh, req.material );

                // TEMP ---
                glDepthFunc( oldDepthTest );
                glCullFace( oldCullMode );
                // TEMP ---

                break;
            }
			case XOF_RENDER_REQUEST_TYPE::RENDER_SPRITE: /* ... */ break;
		}

		// Don't process this request again on the next RenderFrame() call
		// unless it's actually a valid request.
        req.material->GetShader()->Unbind();
		memset( &req, 0x00, sizeof( RenderRequest ) );
		req.type = XOF_RENDER_REQUEST_TYPE::INVALID_RENDERING_REQUEST;
	}
	mRenderRequests.clear();
}

void Renderer::ClearScreen() {
    // TODO: Is clear color being changed/tweaked somewhere else?
    //glClearColor( 0.f, 0.f, 0.f, 1.f );
    glClearColor( 0.4f, 0.4f, 0.4f, 1.f );
	glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
}

void Renderer::SetScreenSize( U32 newWidth, U32 newHeight ) {
	mBufferDimensions.mBufferWidth = newWidth;
	mBufferDimensions.mBufferHeight = newHeight;
	glViewport( 0, 0, mBufferDimensions.mBufferWidth, mBufferDimensions.mBufferHeight );
}

void Renderer::DEBUG_DrawBoundingVolume( const Mesh *visualRepresentation ) {
    // TODO: Should probably preserve previous settings rather than simply restoring them
    //       to something we decide.
	glDisable( GL_CULL_FACE );
	glPolygonMode( GL_FRONT_AND_BACK, GL_LINE );
	glBindVertexArray( visualRepresentation->GetVAO() );

	// No loop here since an AABB will only have one submesh
	glDrawElementsBaseVertex( GL_TRIANGLES, visualRepresentation->GetSubMeshData()[0].indexCount, GL_UNSIGNED_INT,
		(void*)( sizeof( U32 ) * visualRepresentation->GetSubMeshData()[0].baseIndex ),
		visualRepresentation->GetSubMeshData()[0].baseVertex );
	
	glBindVertexArray( 0 );
	glPolygonMode( GL_FRONT, GL_FILL );
    glEnable( GL_CULL_FACE );
}

void Renderer::DrawMesh( const Mesh *mesh, const Material *material ) {
	const U8 diffuseTextureCount = material->GetTextureCount( XOF_TEXTURE_TYPE::DIFFUSE );
	const U8 normalMapCount = material->GetTextureCount( XOF_TEXTURE_TYPE::BUMP );

	glBindVertexArray( mesh->GetVAO() );

	const U8 subMeshCount = mesh->GetSubMeshCount();
	for( U8 i=0; i < subMeshCount; ++i ) {
		const U8 materialIndex = mesh->GetSubMeshData()[i].materialIndex;
        XOF_ASSERT( materialIndex != -1 );

		if( materialIndex < diffuseTextureCount ) {
            Texture *tex = material->GetTexture( XOF_TEXTURE_TYPE::DIFFUSE, materialIndex );

            const_cast<Material*>(material)->GetShader()->SetUniform_texture( "diffuseMap", 0 );

            tex->Bind( GL_TEXTURE0 );
            //tex->Bind( XOF_TEXTURE_TYPE::DIFFUSE );
            //material->GetTexture( XOF_TEXTURE_TYPE::DIFFUSE, materialIndex )->Bind( XOF_TEXTURE_TYPE::DIFFUSE );
		}

		if( materialIndex < normalMapCount ) {
            Texture *tex = material->GetTexture( XOF_TEXTURE_TYPE::BUMP, materialIndex );
            const_cast<Material*>(material)->GetShader()->SetUniform_texture( "normalMap", 1 );

            tex->Bind( GL_TEXTURE1 );//XOF_TEXTURE_TYPE::BUMP );
            //material->GetTexture( XOF_TEXTURE_TYPE::BUMP, materialIndex )->Bind( XOF_TEXTURE_TYPE::BUMP );
		}
#if 1
        {
            CubeMap *cubemap = material->GetCubeMap( 0 );
            if( cubemap ) {
                cubemap->Bind( GL_TEXTURE0 );
                XOF_ASSERT( glGetError() == GL_NO_ERROR );
            }
        }
#endif
        glDrawElementsBaseVertex( GL_TRIANGLES, mesh->GetSubMeshData()[i].indexCount, GL_UNSIGNED_INT,
			(void*)( sizeof( U32 ) * mesh->GetSubMeshData()[i].baseIndex ),
			mesh->GetSubMeshData()[i].baseVertex );
        XOF_ASSERT( glGetError() == GL_NO_ERROR );
	}

    // TEMP ---
    //glBindTexture( GL_TEXTURE_CUBE_MAP, 0 );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
    // TEMP ---

	glBindVertexArray( 0 );
}

void Renderer::SetGPUState( const GPUState *gpuState ) {
	U8 highestFlag = gpuState->GetHighestEnabledFlag();
	U16 flags = gpuState->GetEnabledFlags();
	for( U16 i=0; i <= highestFlag; ++i ) {
		if( flags & ( 1 << i ) ) {
			glEnable( XOF_GL_GPU_STATE_FLAGS[i] );
		}
	}

	highestFlag = gpuState->GetHighestDisabledFlag();
	flags = gpuState->GetDisabledFlags();
	for( U16 i=0; i <= highestFlag; ++i ) {
		if( flags & ( 1 << i ) ) {
			glDisable( XOF_GL_GPU_STATE_FLAGS[i] );
		}
	}
}

void Renderer::DEBUG_PushVAO( GLuint vao ) {
    glBindVertexArray( vao );
}

void Renderer::DEBUG_DrawSubMesh( const Mesh::SubMesh *subMesh, const Material *material ) {
    const U8 diffuseTextureCount = material->GetTextureCount( XOF_TEXTURE_TYPE::DIFFUSE );
    const U8 normalMapCount = material->GetTextureCount( XOF_TEXTURE_TYPE::BUMP );

    const U8 materialIndex = subMesh->materialIndex;
    if( materialIndex < diffuseTextureCount ) {
        material->GetTexture( XOF_TEXTURE_TYPE::DIFFUSE, materialIndex )->Bind( GL_TEXTURE0 );//XOF_TEXTURE_TYPE::DIFFUSE );
    }
    //if( materialIndex < normalMapCount ) {
    //    material->GetTexture( XOF_TEXTURE_TYPE::BUMP, materialIndex )->Bind( XOF_TEXTURE_TYPE::BUMP );
    //}

    glDrawElementsBaseVertex( GL_TRIANGLES, subMesh->indexCount, GL_UNSIGNED_INT,
        (void*)( sizeof( U32 ) * subMesh->baseIndex ), subMesh->baseVertex );
}

void Renderer::DEBUG_PopVAO() {
    glBindVertexArray( 0 );
}

void Renderer::DEBUG_TEMPDrawEditorGrid( const Mesh *visualRepresentation, U32 start, U32 count ) {
    glBindVertexArray( visualRepresentation->GetVAO() );

    glDisable( GL_CULL_FACE );
        glDrawArrays( GL_LINES, start, count );
    glEnable( GL_CULL_FACE );

    glBindVertexArray( 0 );
}
