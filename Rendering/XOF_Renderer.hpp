/*
===============================================================================

	XOF
	===
	File	:	XOF_GraphicsDevice_OpenGL.hpp
	Desc	:	OpenGL implementation of the GraphicsDevice interface.

				NOTE: Add ability to enable/disable certain vertex attributes?

===============================================================================
*/
#ifndef XOF_RENDERER_OPENGL_HPP
#define XOF_RENDERER_OPENGL_HPP


#include <glm/vec4.hpp>
#include <vector>

#include "Platform/XOF_Platform.hpp"
#include "XOF_RenderRequest.hpp"



class Renderer {
public:
    struct BufferDimensions {
        U32                     mBufferWidth;
        U32                     mBufferHeight;
    };

    bool                        StartUp();
    void                        ShutDown();

    void                        SubmitRenderRequest( RenderRequest& request );
    void                        RenderFrame();
    void                        ClearScreen();

    void                        SetClearColor( F32 r, F32 g, F32 b, F32 a ) { glClearColor( r, g, b, a ); }

    void                        SetScreenSize( U32 newWidth, U32 newHeight );
    BufferDimensions&           GetScreenDimensions() const { return const_cast<BufferDimensions&>( mBufferDimensions ); }

    void                        SetCullMode( GLenum mode ) { glCullFace( mode ); }
    void                        SetWindingMode( U32 mode ) { glFrontFace( mode ); }

                                // Debug drawing functions offer a direct path to the renderer (no request queue)
    void                        DEBUG_DrawBoundingVolume( const Mesh *visualRepresentation );
    void                        DEBUG_TEMPDrawEditorGrid( const Mesh *visualRepresentation, U32 start, U32 count );
    void                        DEBUG_PushVAO( GLuint vao );
    void                        DEBUG_DrawSubMesh( const Mesh::SubMesh *submesh, const Material *material );
    void                        DEBUG_PopVAO();

//private:
    BufferDimensions            mBufferDimensions;

								// TODO: Add a dynamic pool allocator here for render requests &&
								// Switch to struct { unordered_map<shaderHash, vector<RenderRequests>>; shader* shader; }? - order by shader
	std::vector<RenderRequest>	mRenderRequests;

    void                        DrawMesh( const Mesh *mesh, const Material *material );
    void                        SetGPUState( const GPUState *gpuState );
};


#endif // XOF_RENDERER_OPENGL_HPP
