/*
===============================================================================

	XOF
	===
	File	:	XOF_Shader.cpp
	Desc	:	Represents a shader; vertex, pixel (fragment) etc...

===============================================================================
*/

#include <algorithm>
#include <fstream>

#include "Core/XOF_CRC32.hpp"
#include "Core/XOF_LoggingManager.hpp"

#include "XOF_Shader.hpp"


static std::string LoadShader(const std::string &fileName);
static GLuint CompileAndLinkShader( const std::string& shaderCode, GLenum shaderType );
static bool CheckShaderForErrors( GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage );


// Shader class
Shader::Shader() {}

Shader::~Shader() {
    for( U8 i=0; i<XOF_SHADER_TYPE::SHADER_TYPE_COUNT; ++i ) {
        glDetachShader( mProgram, mShaders[i] );
        glDeleteShader( mShaders[i] );
        mIsLoaded = false;
    }
    glDeleteProgram( mProgram );
}

// TODO: Add variable-length version of this?
void Shader::AddUniform( const std::string &uniformName ) {
    if( mUniforms.find( uniformName ) != mUniforms.end() ) {
        return;
    }

    I32 uniformLocation = glGetUniformLocation( mProgram, uniformName.c_str() );

    if( uniformLocation == -1 ) {
        std::cerr << "Error: Uniform variable: " << uniformName << " not found in shader" << std::endl;
        return;
    }

    XOF_ASSERT( glGetError() == GL_NO_ERROR );
    mUniforms[uniformName] = uniformLocation;
}

void Shader::SetUniform_bool( const std::string &uniformName, bool value ) {
    glUniform1i( mUniforms[uniformName], value );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::SetUniform_float( const std::string &uniformName, float value ) {
    glUniform1f( mUniforms[uniformName], value );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::SetUniform_vec3( const std::string &uniformName, glm::vec3 &value ) {
    glUniform3fv( mUniforms[uniformName], 1, &value.x );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::SetUniform_vec4( const std::string &uniformName, glm::vec4 &value ) {
    glUniform4fv( mUniforms[uniformName], 1, &value.x );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::SetUniform_mat4x4( const std::string &uniformName, glm::mat4 &value ) {
    glUniformMatrix4fv( mUniforms[uniformName], 1, GL_FALSE, &value[0][0] );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::SetUniform_texture( const std::string &uniformName, U32 texture ) {
    U32 tex = mUniforms[uniformName];
    glUniform1i( tex, (GLint)texture );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );
}

void Shader::Bind() {
    glUseProgram( mProgram );
    GLint error = glGetError();
    XOF_ASSERT( error == GL_NO_ERROR );
}

void Shader::Unbind() {
    glUseProgram( 0 );
}

bool Shader::Load( ResourceDescriptor *desc ) {
    mIsLoaded = false;

    std::string shaderName( desc->directory.CStr() );
    shaderName.append( desc->fileName.CStr() );

	mProgram = glCreateProgram();
	mShaders[XOF_SHADER_TYPE::VERTEX] = CompileAndLinkShader( LoadShader( shaderName + std::string( ".glvs" ) ), GL_VERTEX_SHADER );
	mShaders[XOF_SHADER_TYPE::FRAGMENT] = CompileAndLinkShader( LoadShader( shaderName + std::string( ".glfs" ) ), GL_FRAGMENT_SHADER );

	// Moving this to stand-alone function similar to those for
	// uniforms causes issues like distorted textures
	// Similar to binding a vertexLayoutDesc in D3D
	glBindAttribLocation( mProgram, 0, "position" );
	glBindAttribLocation( mProgram, 1, "normal" );
    glBindAttribLocation( mProgram, 2, "tangent" );
    glBindAttribLocation( mProgram, 3, "texCoord" );
    glBindAttribLocation( mProgram, 4, "colour" );
    XOF_ASSERT( glGetError() == GL_NO_ERROR );

	// Add shaders to overarching shader program
	for( U32 i=0; i<XOF_SHADER_TYPE::SHADER_TYPE_COUNT; ++i ) {
		glAttachShader( mProgram, mShaders[i] );
	}

	glLinkProgram( mProgram );
    mIsLoaded = CheckShaderForErrors( mProgram, GL_LINK_STATUS, true, "Error: Shader program linking failed: " );

	glValidateProgram( mProgram );
    mIsLoaded = CheckShaderForErrors( mProgram, GL_VALIDATE_STATUS, true, "Error: Shader program invalid: " );

    const INT size = std::min<INT>( desc->fileName.GetLength(), 32 );

    memset( this->shaderName, 0x00, 32 );
    memcpy( this->shaderName, desc->fileName.CStr(), size );
    mUID = CalculateCRC32( 0, desc->fileName.CStr(), size );

    return mIsLoaded;
}


// Utility code/helpers
static std::string LoadShader( const std::string& fileName ) {
	std::ifstream file;
	file.open( fileName.c_str() );

	std::string output = "shader " + fileName + " not loaded";
	std::string line;

	if( file.is_open() ) {
		output.clear();
		while( file.good() ) {
			getline( file, line );
			output.append( line + "\n" );
		}
	} else {
        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::rendering, "Shader failed to load! %s", fileName.c_str() );
	}

	return output;
}


static GLuint CompileAndLinkShader( const std::string& shaderCode, GLenum shaderType ) {
	GLuint shader = glCreateShader( shaderType );

	if( shader == 0 ) {
		std::cerr << "Error: Shader creation failed!" << std::endl;
	}

	const GLchar* shaderSourceStrings[1];
	shaderSourceStrings[0] = shaderCode.c_str();

	GLint shaderSourceStringsLengths[1];
	shaderSourceStringsLengths[0] = static_cast<GLint>( shaderCode.length() );

	// Finally, send the shader source code to OpenGL for compilation
	glShaderSource( shader, 1, shaderSourceStrings, shaderSourceStringsLengths );
	glCompileShader( shader );

	CheckShaderForErrors( shader, GL_COMPILE_STATUS, false, "Error: Shader compilation failed: " );

	return shader;
}


static bool CheckShaderForErrors( GLuint shader, GLuint flag, bool isProgram, const std::string& errorMessage ) {
	GLint param = 0;
	GLchar error[1024] = { 0 };

	if( isProgram ) {
		glGetProgramiv( shader, flag, &param );
	}
	else {
		glGetShaderiv( shader, flag, &param );
	}

	if( param == GL_FALSE ) {
		if( isProgram ) {
			glGetProgramInfoLog( shader, sizeof( error ), NULL, error );
		}
		else {
			glGetShaderInfoLog( shader, sizeof( error ), NULL, error );
		}
        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::rendering, "Shader has errors! %s", errorMessage.c_str() );
		return false;
	}

	return true;
}
