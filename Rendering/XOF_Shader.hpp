/*
===============================================================================

    XOF
    ===
    File	:	XOF_Shader.hpp
    Desc	:	Represents a shader; vertex, pixel (fragment) etc...

===============================================================================
*/
#ifndef XOF_SHADER_HPP
#define XOF_SHADER_HPP


#include <GL/glew.h>
#include <unordered_map>

#include "Platform/XOF_Platform.hpp"
#include "Resources/CAS_Resource.hpp"
#include "XOF_Transform.hpp"

struct ShaderDesc;


#include "../GameplayFoundations/XOF_ReflectionMacros.hpp"


static const U32 XOF_MAX_PARAMS_PER_SHADER = 16;


// For use in render requests, putting it here because it's shader related;
// Underscores added to sidestep redefinition issues
enum XOF_SHADER_PARAM_TYPE {
    SIGNED_INT = 0,
    UNSIGNED_INT,
    FLOAT_,
    VEC3F,
    VEC4F,
    MAT4x4F,
    BOOL_,
    TEXTURE_,
    UNUSED_SHADER_PARAM,
};

enum XOF_SHADER_TYPE {
    VERTEX = 0,
    FRAGMENT,
    // TESS_CONTROL, TESS_EVAL, GEOMETRY...
    SHADER_TYPE_COUNT,
};


XOF_REFLECTED_CLASS_BEGIN()
class Shader : public Resource {
public:
    friend class	ResourceManager;

                    Shader();
                    ~Shader();

    bool			Load( ResourceDescriptor *desc );

                    // TODO: This could use a VA_LIST to avoid multiple calls
    bool            HasUniform( const std::string &uniformName ) const { return mUniforms.find( uniformName ) != mUniforms.end(); }
    void			AddUniform( const std::string &uniformName );
                    // TODO: Template this

    void			SetUniform_float( const std::string &uniformName, float value );
    void			SetUniform_vec3( const std::string &uniformName, glm::vec3 &value );
    void			SetUniform_vec4( const std::string &uniformName, glm::vec4 &value );
    void			SetUniform_mat4x4( const std::string &uniformName, glm::mat4 &value );
    void			SetUniform_bool( const std::string &uniformName, bool value );
    void			SetUniform_texture( const std::string &uniformName, U32 texture );

    void			Bind();
    void            Unbind();

    virtual CHAR*   GetName() const override { return (CHAR*)shaderName; }

private:
                    //XOF_REFLECTED_VARIABLE() - Don't need access to this right now...
    SHDRRSRC        shaderName[64];

    GLuint			mProgram;
    GLuint			mShaders[XOF_SHADER_TYPE::SHADER_TYPE_COUNT];

    std::unordered_map<std::string, U32> mUniforms;
};
XOF_REFLECTED_CLASS_END()


#endif // XOF_SHADER_HPP
