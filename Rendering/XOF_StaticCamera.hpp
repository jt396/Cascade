/*
===============================================================================

	XOF
	===
	File	:	XOF_StaticCamera.hpp
	Desc	:	Represents a static camera. This is in its own file
				purely for descriptiveness.

===============================================================================
*/
#ifndef XOF_STATIC_CAMERA_HPP
#define XOF_STATIC_CAMERA_HPP


#include "XOF_Camera.hpp"
using StaticCamera = Camera;


#endif // XOF_STATIC_CAMERA_HPP