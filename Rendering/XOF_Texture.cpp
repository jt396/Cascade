/*
===============================================================================

    XOF
    ===
    File	:	XOF_Texture.cpp
    Desc	:	Represents a texture map; diffuse, normal etc...

===============================================================================
*/

#include "stb_image.h"

#include "Resources/XOF_ResourceManager.hpp"

#include "XOF_Texture.hpp"


Texture::Texture() : mType{XOF_TEXTURE_TYPE::INVALID} {}

Texture::~Texture() {
    glDeleteTextures( 1, &mTexture );
}

bool Texture::Load( ResourceDescriptor *desc ) {
    TextureDescriptor *textureDesc = reinterpret_cast<TextureDescriptor*>( desc );

    std::string fileName( textureDesc->directory.CStr() );
    fileName.append( textureDesc->fileName.CStr() );

    I32 componentCount;
    mRawImageData = reinterpret_cast<U32*>( stbi_load( fileName.c_str(), &mWidth, &mHeight, &componentCount, 4 ) );

    if( !mRawImageData ) {
        return mIsLoaded = false;
    }

    glGenTextures( 1, &mTexture );
    glBindTexture( GL_TEXTURE_2D, mTexture );

    // Set wrapping
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT );

    // Set filtering
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameterf( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );

    // Send texture to GPU
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, mWidth, mHeight, 0, GL_RGBA, GL_UNSIGNED_BYTE, mRawImageData );

    stbi_image_free( mRawImageData );

    memset( texName, 0x00, 32 );
    std::string name( desc->fileName.CStr() );
    memcpy( texName, name.c_str(), std::min<INT>( name.length(), 32 ) );

    mUID = CalculateCRC32( 0, name.c_str(), std::min<INT>( name.length(), 32 ) );
    mType = textureDesc->type;

    return ( mIsLoaded = true );
}
