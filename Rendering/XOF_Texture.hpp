/*
===============================================================================

    XOF
    ===
    File	:	XOF_Texture.hpp
    Desc	:	Represents a texture map; diffuse, normal etc...

===============================================================================
*/
#ifndef XOF_TEXTURE_HPP
#define XOF_TEXTURE_HPP


enum XOF_TEXTURE_TYPE {
    DIFFUSE = 0,
    BUMP,
    // DEPTH...
    TEXTURE_TYPE_COUNT,
    INVALID,
};

// We place the texture type enum above this include to
// prevent cyclic-dependencies in the includes
#include <GL/glew.h>
#if defined( EDITOR_OR_DEBUG )
#include <string>
#endif

#include "Resources/CAS_Resource.hpp"


XOF_REFLECTED_CLASS_BEGIN()
class Texture : public Resource {
public:
    friend class            ResourceManager;

                            Texture();
                            ~Texture();

    virtual bool            Load( ResourceDescriptor *desc ) override;

    inline XOF_TEXTURE_TYPE GetTextureType() const;

    inline I32              GetWidth() const;
    inline I32              GetHeight() const;
    inline U32              GetPixel( U32 x, U32 y ) const;

    inline void             Bind( U32 unit );

    virtual CHAR*           GetName() const override { return (CHAR*)texName; }

    U32                     TEMP_GetGLName() const { return mTexture; }

private:
                            XOF_REFLECTED_VARIABLE()
    TXTRRSRC                texName[32];

    U32                     mTexture;
    I32                     mWidth;
    I32                     mHeight;
    U32*                    mRawImageData;

    XOF_TEXTURE_TYPE        mType;
};
XOF_REFLECTED_CLASS_END()


// Inlines
inline XOF_TEXTURE_TYPE Texture::GetTextureType() const {
    return mType;
}

inline I32 Texture::GetWidth() const {
    return mWidth;
}

inline I32	Texture::GetHeight() const {
    return mHeight;
}

inline U32 Texture::GetPixel( U32 x, U32 y ) const {
    return mRawImageData[x + y * mWidth];
}

inline void Texture::Bind( U32 unit ) {
    //XOF_ASSERT( ( unit >= 0 ) && ( unit <= 31 ) )
    glActiveTexture( unit );
    glBindTexture( GL_TEXTURE_2D, mTexture );
}


#endif // XOF_TEXTURE_HPP
