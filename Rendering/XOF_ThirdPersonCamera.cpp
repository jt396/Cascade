/*
===============================================================================

	XOF
	===
	File	:	XOF_ThirdPersonCamera.cpp
	Desc	:	Third person camera

===============================================================================
*/
#include "XOF_ThirdPersonCamera.hpp"


// To prevent gimbal lock (degrees)
static const float XOF_GIMBAL_LOCK_LIMIT = 0.9f;


ThirdPersonCamera::ThirdPersonCamera() {}
						
ThirdPersonCamera::ThirdPersonCamera( const glm::vec3 &pos, const glm::vec3 &target, 
										float screenHeight, float screenWidth, 
										float aspectRatio, float zNear, float zFar ) {
	Camera::Setup( pos, target, screenHeight, screenWidth, aspectRatio, zNear, zFar );
}

// TODO: Translation...

void ThirdPersonCamera::Pitch( float amount ) {
	if( std::abs( mPitch + amount ) >= XOF_GIMBAL_LOCK_LIMIT ) {
		return;
	}
	mPos = ( glm::mat3( glm::rotate( amount, mRight ) ) * ( mPos - mLookAt ) ) + mLookAt;
	mRight = glm::cross( mLookAt - mPos, mUp );
	mPitch += amount;
}

void ThirdPersonCamera::Yaw( float amount ) {
	mPos = ( glm::mat3( glm::rotate( amount, mUp ) ) * ( mPos - mLookAt ) ) + mLookAt;
	mRight = glm::cross( mLookAt - mPos, mUp );
}