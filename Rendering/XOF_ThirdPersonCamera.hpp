/*
===============================================================================

	XOF
	===
	File	:	XOF_ThirdPersonCamera.hpp
	Desc	:	Third person camera

===============================================================================
*/
#ifndef XOF_THIRD_PERSON_CAMERA_HPP
#define XOF_THIRD_PERSON_CAMERA_HPP


#include "XOF_Camera.hpp"


class ThirdPersonCamera : public Camera {
public:
								ThirdPersonCamera();
								ThirdPersonCamera( const glm::vec3 &pos, const glm::vec3 &target, 
													float screenHeight, float screenWidth, 
													float aspectRatio, float zNear, float zFar );

								// Vector/Matrix-based
	void						Translate( float x, float y, float z );
	void						Pitch( float amount );
	void						Yaw( float amount );

	inline virtual glm::mat4	GetViewMatrix() const override;

private:
	float	mYaw;
	float	mPitch;
};


inline glm::mat4 ThirdPersonCamera::GetViewMatrix() const {
	return glm::lookAt( mPos, mLookAt, mUp );
}


#endif // XOF_THIRD_PERSON_CAMERA_HPP