/*
===============================================================================

	XOF
	===
	File	:	XOF_Vertex.hpp
	Desc	:	Represents a vertex.

===============================================================================
*/
#ifndef XOF_VERTEX_HPP
#define XOF_VERTEX_HPP


#include <glm/glm.hpp>


enum XOF_VERTEX_ATTRIBUTES {
	POSITION = 0,
	NORMAL,
    TANGENT,
	TEX_COORDS,
	JOINT_ID,
	JOINT_WEIGHT,
	INDEX,
	VERTEX_ATTRIBUTES_COUNT,
};


struct Vertex {
	glm::vec3 pos;
	glm::vec3 normal;
    glm::vec3 tangent;
	glm::vec2 texCoords;
	glm::vec3 colour;
	U8		  jointIndex[4];
	float	  jointWeight[4]; // could be set to 3

	Vertex() : pos( glm::vec3( 0.f, 0.f, 0.f ) ), normal( glm::vec3( 0.f, 0.f, 0.f ) ),
               tangent( glm::vec3( 0.f, 0.f, 0.f ) ), texCoords( glm::vec2( 0.f, 0.f ) ),
               colour(glm::vec3( 0.f, 0.f, 0.f ) ) {
		jointIndex[0] = 0; jointIndex[1] = 0; jointIndex[2] = 0; jointIndex[3] = 0;
		jointWeight[0] = 0; jointWeight[1] = 0; jointWeight[2] = 0; jointWeight[3] = 0;
	}

	Vertex( const glm::vec3& pos, glm::vec2& texCoords, 
		const glm::vec3 &normal = glm::vec3( 0.f, 0.f, 0.f ), 
        const glm::vec3 &tangent = glm::vec3( 0.f, 0.f, 0.f ),
		const glm::vec3 &colour = glm::vec3( 0.f, 0.f, 0.f ) ) {
		this->pos = pos; 
		this->texCoords = texCoords;
		this->normal = normal;
        this->tangent = tangent;
		this->colour = colour;
	}

    bool operator==( const Vertex& v ) const {
        if( &v == this ) return true;
        return pos == v.pos && texCoords == v.texCoords && normal == v.normal && v.tangent == tangent && v.colour == colour;
    }
};


// To use vertices in a map
#include <GLM/gtx/hash.hpp>
namespace std {
    template<> struct hash<Vertex> {
        size_t operator()(const Vertex& v) const {
            return ((hash<glm::vec3>()(v.pos) ^ (hash<glm::vec3>()(v.colour) << 1)) >> 1) ^ (hash<glm::vec2>()(v.texCoords) << 1);
        }
    };
}


#endif // XOF_VERTEX_HPP
