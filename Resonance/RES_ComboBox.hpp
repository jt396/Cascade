/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_ComboBox.hpp
    Desc	:	We need a combo-box capable of 'remembering' its previously
                selected item. So we inherit from QComboBox and tweak it a little.

===============================================================================
*/
#ifndef RES_COMBOBOX_HPP
#define RES_COMBOBOX_HPP


#include <QComboBox>
#include <QDir>

#include "Rendering/XOF_Texture.hpp"

// TODO: Surely these two classes can be merged, is there a way to describe the ItemInfo struct fields
//       as template arguments?

class TextureComboBox : public QComboBox {
    Q_OBJECT

public:
    struct ItemInfo {
        XOF_TEXTURE_TYPE    type;
        size_t              index;
        QString             name;
    };

    enum class CurrentText {
        Update,
        DoNotUpdate
    };

                    TextureComboBox( const QString& directory, const ItemInfo& initialValue, QWidget *parent=nullptr ) :
                        QComboBox( parent ), mCurrentItem{initialValue}
                    {
                        foreach( QString asset, QDir{directory}.entryList( QStringList() << "*.png" ) ) {
                            addItem( asset );
                        }
                        setCurrentText( mCurrentItem.name );
                    }

    inline ItemInfo GetCurrentItemInfo() const { return mCurrentItem; }
    inline void     SetItemInfo( const ItemInfo& item, CurrentText text = CurrentText::DoNotUpdate ) {
                        mCurrentItem = item;
                        if( text == CurrentText::Update ) {
                            setCurrentText( item.name );
                        }
                    }

private:
    ItemInfo        mCurrentItem;
};

// ---

class MeshComboBox : public QComboBox {
    Q_OBJECT

public:
    struct ItemInfo {
        QString name;
    };

    enum class CurrentText {
        Update,
        DoNotUpdate
    };

                    MeshComboBox( const QString& directory, const ItemInfo& initialValue, QWidget *parent=nullptr ) :
                        QComboBox( parent ), mCurrentItem{initialValue}
                    {
                        foreach( QString asset, QDir{directory}.entryList( QStringList() << "*.obj" ) ) {
                            addItem( asset );
                        }
                        setCurrentText( mCurrentItem.name );
                    }

    inline ItemInfo GetCurrentItemInfo() const { return mCurrentItem; }
    inline void     SetItemInfo( const ItemInfo& item, CurrentText text = CurrentText::DoNotUpdate ) {
                        mCurrentItem = item;
                        if( text == CurrentText::Update ) {
                            setCurrentText( item.name );
                        }
                    }

private:
    ItemInfo        mCurrentItem;
};


#endif // RES_COMBOBOX_HPP
