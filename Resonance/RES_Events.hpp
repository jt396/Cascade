/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_Events.hpp
    Desc	:	Editor-specific events. A runtime/game entity shouldn't concern
                itself with these, so we use an EntityController to handle them.

===============================================================================
*/
#ifndef RES_EVENTS_HPP
#define RES_EVENTS_HPP


#include "GameplayFoundations/XOF_Event.hpp"
#include "Collision&Physics/XOF_Ray.hpp"
#include "Rendering/XOF_Camera.hpp"
#include "Core/XOF_CRC32.hpp"


class Component;


// NOTE: PLAY/PAUSE/STOP will be handled by buttons in the main window
enum RES_EVENTS {
    FIRED_PICKING_RAY = 0,
    RESET_SIMULATION,
    //CREATE_ENTITY,
    //DESTROY_ENTITY,
    COMPONENT_ADDED,
    COMPONENT_REMOVED,
    ENTITY_VISIBILITY_TOGGLED,
};


struct FiredPickingRay : public Event {
    enum ARGS {
        RAY_ORIGIN = 0,
        RAY_DIRECTION,
    };

    FiredPickingRay( const Ray& pickingRay ) {
        type = RES_EVENTS::FIRED_PICKING_RAY;

        arguments[RAY_ORIGIN].asVec3f[0] = pickingRay.origin.x;
        arguments[RAY_ORIGIN].asVec3f[1] = pickingRay.origin.y;
        arguments[RAY_ORIGIN].asVec3f[2] = pickingRay.origin.z;

        arguments[RAY_DIRECTION].asVec3f[0] = pickingRay.direction.x;
        arguments[RAY_DIRECTION].asVec3f[1] = pickingRay.direction.y;
        arguments[RAY_DIRECTION].asVec3f[2] = pickingRay.direction.z;
    }
};

struct SimulationReset : public Event {
    enum ARGS {};

    SimulationReset() {
        type = RES_EVENTS::RESET_SIMULATION;
    }
};

struct ComponentAdded : public Event {
    enum ARGS {
        COMPONENT_TYPE = 0,
        COMPONENT_ADDRESS,
    };

    ComponentAdded( U32 componentType, Component *component ) {
        type = RES_EVENTS::COMPONENT_ADDED;
        arguments[COMPONENT_TYPE].asU32 = componentType;
        arguments[COMPONENT_ADDRESS].asVoidPtr = reinterpret_cast<void*>( component );
    }
};


struct ComponentRemoved : public Event {
    enum ARGS {
        COMPONENT_TYPE = 0,
    };

    ComponentRemoved( U32 componentType ) {
        type = RES_EVENTS::COMPONENT_REMOVED;
        arguments[COMPONENT_TYPE].asU32 = componentType;
    }
};


struct EntityVisibilityToggled : public Event {
    enum ARGS {
        IS_VISIBLE = 0,
    };

    EntityVisibilityToggled( bool isVisible ) {
        type = RES_EVENTS::ENTITY_VISIBILITY_TOGGLED;
        arguments[IS_VISIBLE].asBool = isVisible;
    }
};


#endif // RES_EVENTS_HPP
