#ifndef RES_REFLECTEDTYPESETTERS_HPP
#define RES_REFLECTEDTYPESETTERS_HPP


#include <QTableWidget>

#include "RES_Simulation.hpp"


//void ConfigureGettersForReflectedTypes( Simulation *simulation );
//void ConfigureSettersForReflectedTypes( Simulation *simulation );

void ConfigureGettersForReflectedTypes_( Simulation *simulation ) {
#if 0
    ReflectionManager *reflectionManager{simulation->engineRuntime.reflectionManager};
    ResourceManager *rm{simulation->engineRuntime.resourceManager};

    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "MSHRSRC" ) ) ) {
        type->getter = []( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            //XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );

            size_t instanceAddress = reinterpret_cast<size_t>( instance ) + reflectedVariable->memoryOffset ;

            // Create a combo-box and populate it with all the assets of this type
            QComboBox *comboBox = new QComboBox();

            QDir resourceDirectory( "" );//*mResourceDirectoryPaths[Underlying( ResourceType::mesh )] );
            QStringList assetList = resourceDirectory.entryList( QStringList() << "*.obj" );
            for( I32 i=0; i<assetList.size(); ++i ) {
                comboBox->addItem( assetList[i] );
            }
            comboBox->setCurrentText( QString( reinterpret_cast<CHAR*>( instanceAddress + sizeof(int) ) ) );

            // Set callback for the combo-box
            QObject::connect( comboBox, &QComboBox::currentTextChanged,
                table, [=] () {
                    ResourceManager *rm = nullptr;//mSimulation->engineRuntime.resourceManager->Get();
                    Renderable *renderable = reinterpret_cast<Renderable*>(
                                entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );
                                //mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );

                    // Now get the new mesh ready
                    ResourceDescriptor meshDesc{"",//mResourceDirectoryPaths[Underlying( ResourceType::mesh )]->toStdString().c_str(),
                                                comboBox->currentText().toStdString().c_str()};

                    // Now the material
                    ResourceName materialName{meshDesc.fileName.SubString( 0, meshDesc.fileName.LastOf( "." ).second ).CStr()};
                    materialName.Append( ".mtl" );

                    MaterialDescriptor materialDesc{meshDesc.directory, materialName};
                    materialDesc.shaders = {"",//{mResourceDirectoryPaths[Underlying( ResourceType::shader )]->toStdString().c_str(),
                                            "DiffuseTexture_AmbientDiffuseLight_DirectionalPointSpot"};

                    Mesh *mesh = rm->LoadResource<Mesh>( meshDesc );
                    XOF_ASSERT( mesh );
                    renderable->SetMesh( mesh );

                    Material *material = rm->LoadResource<Material>( materialDesc );
                    XOF_ASSERT( material );
                    renderable->SetMaterial( material );

                    return;
                }
            );
            table->setCellWidget( table->rowCount()-1, 1, comboBox );
            return nullptr;
        };
    }
#endif
#if 0
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "TXTRRSRC" ) ) ) {
        type->getter = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );
            size_t instanceAddress = reinterpret_cast<size_t>( instance ) + reflectedVariable->memoryOffset ;

            // Create a combo-box and populate it with all the assets of this type
            //QComboBox *comboBox = new QComboBox();
            ComboBox *comboBox = new ComboBox();

            QDir resourceDirectory( *mResourceDirectoryPaths[Underlying( ResourceType::texture )] );
            QStringList assetList = resourceDirectory.entryList( QStringList() << "*.png" );
            for( I32 i=0; i<assetList.size(); ++i ) {
                comboBox->addItem( assetList[i] );
            }
            comboBox->setCurrentText( reinterpret_cast<CHAR*>( instanceAddress )  );
            //comboBox->StorePreviousItemText( comboBox->currentText() );

            Renderable *renderable = reinterpret_cast<Renderable*>(
                        mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );

            TextureInfo prevTextureInfo = renderable->InspectMaterial()->GetTextureInfo( comboBox->currentText().toStdString() );
            comboBox->mTextureType = prevTextureInfo.type;
            comboBox->mTextureIndex = prevTextureInfo.index;

            // Set callback for the combo-box
            connect( comboBox, &ComboBox::currentTextChanged,
                this, [=] () {
                    ResourceManager *rm = mSimulation->engineRuntime.resourceManager->Get();
                    Renderable *renderable = reinterpret_cast<Renderable*>(
                                mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) );

                    // Load the new texture
                    ResourceDescriptor textureDesc{mResourceDirectoryPaths[Underlying( ResourceType::texture )]->toStdString().c_str(),
                                                   comboBox->currentText().toStdString().c_str()};

                    Texture *newTexture = rm->LoadResource<Texture>( textureDesc );
                    XOF_ASSERT( newTexture );

                    // Now look up information on the appropriate texture to update
                    //TextureInfo prevTextureInfo = renderable->InspectMaterial()->GetTextureInfo( comboBox->GetPreviousItemText().toStdString() );

                    // Now update the texture
                    renderable->GetMaterial()->SetTexture( comboBox->mTextureType, comboBox->mTextureIndex, newTexture );
                }
            );
            table->setCellWidget( table->rowCount()-1, 1, comboBox );
            return nullptr;
        };
    }
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "glm::vec3" ) ) ) {
        type->getter = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            size_t instanceAddress = reinterpret_cast<size_t>( instance );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );

            glm::vec3 *vec = reinterpret_cast<glm::vec3*>( instanceAddress + reflectedVariable->memoryOffset );

            struct VecComponent { QString name; float value; QString styleSheet; };
            VecComponent vecComponents[3] {
                { "X", vec->x, "QLabel { background-color : red; font : bold; color : white; }" },
                { "Y", vec->y, "QLabel { background-color : green; font : bold; color : white; }" },
                { "Z", vec->z, "QLabel { background-color : blue; font : bold; color : white; }" }
            };

            QHBoxLayout *layout = new QHBoxLayout();
            layout->setMargin( 5 );

            for( U32 i=0; i<3; ++i ) {
                QDoubleSpinBox *fieldValue = new QDoubleSpinBox();
                fieldValue->setMinimum( -( std::numeric_limits<float>::max() - 1 ) );
                fieldValue->setMaximum( std::numeric_limits<float>::max() );

                // Add field name and value
                fieldValue->setValue( static_cast<double>( vecComponents[i].value ) );
                connect( fieldValue, SIGNAL( valueChanged( double ) ), this, SLOT( OnPropertyTableItemValueChanged() ) );

                QLabel *label = new QLabel( vecComponents[i].name );
                label->setStyleSheet( vecComponents[i].styleSheet );

                layout->addWidget( label );
                layout->addWidget( fieldValue );
            }

            // Widget to own the layout + components
            QWidget *widget = new QWidget();
            widget->setLayout( layout );
            table->setCellWidget( table->rowCount()-1, 1, widget );

            return nullptr;
        };
    }
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "CHAR" ) ) ) {
        type->getter = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );

            size_t instanceAddress = reinterpret_cast<size_t>( instance );

            QString val( reinterpret_cast<char*>( instanceAddress + reflectedVariable->memoryOffset ) );
            table->setCellWidget( table->rowCount()-1, 1, new QLineEdit( val ) );

            return nullptr;
        };
    }
#endif
}


#endif // RES_REFLECTEDTYPESETTERS_HPP
