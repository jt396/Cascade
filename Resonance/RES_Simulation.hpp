/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_Simuations.hpp
    Desc	:	Editor-specific representation/means of interaction of/with
                the simulation/game-world.

===============================================================================
*/
#ifndef RES_SIMULATION_HPP
#define RES_SIMULATION_HPP


#include <vector>

#include "GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp"
#include "RES_EntityController.hpp"


struct Simulation {
    EngineRuntime                                   engineRuntime;

    using EntityControllerContainer = std::vector<std::unique_ptr<EntityController>>;
    EntityControllerContainer entityControllers;

    // TODO: Add "Setup/Configure()" function to side-step dynamically allocating everything
    std::vector<std::unique_ptr<Event>>             events;

    enum STATE { PLAY = 0, PAUSE, STOP, };
    STATE                                           state;
};


#endif // RES_SIMULATION_HPP
