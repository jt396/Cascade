#-------------------------------------------------
#
# Project created by QtCreator 2016-04-03T17:46:19
#
#-------------------------------------------------

QT += core gui

CONFIG += debug
#CONFIG += /std:c++17
#CONFIG += console
QMAKE_CXXFLAGS += /std:c++17

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Resonance
TEMPLATE = app

# CASCADE top-level
INCLUDEPATH += "$$PWD/.."
# RESONANCE top-level
INCLUDEPATH += $$PWD

# GLM
INCLUDEPATH += "$$PWD/../ThirdParty/glm"
DEPENDPATH += "$$PWD/../ThirdParty/glm"

# SFML
INCLUDEPATH += "$$PWD/../ThirdParty/SFML/include"
DEPENDPATH += "$$PWD/../ThirdParty/SFML/include"
LIBS += -L"$$PWD/../ThirdParty/SFML/lib"
CONFIG(debug, debug|release): LIBS += -lsfml-audio-d -lsfml-graphics-d -lsfml-main-d -lsfml-network-d -lsfml-window-d -lsfml-system-d

# GLEW
INCLUDEPATH += "$$PWD/../ThirdParty/glew-1.13.0/include"
DEPENDPATH += "$$PWD/../ThirdParty/glew-1.13.0/include"
LIBS += -L"$$PWD/../ThirdParty/glew-1.13.0/lib/Release/Win32"
CONFIG(debug, debug|release): LIBS += -lglew32

# ASSIMP
INCLUDEPATH += "$$PWD/../ThirdParty/assimp--3.0.1270-sdk/include"
DEPENDPATH += "$$PWD/../ThirdParty/assimp--3.0.1270-sdk/include"
LIBS += -L"$$PWD/../ThirdParty/assimp--3.0.1270-sdk/lib/assimp_release-dll_win32"
CONFIG(debug, debug|release): LIBS += -lassimp

# CASCADE RUNTIME
LIBS += -L"$$PWD/../debug/"
CONFIG(debug, debug|release): LIBS += -lCascade

# OTHER
#LIBS += -L"C:/Program Files (x86)/Microsoft SDKs/Windows/v7.1A/Lib"
CONFIG(debug, debug|release): LIBS += -lOpenGL32

SOURCES += main.cpp \
    UI/RES_MainWindow.cpp \
    UI/RES_OutputTerminal.cpp \
    UI/RES_SceneGraph.cpp \
    UI/RES_NewEntityWindow.cpp \
    UI/RES_CreateEntityWindow.cpp \
    UI/QSFMLCanvas.cpp \
    RES_EntityController.cpp \
    main.cpp \
    TransformGizmos/RES_RotationGizmo.cpp \
    TransformGizmos/RES_ScaleGizmo.cpp \
    TransformGizmos/RES_TranslationGizmo.cpp \
    UI/RES_ComponentConfigurationWindow.cpp \
    UI/RES_AssetBrowser.cpp \
    UI/QSFMLCanvas.cpp

HEADERS += \
    UI/RES_MainWindow.hpp \
    UI/QSFMLCanvas.hpp \
    UI/RES_OutputTerminal.hpp \
    UI/RES_SceneGraph.hpp \
    UI/RES_NewEntityWindow.hpp \
    UI/RES_CreateEntityWindow.hpp \
    RES_EntityController.hpp \
    RES_Simulation.hpp \
    RES_Events.hpp \
    res_tempgrid.hpp \
    TransformGizmos/RES_RotationGizmo.hpp \
    TransformGizmos/RES_ScaleGizmo.hpp \
    TransformGizmos/RES_TransformGizmo.hpp \
    TransformGizmos/RES_TranslationGizmo.hpp \
    UI/RES_ComponentConfigurationWindow.hpp \
    RES_ReflectedTypeGettersSetters.hpp \
    UI/RES_AssetBrowser.hpp \
    RES_ComboBox.hpp

FORMS += \
    UI/RES_MainWindow.ui \
    UI/RES_OutputTerminal.ui \
    UI/RES_SceneGraph.ui \
    UI/RES_NewEntityWindow.ui \
    UI/RES_CreateEntityWindow.ui \
    UI/RES_ComponentConfigurationWindow.ui \
    UI/RES_AssetBrowser.ui

RESOURCES += \
    RES_EditorIcons.qrc
