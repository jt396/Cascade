/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_RotationGizmo.hpp
    Desc	:	Gizmo used to rotate entities.

===============================================================================
*/


#include "Resources/CAS_ResourceDescriptors.hpp"
#include "Resources/XOF_ResourceManager.hpp"

#include "RES_RotationGizmo.hpp"


// Setup the gizmo visual representation and its bounding volumes (axis, planes and the gizmo)
bool RotationGizmo::Initialise() {
    ResourceManager *rm = ResourceManager::Get();

    MeshDescriptor meshDesc{"../Resources/Meshes/", "RotationGizmo.obj"};
    meshDesc.retainVertexData = false;
    meshDesc.retainIndexData = false;

    MaterialDescriptor materialDesc{"../Resources/Meshes/", "RotationGizmo.mtl"};
    materialDesc.shaders = {"../Resources/Shaders/", "RESONANCE_GIZMO_SHADER"};

    mVisualRepresentation.mesh = rm->LoadResource<Mesh>( meshDesc );
    XOF_ASSERT( mVisualRepresentation.mesh );

    mVisualRepresentation.material = rm->LoadResource<Material>( materialDesc );
    XOF_ASSERT( mVisualRepresentation.material );

    mVisualRepresentation.material->GetShader()->AddUniform( "transform" );
    mVisualRepresentation.material->GetShader()->AddUniform( "world" );

    mVisualRepresentation.material->GetShader()->AddUniform( "eyeWorldPos" );
    mVisualRepresentation.material->GetShader()->AddUniform( "isSelected" );

    // NOTE: Changes here require a corresponding change to translationFactors[1]
    mVisualRepresentation.transform.scale = glm::vec3( 0.5f );

    // Generate a bounding volume for the entire gizmo
    Mesh::Dimensions gizmoBoundingBoxDims = mVisualRepresentation.mesh->GetDimensions();
    gizmoBoundingBoxDims.min *= mVisualRepresentation.transform.scale;
    gizmoBoundingBoxDims.max *= mVisualRepresentation.transform.scale;
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].ScaleToFit( gizmoBoundingBoxDims );

    // Generate the bounding volumes for the axis
    {
        Mesh::Dimensions dimensions;
        const float scaleFactors[] { 3.5f, 0.25f };
        const float translateFactors[] { 0.f, 0.f };

        constexpr auto xAxis = Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS );
        constexpr auto zAxis = Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS );
        for( auto i=xAxis; i<=zAxis; ++i ) {
            glm::vec3 scale( scaleFactors[0] ); scale[i-1] = scaleFactors[1];

            dimensions.max = scale * mVisualRepresentation.transform.scale;
            dimensions.min = -dimensions.max;
            mBoundingVolumes[i].ScaleToFit( dimensions );

            glm::vec3 translation( translateFactors[0] ); translation[i-1] = translateFactors[1];
            mBoundingVolumes[i].Transform( translation, nullptr );
        }
    }

    // Setup the bounding volumes for the gizmo planes (if applicable)...
    {
        // ...
    }

#if 1
    MeshDescriptor boundingVolMeshDesc{"../Resources/Meshes/", "UNIT_CUBE.obj"};

    MaterialDescriptor boundingVolMaterialDesc{"../Resources/Meshes/", "UNIT_CUBE.mtl"};
    boundingVolMaterialDesc.shaders = {"../Resources/Shaders/", "RES_BOUNDING_VOLUME_SHADER"};

    // Set visual representation for the gizmo' axis/plane bounding volumes
    Mesh *boundingVolumeMesh = rm->LoadResource<Mesh>( boundingVolMeshDesc );
    Material *boundingVolumeMaterial = rm->LoadResource<Material>( boundingVolMaterialDesc );
    boundingVolumeMaterial->GetShader()->AddUniform( "wvp" );

    constexpr auto boundingVolumeCount = Underlying( GIZMO_BOUNDING_VOLUMES::COUNT );
    for( U32 i=0; i<boundingVolumeCount; ++i ) {
        mBoundingVolumes[i].GetVisualRepresentation()->mesh = boundingVolumeMesh;
        mBoundingVolumes[i].GetVisualRepresentation()->material = boundingVolumeMaterial;
    }
#endif

    return ( mIsInitialised = true );
}

bool RotationGizmo::TestForMouseOver( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        return ( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) );
    }
    return false;
}

bool RotationGizmo::TestForSelection( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::X_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES:: Y_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::Z_AXIS;
        }
    }
    return ( mSelectedAxis != GIZMO_BOUNDING_VOLUMES::NONE );
}

void RotationGizmo::HandleMouseMovement( float xDelta, float yDelta ) {
    Transformable *entityTransform = (Transformable*)mSelectedEntity->GetEntity()->GetComponent( CAS_COMPONENTS::TRANSFORMABLE );

    glm::vec3 *rotation = &entityTransform->GetTransform().rotation;

    // Move the visual representation of the gizmo
    switch( mSelectedAxis ) {
        case GIZMO_BOUNDING_VOLUMES::NONE: break;
        case GIZMO_BOUNDING_VOLUMES::X_AXIS: rotation->x = xDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Y_AXIS: rotation->y = yDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Z_AXIS: rotation->z = xDelta; break;
    }

    mSelectedEntity->Rotate( rotation->x, rotation->y, rotation->z );
}
