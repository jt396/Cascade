#ifndef RES_ROTATIONGIZMO_HPP
#define RES_ROTATIONGIZMO_HPP


#include "RES_TransformGizmo.hpp"


class RotationGizmo : public TransformGizmo {
public:
                    RotationGizmo() {}
                    ~RotationGizmo() {}

    virtual bool    Initialise() override;

                    // Test for mouse-over on mouse move or click
    virtual bool    TestForMouseOver( const Ray& ray ) override;
    virtual bool    TestForSelection( const Ray& ray ) override;

                    // Respond to mouse movement if/when gizmo is selected
    virtual void    HandleMouseMovement( F32 xDelta, F32 yDelta ) override;
};



#endif // RES_ROTATIONGIZMO_HPP
