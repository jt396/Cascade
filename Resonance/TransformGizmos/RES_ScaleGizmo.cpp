/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_TranslationGizmo.hpp
    Desc	:	Gizmo used to scale entities.

===============================================================================
*/


#include "Resources/CAS_ResourceDescriptors.hpp"
#include "Resources/XOF_ResourceManager.hpp"

#include "RES_ScaleGizmo.hpp"


// Setup the gizmo visual representation and its bounding volumes (axis, planes and the gizmo)
bool ScaleGizmo::Initialise() {
    ResourceManager *rm = ResourceManager::Get();

    MeshDescriptor meshDesc{"../Resources/Meshes/", "ScaleGizmo.obj"};
    meshDesc.retainVertexData = false;
    meshDesc.retainIndexData = false;

    MaterialDescriptor materialDesc{"../Resources/Meshes/", "ScaleGizmo.mtl"};
    materialDesc.shaders = {"../Resources/Shaders/", "RESONANCE_GIZMO_SHADER"};

    mVisualRepresentation.mesh = rm->LoadResource<Mesh>( meshDesc );
    XOF_ASSERT( mVisualRepresentation.mesh );

    mVisualRepresentation.material = rm->LoadResource<Material>( materialDesc );
    XOF_ASSERT( mVisualRepresentation.material );

    mVisualRepresentation.material->GetShader()->AddUniform( "transform" );
    mVisualRepresentation.material->GetShader()->AddUniform( "world" );

    mVisualRepresentation.material->GetShader()->AddUniform( "eyeWorldPos" );
    mVisualRepresentation.material->GetShader()->AddUniform( "isSelected" );

    // NOTE: Changes here require a corresponding change to translationFactors[1]
    mVisualRepresentation.transform.scale = glm::vec3( 0.5f );

    // Generate bounding volume for the entire gizmo
    Mesh::Dimensions gizmoBoundingBoxDims = mVisualRepresentation.mesh->GetDimensions();
    gizmoBoundingBoxDims.min *= mVisualRepresentation.transform.scale;
    gizmoBoundingBoxDims.max *= mVisualRepresentation.transform.scale;
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].ScaleToFit( gizmoBoundingBoxDims );

    // Generate the bounding volumes for the gizmo axis
    {
        Mesh::Dimensions dimensions;
        const float scaleFactors[] { 0.125f, 1.75f };
        const float translateFactors[] { 0.f, 1.f };

        constexpr auto xAxis = Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS );
        constexpr auto zAxis = Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS );
        for( auto i=xAxis; i<=zAxis; ++i ) {
            glm::vec3 scale( scaleFactors[0] ); scale[i-1] = scaleFactors[1];

            dimensions.max = scale * mVisualRepresentation.transform.scale;
            dimensions.min = -dimensions.max;
            mBoundingVolumes[i].ScaleToFit( dimensions );

            glm::vec3 translation( translateFactors[0] ); translation[i-1] = translateFactors[1];
            mBoundingVolumes[i].Transform( translation, nullptr );
        }
    }

    // Setup the bounding volumes for the gizmo planes (if applicable)...
    {
        // ...
    }

#if 1
    MeshDescriptor boundingVolMeshDesc{"../Resources/Meshes/", "UNIT_CUBE.obj"};

    MaterialDescriptor boundingVolMaterialDesc{"../Resources/Meshes/", "UNIT_CUBE.mtl"};
    boundingVolMaterialDesc.shaders = {"../Resources/Shaders/", "RES_BOUNDING_VOLUME_SHADER"};

    // Set visual representation for the gizmo' axis/plane bounding volumes
    Mesh *boundingVolumeMesh = rm->LoadResource<Mesh>( boundingVolMeshDesc );
    Material *boundingVolumeMaterial = rm->LoadResource<Material>( boundingVolMaterialDesc );
    boundingVolumeMaterial->GetShader()->AddUniform( "wvp" );

    constexpr auto boundingVolumeCount = Underlying( GIZMO_BOUNDING_VOLUMES::COUNT );
    for( U32 i=0; i<boundingVolumeCount; ++i ) {
        mBoundingVolumes[i].GetVisualRepresentation()->mesh = boundingVolumeMesh;
        mBoundingVolumes[i].GetVisualRepresentation()->material = boundingVolumeMaterial;
    }
#endif

    return ( mIsInitialised = true );
}

bool ScaleGizmo::TestForMouseOver( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        return ( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) );
    }
    return false;
}

bool ScaleGizmo::TestForSelection( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::X_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES:: Y_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::Z_AXIS;
        }
    }
    return ( mSelectedAxis != GIZMO_BOUNDING_VOLUMES::NONE );
}

void ScaleGizmo::HandleMouseMovement( float xDelta, float yDelta ) {
    Transformable *entityTransform = (Transformable*)mSelectedEntity->GetEntity()->GetComponent( CAS_COMPONENTS::TRANSFORMABLE );

    glm::vec3 *scale = &entityTransform->GetTransform().scale;

    // Move the visual representation of the gizmo
    switch( mSelectedAxis ) {
        case GIZMO_BOUNDING_VOLUMES::NONE: break;
        case GIZMO_BOUNDING_VOLUMES::X_AXIS: scale->x = xDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Y_AXIS: scale->y = yDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Z_AXIS: scale->z = xDelta; break;
    }

    mSelectedEntity->Scale( scale->x, scale->y, scale->z );
}
