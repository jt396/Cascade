/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_ScaleGizmo.hpp
    Desc	:	Gizmo used to scale entities.

===============================================================================
*/
#ifndef RES_SCALEGIZMO_HPP
#define RES_SCALEGIZMO_HPP


#include "RES_TransformGizmo.hpp"


class ScaleGizmo : public TransformGizmo {
public:
                    ScaleGizmo() {}
                    ~ScaleGizmo() {}

    virtual bool    Initialise() override;

    // Test for mouse-over on mouse move or click
    virtual bool    TestForMouseOver( const Ray& ray ) override;
    virtual bool    TestForSelection( const Ray& ray ) override;

    // Respond to mouse movement if/when gizmo is selected
    virtual void    HandleMouseMovement( F32 xDelta, F32 yDelta ) override;
};


#endif // RES_SCALEGIZMO_HPP
