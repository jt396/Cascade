/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_TransformGizmo.hpp
    Desc	:	Base class from which all transform gizmos derive
                (Translate, Rotate, Scale).

===============================================================================
*/
#ifndef RES_TRANSFORMGIZMO_HPP
#define RES_TRANSFORMGIZMO_HPP


#include <array>
#include <type_traits>

#include "RES_EntityController.hpp"
#include "Platform/XOF_Platform.hpp"
#include "Core/XOF_DynamicPoolAllocator.hpp"
#include "Rendering/XOF_Transform.hpp"
#include "Collision&Physics/XOF_AABB.hpp"
#include "GameplayFoundations/XOF_Entity.hpp"
#include "GameplayFoundations/CAS_Components.hpp"

#include <glm/gtx/rotate_vector.hpp>

class Mesh;
class Material;
class Camera;


class TransformGizmo {
public:
    struct VisualRepresentation {
        Mesh        *mesh;
        Material    *material;
        Transform   transform;
    };

    enum class GIZMO_BOUNDING_VOLUMES : U32 {
        GIZMO = 0,
        X_AXIS,
        Y_AXIS,
        Z_AXIS,
        XZ_PLANE,
        XY_PLANE,
        YZ_PLANE,
        COUNT,
        // Used for selection tests
        NONE,
    };

                                                    TransformGizmo() {}
    virtual                                         ~TransformGizmo() {}

    virtual bool                                    Initialise() = 0;
    inline bool                                     IsInitialised() const;

                                                    // Camera corresponding to the viewport the gizmo is now being viewed from
    inline void                                     SetViewportCamera( const Camera *viewportCamera );

    inline void                                     SetSelectedEntity( EntityController *entity );
    inline void                                     ClearSelectedEntities();
    inline EntityController*                        GetSelectedEntity() const;    

    inline bool                                     IsActive() const;
    inline bool                                     IsSelected() const;

                                                    // Test for mouse-over on mouse move or click
    virtual bool                                    TestForMouseOver( const Ray& ray ) = 0;
    virtual bool                                    TestForSelection( const Ray& ray ) = 0;
                                                    // Fire on mouse button release
    inline void                                     Deselect();
                                                    // Respond to mouse movement if/when gizmo is selected
    virtual void                                    HandleMouseMovement( F32 xDelta, F32 yDelta ) = 0;

    inline TransformGizmo::VisualRepresentation*    GetVisualRepresentation();
    inline AABB*                                    GetBoundingVolumes();

protected:
    static constexpr U32                            mBoundVolumeCount{Underlying( GIZMO_BOUNDING_VOLUMES::COUNT )};
    std::array<AABB, mBoundVolumeCount>             mBoundingVolumes;
    GIZMO_BOUNDING_VOLUMES                          mSelectedAxis{GIZMO_BOUNDING_VOLUMES::NONE};

    // Perspective, left, top etc...
    Camera                                          *mCurrentViewportCamera{nullptr};
    VisualRepresentation                            mVisualRepresentation;

    EntityController                                *mSelectedEntity{nullptr};

    bool                                            mIsInitialised{false};
};


// Inlines
// ---
inline bool TransformGizmo::IsInitialised() const {
    return mIsInitialised;
}

inline void TransformGizmo::SetViewportCamera( const Camera *viewportCamera ) {
    mCurrentViewportCamera = const_cast<Camera*>( viewportCamera );
}

inline void TransformGizmo::SetSelectedEntity( EntityController *entityController ) {
    Entity *underlyingEntity = entityController->GetEntity();
    Transformable *transformable = (Transformable*)underlyingEntity->GetComponent( CAS_COMPONENTS::TRANSFORMABLE );

    glm::vec3 delta = transformable->GetTransform().translation - mVisualRepresentation.transform.translation;
    mVisualRepresentation.transform.translation = transformable->GetTransform().translation;

    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO)].Transform(  delta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS)].Transform( delta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS)].Transform( delta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS)].Transform( delta, nullptr );

    mSelectedEntity = entityController;
}

inline void TransformGizmo::ClearSelectedEntities() {
    mSelectedEntity->SetSelected( false );
    mSelectedEntity = nullptr;
}

inline EntityController* TransformGizmo::GetSelectedEntity() const {
    return reinterpret_cast<EntityController*>( mSelectedEntity );
}

inline bool TransformGizmo::IsActive() const {
    return ( mSelectedEntity != nullptr );
}

inline bool TransformGizmo::IsSelected() const {
    return ( mSelectedAxis != GIZMO_BOUNDING_VOLUMES::NONE );
}

inline TransformGizmo::VisualRepresentation* TransformGizmo::GetVisualRepresentation() {
    return reinterpret_cast<TransformGizmo::VisualRepresentation*>( &mVisualRepresentation );
}

inline AABB* TransformGizmo::GetBoundingVolumes() {
    return mBoundingVolumes.data();
}

inline void TransformGizmo::Deselect() {
    mBoundingVolumes[Underlying( mSelectedAxis )].IsIntersected( Ray( glm::vec3( 0.f ), glm::vec3( 0.f ) ) );
    mSelectedAxis = GIZMO_BOUNDING_VOLUMES::NONE;
}


#endif // RES_TRANSFORMGIZMO_HPP
