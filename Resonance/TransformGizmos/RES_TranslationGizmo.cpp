/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_TranslationGizmo.hpp
    Desc	:	Gizmo used to translate entities.

===============================================================================
*/


#include "Resources/CAS_ResourceDescriptors.hpp"
#include "Resources/XOF_ResourceManager.hpp"

#include "RES_TranslationGizmo.hpp"


// Setup the gizmo visual representation and its bounding volumes (axis, planes and the gizmo)
bool TranslationGizmo::Initialise() {
    ResourceManager *rm = ResourceManager::Get();

    MeshDescriptor meshDesc{"../Resources/Meshes/", "TranslationGizmo.obj"};
    meshDesc.retainVertexData = false;
    meshDesc.retainIndexData = false;

    MaterialDescriptor materialDesc{"../Resources/Meshes/", "TranslationGizmo.mtl"};
    materialDesc.shaders = {"../Resources/Shaders/", "RESONANCE_GIZMO_SHADER"};

    mVisualRepresentation.mesh = rm->LoadResource<Mesh>( meshDesc );
    XOF_ASSERT( mVisualRepresentation.mesh );

    mVisualRepresentation.material = rm->LoadResource<Material>( materialDesc );
    XOF_ASSERT( mVisualRepresentation.material );

    mVisualRepresentation.material->GetShader()->AddUniform( "transform" );
    mVisualRepresentation.material->GetShader()->AddUniform( "world" );

    mVisualRepresentation.material->GetShader()->AddUniform( "eyeWorldPos" );
    mVisualRepresentation.material->GetShader()->AddUniform( "isSelected" );

    // NOTE: Changes here require a corresponding change to translationFactors[1]
    mVisualRepresentation.transform.scale = glm::vec3( 0.5f );

    // Generate a bounding volume for the entire gizmo
    Mesh::Dimensions gizmoBoundingBoxDims = mVisualRepresentation.mesh->GetDimensions();
    gizmoBoundingBoxDims.min *= mVisualRepresentation.transform.scale;
    gizmoBoundingBoxDims.max *= mVisualRepresentation.transform.scale;
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].ScaleToFit( gizmoBoundingBoxDims );

    // Generate the bounding volumes for the axis
    {
        Mesh::Dimensions dimensions;
        const float scaleFactors[] { 0.125f, 1.75f };
        const float translateFactors[] { 0.f, 1.f };

        constexpr auto xAxis = Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS );
        constexpr auto zAxis = Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS );
        for( auto i=xAxis; i<=zAxis; ++i ) {
            glm::vec3 scale( scaleFactors[0] ); scale[i-1] = scaleFactors[1];

            dimensions.max = scale * mVisualRepresentation.transform.scale;
            dimensions.min = -dimensions.max;
            mBoundingVolumes[i].ScaleToFit( dimensions );

            glm::vec3 translation( translateFactors[0] ); translation[i-1] = translateFactors[1];
            mBoundingVolumes[i].Transform( translation, nullptr );
        }
    }
#if 0
    {
        Mesh::Dimensions dimensions;
        //const float scaleFactors[] {  };
        //const float translateFactors[] { 0.25f, 0.f, 0.f };

        for( U32 i=XZ_PLANE; i<XY_PLANE; ++i ) {
            glm::vec3 scale( 0.6f, 0.125f, 0.6f );

            dimensions.max = scale * mVisualRepresentation.transform.scale;
            dimensions.min = -dimensions.max;
            mBoundingVolumes[i].ScaleToFit( dimensions );

            glm::vec3 translation( 0.4f, 0.f, 0.4f );
            mBoundingVolumes[i].Transform( translation, nullptr );
        }
    }
#endif
    // Setup the bounding volumes for the gizmo planes (if applicable)...
    {
        // ...
    }

#if 1
    MeshDescriptor boundingVolMeshDesc{"../Resources/Meshes/", "UNIT_CUBE.obj"};

    MaterialDescriptor boundingVolMaterialDesc{"../Resources/Meshes/", "UNIT_CUBE.mtl"};
    boundingVolMaterialDesc.shaders = {"RES_BOUNDING_VOLUME_SHADER", "../Resources/Shaders/"};

    // Set visual representation for the gizmo' axis/plane bounding volumes
    Mesh *boundingVolumeMesh = rm->LoadResource<Mesh>( boundingVolMeshDesc );
    Material *boundingVolumeMaterial = rm->LoadResource<Material>( boundingVolMaterialDesc );
    boundingVolumeMaterial->GetShader()->AddUniform( "wvp" );

    constexpr auto boundingVolumeCount = Underlying( GIZMO_BOUNDING_VOLUMES::COUNT );
    for( auto i=0; i<boundingVolumeCount; ++i ) {
        mBoundingVolumes[i].GetVisualRepresentation()->mesh = boundingVolumeMesh;
        mBoundingVolumes[i].GetVisualRepresentation()->material = boundingVolumeMaterial;
    }
#endif

    return ( mIsInitialised = true );
}

bool TranslationGizmo::TestForMouseOver( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        return ( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ||
                 mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) );
    }
    return false;
}

bool TranslationGizmo::TestForSelection( const Ray& ray ) {
    if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].IsIntersected( ray ) ) {
        if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::X_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES:: Y_AXIS;
        } else if( mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsIntersected( ray ) ) {
            mSelectedAxis = GIZMO_BOUNDING_VOLUMES::Z_AXIS;
        }
        // TODO: XZ_PLANE
    }
    return ( mSelectedAxis != GIZMO_BOUNDING_VOLUMES::NONE );
}

void TranslationGizmo::HandleMouseMovement( float xDelta, float yDelta ) {
    Transformable *entityTransform = (Transformable*)mSelectedEntity->GetEntity()->GetComponent( CAS_COMPONENTS::TRANSFORMABLE );

    glm::vec3 originalTranslation( entityTransform->GetTransform().translation );
    glm::vec3 *translation = &entityTransform->GetTransform().translation;
/*
    float max = std::max<float>( std::abs( translation->x ), std::abs( xDelta ) );
    float min = std::min<float>( std::abs( translation->x ), std::abs( xDelta ) );

    XOF_LOG_MESSAGE( XOF_LOGGING_VERBOSITY::LOW, XOF_LOGGING_CHANNELS::WORLD_EDITOR_LC,
                     "Gizmo pos: %f | delta: %f", translation->x, xDelta );
*/
    // Move the visual representation of the gizmo
    switch( mSelectedAxis ) {
        case GIZMO_BOUNDING_VOLUMES::NONE: break;
        case GIZMO_BOUNDING_VOLUMES::X_AXIS: translation->x = xDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Y_AXIS: translation->y = yDelta; break;
        case GIZMO_BOUNDING_VOLUMES::Z_AXIS: translation->z = xDelta; break;


        case GIZMO_BOUNDING_VOLUMES::XZ_PLANE: {
            translation->x = xDelta;
            translation->z = -yDelta;
            break;
        }
    }

    // Now the gizmo collision volumes
    glm::vec3 translationDelta = *translation - originalTranslation;

    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::GIZMO )].Transform( translationDelta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::X_AXIS )].Transform( translationDelta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Y_AXIS )].Transform( translationDelta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::Z_AXIS )].Transform( translationDelta, nullptr );
    mBoundingVolumes[Underlying( GIZMO_BOUNDING_VOLUMES::XZ_PLANE )].Transform( translationDelta, nullptr );

    // Visual representation of the gizmo
    mVisualRepresentation.transform.translation = *translation;

    // Now the underlying entity
    mSelectedEntity->Translate( translation->x, translation->y, translation->z );
}
