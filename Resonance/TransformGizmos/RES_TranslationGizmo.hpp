/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_TranslationGizmo.hpp
    Desc	:	Gizmo used to translate entities.

===============================================================================
*/
#ifndef RES_TRANSLATIONGIZMO_HPP
#define RES_TRANSLATIONGIZMO_HPP


#include "RES_TransformGizmo.hpp"


class TranslationGizmo : public TransformGizmo {
public:
                    TranslationGizmo() {}
                    ~TranslationGizmo() {}

    virtual bool    Initialise() override;

                    // Test for mouse-over on mouse move or click
    virtual bool    TestForMouseOver( const Ray& ray ) override;
    virtual bool    TestForSelection( const Ray& ray ) override;

                    // Respond to mouse movement if/when gizmo is selected
    virtual void    HandleMouseMovement( F32 xDelta, F32 yDelta ) override;
};


#endif // RES_TRANSLATIONGIZMO_HPP
