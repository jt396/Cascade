#include <QMouseEvent>
#include <QMimeData>

#include "RES_Simulation.hpp"
#include "RES_Events.hpp"

#include "Platform/XOF_Timer.hpp"
#include "Resources/XOF_ResourceManager.hpp"
#include "Rendering/XOF_Renderer.hpp"
#include "GameplayFoundations/XOF_EntityManager.hpp"
#include "Core/XOF_Assert.hpp"

#include "QSFMLCanvas.hpp"


// TEMP DIRECTIONAL LIGHT
#include "../../Rendering/XOF_Lights.hpp"
DirectionalLight gTempDirectionalLight;

Timer gTempTimer;
double gTempThisTime = 0.0;
double gTempLastTime = 0.0;
double gTempTimeDelta = 0.0;

// TEMP - GRID
#include "res_tempgrid.hpp"
std::unique_ptr<Grid> gGrid;


QSFMLCanvas::QSFMLCanvas( QWidget *parent ) : QWidget( parent ), mIsInitialised( false ) {
    // Setup state to allow direct rendering into the widget
    QWidget::setAttribute( Qt::WA_PaintOnScreen );       // don't need Qt's painting functions
    QWidget::setAttribute( Qt::WA_OpaquePaintEvent );    // don't draw the widgets background (could cause flicker)
    QWidget::setAttribute( Qt::WA_NoSystemBackground );  // ^

    // Set strong focus so we receive keyboard events
    QWidget::setFocusPolicy( Qt::StrongFocus );

    QWidget::move( parent->pos() );
    QSize s = parent->size();
    QWidget::resize( s );
    // Setup timer
    mTimer.setInterval( /*frameTime*/0 );

    gTempTimer.Reset();
    gTempLastTime = gTempTimer.GetMilliseconds();

    //
    gTempDirectionalLight.ambientIntensity = 0.4f;
    gTempDirectionalLight.diffuseIntensity = 0.8f;
    gTempDirectionalLight.direction = glm::vec3( 1.f, -1.f, 0.f );
    gTempDirectionalLight.color = glm::vec3( 0.f, 0.f, 1.f );
    //

    setMouseTracking( true );
    setAcceptDrops( true );

    // Create SFML window with the widget handle
    Window::create( (sf::WindowHandle)QWidget::winId(), sf::ContextSettings( 24, 8 ) );
}

QSFMLCanvas::QSFMLCanvas( QWidget *parent, const QPoint& position, const QSize& size, unsigned int frameTime ) : QWidget( parent ), mIsInitialised( false ) {
    // Setup state to allow direct rendering into the widget

    // Don't need Qt's painting functions
    setAttribute( Qt::WA_PaintOnScreen );
    // Don't draw the widgets background (could cause flicker)
    setAttribute( Qt::WA_OpaquePaintEvent );
    setAttribute( Qt::WA_NoSystemBackground );

    // Set strong focus so we receive keyboard events
    setFocusPolicy( Qt::StrongFocus );

    // Setup widget geometry
    move( position );
    QWidget::resize( size );
    mSimulation->engineRuntime.renderer->SetScreenSize( size.width(), size.height() );

    // Setup timer
    mTimer.setInterval( frameTime );

    setMouseTracking( true );

    // Create SFML window with the widget handle
    Window::create( (sf::WindowHandle)QWidget::winId(), sf::ContextSettings( 24, 8 ) );
}

QSFMLCanvas::~QSFMLCanvas() {}

void QSFMLCanvas::SetSimulation( Simulation *simulation ) {
    mSimulation = simulation;
}

bool QSFMLCanvas::IsInitialised() const {
    return mIsInitialised;
}

QPaintEngine* QSFMLCanvas::paintEngine() const {
    return nullptr;
}

void QSFMLCanvas::showEvent( QShowEvent *event ) {
    Q_UNUSED( event );
    if( !mIsInitialised ) {
        // X11 specifics go here if needed...

        Window::setSize( sf::Vector2u( QWidget::size().width(), QWidget::size().height() ) );

        // Let the derived class do its specific stuff
        Init();

        // Setup the timer to trigger a refresh at the specified rate
        connect( &mTimer, SIGNAL( timeout() ), this, SLOT( repaint() ) );
        mTimer.start();

        mSimulation->engineRuntime.renderer->SetScreenSize( Window::getSize().x, Window::getSize().y );
        mSimulation->engineRuntime.renderer->SetClearColor( 0.5f, 0.5f, 0.5f, 1.f );

        mCamera.Setup( glm::vec3( 0.f, 2.5f, 7.5f ), glm::vec3( 0.f, 0.f, -1.f ),
                       static_cast<float>( Window::getSize().x ),  static_cast<float>( Window::getSize().y ),
                       static_cast<float>( Window::getSize().x ) / static_cast<float>( Window::getSize().y ),
                       0.1f, 1000.f );

        mCamera.mYaw = mCamera.mPitch = 0.f;

        mIsInitialised = true;

        gGrid.reset( new Grid() );
    }
}

void QSFMLCanvas::resizeEvent( QResizeEvent  *event ) {
    mSimulation->engineRuntime.renderer->SetScreenSize( event->size().width(), event->size().height() );
}

void QSFMLCanvas::Init() {}

// ---
void QSFMLCanvas::Update() {
    // Process events
    for( auto& event : mSimulation->events ) {
        for( auto& controller: mSimulation->entityControllers ) {
            controller->HandleEvent( event.get() );
        }
    }
    // Check for any replies
#if 1
    for( auto& event : mSimulation->events ) {
        //if( event.get()->replyCount ) {
            switch( event.get()->type ) {
                case RES_EVENTS::FIRED_PICKING_RAY: {
                    // If a gizmo is currently enabled
                    if( mTransformGizmo ) {
                        // If any entities replied, add them to the list of entities under the gizmo's influence
                        if( event.get()->replyCount > 0 ) {
                            Entity *e = (Entity*)(event.get()->replies[0].asVoidPtr);

                            for( U32 i=0; i<mSimulation->entityControllers.size(); ++i ) {
                                if( mSimulation->entityControllers[i].get()->GetEntity() == e ) {
                                    mSimulation->entityControllers[i]->SetSelected( true );
                                    mTransformGizmo->SetSelectedEntity( mSimulation->entityControllers[i].get() );
                                }
                            }
                        } else if( mTransformGizmo->IsActive() && event.get()->replyCount == 0 ) {
                            // Release any entities under the gizmo's influence
                            mTransformGizmo->ClearSelectedEntities();
                        }
                    }
                    break;
                }
            }
        //}
    }
#endif
    mSimulation->events.clear();
    // Update
    for( auto& controller: mSimulation->entityControllers ) {
        controller->Update( ( mSimulation->state == Simulation::STATE::PLAY )? gTempTimeDelta : 0.f );
    }
}

void QSFMLCanvas::TEMP_DrawTransformGizmo() {
    // Setup material/shaders
    TransformGizmo::VisualRepresentation *visualRep = mTransformGizmo->GetVisualRepresentation();

    visualRep->material->GetShader()->Bind();

    glm::mat4x4 t0( mCamera.GetProjectionMatrix() *
                    mCamera.GetViewMatrix() *
                    visualRep->transform.GetModelToWorldMatrix() );

    visualRep->material->GetShader()->SetUniform_mat4x4( "transform", t0 );

    visualRep->material->GetShader()->SetUniform_mat4x4( "world", (glm::mat4x4&)visualRep->transform.GetModelToWorldMatrix() );
    visualRep->material->GetShader()->SetUniform_vec3( "eyeWorldPos", (glm::vec3&)mCamera.GetPosition() );

    const std::vector<Mesh::SubMesh> gizmoSubMeshData = visualRep->mesh->GetSubMeshData();
    const U32 gizmoSubMeshCount = gizmoSubMeshData.size();
    const AABB *gizmoBoundingVolumes = mTransformGizmo->GetBoundingVolumes();

    // We need the gizmo to be rendered OVER the scene but also
    // need the depth test to be respected when rendering the gizmo itself
    glClear( GL_DEPTH_BUFFER_BIT );

    mSimulation->engineRuntime.renderer.get()->DEBUG_PushVAO( visualRep->mesh->GetVAO() );

    bool selectedAxisDrawn = false;
    for( U32 i=0; i<gizmoSubMeshCount; ++i ) {
        visualRep->material->GetShader()->SetUniform_bool( "isSelected", false );

        if( !selectedAxisDrawn ) {
            if( //gizmoSubMeshData[i].name[1] == 'z' && gizmoBoundingVolumes[TransformGizmo::XZ_PLANE].IsHit() ||
              ( gizmoSubMeshData[i].name[0] == 'x' && gizmoBoundingVolumes[Underlying( TransformGizmo::GIZMO_BOUNDING_VOLUMES::X_AXIS )].IsHit() ) ||
              ( gizmoSubMeshData[i].name[0] == 'y' && gizmoBoundingVolumes[Underlying( TransformGizmo::GIZMO_BOUNDING_VOLUMES::Y_AXIS )].IsHit() ) ||
              ( gizmoSubMeshData[i].name[0] == 'z' && gizmoBoundingVolumes[Underlying( TransformGizmo::GIZMO_BOUNDING_VOLUMES::Z_AXIS )].IsHit() ) )  {
                visualRep->material->GetShader()->SetUniform_bool( "isSelected", true );
                selectedAxisDrawn = true;
            }
        }

        mSimulation->engineRuntime.renderer.get()->DEBUG_DrawSubMesh(
            (const Mesh::SubMesh*)&gizmoSubMeshData[i], (const Material*)visualRep->material );
    }

    mSimulation->engineRuntime.renderer.get()->DEBUG_PopVAO();

    visualRep->material->GetShader()->Unbind();
#if 0
    // Draw bounding volumes
    for( U32 i=0; i<5; ++i ) {
        Material *boundingVolMaterial = gizmoBoundingVolumes[i].GetVisualRepresentation()->material;
        boundingVolMaterial->GetShader()->Bind();

        glm::mat4x4 MVP = mCamera.GetProjectionMatrix() * mCamera.GetViewMatrix() *
                            gizmoBoundingVolumes[i].GetVisualRepresentationTransform();

        boundingVolMaterial->GetShader()->SetUniform( "wvp", MVP );

        boundingVolMaterial->GetShader()->SetUniform(
            "colour", gizmoBoundingVolumes[i].IsHit() ? glm::vec4( 1.f, 0.f, 0.f, 1.f ) : glm::vec4( 0.f, 1.f, 0.f, 1.f ) );

        mSimulation->engineRuntime.renderer.get()->DEBUG_DrawBoundingVolume( gizmoBoundingVolumes[0].GetVisualRepresentation()->mesh );

        boundingVolMaterial->GetShader()->Unbind();
    }
#endif
}

void QSFMLCanvas::paintEvent( QPaintEvent *event ) {
    Q_UNUSED( event );

    gTempThisTime = gTempTimer.GetMilliseconds();
    gTempTimeDelta = gTempThisTime - gTempLastTime;

    // Let the derived class do its specific stuff
    Update();

    // Display on screen
    //mSimulation->engineRuntime.renderer->RenderFrame();
#if 1
    /*
    if( mTransformGizmo && mTransformGizmo->IsActive() ) {
        TEMP_DrawTransformGizmo();
        //mSimulation->engineRuntime.renderer->DEBUG_DrawTransformGizmo();
    }*/

    // TEMP - Draw grid
    //if( gGrid->mShader->IsLoaded() ) {
#if 1
        gGrid->mShader->Bind();

        glm::vec3 gridColors[] { glm::vec3( 0.f, 0.f, 1.f ), glm::vec3( 1.f, 0.f, 0.f ), glm::vec3( 0.75f ), glm::vec3( 0.5f ) };

        U32 quarterHalfPointLines = 16 * 4; /* gridSize (0.5 spacing), gridSize*2 (0.25 spacing) */

#if 1
        U32 drawHalfPointLines = 16 * 2; /* gridSize (0.5 spacing), gridSize*2 (0.25 spacing) */
        for( U32 i=0; i<2; ++i ) {
            gGrid->mTransform.rotation.y = 1.57f * static_cast<float>( i );

            gGrid->mShader->SetUniform_vec3( "col", gridColors[3] );

            gGrid->mShader->SetUniform_mat4x4( "wvp", (glm::mat4x4&)( mCamera.GetProjectionMatrix() *
                                               mCamera.GetViewMatrix() *
                                               gGrid->mTransform.GetModelToWorldMatrix() ) );

            mSimulation->engineRuntime.renderer->DEBUG_TEMPDrawEditorGrid( &gGrid->mMesh, quarterHalfPointLines, drawHalfPointLines );
        }
        gGrid->mTransform.rotation.y = 0.f;
#endif
        for( U32 i=0; i<2; ++i ) {
            gGrid->mTransform.rotation.y = 1.57f * static_cast<float>( i );

            gGrid->mShader->SetUniform_vec3( "col", gridColors[i] );

            gGrid->mShader->SetUniform_mat4x4( "wvp", (glm::mat4x4&)( mCamera.GetProjectionMatrix() *
                                               mCamera.GetViewMatrix() *
                                               gGrid->mTransform.GetModelToWorldMatrix() ) );

            mSimulation->engineRuntime.renderer->DEBUG_TEMPDrawEditorGrid(
                        &gGrid->mMesh,
                        quarterHalfPointLines + drawHalfPointLines,
                        2 );//gGrid->mMesh.mVertexData.size() - (quarterHalfPointLines+drawHalfPointLines) );
        }
        gGrid->mTransform.rotation.y = 0.f;

        for( U32 i=0; i<2; ++i ) {
            gGrid->mTransform.rotation.y = 1.57f * static_cast<float>( i );

            gGrid->mShader->SetUniform_vec3( "col", gridColors[2] );

            gGrid->mShader->SetUniform_mat4x4( "wvp", (glm::mat4x4&)( mCamera.GetProjectionMatrix() *
                                               mCamera.GetViewMatrix() *
                                               gGrid->mTransform.GetModelToWorldMatrix() ) );

            mSimulation->engineRuntime.renderer->DEBUG_TEMPDrawEditorGrid(
                        &gGrid->mMesh,
                        quarterHalfPointLines + drawHalfPointLines + 2,
                        gGrid->mMesh.mVertexData.size() - (quarterHalfPointLines+drawHalfPointLines+2) );
        }
        gGrid->mTransform.rotation.y = 0.f;

        gGrid->mShader->Unbind();
#endif
    //

#endif

    mSimulation->engineRuntime.renderer->RenderFrame();

    if( mTransformGizmo && mTransformGizmo->IsActive() ) {
        TEMP_DrawTransformGizmo();
        //mSimulation->engineRuntime.renderer->DEBUG_DrawTransformGizmo();
    }

    Window::display();
    mSimulation->engineRuntime.renderer->ClearScreen();

    gTempLastTime = gTempThisTime;
}
// ---

void QSFMLCanvas::mouseMoveEvent( QMouseEvent *event ) {
    static int prevX0 = event->x();
    static int prevX1 = prevX0;

    static int prevY0 = event->y();
    static int prevY1 = prevY0;

    if( mTransformGizmo && mTransformGizmo->IsSelected() ) {
        U32 viewportWidth = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferWidth;
        U32 viewportHeight = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferHeight;

        // Into 3D NDC space
        float x = ( 2.f * event->pos().x() ) / viewportWidth - 1.f;
        float y = -( ( 2.f * event->pos().y() ) / viewportHeight - 1.f );

        // TODO: WE NEED TO FACTOR IN THE DISTANCE FROM THE CAMERA TO THE GIZMO?
        // TODO: Factor in time delta?
        mTransformGizmo->HandleMouseMovement( 8.f * x, 8.f * y );
    } else {
        // Viewport camera movement
        // TODO: Needs to accout for time delta, speed should also be adjustable via GUI
        if( QWidget::hasFocus() ) {
            // x/y axis
            if( ( event->buttons() & Qt::LeftButton ) && ( event->buttons() & Qt::RightButton ) ) {
                // X AXIS
                if( event->x() < prevX1 && prevX1 < prevX0 ) {
                    mCamera.Translate( -0.05f, 0.f, 0.0f );
                } else if( event->x() > prevX1 && prevX1 > prevX0 ) {
                    mCamera.Translate( 0.05f, 0.f, 0.0f );
                }
                // Y AXIS
                if( event->y() < prevY1 && prevY1 < prevY0 ) {
                    mCamera.Translate( 0.f, 0.05f, 0.0f );
                } else if( event->y() > prevY1 && prevY1 > prevY0 ) {
                    mCamera.Translate( 0.f, -0.05f, 0.0f );
                }
            } else if( event->buttons() & Qt::LeftButton ) {
                // Z AXIS
                if( event->y() < prevY1 && prevY1 < prevY0 ) {
                    mCamera.Translate( 0.f, 0.f, -0.05f );
                } else if( event->y() > prevY1 && prevY1 > prevY0 ) {
                    mCamera.Translate( 0.f, 0.f, 0.05f );
                }
                // YAW
                if( event->x() < prevX1 && prevX1 < prevX0 ) {
                    mCamera.Yaw( 0.02f );
                } else if( event->x() > prevX1 && prevX1 > prevX0 ) {
                    mCamera.Yaw( -0.02f );
                }
            }
#if 0
            else if( event->buttons() & Qt::RightButton ) {
                // PITCH
                if( event->y() < prevY1 && prevY1 < prevY0 ) {
                    mCamera.Pitch( 0.02f );
                } else if( event->y() > prevY1 && prevY1 > prevY0 ) {
                    mCamera.Pitch( -0.02f );
                }
                // YAW
                if( event->x() < prevX1 && prevX1 < prevX0 ) {
                    mCamera.Yaw( 0.02f );
                } else if( event->x() > prevX1 && prevX1 > prevX0 ) {
                    mCamera.Yaw( -0.02f );
                }
#endif
        } // QWidget::hasFocus()

        // Should we fire a picking ray against the current gizmo?
        if( mTransformGizmo && mTransformGizmo->IsActive() ) {
            // TODO: If entities are in the camera frustum, then construct the ray and do the check...
            U32 viewportWidth = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferWidth;
            U32 viewportHeight = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferHeight;

            const float FORWARDS = -1.f;

            // into 3D NDC space
            float x = ( 2.f * event->pos().x() ) / viewportWidth - 1.f;
            float y = -( ( 2.f * event->pos().y() ) / viewportHeight - 1.f );
            float z = 1.f; // not strictly necessary yet
            glm::vec3 rayNDS( x, y, z );

            // Into 4D Homogeneous Clip space
            // NOTE: No need to reverse perspective divide here since this is a ray with no intrinsic depth
            glm::vec4 rayCLIP( rayNDS.x, rayNDS.y, FORWARDS, 1.f );

            // Into 4D Camera Coordinates;
            // Normally, you use a projection matrix to go from eye/camera space to clip space.
            // In this instance however we're going the other way, so we invert this matrix.
            glm::vec4 rayEYE( glm::inverse( mCamera.GetProjectionMatrix() ) * rayCLIP );
            // zw part = "forwards(FOWARDS)" and "not a point (0.f)"
            rayEYE = glm::vec4( rayEYE.x, rayEYE.y, FORWARDS, 0.f );

            // into 4D World Coordinates
            // -1 for forwards means the ray isn't normalised, normalise the vector here
            glm::vec3 rayWORLD( glm::inverse( mCamera.GetViewMatrix() ) * rayEYE );
            glm::normalize( rayWORLD );

            Ray ray( mCamera.GetPosition(), rayWORLD );
            ray.direction = glm::normalize( ray.direction );
    #if 0
            AABB *gizmoBBs = mTransformGizmo->GetBoundingVolumes();
            for( U32 i=0; i<4; ++i ) {
                gizmoBBs[i].IsIntersected( ray );
            }
    #endif
            mTransformGizmo->TestForMouseOver( ray );
        }
    }

    prevX0 = prevX1;
    prevX1 = event->x();

    prevY0 = prevY1;
    prevY1 = event->y();
}

void QSFMLCanvas::mousePressEvent( QMouseEvent *event ) {
    if( event->button() != Qt::LeftButton ) {
        return;
    }

    // TODO: If entities are in the camera frustum, then construct the ray and do the check...
    U32 viewportWidth = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferWidth;
    U32 viewportHeight = mSimulation->engineRuntime.renderer->GetScreenDimensions().mBufferHeight;

    const float FORWARDS = -1.f;

    // into 3D NDC space
    float x = ( 2.f * event->pos().x() ) / viewportWidth - 1.f;
    float y = -( ( 2.f * event->pos().y() ) / viewportHeight - 1.f );
    float z = 1.f; // not strictly necessary yet
    glm::vec3 rayNDS( x, y, z );

    // Into 4D Homogeneous Clip space
    // NOTE: No need to reverse perspective divide here since this is a ray with no intrinsic depth
    glm::vec4 rayCLIP( rayNDS.x, rayNDS.y, FORWARDS, 1.f );

    // Into 4D Camera Coordinates;
    // Normally, you use a projection matrix to go from eye/camera space to clip space.
    // In this instance however we're going the other way, so we invert this matrix.
    glm::vec4 rayEYE( glm::inverse( mCamera.GetProjectionMatrix() ) * rayCLIP );
    // zw part = "forwards(FOWARDS)" and "not a point (0.f)"
    rayEYE = glm::vec4( rayEYE.x, rayEYE.y, FORWARDS, 0.f );

    // into 4D World Coordinates
    // -1 for forwards means the ray isn't normalised, normalise the vector here
    glm::vec3 rayWORLD( glm::inverse( mCamera.GetViewMatrix() ) * rayEYE );
    glm::normalize( rayWORLD );

    Ray ray( mCamera.GetPosition(), rayWORLD );
    ray.direction = glm::normalize( ray.direction );

    // Check against the transform gizmo first (if one is present)
    if( mTransformGizmo && mTransformGizmo->IsActive() ) {
        if( mTransformGizmo->TestForSelection( ray ) ) {
            return;
        } else if( mTransformGizmo->IsSelected() ){
            mTransformGizmo->Deselect();
        }
    }

    // No gizmo is currently active, check against entities in the scene/camera frustum
    std::unique_ptr<Event> firedPickingRay( new FiredPickingRay( ray ) );
    mSimulation->events.push_back( std::move( firedPickingRay ) );
}

void QSFMLCanvas::mouseReleaseEvent( QMouseEvent *event ) {
    static_cast<void>( event );
    if( event->button() == Qt::LeftButton ) {
        if( mTransformGizmo && mTransformGizmo->IsSelected() ) {
            mTransformGizmo->Deselect();
        }
    }
}

void QSFMLCanvas::keyPressEvent( QKeyEvent *event ) {
    float translateAmount = 0.5f;

    switch( event->key() ) {
        case Qt::Key_W: mCamera.Translate( 0.f, 0.f, translateAmount ); break;
        case Qt::Key_A: break;
        case Qt::Key_S: mCamera.Translate( 0.f, 0.f, -translateAmount ); break;
        case Qt::Key_D: break;

        case Qt::Key_Q: mSimulation->engineRuntime.renderer->SetCullMode( GL_BACK ); break;
        case Qt::Key_E: mSimulation->engineRuntime.renderer->SetCullMode( GL_FRONT ); break;
        case Qt::Key_R: mSimulation->engineRuntime.renderer->SetWindingMode( GL_CW ); break;
        case Qt::Key_T: mSimulation->engineRuntime.renderer->SetWindingMode( GL_CCW ); break;
    }
}

void QSFMLCanvas::dragEnterEvent( QDragEnterEvent *event ) {
    if( event->mimeData()->text() != "MESH" ) {
        return;
    }
    event->acceptProposedAction();
}

void QSFMLCanvas::dropEvent( QDropEvent *event ) {
    static_cast<void>( event );
}
