#ifndef QSFMLCANVAS_HPP
#define QSFMLCANVAS_HPP


#include <QtWidgets/qwidget.h>
#include <QtCore/qtimer.h>
#include <QInputEvent>

#include <SFML/Graphics.hpp>

#include "Rendering/XOF_FirstPersonCamera.hpp"
#include "TransformGizmos/RES_TranslationGizmo.hpp"

struct Simulation;


class QSFMLCanvas : public QWidget, public sf::Window {
public:
                            QSFMLCanvas( QWidget *parent );
                            QSFMLCanvas( QWidget *parent, const QPoint& position, const QSize& size, unsigned int frameTime = 0 );
    virtual                 ~QSFMLCanvas();

    void                    SetSimulation( Simulation *simulation );
    inline void             SetCurrentTransformGizmo( TransformGizmo *gizmo );

    bool                    IsInitialised() const;

    inline Camera*          GetCamera() const;

private:
    QTimer                  mTimer;
    bool                    mIsInitialised;

    Simulation              *mSimulation;

    TransformGizmo          *mTransformGizmo{nullptr};

                            // NOTE: What to do with this?
    FirstPersonCamera       mCamera;

    virtual void            Init();
    virtual void            Update();

    virtual QPaintEngine*   paintEngine() const;
    virtual void            showEvent( QShowEvent *event );
    virtual void            paintEvent( QPaintEvent *event );
    virtual void            resizeEvent( QResizeEvent *event );

    virtual void            mouseMoveEvent( QMouseEvent *event );
    virtual void            mousePressEvent( QMouseEvent *event );
    virtual void            mouseReleaseEvent( QMouseEvent *event );

    virtual void            keyPressEvent( QKeyEvent *event );

    virtual void            dragEnterEvent( QDragEnterEvent *event ) override;
    virtual void            dropEvent( QDropEvent *event ) override;

    void                    TEMP_DrawTransformGizmo();
};


inline void QSFMLCanvas::SetCurrentTransformGizmo( TransformGizmo *gizmo ) {
    if( gizmo && gizmo != mTransformGizmo ) {
        if( !gizmo->IsInitialised() ) {
            gizmo->Initialise();
        }
        if( mTransformGizmo && mTransformGizmo->IsActive() ) {
            gizmo->SetSelectedEntity( mTransformGizmo->GetSelectedEntity() );
        }
        gizmo->SetViewportCamera( &mCamera );
        mTransformGizmo = gizmo;
    }
}


inline Camera* QSFMLCanvas::GetCamera() const {
    return reinterpret_cast<Camera*>( const_cast<FirstPersonCamera*>( &mCamera ) );
}


#endif // QSFMLCANVAS_HPP
