#include <memory>

#include <QDateTime>
#include <QDir>
#include <QFileDialog>
#include <QLabel>
#include <QTimer>
#include <QMouseEvent>
#include <QDrag>
#include <QMimeData>

#include "RES_AssetBrowser.hpp"
#include "ui_RES_AssetBrowser.h"


// TODO: Replace this with something else
static const QString gTEMPIconPath{"./Icons/"};

// Asset Browser base --------------------------------------------------------------------------------------------------------------------------- START
AssetPreview::AssetPreview( const QString& text ) : mText{text} {}

QString AssetPreview::GetText() const {
    return mText.text();
}

void AssetPreview::SetPixmap( const QString& path ) {
    mPixmap.setPixmap( QPixmap{path}.scaled( 64, 64 ) );
}

void AssetPreview::GenerateLayout() {
    mText.setAlignment( Qt::AlignCenter );
    mPixmap.setAlignment( Qt::AlignCenter );

    mLayout.addWidget( &mPixmap );
    mLayout.addWidget( &mText );

    setLayout( &mLayout );
}

void AssetPreview::enterEvent( QEvent *event ) {
    static_cast<void>( event );
    emit entered( this );
}

void AssetPreview::leaveEvent( QEvent *event ) {
    static_cast<void>( event );
    emit left( this );
}

void AssetPreview::mouseMoveEvent( QMouseEvent *event ) {
    if( !( event->buttons() & Qt::LeftButton ) ) {
        return;
    }
    int x = QApplication::startDragDistance();
    if( ( event->pos() - mMouseLeftClickPos ).manhattanLength() < x ) {
        return;
    }

    QDrag *drag = new QDrag( this );
    QMimeData *mimeData = new QMimeData();

    drag->setMimeData( mimeData );
    drag->setPixmap( *(mPixmap.pixmap()) );

    Qt::DropAction dropAction = drag->exec();
    // ...
}

void AssetPreview::mousePressEvent( QMouseEvent *event ) {
    if( ( event->button() == Qt::LeftButton ) && mPixmap.geometry().contains( event->pos() ) ) {
        mMouseLeftClickPos = event->pos();
    }
    emit pressed( this );
}
// ------------------------------------------------------------------------
AssetBrowser::AssetBrowser( const QStringList &filters, QWidget *parent ) :
    QWidget{parent}, ui{new Ui::AssetBrowser}, mFilters{filters}
{
    ui->setupUi( this );
}

AssetBrowser::~AssetBrowser() {
    delete ui;
}

QString* AssetBrowser::GetDirectoryPath() const {
    return const_cast<QString*>( &mDirectoryPath );
}

void AssetBrowser::on_changeDirectoryPathButton_released() {
    const QString newPath {QFileDialog::getExistingDirectory( this, "Select Asset Directory...", ui->directoyPathLineEdit->text() )};
    if( !newPath.isEmpty() ) {
        PopulateDirectoryContentsTable( newPath );
    }
}

void AssetBrowser::on_directoyPathLineEdit_returnPressed() {
    PopulateDirectoryContentsTable( ui->directoyPathLineEdit->text() );
}

void AssetBrowser::PopulateDirectoryContentsTable( const QString& path ) {
    QDir directory {path};
    if( !directory.exists() || directory.isEmpty() || path == mDirectoryPath ) {
        return;
    }
    mDirectoryPath = path;

    directory.setFilter( QDir::Files | QDir::NoDotAndDotDot );
    directory.setNameFilters( mFilters );

    // Fill in contents table with preview images of the assets in the given directory
    const QFileInfoList list {directory.entryInfoList()};
    const auto listSize {list.size()};

    const auto columnCount {3};
    ui->directoryContentsTable->setColumnCount( columnCount );
    ui->directoryContentsTable->setRowCount( ( listSize / columnCount ) + ( listSize % columnCount ) );

    ui->directoryContentsTable->setShowGrid( false );

    ui->directoryContentsTable->horizontalHeader()->hide();
    ui->directoryContentsTable->horizontalHeader()->setSectionResizeMode( QHeaderView::Stretch );
    ui->directoryContentsTable->verticalHeader()->hide();
    ui->directoryContentsTable->verticalHeader()->setSectionResizeMode( QHeaderView::ResizeToContents );

    for( auto i=0; i<listSize; ++i ) {
        const QFileInfo& fileInfo {list.at( i )};

        AssetPreview *preview {new AssetPreview( fileInfo.fileName() )};
        mAssetPreviewSetup( fileInfo, preview );
        preview->GenerateLayout();

        ui->directoryContentsTable->setCellWidget( i / columnCount, i % columnCount, preview );
    }

    // Set line-edit directory path
    ui->directoyPathLineEdit->setText( path );
}
// Asset Browser base --------------------------------------------------------------------------------------------------------------------------- END

// Texture Browser ------------------------------------------------------------------------------------------------------------------------------ START
TextureBrowser::TextureBrowser( const QString &path, const QStringList &filters, QWidget *parent ) : AssetBrowser( filters, parent ) {
    mAssetPreviewSetup = [this]( const QFileInfo& fileInfo, AssetPreview *preview ) {
        // Setup tool-tip preview with texture information
        connect( preview, &AssetPreview::entered, [this]( AssetPreview *preview ){
            const QString assetName{preview->GetText()};
            if( mNameOfAssetInFocus == assetName ) {
                return;
            }
            mNameOfAssetInFocus = assetName;

            const TextureDetails& pixmapDetails {mAssetDetails.find( mNameOfAssetInFocus )->second};

            QString toolTipText;

            toolTipText += "<p style='white-space:pre'>";

            toolTipText += "Path: "    + QString( "<b>" + mDirectoryPath                                      + "</b>\n" );
            toolTipText += "Name: "    + QString( "<b>" + mNameOfAssetInFocus                                 + "</b>\n" );
            toolTipText += "Size: "    + QString( "<b>" + QString::number( pixmapDetails.sizeInKB / 1024 )    + " (kB) </b>\n" );
            toolTipText += "Width: "   + QString( "<b>" + QString::number( pixmapDetails.width )              + "</b>\n" );
            toolTipText += "Height: "  + QString( "<b>" + QString::number( pixmapDetails.height )             + "</b>\n" );

            toolTipText += "</p>";

            setToolTip( toolTipText );
        } );

        preview->SetPixmap( fileInfo.absoluteFilePath() );

        // Store data about the original texture, unfortunately entails a second load of the texture...
        const QPixmap pixmap{fileInfo.absoluteFilePath()};
        mAssetDetails.insert( std::pair<QString, TextureDetails>( fileInfo.fileName(), {pixmap.width(), pixmap.height(), fileInfo.size()} ) );
    };
    PopulateDirectoryContentsTable( path );
}

TextureBrowser::~TextureBrowser() {}
// Texture Browser ------------------------------------------------------------------------------------------------------------------------------ END

// Audio Browser -------------------------------------------------------------------------------------------------------------------------------- START
AudioBrowser::AudioBrowser( const QString &path, const QStringList &filters, QWidget *parent ) : AssetBrowser( filters, parent ) {
    mAssetPreviewSetup = [this]( const QFileInfo& fileInfo, AssetPreview *preview ) {
        static_cast<void>( fileInfo );

        connect( preview, &AssetPreview::entered, [this]( AssetPreview *label ){
            label->SetPixmap( gTEMPIconPath + "AudioPlaying_128x128.png" );
            mIcon = Icon::playing;

            ResourceDescriptor musicDesc {"../Resources/Audio/", label->GetText().toStdString().c_str()};
            mCurrentAudioTrack.Load( &musicDesc );
            mCurrentAssetPreview = label;
        } );
        connect( preview, &AssetPreview::left, [this]( AssetPreview *label ){
            label->SetPixmap( gTEMPIconPath + "AudioPreview_128x128.png" );
            mCurrentAudioTrack.Stop();
            mIcon = Icon::preview;
        } );
        connect( preview, &AssetPreview::pressed, [this]( AssetPreview *label ){
            QString iconFileName;
            switch( mIcon ) {
                case Icon::playing: {
                    mCurrentAudioTrack.Play();

                    QTimer::singleShot( mCurrentAudioTrack.GetDuration() * 1000.f, this, [this](){
                        if( mIcon == Icon::stopped ) {
                            mCurrentAssetPreview->SetPixmap( gTEMPIconPath + "AudioPlaying_128x128.png" );
                            mIcon = Icon::playing;
                        }
                    } );

                    iconFileName = "AudioStopped_128x128.png";
                    mIcon = Icon::stopped;
                    break;
                }
                case Icon::stopped: {
                    mCurrentAudioTrack.Stop();
                    iconFileName = "AudioPlaying_128x128.png";
                    mIcon = Icon::playing;
                    break;
                }
            }
            label->SetPixmap( gTEMPIconPath + iconFileName );
        } );

        preview->SetPixmap( gTEMPIconPath + "AudioPreview_128x128.png" );
    };
    PopulateDirectoryContentsTable( path );
}

AudioBrowser::~AudioBrowser() {}
// Audio Browser -------------------------------------------------------------------------------------------------------------------------------- END

// Material Browser ----------------------------------------------------------------------------------------------------------------------------- START
MaterialBrowser::MaterialBrowser( const QString &path, const QStringList &filters, QWidget *parent ) : AssetBrowser( filters, parent ) {
    mAssetPreviewSetup = [this]( const QFileInfo& fileInfo, AssetPreview *preview ) {
        static_cast<void>( fileInfo );
        preview->SetPixmap( gTEMPIconPath + "PreviewUnavailable_128x128.png" );
    };
    PopulateDirectoryContentsTable( path );
}

MaterialBrowser::~MaterialBrowser() {}
// Material Browser ----------------------------------------------------------------------------------------------------------------------------- END

// Mesh Browser --------------------------------------------------------------------------------------------------------------------------------- START
MeshBrowser::MeshBrowser( const QString &path, const QStringList &filters, QWidget *parent ) : AssetBrowser( filters, parent ) {
    mAssetPreviewSetup = [this]( const QFileInfo& fileInfo, AssetPreview *preview ) {
        static_cast<void>( fileInfo );
        preview->SetPixmap( gTEMPIconPath + "PreviewUnavailable_128x128.png" );
    };
    PopulateDirectoryContentsTable( path );
}

MeshBrowser::~MeshBrowser() {}
// Mesh Browser --------------------------------------------------------------------------------------------------------------------------------- END
