#ifndef RES_ASSETBROWSER_HPP
#define RES_ASSETBROWSER_HPP


#include <map>

#include <QLabel>
#include <QWidget>
#include <QVBoxLayout>
#include <QFileSystemModel>

#include "Resources/CAS_ResourceTypes.hpp"

class QTableWidget;
class QTableWidgetItem;

namespace Ui {
    class AssetBrowser;
}


// Asset Browser base --------------------------------------------------------------------------------------------------------------------------- START
// Label storing text-based asset name
class AssetBrowser;

class AssetPreview : public QWidget {
    Q_OBJECT
public:
    explicit        AssetPreview( const QString& text );

    QString         GetText() const;
    void            SetPixmap( const QString& path );

    QLabel*         GetTextLabel() { return &mText; }
    QLabel*         GetPixmapLabel() { return &mPixmap; }

    void            GenerateLayout();
signals:
    void            entered( AssetPreview *preview );
    void            left( AssetPreview *preview );
    void            pressed( AssetPreview *preview );
private:
    QLabel          mText;
    QLabel          mPixmap;
    QVBoxLayout     mLayout;

    QPoint          mMouseLeftClickPos;

    virtual void    enterEvent( QEvent *event ) override;
    virtual void    leaveEvent( QEvent *event ) override;
    virtual void    mouseMoveEvent( QMouseEvent *event ) override;
    virtual void    mousePressEvent( QMouseEvent *event ) override;
};

class AssetBrowser : public QWidget {
    Q_OBJECT
public:
    friend class        AssetPreview;

                        AssetBrowser( const QStringList &filters, QWidget *parent = nullptr );
    virtual             ~AssetBrowser();

    QString*            GetDirectoryPath() const;

private slots:
    void                on_changeDirectoryPathButton_released();
    void                on_directoyPathLineEdit_returnPressed();

protected:
    Ui::AssetBrowser*   ui;

    QString             mDirectoryPath;
    QStringList         mFilters;

    QString             mNameOfAssetInFocus;

    using AssetPreviewSetter = std::function<void( const QFileInfo& file, AssetPreview *preview )>;
    AssetPreviewSetter  mAssetPreviewSetup;

    void                PopulateDirectoryContentsTable( const QString& path );
};
// Asset Browser base --------------------------------------------------------------------------------------------------------------------------- END

// Texture Browser ------------------------------------------------------------------------------------------------------------------------------ START
class TextureBrowser : public AssetBrowser {
public:
                    TextureBrowser( const QString &path, const QStringList &filters, QWidget *parent = nullptr );
                    ~TextureBrowser();
private:
    // We'll have to scale-down each pixmap for the preview,
    // so store some basic information about each asset now.
    struct TextureDetails {
        int     width;
        int     height;
        qint64  sizeInKB;
    };
    std::map<QString, TextureDetails>    mAssetDetails;
};
// Texture Browser ------------------------------------------------------------------------------------------------------------------------------ END

// Audio Browser -------------------------------------------------------------------------------------------------------------------------------- START
#include "../../Audio/XOF_Music.hpp"

class AudioBrowser : public AssetBrowser {
public:
                    AudioBrowser( const QString &path, const QStringList &filters, QWidget *parent = nullptr );
                    ~AudioBrowser();
private:
                    enum class Icon {
                        preview,
                        playing,
                        stopped,
                    };
    Icon            mIcon {Icon::preview};

    Music           mCurrentAudioTrack;
    AssetPreview*   mCurrentAssetPreview;
};
// Audio Browser -------------------------------------------------------------------------------------------------------------------------------- END

// Material Browser ----------------------------------------------------------------------------------------------------------------------------- START
class MaterialBrowser : public AssetBrowser {
public:
    MaterialBrowser( const QString &path, const QStringList &filters, QWidget *parent = nullptr );
    ~MaterialBrowser();
};
// Material Browser ----------------------------------------------------------------------------------------------------------------------------- END

// Mesh Browser --------------------------------------------------------------------------------------------------------------------------------- START
class MeshBrowser : public AssetBrowser {
public:
    MeshBrowser( const QString &path, const QStringList &filters, QWidget *parent = nullptr );
    ~MeshBrowser();
};
// Mesh Browser --------------------------------------------------------------------------------------------------------------------------------- END


#endif // RES_ASSETBROWSER_HPP
