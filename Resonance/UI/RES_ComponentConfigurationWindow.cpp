#include "RES_Simulation.hpp"

#include "RES_ComponentConfigurationWindow.hpp"
#include "ui_RES_ComponentConfigurationWindow.h"


ComponentConfigurationWindow::ComponentConfigurationWindow( Entity *entity, Component *component,
                                                            Simulation *simulation, QWidget *parent ) :
    mEntity{entity}, mComponent{component}, mSimulation{simulation},
    QWidget( parent ), ui( new Ui::ComponentConfigurationWindow )
{
    ui->setupUi(this);

    // hide maximize and minimize
    setWindowFlags( Qt::WindowCloseButtonHint );

    setWindowTitle( QString( "Configure " ).append( component->DEBUG_GetTypeAsString() ) );
}

ComponentConfigurationWindow::~ComponentConfigurationWindow() {
    delete ui;
}

void ComponentConfigurationWindow::AddToPropertyTable( Entity *entity, const ReflectedClass *reflectedClass, void *component, void *table ) {
    // Start with base classes (if any)
    for( U32 i=0; i<reflectedClass->baseClassCount; ++i ) {
        AddToPropertyTable( entity, (const ReflectedClass*)reflectedClass->baseClasses[i], component, table );
    }
    // Now the actual class itself
    for( U32 i=0; i<reflectedClass->reflectedVariableCount; ++i ) {
        const ReflectedVariable *variable = &reflectedClass->memberVariables[i];
        // Handle members which are themselves class instances
        if( variable->isClassInstance ) {
            // Arrays of instances (like arrays of textures within a material) must all be handled
            for( U32 j=0; j<variable->count; ++j ) {
                // Component address + offset + (sizeof element * j)
                if( variable->isRawPointer ) {
                    U32 *p = reinterpret_cast<U32*>( *reinterpret_cast<U32*>( (size_t)component + variable->memoryOffset + ( sizeof( p ) * j ) ) );
                    if( !p ) {
                        continue;
                    }
                    AddToPropertyTable( entity, variable->classInstance, (void*)&(*p), table );
                } else {
                    AddToPropertyTable( entity, variable->classInstance, (void*)((size_t)component + variable->memoryOffset +
                        ( mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( variable->typeAsHash )->size * j ) ), table );
                }
            }
        } else {
            // Now insert the variable name and value into the scene-graph tree
            QTableWidget *tableWidget = (QTableWidget*)table;
            QTableWidgetItem *propertyItem = new QTableWidgetItem( variable->nameAsString );

            tableWidget->insertRow( tableWidget->rowCount() );
            tableWidget->setItem( tableWidget->rowCount()-1, 0, propertyItem );

            auto getter = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( variable->typeAsHash )->Get;
            // Use the data-type appropriate getter() function to insert the variables value into the property table
            if( getter ) {
                getter( entity, (CHAR*)reflectedClass->nameAsString, component, (CHAR*)variable, (void*)table );
            }
        }
    }
}

void ComponentConfigurationWindow::PopulatePropertyTable() {
    ReflectedClass *componentReflectionData{mSimulation->engineRuntime.reflectionManager->GetReflectedClass( mComponent->DEBUG_GetTypeAsString() )};
    AddToPropertyTable( mEntity, componentReflectionData, (void*)mComponent, (void*)ui->componentPropertyTableWidget );
}

void ComponentConfigurationWindow::on_cancelPushButton_clicked() {
    close();
}

void ComponentConfigurationWindow::on_applyPushButton_clicked() {}
