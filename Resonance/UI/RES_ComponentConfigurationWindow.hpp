#ifndef RES_COMPONENTCONFIGURATIONWINDOW_HPP
#define RES_COMPONENTCONFIGURATIONWINDOW_HPP


#include <QWidget>

struct Simulation;
class Entity;
class Component;
struct ReflectedClass;

namespace Ui {
class ComponentConfigurationWindow;
}


class ComponentConfigurationWindow : public QWidget
{
    Q_OBJECT

public:
    ComponentConfigurationWindow( Entity *entity, Component *component, Simulation *simulation, QWidget *parent = nullptr );
    ~ComponentConfigurationWindow();

    void PopulatePropertyTable();

private slots:
    void on_cancelPushButton_clicked();
    void on_applyPushButton_clicked();

private:
    Ui::ComponentConfigurationWindow *ui;

    Simulation  *mSimulation{nullptr};
    Entity      *mEntity;
    Component   *mComponent;

    void        AddToPropertyTable( Entity *entity, const ReflectedClass *reflectedClass, void *component, void *table );
};

#endif // RES_COMPONENTCONFIGURATIONWINDOW_HPP
