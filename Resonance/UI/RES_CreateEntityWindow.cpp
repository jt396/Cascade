#include "RES_ComponentConfigurationWindow.hpp"
#include "RES_Simulation.hpp"

#include "RES_CreateEntityWindow.hpp"
#include "ui_RES_CreateEntityWindow.h"


CreateEntityWindow::CreateEntityWindow( Simulation *simulation, QWidget *parent ) :
    mSimulation{simulation}, QWidget(parent), ui(new Ui::CreateEntityWindow)
{
    ui->setupUi(this);

    // hide maximize and minimize
    setWindowFlags( Qt::WindowCloseButtonHint );

    // populate lists with the names of the available components
    ReflectionManager::ReflectedClassStore& reflectedClasses{mSimulation->engineRuntime.reflectionManager->GetReflectedClasses()};

    for( const auto& pair : reflectedClasses ) {
        if( pair.second.baseClassCount == 1 ) {
            const std::string baseClassName{pair.second.baseClasses[0]->nameAsString};
            if( baseClassName.find( "Component" ) != std::string::npos ) {
                ui->availableDataCompList->addItem( pair.second.nameAsString );
            }
        }
    }

    //ui->availableDataCompList->addItems( dataComponentNames );
    //ui->availableLogicCompList->addItems( logicComponentNames );
}

CreateEntityWindow::~CreateEntityWindow() {
    delete ui;
}

void CreateEntityWindow::closeEvent( QCloseEvent *event ) {
    XOF_UNUSED_PARAMETER( event );
    mComponentConfigurationWindow.reset( nullptr );
}

void CreateEntityWindow::on_cancelPushButton_clicked() {
    close();
}

// ---
using TEMP_NewComponentPtr = std::unique_ptr<Component>;
TEMP_NewComponentPtr TEMP_CreateDefaultComponentForNewEntity( const QString& componentTypeName ) {
    TEMP_NewComponentPtr component{nullptr};

    if( componentTypeName == "Renderable" ) {
        component.reset( new Renderable( "DEFAULT", nullptr, nullptr, nullptr ) );
    } else if( componentTypeName == "Transformable" ) {
        component.reset( new Transformable() );
    } else {
        XOF_LOG_MESSAGE( LoggingVerbosity::high, LoggingChannels::worldEditor,
                         "TEMP_CreateDefaultComponentForNewEntity( %s ) -> Failed! Component type not found!",
                         componentTypeName.toStdString().c_str() );
    }

    return component;
}
// ---

// TODO: Generic swap code to move items between these lists
void CreateEntityWindow::on_addDataCompPushButton_clicked() {
    const auto selectedComponents{ui->availableDataCompList->selectedItems()};
    for( const auto& component : selectedComponents ) {
        // move the component from available list to in-use list
        auto item{ui->availableDataCompList->takeItem( ui->availableDataCompList->row( component ) )};
        ui->inUseDataCompList->insertItem( ui->inUseDataCompList->count(), item );
        // create the component
        mNewEntityComponents.insert(
            {component->text().toStdString(), TEMP_CreateDefaultComponentForNewEntity( component->text() )}
        );
    }

    if( ui->availableDataCompList->count() == 0 ) {
        ui->addDataCompPushButton->setEnabled( false );
    }

    if( !ui->removeDataCompPushButton->isEnabled() ) {
        ui->removeDataCompPushButton->setEnabled( true );
    }
}

void CreateEntityWindow::on_removeDataCompPushButton_clicked(){
    // Move selected components to the availableComponents list
    const auto selectedComponents{ui->inUseDataCompList->selectedItems()};
    for( const auto& component : selectedComponents ) {
        auto item{ui->inUseDataCompList->takeItem( ui->inUseDataCompList->row( component ) )};
        ui->availableDataCompList->insertItem( ui->inUseDataCompList->count(), item );
    }

    if( ui->inUseDataCompList->count() == 0 ) {
        ui->removeDataCompPushButton->setEnabled( false );
    }

    if( !ui->addDataCompPushButton->isEnabled() ) {
        ui->addDataCompPushButton->setEnabled( true );
    }
}

void CreateEntityWindow::on_inUseDataCompList_itemDoubleClicked( QListWidgetItem *item ) {
    mComponentConfigurationWindow.reset( new ComponentConfigurationWindow( nullptr,
                                                                           mNewEntityComponents.find( item->text().toStdString() )->second.get(),
                                                                           mSimulation ) );
    mComponentConfigurationWindow->PopulatePropertyTable();
    mComponentConfigurationWindow->show();
}
