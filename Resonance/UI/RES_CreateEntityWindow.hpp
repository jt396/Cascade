#ifndef RES_CREATEENTITYWINDOW_HPP
#define RES_CREATEENTITYWINDOW_HPP


#include <unordered_map>

#include <QWidget>
#include <QStringList>

#include "GameplayFoundations/CAS_Components.hpp"

struct Simulation;
class Component;
class QListWidgetItem;
class ComponentConfigurationWindow;


namespace Ui {
class CreateEntityWindow;
}

class CreateEntityWindow : public QWidget {
    Q_OBJECT

public:
                        explicit CreateEntityWindow( Simulation *simulation, QWidget *parent = nullptr );
                        ~CreateEntityWindow();

protected:
    void                closeEvent( QCloseEvent *event ) override;

private slots:
    void                on_cancelPushButton_clicked();
    void                on_addDataCompPushButton_clicked();
    void                on_removeDataCompPushButton_clicked();

    void                on_inUseDataCompList_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::CreateEntityWindow *ui;

    Simulation *mSimulation;
    std::unordered_map<std::string, std::unique_ptr<Component>> mNewEntityComponents;
    std::unique_ptr<ComponentConfigurationWindow>   mComponentConfigurationWindow;
};

#endif // RES_CREATEENTITYWINDOW_HPP
