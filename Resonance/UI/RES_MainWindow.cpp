/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_MainWindow.cpp
    Desc	:	Editor main window.

===============================================================================
*/
#include "RES_MainWindow.hpp"
#include "ui_RES_MainWindow.h"

#include "RES_Events.hpp"
#include "GameplayFoundations/CAS_Components.hpp"

#include <limits>

#include <QtCore>
#include <qdir.h>
#include <qstring.h>
#include <QtWidgets>

#include "UI/RES_OutputTerminal.hpp"
#include "RES_SceneGraph.hpp"


static const unsigned int PROP_NAME_COL_INDEX = 0;
static const unsigned int PROP_VALUE_COL_INDEX = 1;


EditorWindow::EditorWindow( QWidget *parent ) : QMainWindow(parent), ui(new Ui::EditorWindow )  {
    // Editor UI start up --- START
    ui->setupUi(this);

    // Init integrated asset browser
    mAssetBrowserDockWidget.reset( new QDockWidget( "Asset Browser", this ) );
    mAssetBrowserDockWidget->setAllowedAreas( Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea );
    // Don't allow the terminal to span across the entiriety of the window
    setCorner( Qt::Corner::BottomLeftCorner, Qt::LeftDockWidgetArea );
    setCorner( Qt::Corner::TopLeftCorner, Qt::LeftDockWidgetArea );

    // Configure/setup the individual asset-type tabs/info
    struct ResourceTypeInfo{ QString dir; QStringList filters; QString tabName; };
    ResourceTypeInfo resourceTypes[] {
        {"Textures/",      QStringList() << "*.png" << "*.jpg",    "Texture"    },
        {"",               QStringList(),                          "Cubemap"    },
        {"Shaders/",       QStringList() << "*.glvs" << "*.glfs",  "Shader"     },
        {"Materials/",     QStringList(),                          "Material"   },
        {"Meshes/",        QStringList() << "*.obj",               "Mesh"       },
        {"Audio/",         QStringList() << "*.wav",               "Audio"      },
        {"",               QStringList(),                          ""           },
    };
    // Now create the tabs
    QString resourcePath( "../Resources/" );
    QTabWidget *assetBrowserTabs = new QTabWidget( mAssetBrowserDockWidget.get() );

    // TEMP
    // NOTE: Differences/mix-up between the AssetBrowserTypes and ResourceTypes enums are causing issues in property fetching and display
    {
        auto index = Underlying( ResourceType::texture );
        mAssetDirectories[index] = QString( resourcePath ).append( resourceTypes[index].dir );
        mAssetBrowserWidgets[index] = new TextureBrowser( mAssetDirectories[index], resourceTypes[index].filters );
        assetBrowserTabs->addTab( mAssetBrowserWidgets[index], resourceTypes[index].tabName );

        index = Underlying( ResourceType::shader );
        mAssetDirectories[index] = QString( resourcePath ).append( resourceTypes[index].dir );
        //mAssetBrowserWidgets[index] = new MaterialBrowser( mAssetDirectories[index], resourceTypes[index].filters );
        //assetBrowserTabs->addTab( mAssetBrowserWidgets[index], resourceTypes[index].tabName );

        index = Underlying( ResourceType::material );
        mAssetDirectories[index] = QString( resourcePath ).append( resourceTypes[index].dir );
        mAssetBrowserWidgets[index] = new MaterialBrowser( mAssetDirectories[index], resourceTypes[index].filters );
        assetBrowserTabs->addTab( mAssetBrowserWidgets[index], resourceTypes[index].tabName );

        index = Underlying( ResourceType::mesh );
        mAssetDirectories[index] = QString( resourcePath ).append( resourceTypes[index].dir );
        mAssetBrowserWidgets[index] = new MeshBrowser( mAssetDirectories[index], resourceTypes[index].filters );
        assetBrowserTabs->addTab( mAssetBrowserWidgets[index], resourceTypes[index].tabName );

        index = Underlying( ResourceType::music );
        mAssetDirectories[index] = QString( resourcePath ).append( resourceTypes[index].dir );
        mAssetBrowserWidgets[index] = new AudioBrowser( mAssetDirectories[index], resourceTypes[index].filters );
        assetBrowserTabs->addTab( mAssetBrowserWidgets[index], resourceTypes[index].tabName );
    }

    // Lastly, dock the asset-browser
    mAssetBrowserDockWidget->setWidget( assetBrowserTabs );
    addDockWidget( Qt::LeftDockWidgetArea, mAssetBrowserDockWidget.get() );

    // Init output terminal
    mOutputTerminalDockWidget.reset( new QDockWidget( "Output Terminal", this ) );
    mOutputTerminalDockWidget->setAllowedAreas( Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea );

    OutputTerminal *outputTerminal = new OutputTerminal( mSimulation.engineRuntime.loggingManager, mOutputTerminalDockWidget.get() );
    mOutputTerminalDockWidget->setWidget( outputTerminal );

    outputTerminal->addAction( mOutputTerminalDockWidget->toggleViewAction() );
    addDockWidget( Qt::BottomDockWidgetArea, mOutputTerminalDockWidget.get() );

    // Setup logging
    LoggingManagerConfig config;
    config.verbosity = LoggingVerbosity::high;
    config.perChannelLogging = true;
    config.logToFile = false;

    constexpr auto start = 1;//Underlying( LoggingChannels::rendering );
    constexpr auto loggingChannelCount = Underlying( LoggingChannels::count );
    for( auto i=start; i<=loggingChannelCount; ++i ) {
        LoggingChannelDesc channelDesc;
        channelDesc.mask = static_cast<LoggingChannelBits>( 1 << (i-1) );
        channelDesc.loggingFunction = [=] ( const char *msg ) -> void { outputTerminal->AppendMessageToLog( (RES_LOGGING_CHANNELS)(i), msg ); };

        config.channelConfigs.push_back( channelDesc );
    }
    XOF_ASSERT( mSimulation.engineRuntime.loggingManager->Startup( config ) );

    // Init scene-graph
    mSceneGraphDockWidget.reset( new QDockWidget( "Scene Graph", this ) );
    mSceneGraphDockWidget->setAllowedAreas( Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea );
    setCorner( Qt::Corner::BottomRightCorner, Qt::RightDockWidgetArea );
    setCorner( Qt::Corner::TopRightCorner, Qt::RightDockWidgetArea );

    mSceneGraph = new SceneGraph( &mSimulation, mAssetDirectories.data(), ui->xfeViewport->GetCamera(), mSceneGraphDockWidget.get() );
    mSceneGraphDockWidget->setWidget( mSceneGraph );
    addDockWidget( Qt::RightDockWidgetArea, mSceneGraphDockWidget.get() );

    ui->xfeStatusBar->showMessage( "Testing..." );

    // Gizmo and Simulation management 'buttons/icons'
    mSimulationStateActionGroup = new QActionGroup( this );
    mSimulationStateActionGroup->addAction( ui->actionPlay );
    mSimulationStateActionGroup->addAction( ui->actionPause );
    mSimulationStateActionGroup->addAction( ui->actionStop );

    mTransformActionGroup = new QActionGroup( this );
    mTransformActionGroup->addAction( ui->actionTranslate );
    mTransformActionGroup->addAction( ui->actionRotate );
    mTransformActionGroup->addAction( ui->actionScale );
    // Editor UI start up --- END

    // Runtime startup --- START
    mSimulation.engineRuntime.renderer.reset( new Renderer() );
    mSimulation.engineRuntime.renderer->StartUp();

    mSimulation.engineRuntime.resourceManager = ResourceManager::Get();
    mSimulation.engineRuntime.resourceManager->StartUp();

    // Start the simulation in the 'stopped' state
    mSimulation.state = Simulation::STATE::STOP;
    mSimulation.engineRuntime.entityManager.mEntities.resize( 0 );
    ui->xfeViewport->SetSimulation( &mSimulation );

    // Load bounding box resource (needed in order to actually 'pick' entities within the viewports)
    MeshDescriptor boundingVolMeshDesc{"../Resources/Meshes/", "UNIT_CUBE.obj"};

    MaterialDescriptor boundingVolMaterialDesc{"../Resources/Meshes/", "UNIT_CUBE.mtl"};
    boundingVolMaterialDesc.shaders = {"../Resources/Shaders/", "RES_BOUNDING_VOLUME_SHADER"};

    // Set visual representation for the gizmo' axis/plane bounding volumes
    ResourceManager *resourceManager = mSimulation.engineRuntime.resourceManager;

    Mesh *boundingVolumeMesh = resourceManager->LoadResource<Mesh>( boundingVolMeshDesc );
    XOF_ASSERT( boundingVolumeMesh );

    Material *boundingVolumeMaterial = resourceManager->LoadResource<Material>( boundingVolMaterialDesc );
    XOF_ASSERT( boundingVolumeMaterial );
    boundingVolumeMaterial->GetShader()->AddUniform( "wvp" );
    boundingVolumeMaterial->GetShader()->AddUniform( "colour" );
    // Runtime startup --- END
}

EditorWindow::~EditorWindow() {
    // Anything we've set-up related to the editor UI
    mOutputTerminalDockWidget.release();

    // Now the editor UI itself
    delete ui;

    // Now anything we've set up for the engine runtime
    mSimulation.engineRuntime.resourceManager->ShutDown();

    // Must be last - mOutputTerminal.AppendMessageToLog() is set as the log function for all channels;
    // so runtime components shutting down will still need to call this.
    //mOutputTerminal.release();
}

void EditorWindow::on_actionExit_triggered() {
    QWidget::close();
}

void EditorWindow::on_actionPlay_triggered( bool checked ) {
    if( checked ) {
        mSimulation.state = Simulation::STATE::PLAY;
    }
}

void EditorWindow::on_actionPause_triggered( bool checked ) {
    if( checked ) {
        mSimulation.state = Simulation::STATE::PAUSE;
    }
}

void EditorWindow::on_actionStop_triggered( bool checked ) {
    if( checked ) {
        mSimulation.state = Simulation::STATE::STOP;
        std::unique_ptr<Event> resetEvent( new SimulationReset() );
        mSimulation.events.push_back( std::move( resetEvent ) );
    }
}

void EditorWindow::on_actionTranslate_triggered( bool checked ) {
    mCurrentlySelectedGizmo = ( checked && ( mCurrentlySelectedGizmo != &mTranslationGizmo ) ) ? &mTranslationGizmo : nullptr;
    ui->xfeViewport->SetCurrentTransformGizmo( mCurrentlySelectedGizmo );
}

void EditorWindow::on_actionScale_triggered( bool checked ) {
    mCurrentlySelectedGizmo = ( checked && ( mCurrentlySelectedGizmo != &mScaleGizmo ) ) ? &mScaleGizmo : nullptr;
    ui->xfeViewport->SetCurrentTransformGizmo( mCurrentlySelectedGizmo );
}

void EditorWindow::on_actionRotate_triggered( bool checked ) {
    mCurrentlySelectedGizmo = ( checked && ( mCurrentlySelectedGizmo != &mRotationGizmo ) ) ? &mRotationGizmo : nullptr;
    ui->xfeViewport->SetCurrentTransformGizmo( mCurrentlySelectedGizmo );
}

void EditorWindow::on_actionOpenOutputTerminal_triggered() {
    if( !mOutputTerminalDockWidget->isVisible() ) {
        mOutputTerminalDockWidget->setFloating( false );
        mOutputTerminalDockWidget->show();
    }
}

void EditorWindow::on_actionOpenSceneGraph_triggered() {
    if( !mSceneGraphDockWidget->isVisible() ) {
        mSceneGraphDockWidget->setFloating( false );
        mSceneGraphDockWidget->show();
    }
}

void EditorWindow::on_actionOpenAssetBrowser_triggered() {
    if( !mAssetBrowserDockWidget->isVisible() ) {
        mAssetBrowserDockWidget->setFloating( false );
        mAssetBrowserDockWidget->show();
    }
}

void EditorWindow::on_actionDockAll_triggered() {
    if( mOutputTerminalDockWidget->isFloating() ) {
        mOutputTerminalDockWidget->setFloating( false );
    }

    if( mAssetBrowserDockWidget ) {
        mAssetBrowserDockWidget->setFloating( false );
    }

    if( mSceneGraphDockWidget ) {
        mSceneGraphDockWidget->setFloating( false );
    }
}

void EditorWindow::on_actionOpenAll_triggered() {
    if( !mSceneGraphDockWidget->isVisible() ) {
        mSceneGraphDockWidget->setFloating( false );
        mSceneGraphDockWidget->show();
    }

    if( !mAssetBrowserDockWidget->isVisible() ) {
        mAssetBrowserDockWidget->setFloating( false );
        mAssetBrowserDockWidget->show();
    }

    if( !mOutputTerminalDockWidget->isVisible() ) {
        mOutputTerminalDockWidget->setFloating( false );
        mOutputTerminalDockWidget->show();
    }
}

void EditorWindow::on_actionPointLight_triggered() {
    // Create default name
    static auto pointLightCount {0u};
    QString defaultName( "PointLight_" );
    defaultName.append( QString::number( pointLightCount++ ) );

    mNewEntityWindow.reset( new NewEntityWindow( &mSimulation, mSceneGraph, "PointLight", defaultName ) );
    mNewEntityWindow->show();

#if 0
// TEMP ---
    Entity *tempBarrel = mSimulation.engineRuntime.entityManager.mEntities.at( 0 ).get();
    XOF_ASSERT( tempBarrel );
    Entity *tempLight = mSimulation.engineRuntime.entityManager.mEntities.at( 1 ).get();
    XOF_ASSERT( tempLight );
    ((PassThrough*)tempBarrel->GetComponent( CAS_COMPONENTS::PASS_THROUGH_LOGIC ))->TEMP_HACK_LIGHT = tempLight;
// TEMP ---
#endif
}

void EditorWindow::on_action_SkyBox_triggered() {
    // Create default name
    static auto pointLightCount {0u};
    QString defaultName( "SkyBox_" );
    defaultName.append( QString::number( pointLightCount++ ) );

    mNewEntityWindow.reset( new NewEntityWindow( &mSimulation, mSceneGraph, "SkyBox", defaultName ) );
    mNewEntityWindow->show();

    // TEMP ---
        Entity *tempBarrel = mSimulation.engineRuntime.entityManager.mEntities.at( 0 ).get();
        XOF_ASSERT( tempBarrel );
        Entity *tempLight = mSimulation.engineRuntime.entityManager.mEntities.at( 1 ).get();
        XOF_ASSERT( tempLight );
        ((PassThrough*)tempBarrel->GetComponent( CAS_COMPONENTS::PASS_THROUGH_LOGIC ))->TEMP_HACK_LIGHT = tempLight;
    // TEMP ---
}
