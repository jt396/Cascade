/*
===============================================================================

    RESONANCE
    =========
    File	:	RES_MainWindow.hpp
    Desc	:	Editor main window.

===============================================================================
*/
#ifndef RES_MAINWINDOW_HPP
#define RES_MAINWINDOW_HPP


#include <array>

// This ordering must be respected to avoid include ordering issues between gl.h and glew.h
#include "QSFMLCanvas.hpp"

#include <QMainWindow>
#include <QtCore>
#include <QtGui>
// ---

#include "ThirdParty/JSON/json.hpp"

#include "RES_EntityController.hpp"
#include "RES_NewEntityWindow.hpp"
#include "TransformGizmos/RES_TranslationGizmo.hpp"
#include "TransformGizmos/RES_RotationGizmo.hpp"
#include "TransformGizmos/RES_ScaleGizmo.hpp"
#include "RES_Simulation.hpp"
#include "Resources/CAS_ResourceTypes.hpp"
#include "GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp"

class Entity;
class Component;

class AssetBrowser;
class OutputTerminal;
class SceneGraph;

class QTreeWidgetItem;
class QTableWidgetItem;
class QActionGroup;

namespace Ui {
    class EditorWindow;
}


class EditorWindow : public QMainWindow {
    Q_OBJECT

public:
    friend class PropertyTableEntryEventHandler;

    explicit EditorWindow( QWidget *parent = nullptr );
    ~EditorWindow();

private slots:
    // Menu - File
    void on_actionExit_triggered();
    // Menu - Transformations
    void on_actionScale_triggered(bool checked);
    void on_actionTranslate_triggered( bool checked );
    void on_actionRotate_triggered( bool checked );

    // Simulation
    void on_actionPlay_triggered( bool checked );
    void on_actionPause_triggered( bool checked );
    void on_actionStop_triggered( bool checked );

    // UI
    void on_actionOpenOutputTerminal_triggered();
    void on_actionOpenSceneGraph_triggered();
    void on_actionOpenAssetBrowser_triggered();
    void on_actionDockAll_triggered();
    void on_actionOpenAll_triggered();

    // ...
    void on_actionPointLight_triggered();
    void on_action_SkyBox_triggered();

private:
    // Editor UI
    Ui::EditorWindow*                   ui;

    std::unique_ptr<QDockWidget>        mAssetBrowserDockWidget{nullptr};

    SceneGraph*                         mSceneGraph{nullptr};
    std::unique_ptr<QDockWidget>        mSceneGraphDockWidget{nullptr}; // TODO: Store a pointer to the actual scene-graph so can call serialization functions on it

    std::unique_ptr<QDockWidget>        mOutputTerminalDockWidget{nullptr};

    std::unique_ptr<NewEntityWindow>    mNewEntityWindow;

    std::array<QString, Underlying( ResourceType::count )>       mAssetDirectories;
    std::array<AssetBrowser*, Underlying( ResourceType::count )> mAssetBrowserWidgets;

    // Transform gizmos
    TranslationGizmo                    mTranslationGizmo;
    RotationGizmo                       mRotationGizmo;
    ScaleGizmo                          mScaleGizmo;
    TransformGizmo*                     mCurrentlySelectedGizmo{nullptr};

    QActionGroup*                       mTransformActionGroup;

    // Game/simiulation management
    Simulation                          mSimulation;
    QActionGroup*                       mSimulationStateActionGroup;

    void                                GetReflectedVariableValue( const ReflectedVariable *rv, const void *instance, CHAR *buffer );
};


#endif // RES_MAINWINDOW_HPP
