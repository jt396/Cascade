// Qt/system includess
#include <QMessageBox>
#include <QTreeWidget>
#include <QCheckBox>

// Resonance includes
#include "RES_Simulation.hpp"
#include "RES_Events.hpp"
#include "RES_CreateEntityWindow.hpp"

#include "GameplayFoundations/XOF_Entity.hpp"   // For serialization
#include "Rendering/XOF_Camera.hpp"             // TEMP?

#include "RES_NewEntityWindow.hpp"
#include "RES_SceneGraph.hpp"
#include "ui_RES_NewEntityWindow.h"


NewEntityWindow::NewEntityWindow( Simulation *simulation, SceneGraph *sceneGraph, QWidget *parent ) :
    mSimulation{simulation}, mSceneGraph{sceneGraph}, QWidget( parent ), ui( new Ui::NewEntityWindow )
{
    ui->setupUi(this);

    // remove minimize and maximize options
    setWindowFlags( Qt::WindowCloseButtonHint );

    // set default name
    QString defaultEntityName( "Entity_" );
    defaultEntityName.append( QString::number( Entity::GetEntitiesCreatedCount() ) );
    ui->nameLineEdit->setText( defaultEntityName );

    // populate entity type combo-box
    ui->typeComboBox->addItem( "PassThrough" );
}

NewEntityWindow::NewEntityWindow( Simulation *simulation, SceneGraph *sceneGraph, const QString& entityType, const QString& defaultName, QWidget *parent ) :
    mSimulation{simulation}, mSceneGraph{sceneGraph}, QWidget( parent ), ui( new Ui::NewEntityWindow )
{
    ui->setupUi(this);

    // remove minimize and maximize options
    setWindowFlags( Qt::WindowCloseButtonHint );

    // set default name
    ui->nameLineEdit->setText( defaultName );

    // populate entity type combo-box
    ui->typeComboBox->addItem( entityType );
    ui->typeComboBox->setEnabled( false );
}

NewEntityWindow::~NewEntityWindow() {
    delete ui;
}

void NewEntityWindow::closeEvent( QCloseEvent *event ) {
    XOF_UNUSED_PARAMETER( event );
    mCreateEntityWindow.reset( nullptr );
}

void NewEntityWindow::on_cancelPushButton_clicked() {
    close();
}

void NewEntityWindow::on_addPushButton_clicked() {
    const QString entityName( ui->nameLineEdit->text() );

    if( entityName.isEmpty() ) {
        QMessageBox emptyNameErrorBox( QMessageBox::Warning, "Cascade - Error", "Name cannot be empty!" );
        emptyNameErrorBox.exec();
        return;
    }

    // Callback to the scenegraph with the selected entity (name + type)
    mSceneGraph->AddToSceneGraph( ui->typeComboBox->currentText(), entityName );

    // Done
    close();
}

void NewEntityWindow::on_createPushButton_clicked() {
    mCreateEntityWindow.reset( new CreateEntityWindow( mSimulation ) );
    mCreateEntityWindow->show();
}
