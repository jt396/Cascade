#ifndef RES_NEWENTITYWINDOW_HPP
#define RES_NEWENTITYWINDOW_HPP


#include <memory>
#include <functional>

#include <QWidget>

#include "ThirdParty/JSON/json.hpp"

class   QTreeWidget;
class   CreateEntityWindow;
class   Camera;
struct  Simulation;
class   Entity;
class   Component;
class   SceneGraph;
struct  ReflectedClass;

namespace Ui {
    class NewEntityWindow;
}


class NewEntityWindow : public QWidget
{
    Q_OBJECT

public:
                        NewEntityWindow( Simulation *simulation, SceneGraph *sceneGraph, QWidget *parent = nullptr );
                        // The "entityType" will lock the window to that entity type
                        NewEntityWindow( Simulation *simulation, SceneGraph *sceneGraph, const QString& entityType, const QString& defaultName, QWidget *parent = nullptr );
                        ~NewEntityWindow();

protected:
    virtual void        closeEvent( QCloseEvent *event ) override;

private slots:
    void                on_cancelPushButton_clicked();
    void                on_addPushButton_clicked();

    void                on_createPushButton_clicked();

private:
    Ui::NewEntityWindow *ui;

    std::unique_ptr<CreateEntityWindow> mCreateEntityWindow;

    Simulation          *mSimulation;
    Camera              *mTempCamera;

    SceneGraph          *mSceneGraph;
};


#endif // RES_NEWENTITYWINDOW_HPP
