#include <QTextEdit>

#include "Core/XOF_LoggingManager.hpp"

#include "RES_OutputTerminal.hpp"
#include "ui_RES_OutputTerminal.h"


OutputTerminal::OutputTerminal( LoggingManager *logManager, QWidget *parent ) :
    QWidget( parent ), ui( new Ui::OutputTerminal ) {
    // Init UI
    ui->setupUi( this );

    // Add in general, editor and runtime tabs
    ui->tabWidget->clear();

    ui->tabWidget->addTab( new QTextEdit(), "All" );
    ui->tabWidget->addTab( new QTextEdit(), "Rendering" );
    ui->tabWidget->addTab( new QTextEdit(), "Audio" );
    ui->tabWidget->addTab( new QTextEdit(), "Core" );
    ui->tabWidget->addTab( new QTextEdit(), "Collision/Physics" );
    ui->tabWidget->addTab( new QTextEdit(), "Animation" );
    ui->tabWidget->addTab( new QTextEdit(), "Resources" );
    ui->tabWidget->addTab( new QTextEdit(), "PlayerIO" );
    ui->tabWidget->addTab( new QTextEdit(), "Platform" );
    ui->tabWidget->addTab( new QTextEdit(), "Editor" );

    // Low, medium and high verbosity
    ui->verticalSlider->setRange( 0, 2 );

    mEngineLogManager = logManager;
}

OutputTerminal::~OutputTerminal() {
    delete ui;
}

void OutputTerminal::AppendMessageToLog( RES_LOGGING_CHANNELS channel, const char *msg ) {
    reinterpret_cast<QTextEdit*>( ui->tabWidget->widget( Underlying( channel ) ) )->append( msg );
    reinterpret_cast<QTextEdit*>( ui->tabWidget->widget( Underlying( RES_LOGGING_CHANNELS::ALL ) ) )->append( msg );
}

void OutputTerminal::on_verticalSlider_valueChanged( int value ) {
    mEngineLogManager->SetVerbosity( static_cast<LoggingVerbosity>( value ) );
}
