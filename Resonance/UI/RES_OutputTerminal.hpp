#ifndef RES_OUTPUTTERMINAL_HPP
#define RES_OUTPUTTERMINAL_HPP


#include <functional>

#include <QWidget>
#include <QTextEdit>

class LoggingManager;

namespace Ui {
    class OutputTerminal;
}


enum class RES_LOGGING_CHANNELS : unsigned int {
    ALL = 0,
    RENDERING,
    AUDIO,
    CORE,
    COLLISION_AND_PHYSICS,
    ANIMATION,
    RESOURCES,
    PLAYER_IO,
    PLATFORM,
    WORLD_EDITOR,
    COUNT
};


class OutputTerminal : public QWidget {
    Q_OBJECT

public:
    explicit            OutputTerminal( LoggingManager *logManager, QWidget *parent = nullptr );
                        ~OutputTerminal();

    void                AppendMessageToLog( RES_LOGGING_CHANNELS channel, const char *msg );

private slots:
    void                on_verticalSlider_valueChanged(int value);

private:
    Ui::OutputTerminal  *ui;
    LoggingManager      *mEngineLogManager;
};


#endif // RES_OUTPUTTERMINAL_HPP
