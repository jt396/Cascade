#include <array>

#include <QInputDialog>
#include <QDoubleSpinBox>
#include <QLabel>
#include <QComboBox>
#include <QCheckBox>
#include <QDir>

#include "ThirdParty/JSON/json.hpp"
#include "Rendering/XOF_Camera.hpp" // TEMP?
#include "Core/XOF_CRC32.hpp"

#include "UI/RES_NewEntityWindow.hpp"
#include "RES_Simulation.hpp"
#include "RES_Events.hpp"
#include "RES_ComboBox.hpp"

#include "RES_SceneGraph.hpp"
#include "ui_RES_SceneGraph.h"


#define CRC32( type ) CalculateCRC32( 0, type, sizeof( type )-1 )


SceneGraph::SceneGraph( Simulation *simulation, QString *resourceDirs, Camera *tempCamera, QWidget *parent ) :
    QWidget( parent ), ui( new Ui::SceneGraph )
{
    ui->setupUi(this);

    mSimulation = simulation;
    mTempCamera = tempCamera;

    for( U32 i=0; i<Underlying( ResourceType::count ); ++i ) {
        mResourceDirectoryPaths[i] = &resourceDirs[i];
    }

    ConfigureReflectedTypesSet();
    ConfigureReflectedTypesGet();

    ConfigureReflectedTypesSerializeIn();
    ConfigureReflectedTypesSerializeOut();

    std::ifstream{ "TestFile.json" } >> mEntitiesJSON;

    // TODO: Rework component system to use GetComponent<T>() rather than an enum
    {
        mSerializeInFunctionTable.insert( std::pair<std::string, serializeInFunction> {
                                            "Transformable",
                                            [this] ( Entity *entity, const nlohmann::json& componentJSON ) {
                                                // Add the component to the entity
                                                entity->AddDataComponent<Transformable>( componentJSON["mName"].get<std::string>() );
                                                auto component {static_cast<Transformable*>( entity->GetDataComponent( CAS_COMPONENTS::TRANSFORMABLE ) )};

                                                // Get the component reflection data
                                                const auto rm {const_cast<ReflectionManager*>( mSimulation->engineRuntime.reflectionManager )};
                                                const auto componentReflectionInfo {rm->GetReflectedClass( componentJSON["mType"].get<std::string>().c_str() )};

                                                // Serialize in the component's reflected variables
                                                SerializeInComponent( componentReflectionInfo, component, componentJSON );
                                            }
                                        } );

        mSerializeInFunctionTable.insert( std::pair<std::string, serializeInFunction> {
                                            "Renderable",
                                            [this] ( Entity *entity, const nlohmann::json& componentJSON ) {
                                                // Add the component to the entity
                                                entity->AddDataComponent<Renderable>( componentJSON["mName"].get<std::string>() );
                                                auto component {static_cast<Renderable*>( entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) )};

                                                // Get the component reflection data
                                                const auto rm {const_cast<ReflectionManager*>( mSimulation->engineRuntime.reflectionManager )};
                                                const auto componentReflectionInfo {rm->GetReflectedClass( componentJSON["mType"].get<std::string>().c_str() )};

                                                // Serialize in the component's reflected variables (mesh + material etc.)
                                                SerializeInComponent( componentReflectionInfo, component, componentJSON );

                                                // TEMP: Use the editors default camera for now
                                                component->SetCamera( mTempCamera );
                                            }
                                        } );

        mSerializeInFunctionTable.insert( std::pair<std::string, serializeInFunction> {
                                            "PassThrough",
                                            [this] ( Entity *entity, const nlohmann::json& componentJSON ) {
                                                XOF_UNUSED_PARAMETER( componentJSON );
                                                entity->AddLogicComponent<PassThrough>( entity, &mSimulation->engineRuntime );
                                            }
                                        } );

        mSerializeInFunctionTable.insert( std::pair<std::string, serializeInFunction> {
                                            "SkyBoxLogic",
                                            [this] ( Entity *entity, const nlohmann::json& componentJSON ) {
                                                XOF_UNUSED_PARAMETER( componentJSON );
                                                entity->AddLogicComponent<SkyBoxLogic>( entity, &mSimulation->engineRuntime );
                                            }
                                        } );

        mSerializeInFunctionTable.insert( std::pair<std::string, serializeInFunction> {
                                            "PointLightSource",
                                            [this] ( Entity *entity, const nlohmann::json& componentJSON ) {
                                                // Add the component to the entity
                                                entity->AddDataComponent<PointLightSource>( componentJSON["mName"].get<std::string>() );
                                                auto component {static_cast<PointLightSource*>( entity->GetDataComponent( CAS_COMPONENTS::POINT_LIGHT ) )};

                                                // Get the component reflection data
                                                const auto rm {const_cast<ReflectionManager*>( mSimulation->engineRuntime.reflectionManager )};
                                                const auto componentReflectionInfo {rm->GetReflectedClass( componentJSON["mType"].get<std::string>().c_str() )};

                                                // Serialize in the component's reflected variables
                                                SerializeInComponent( componentReflectionInfo, component, componentJSON );
                                            }
                                        } );
    }
}

SceneGraph::~SceneGraph() {
    delete ui;
}

void SceneGraph::AddToSceneGraph( const QString& typeAsString, const QString& name ) {
    // Get the JSON for the specific entity
    const auto entityJSON {mEntitiesJSON[typeAsString.toStdString()]};

    // Serialize it in...
    Entity *newEntity = new Entity( name.toStdString() );

    // Now create each component
    if( !entityJSON.is_null() ) {
        auto i {0u};
        auto componentName {"Component_" + std::to_string( i )};
        while( entityJSON.find( componentName ) != entityJSON.end()  ) {
            // Current component
            const auto componentJSON {entityJSON[componentName]};
            const auto componentType {componentJSON["mType"].get<std::string>()};

            mSerializeInFunctionTable[componentType]( newEntity, componentJSON );

            // Prep for next component
            componentName = "Component_" + std::to_string( ++i );
        }
    } else {
        XOF_ASSERT( 0 );
    }

    // Create a entry for the entity in the scene-graph tree
    auto newEntityTreeItem {new QTreeWidgetItem( ui->sceneGraphTree )};
    newEntityTreeItem->setText( 0, newEntity->GetName() );
    ui->sceneGraphTree->addTopLevelItem( newEntityTreeItem );

    // Add to entity controllers
    mSimulation->entityControllers.emplace_back( new EntityController( newEntity,
                                                                       mSimulation->engineRuntime.renderer.get(),
                                                                       mSimulation->engineRuntime.resourceManager ) );

    mSimulation->engineRuntime.entityManager.mEntities.push_back( std::unique_ptr<Entity>( newEntity ) );

    // Setup visibility and selectability toggles
#if 0
    QCheckBox *visibleCB = new QCheckBox();
    visibleCB->setCheckState( Qt::Checked );
    connect( visibleCB, &QCheckBox::stateChanged, this, [] ( int newState ) {
        XOF_UNUSED_PARAMETER( newState );
        entityController->SetVisibility( newState != Qt::Unchecked );
    } );
    mSceneGraphTree->setItemWidget( newEntityTreeItem, 1, visibleCB );

    QCheckBox *selectableCB = new QCheckBox();
    selectableCB->setCheckState( Qt::Checked );
    connect( selectableCB, &QCheckBox::stateChanged, this, [] ( int newState ) {
        XOF_UNUSED_PARAMETER( newState );
    } );
    mSceneGraphTree->setItemWidget( newEntityTreeItem, 2, selectableCB );
#endif
}

void SceneGraph::on_addEntityButton_released() {
    NewEntityWindow *newEntityWindow = new NewEntityWindow( mSimulation, this );
    newEntityWindow->show();
}

void SceneGraph::on_sceneGraphTree_itemClicked( QTreeWidgetItem *item, int column ) {
    XOF_UNUSED_PARAMETER( column );

    // No point doing everything again
    if( item == mSelectedItem.sceneGraphEntity ) {
        return;
    }
    mSelectedItem.sceneGraphEntity = item;

    // Get the reflection data for the selected entites' components
    EntityController *ec = nullptr;
    for( auto& controller : mSimulation->entityControllers ) {
        if( strcmp( controller->GetEntity()->GetName(), item->text( 0 ).toStdString().c_str() ) == 0 ) {
            ec = controller.get();
            mSelectedItem.entityController = ec;
            mSelectedItem.entity = ec->GetEntity();
        }
    }
    XOF_ASSERT( ec );

    ui->scenePropertyTree->clear();

    ReflectionManager *reflectionManager = mSimulation->engineRuntime.reflectionManager;

    Entity *entity = ec->GetEntity();

    for( const auto& component : entity->DEBUG_GetDataComponents() ) {
        // Place the component at the top, with a drop-down table of editable fields
        const std::string componentTypeAsString {component->DEBUG_GetTypeAsString()};

        auto componentItem {new QTreeWidgetItem()};
        componentItem->setText( 0, QString( componentTypeAsString.c_str() ) );

        // Get the reflection info for this component type
        const auto rc {reflectionManager->GetReflectedClass( componentTypeAsString.c_str() )};
        if( rc ) {
            // Create the property table for this component
            auto componentPropertyTable {new QTableWidget()};
            componentPropertyTable->setRowCount( 0 );
            componentPropertyTable->setColumnCount( 2 );

            componentPropertyTable->horizontalHeader()->hide();
            componentPropertyTable->horizontalHeader()->setSectionResizeMode( 1, QHeaderView::Stretch );
            componentPropertyTable->verticalHeader()->hide();

            componentPropertyTable->setMouseTracking( true );

            // Populate it
            AddToPropertyTable( entity, rc, (void*)component.get(), (void*)componentPropertyTable );
            componentPropertyTable->resizeColumnsToContents();

            // Finally, place it in the top-level component item
            auto componentPropertyTableItem {new QTreeWidgetItem( componentItem )};

            // connect table item entered -> get sceneGraphTree item parent, set parent tree current item
            connect( componentPropertyTable, &QTableWidget::itemEntered, this, [=] () {
                ui->scenePropertyTree->setCurrentItem( componentItem );
            } );

            // Remove empty space from the bottom of the table
            //componentPropertyTable->setAlternatingRowColors( true );
            auto newHeight = 0;
            for( auto i=0; i<componentPropertyTable->rowCount(); ++i ) {
                newHeight += componentPropertyTable->rowHeight( i );
            }
            // We add 2 to prevent 'micro-scrolling' that's still possible if we took the combined row-height exactly as-is
            componentPropertyTable->setMaximumHeight( newHeight + 2 );
            componentPropertyTable->setVerticalScrollBarPolicy( Qt::ScrollBarAlwaysOff );

            ui->scenePropertyTree->addTopLevelItem( componentItem );
            ui->scenePropertyTree->setItemWidget( componentPropertyTableItem, 0, componentPropertyTable );
        }
    }
    // TODO: Add logic components?
}

void SceneGraph::on_scenePropertyTree_currentItemChanged( QTreeWidgetItem *current, QTreeWidgetItem *previous ) {
#if 1
    XOF_LOG_MESSAGE( LoggingVerbosity::low,
                     LoggingChannels::worldEditor,
                     "CHANGED item: %x|%s -> %x|%s",
                     previous, previous ? previous->text(0).toStdString().c_str() : "NULL",
                     current, current ? current->text(0).toStdString().c_str() : "NULL" );
#endif
    XOF_UNUSED_PARAMETER( previous );

    if( !current || current->parent() || ( mSelectedItem.sceneGraphComponent == current ) ) {
        return;
    }
    mSelectedItem.sceneGraphComponent = current;

    std::string currName( current->text( 0 ).toStdString() );
    mSelectedItem.component = mSelectedItem.entity->GetDataComponent( currName.c_str() );
    XOF_ASSERT( mSelectedItem.component );
}

void SceneGraph::OnPropertyTableItemValueChanged() {
    XOF_ASSERT( mSelectedItem.sceneGraphComponent );

    // Get the class
    ReflectionManager *reflectionManager = mSimulation->engineRuntime.reflectionManager;
    ReflectedClass *reflectedClass = reflectionManager->GetReflectedClass( mSelectedItem.component->DEBUG_GetTypeAsString() );
    XOF_ASSERT( reflectedClass );

    QTableWidget *componentPropertyTable = reinterpret_cast<QTableWidget*>(
                ui->scenePropertyTree->itemWidget( mSelectedItem.sceneGraphComponent->child( 0 ), 0 ) );

    const std::string variableName( componentPropertyTable->currentItem()->text().toStdString() );
    ReflectedVariable *reflectedVariable = reflectedClass->GetMemberVariable( variableName.c_str(), variableName.length() );
    XOF_ASSERT( reflectedVariable );

    U32 offset = reflectedVariable->memoryOffset;
    while( reflectedVariable->isClassInstance ) {
        reflectedVariable = reflectedVariable->classInstance->GetMemberVariable( variableName.c_str(), variableName.length() );
        offset += reflectedVariable->memoryOffset;
    }

    // Set the new value
    auto setter = reflectionManager->GetReflectedTypeInfo( reflectedVariable->typeAsHash )->Set;
    if( setter ) {
        // Let the reflected type setter worry about how to use the widget
        void *instance = (void*)(((char*)mSelectedItem.component) + 0 );

        setter( mSelectedItem.entity,
                reflectedClass->nameAsString,
                instance,
                reflectedVariable->nameAsString,
                (void*)componentPropertyTable->cellWidget( componentPropertyTable->currentRow(), 1 ) );
    }
}

// ---
void SceneGraph::AddToPropertyTable( Entity *entity, const ReflectedClass *reflectedClass, void *component, void *table ) {
    // Start with base classes (if any)
    for( U32 i=0; i<reflectedClass->baseClassCount; ++i ) {
        AddToPropertyTable( entity, (const ReflectedClass*)reflectedClass->baseClasses[i], component, table );
    }
    // Now the actual class itself
    for( U32 i=0; i<reflectedClass->reflectedVariableCount; ++i ) {
        const ReflectedVariable *variable = &reflectedClass->memberVariables[i];
        // Handle members which are themselves class instances
        if( variable->isClassInstance ) {
            // Arrays of instances (like arrays of textures within a material) must all be handled
            for( U32 j=0; j<variable->count; ++j ) {
                // Component address + offset + (sizeof element * j)
                if( variable->isRawPointer ) {
                    U32 *p = reinterpret_cast<U32*>( *reinterpret_cast<U32*>( (size_t)component + variable->memoryOffset + ( sizeof( p ) * j ) ) );
                    if( !p ) {
                        continue;
                    }
                    AddToPropertyTable( entity, variable->classInstance, (void*)&(*p), table );
                } else {
                    AddToPropertyTable( entity, variable->classInstance, (void*)((size_t)component + variable->memoryOffset +
                        ( mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( variable->typeAsHash )->size * j ) ), table );
                }
            }
        } else {
            // Now get on with inserting the actual name and value
            QTableWidget *tableWidget = (QTableWidget*)table;
            QTableWidgetItem *propertyItem = new QTableWidgetItem( variable->nameAsString );

            tableWidget->insertRow( tableWidget->rowCount() );
            tableWidget->setItem( tableWidget->rowCount()-1, 0, propertyItem );

            auto getter = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( variable->typeAsHash )->Get;
            // Use the data-type appropriate getter() function to insert the variables value into the property table
            if( getter ) {
                getter( entity, (CHAR*)reflectedClass->nameAsString, component, (CHAR*)variable, (void*)table );
            }
        }
    }
}

void SceneGraph::ConfigureReflectedTypesGet() {
    ReflectionManager *reflectionManager = mSimulation->engineRuntime.reflectionManager;

    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "MSHRSRC" ) ) ) {
        type->Get = [this]( Entity*, CHAR*, void *instance, void *variable, void *extra ) -> void*
        {
            XOF_UNUSED_PARAMETER( instance );
            XOF_UNUSED_PARAMETER( variable );

            auto table {reinterpret_cast<QTableWidget*>( extra )};

            // Create a combo-box and populate it with all the assets of this type
            const auto renderable {reinterpret_cast<Renderable*>( mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) )};

            auto comboBox {new MeshComboBox( *mResourceDirectoryPaths[Underlying( ResourceType::mesh )],
                                             MeshComboBox::ItemInfo{QString{renderable->GetMesh()->GetName()}} )};

            // Set callback for the combo-box
            connect( comboBox, &TextureComboBox::currentTextChanged, this,
                [=] () {
                    // Fetch the necessary/required engine system and entity component
                    auto resourceManager    {mSimulation->engineRuntime.resourceManager->Get()};
                    auto renderable         {reinterpret_cast<Renderable*>( mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) )};

                    // Calculate the number of textures in the current material and the 'new' material
                    // (the material belonging to the new mesh we're about the load). If there's a mis-match
                    // then we need to add or remove some ComboBoxes to make sure every texture in the
                    // new material is displayed and available for editing.
                    auto oldMaterial {renderable->GetMaterial()};

                    const auto oldMaterialTotalTextureCount {oldMaterial->GetTextureCount( XOF_TEXTURE_TYPE::DIFFUSE ) +
                                                             oldMaterial->GetTextureCount( XOF_TEXTURE_TYPE::BUMP )};

                    // Load the new mesh
                    const QString newMeshName   {comboBox->currentText()};
                    const auto meshDirectory    {mResourceDirectoryPaths[Underlying( ResourceType::mesh )]->toStdString()};
                    MeshDescriptor meshDesc     {meshDirectory.c_str(), newMeshName.toStdString().c_str()};

                    Mesh *newMesh {resourceManager->LoadResource<Mesh>( meshDesc )};
                    XOF_ASSERT( newMesh );

                    // Now load/set-up the material for the new mesh
                    const std::string   currentText{comboBox->currentText().toStdString()};
                    const std::string   newMaterialName( currentText.substr( 0, currentText.find_last_of( "." ) ).append( ".mtl" ) );
                    MaterialDescriptor  materialDesc {meshDirectory.c_str(), newMaterialName.c_str()};

                    Material *newMaterial {resourceManager->LoadResource<Material>( materialDesc )};
                    XOF_ASSERT( newMaterial );

                    // We'll want to use the same shader as the previous material
                    newMaterial->SetShader( renderable->GetMaterial()->GetShader() );

                    // Take a note of how many textures this new material has
                    const std::array<size_t, XOF_TEXTURE_TYPE::TEXTURE_TYPE_COUNT> newMaterialTextureCount {
                        newMaterial->GetTextureCount( XOF_TEXTURE_TYPE::DIFFUSE ),
                        newMaterial->GetTextureCount( XOF_TEXTURE_TYPE::BUMP )
                    };
                    const auto newMaterialTotalTextureCount {std::accumulate( newMaterialTextureCount.begin(), newMaterialTextureCount.end(), 0 )};

                    // Do we have to add or remove TextureComboBox objects?
                    if( newMaterialTotalTextureCount > oldMaterialTotalTextureCount ) {
                        // add
                        const auto difference {newMaterialTotalTextureCount - oldMaterialTotalTextureCount};
                        for( auto i=0; i<difference; ++i ) {
                            // TODO: Add TextureComboBoxes as required
                        }
                    } else {
                        // remove
                        const auto difference {oldMaterialTotalTextureCount - newMaterialTotalTextureCount};
                        for( auto i=0; i<difference; ++i ) {
                            table->removeRow( table->rowCount() - ( 1 + i ) );
                        }
                    }

                    // Configure the TextureComboBox objects so they display the correct texture names
                    auto rowOffset {0};
                    for( auto type : {XOF_TEXTURE_TYPE::DIFFUSE, XOF_TEXTURE_TYPE::BUMP} ) {
                        size_t index;
                        for( index=0; index<newMaterialTextureCount[type]; ++index ) {
                            auto textureComboBox  {reinterpret_cast<TextureComboBox*>( table->cellWidget( 2 + index + rowOffset, 1 ) )};
                            const QString texName {newMaterial->GetTexture( type, index )->GetName()};
                            textureComboBox->SetItemInfo( TextureComboBox::ItemInfo{type, index, texName}, TextureComboBox::CurrentText::Update );
                        }
                        rowOffset += index;
                    }

                    // Now update the entity's renderable component
                    renderable->SetMesh( newMesh );
                    renderable->SetMaterial( newMaterial );

                    // Update combo-box such that the newly selected text is stored in the combo-boxes mCurrentItem.name field
                    comboBox->SetItemInfo( MeshComboBox::ItemInfo{newMeshName} );
                }
            );

            table->setCellWidget( table->rowCount()-1, 1, comboBox );

            return nullptr;
        };
    }
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "TXTRRSRC" ) ) ) {
        type->Get = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            auto        table               {reinterpret_cast<QTableWidget*>( extra )};
            const auto  reflectedVariable   {reinterpret_cast<ReflectedVariable*>( variable )};
            const auto  instanceAddress     {reinterpret_cast<size_t>( instance ) + reflectedVariable->memoryOffset};

            // Create a combo-box and populate it with all the assets of this type
            const auto renderable           {reinterpret_cast<Renderable*>( mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) )};
            const QString str               {reinterpret_cast<CHAR*>( instanceAddress )};
            const TextureInfo textureInfo   {renderable->InspectMaterial()->GetTextureInfo( str.toStdString() )};

            auto comboBox {new TextureComboBox( *mResourceDirectoryPaths[Underlying( ResourceType::texture )],
                                                TextureComboBox::ItemInfo{textureInfo.type, textureInfo.index, str} )};

            // Set callback for the combo-box
            connect( comboBox, &TextureComboBox::currentTextChanged, this,
                [=] () {
                    // Fetch the necessary/required engine system and entity component
                    auto resourceManager    {mSimulation->engineRuntime.resourceManager->Get()};
                    auto renderable         {reinterpret_cast<Renderable*>( mSelectedItem.entity->GetDataComponent( CAS_COMPONENTS::RENDERABLE ) )};
                    // auto renderable      {mSelectedItem.entity->GetDataComponent<Renderable>()}; <- TODO: Refactor fetching components to look like this

                    // Get info on the current texture
                    const auto currentItem  {comboBox->GetCurrentItemInfo()};

                    // Load and set the new texture
                    const QString newText           {comboBox->currentText()};
                    TextureDescriptor textureDesc   {mResourceDirectoryPaths[Underlying( ResourceType::texture )]->toStdString().c_str(),
                                                     newText.toStdString().c_str()};

                    renderable->GetMaterial()->SetTexture( currentItem.type, currentItem.index,
                                                           resourceManager->LoadResource<Texture>( textureDesc ) );

                    // Update combo-box such that the newly selected text is stored in the combo-boxes mCurrentItem.name field
                    comboBox->SetItemInfo( TextureComboBox::ItemInfo{currentItem.type, currentItem.index, newText} );
                }
            );

            table->setCellWidget( table->rowCount()-1, 1, comboBox );
            return nullptr;
        };
    }
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "glm::vec3" ) ) ) {
        type->Get = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            size_t instanceAddress = reinterpret_cast<size_t>( instance );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );

            glm::vec3 *vec = reinterpret_cast<glm::vec3*>( instanceAddress + reflectedVariable->memoryOffset );

            struct VecComponent { QString name; float value; QString styleSheet; };
            VecComponent vecComponents[3] {
                { "X", vec->x, "QLabel { background-color : red; font : bold; color : white; }" },
                { "Y", vec->y, "QLabel { background-color : green; font : bold; color : white; }" },
                { "Z", vec->z, "QLabel { background-color : blue; font : bold; color : white; }" }
            };

            QHBoxLayout *layout = new QHBoxLayout();
            layout->setMargin( 5 );

            for( U32 i=0; i<3; ++i ) {
                QDoubleSpinBox *fieldValue = new QDoubleSpinBox();
                fieldValue->setMinimum( -( std::numeric_limits<float>::max() - 1 ) );
                fieldValue->setMaximum( std::numeric_limits<float>::max() );

                // Add field name and value
                fieldValue->setValue( static_cast<double>( vecComponents[i].value ) );
                connect( fieldValue, SIGNAL( valueChanged( double ) ), this, SLOT( OnPropertyTableItemValueChanged() ) );

                QLabel *label = new QLabel( vecComponents[i].name );
                label->setStyleSheet( vecComponents[i].styleSheet );

                layout->addWidget( label );
                layout->addWidget( fieldValue );
            }

            // Widget to own the layout + components
            QWidget *widget = new QWidget();
            widget->setLayout( layout );
            table->setCellWidget( table->rowCount()-1, 1, widget );

            return nullptr;
        };
    }
    // ---
    if( auto type = reflectionManager->GetReflectedTypeInfo( CRC32( "CHAR" ) ) ) {
        type->Get = [this]( Entity *entity, CHAR *className, void *instance, void *variable, void *extra ) -> void* {
            XOF_UNUSED_PARAMETER( entity );
            XOF_UNUSED_PARAMETER( className );

            QTableWidget *table = reinterpret_cast<QTableWidget*>( extra );
            ReflectedVariable *reflectedVariable = reinterpret_cast<ReflectedVariable*>( variable );

            size_t instanceAddress = reinterpret_cast<size_t>( instance );

            QString val( reinterpret_cast<char*>( instanceAddress + reflectedVariable->memoryOffset ) );
            table->setCellWidget( table->rowCount()-1, 1, new QLineEdit( val ) );

            return nullptr;
        };
    }
}

void SceneGraph::ConfigureReflectedTypesSet() {
    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "glm::vec3" ) ) ) {
        type->Set = [this]( Entity *entity, CHAR *className, void *classInstance, void *variableName, void *newValue ) -> bool {
            XOF_UNUSED_PARAMETER( entity );

            #define DSB_VAL_TO_FLOAT( val ) static_cast<float>( val )
            // Reconstruct the vec3
            QLayout *layout = ((QWidget*)newValue)->layout();
            glm::vec3 vec( DSB_VAL_TO_FLOAT( ((QDoubleSpinBox*)layout->itemAt( 1 )->widget())->value() ),
                           DSB_VAL_TO_FLOAT( ((QDoubleSpinBox*)layout->itemAt( 3 )->widget())->value() ),
                           DSB_VAL_TO_FLOAT( ((QDoubleSpinBox*)layout->itemAt( 5 )->widget())->value() ) );

            // TEMP: The entity controller collision volume has to be transformed along with the actual mesh;
            // For entities that have a collidable, we'll use the one with the largest volume
            if( strcmp( "translation", (const char*)variableName ) == 0 ) {
                mSelectedItem.entityController->Translate( vec.x, vec.y, vec.z );
                //if( mCurrentlySelectedGizmo && mCurrentlySelectedGizmo->IsActive() ) {
                //    mCurrentlySelectedGizmo->SetPosition( vec.x, vec.y, vec.z );
                //}
                //return true; // gizmo will move the entity with it
            } else if( strcmp( "scale", (const char*)variableName ) == 0 ) {
                mSelectedItem.entityController->Scale( vec.x, vec.y, vec.z );
            }

            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<glm::vec3>( className, classInstance, (CHAR*)variableName, &vec );
        };
    }
}

void SceneGraph::ConfigureReflectedTypesSerializeIn() {
    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "CHAR" ) ) ) {
        type->SerializeIn = [this]( const CHAR *componentName, void *component, const CHAR *variableName, const nlohmann::json& valueJSON ) -> bool
        {
            const std::string str{valueJSON.get<std::string>()};
            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<CHAR>( componentName, component, variableName, str.c_str() );
        };
    }

    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "F32" ) ) ) {
        type->SerializeIn = [this]( const CHAR *componentName, void *component, const CHAR *variableName, const nlohmann::json& valueJSON ) -> bool
        {
            const float val{valueJSON.get<float>()};
            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<F32>( componentName, component, variableName, &val );
        };
    }

    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "glm::vec3" ) ) ) {
        type->SerializeIn = [this]( const CHAR *componentName, void *component, const CHAR *variableName, const nlohmann::json& valueJSON ) -> bool
        {
            const glm::vec3 vec( valueJSON[0].get<float>(), valueJSON[1].get<float>(), valueJSON[2].get<float>() );
            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<glm::vec3>( componentName, component, variableName, &vec );
        };
    }

    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "Mesh" ) ) ) {
        type->SerializeIn = [this]( const CHAR *componentName, void *component, const CHAR *variableName, const nlohmann::json& valueJSON ) -> bool
        {
            MeshDescriptor meshDesc{mResourceDirectoryPaths[Underlying( ResourceType::mesh )]->toStdString().c_str(), valueJSON["objName"].get<std::string>().c_str()};
            const Mesh *mesh = mSimulation->engineRuntime.resourceManager->LoadResource<Mesh>( meshDesc );
            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<Mesh*>( componentName, component, variableName, mesh );
        };
    }

    if( auto type = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( CRC32( "Material" ) ) ) {
        type->SerializeIn = [this]( const CHAR *componentName, void *component, const CHAR *variableName, const nlohmann::json& valueJSON ) -> bool
        {
            MaterialDescriptor materialDesc{mResourceDirectoryPaths[Underlying( ResourceType::mesh )]->toStdString().c_str(),
                                            valueJSON["mtlFile"].get<std::string>().c_str()};

            if( valueJSON.find( "cubeMap" ) != valueJSON.end() ) {
                // TODO: Change path to texture?
                materialDesc.cubemaps[0] = {mResourceDirectoryPaths[Underlying( ResourceType::texture )]->toStdString().c_str(),
                                            valueJSON["cubeMap"].get<std::string>().c_str()};
            }

            materialDesc.shaders = {mResourceDirectoryPaths[Underlying( ResourceType::shader )]->toStdString().c_str(),
                                    valueJSON["mShader"].get<std::string>().c_str()};

            Material *material = mSimulation->engineRuntime.resourceManager->LoadResource<Material>( materialDesc );

            // TEMP: Add the uniforms (this should be made part of the material loading process)
            material->GetShader()->AddUniform( "diffuseMap" );
            material->GetShader()->AddUniform( "normalMap" );

            material->GetShader()->AddUniform( "wvp" );
            material->GetShader()->AddUniform( "world" );

            material->GetShader()->AddUniform( "gPointLight.base.color" );
            material->GetShader()->AddUniform( "gPointLight.base.ambientIntensity" );
            material->GetShader()->AddUniform( "gPointLight.base.diffuseIntensity" );

            material->GetShader()->AddUniform( "gPointLight.position" );
            // ---

            return mSimulation->engineRuntime.reflectionManager->SetReflectedVariable<Material*>( componentName, component, variableName, material );
        };
    }
}

void SceneGraph::ConfigureReflectedTypesSerializeOut() {}

void SceneGraph::SerializeInComponent( ReflectedClass *reflectedClass, void *component, const nlohmann::json& componentJSON ) {
    // Start with base classes (if any)
    for( U32 i=0; i<reflectedClass->baseClassCount; ++i ) {
        SerializeInComponent( reflectedClass->baseClasses[i], component, componentJSON );
    }

    // Now the actual class itself
    for( U32 i=0; i<reflectedClass->reflectedVariableCount; ++i ) {
        const auto rm {mSimulation->engineRuntime.reflectionManager};
        auto variable {&reflectedClass->memberVariables[i]};

        // Handle members which are themselves class instances
        if( variable->isClassInstance && (!variable->isRawPointer ) ) {
            // Arrays of instances (like arrays of textures within a material) must all be handled
            for( U32 j=0; j<variable->count; ++j ) {
                // Handle pointers
                // NOTE: ReflectedClass instance pointers being serialized in MUST BE ALLOCATED FIRST before their properties can be set/initialized
#if 0
                if(variable->isRawPointer) {
                    U32 *p = reinterpret_cast<U32*>( *reinterpret_cast<U32*>( (size_t)instanceAddress + variable->memoryOffset + ( sizeof( p ) * j ) ) );
                    if( !p ) {
                        continue;
                    }
                    SerializeIn( variable->classInstance, reinterpret_cast<void*>( &(*p) ), json[variable->typeAsString] );
                } else
#endif
                {
                    SerializeInComponent( variable->classInstance, (void*)((size_t)component + variable->memoryOffset +
                        ( rm->GetReflectedTypeInfo( variable->typeAsHash )->size * j ) ), componentJSON[variable->nameAsString] );
                }
            }
        } else {
            // Now do perform the actual serialization
            auto Serialize = mSimulation->engineRuntime.reflectionManager->GetReflectedTypeInfo( variable->typeAsHash )->SerializeIn;
            XOF_ASSERT( Serialize );

            if( Serialize ) {
                Serialize( reflectedClass->nameAsString, component, variable->nameAsString, componentJSON[variable->nameAsString] );
            }
        }
    }

}
