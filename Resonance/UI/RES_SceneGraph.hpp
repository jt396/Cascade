#ifndef RES_SCENEGRAPH_HPP
#define RES_SCENEGRAPH_HPP


#include "../Platform/XOF_Platform.hpp"
#include "RES_AssetBrowser.hpp"

#include "../ThirdParty/JSON/json.hpp"

#include <QWidget>
#include <QTreeWidget>
#include <QTableWidget>

class Entity;
class EntityController;
class Component;
class Camera;
struct ReflectedClass;
struct Simulation;


namespace Ui {
    class SceneGraph;
}

class SceneGraph : public QWidget
{
    Q_OBJECT

public:
                    explicit SceneGraph( Simulation *simulation,
                                         QString *resourceDirs,
                                         Camera *tempCamera,
                                         QWidget *parent = nullptr );
                    ~SceneGraph();

    Simulation*     GetSimulation() { return mSimulation; }

    void            AddToSceneGraph( const QString& typeAsString, const QString& name );

private slots:
    void            on_addEntityButton_released();

    void            on_sceneGraphTree_itemClicked( QTreeWidgetItem *item, int column );
    void            on_scenePropertyTree_currentItemChanged(QTreeWidgetItem *current, QTreeWidgetItem *previous);

    void            OnPropertyTableItemValueChanged();

private:
    void            AddToPropertyTable( Entity *entity, const ReflectedClass *reflectedClass, void *component, void *table );

    void            ConfigureReflectedTypesGet();
    void            ConfigureReflectedTypesSet();
    void            ConfigureReflectedTypesSerializeIn();
    void            ConfigureReflectedTypesSerializeOut();

    void            SerializeInComponent( ReflectedClass *reflectedClass, void *component, const nlohmann::json& componentJSON );

    Ui::SceneGraph  *ui;

    Simulation      *mSimulation;
    Camera          *mTempCamera;

    using serializeInFunction = std::function<void( Entity*, const nlohmann::json& )>;
    std::map<std::string, serializeInFunction>    mSerializeInFunctionTable;

    nlohmann::json  mEntitiesJSON;

    struct SelectedItem {
        EntityController    *entityController{nullptr};
        Entity              *entity{nullptr};
        Component           *component{nullptr};
        QTreeWidgetItem     *sceneGraphEntity{nullptr};
        QTreeWidgetItem     *sceneGraphComponent{nullptr};
        // prop-table item...
    };
    SelectedItem    mSelectedItem;

    QString*        mResourceDirectoryPaths[Underlying( ResourceType::count )];
};


#endif // RES_SCENEGRAPH_HPP
