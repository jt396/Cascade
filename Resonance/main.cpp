#include <iostream>

#include <QtWidgets/qapplication.h>
#include <QtWidgets/qframe.h>

#include "UI/RES_MainWindow.hpp"


int main( int argc, char **argv ) {
    QApplication app( argc, argv );

    // Set the style sheet for the ENTIRE application, not just a single widget/window type
    QFile file( "./Resources/darkorange.qss" );
    file.open( QFile::ReadOnly );
    app.setStyle( "plastique" );
    app.setStyleSheet( QLatin1String( file.readAll() ) );

    // Now create and show the main editor window
    EditorWindow worldEditor;
    worldEditor.showMaximized();

    // Run the editor
    try {
        app.exec();
    } catch( std::exception e ) {
        std::cout << e.what() << std::endl;
    }

    return 0;
}
