#ifndef RES_TEMPGRID_HPP
#define RES_TEMPGRID_HPP


#include "Rendering/XOF_Mesh.hpp"
#include "Resources/XOF_ResourceManager.hpp"


struct Grid {
                Grid();
                ~Grid();

    Mesh        mMesh;
    Shader*     mShader;
    Transform   mTransform;
    //Material*   mMaterial;
};

Grid::Grid() {
    // Setup grid mesh data
   // mMesh.mVertexData.resize( 1 );

    // TODO: Generate VAO
    MeshDescriptor meshDesc{"", "EditorGrid"};
    meshDesc.loadFromAssimpFile = false;

#if 0
    mMesh.mVertexData.push_back( glm::vec3( -2.5f, 0.f, 0.f ) );
    mMesh.mVertexData.push_back( glm::vec3(   2.f, 0.f, 0.f ) );
    mMesh.mVertexData.push_back( glm::vec3( -1.5f, 0.f, 5.f ) );
    mMesh.mVertexData.push_back( glm::vec3(  -1.f, 0.f, 1.f ) );
    mMesh.mVertexData.push_back( glm::vec3( -0.5f, 0.f, 2.f ) );
#endif

    const I32 gridSizeOverTwo = 8;
#if 1
    for( I32 i=0; i<=gridSizeOverTwo-1; ++i ) {
        float f = (float)i;
        if( i != 0 ) {
            mMesh.mVertexData.push_back( glm::vec3( -f - 0.25f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -f - 0.25f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  f + 0.25f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  f + 0.25f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3( -f - 0.75f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -f - 0.75f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  f + 0.75f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  f + 0.75f, 0.f, -gridSizeOverTwo ) );
        } else {
            mMesh.mVertexData.push_back( glm::vec3( -0.25f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -0.25f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  0.25f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  0.25f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3( -0.75f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -0.75f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  0.75f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  0.75f, 0.f, -gridSizeOverTwo ) );
        }
    }
#endif
#if 1
    for( I32 i=0; i<=gridSizeOverTwo-1; ++i ) {
        float f = (float)i;
        if( i != 0 ) {
            mMesh.mVertexData.push_back( glm::vec3( -f - 0.5f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -f - 0.5f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  f + 0.5f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  f + 0.5f, 0.f, -gridSizeOverTwo ) );
        } else {
            mMesh.mVertexData.push_back( glm::vec3( -0.5f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -0.5f, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  0.5f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  0.5f, 0.f, -gridSizeOverTwo ) );
        }
    }
#endif
    for( I32 i=0; i<=gridSizeOverTwo; ++i ) {
        if( i != 0 ) {
            mMesh.mVertexData.push_back( glm::vec3( -i, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( -i, 0.f, -gridSizeOverTwo ) );

            mMesh.mVertexData.push_back( glm::vec3(  i, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3(  i, 0.f, -gridSizeOverTwo ) );
        } else {
            mMesh.mVertexData.push_back( glm::vec3( 0.f, 0.f,  gridSizeOverTwo ) );
            mMesh.mVertexData.push_back( glm::vec3( 0.f, 0.f, -gridSizeOverTwo ) );
        }
    }

/*
    mMesh.mVertexData.push_back( glm::vec3( 1.f, 0.f,  1.f ) );
    mMesh.mVertexData.push_back( glm::vec3( 1.f, 0.f, -1.f ) );
    // ---
    mMesh.mVertexData.push_back( glm::vec3( 0.f, 0.f,  1.f ) );
    mMesh.mVertexData.push_back( glm::vec3( 0.f, 0.f, -1.f ) );
    // ---
    mMesh.mVertexData.push_back( glm::vec3( -1.f, 0.f, 1.f ) );
    mMesh.mVertexData.push_back( glm::vec3( -1.f, 0.f, -1.f ) );
*/
    mMesh.Load( (ResourceDescriptor*)&meshDesc );

    // Material data
    ResourceDescriptor shaderDesc{"../Resources/Shaders/", "PassThrough"};
    mShader = new Shader();
    mShader->Load( &shaderDesc );
    bool shaderLoaded = mShader->IsLoaded();
    XOF_ASSERT( shaderLoaded );
    mShader->AddUniform( "wvp" );
    mShader->AddUniform( "col" );
}

Grid::~Grid() { delete mShader; }


#endif // RES_TEMPGRID_HPP
