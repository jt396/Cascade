/*
===============================================================================

    CASCADE
    =======
    File	:	CAS_Resource.hpp
    Desc	:	Base class for all resource types. Provides data identifying the
                asset and its status in addition to forbidding operations we
                don't want to have happen (yet are quite easy traps to fall into).

===============================================================================
*/
#ifndef CAS_RESOURCE_HPP
#define CAS_RESOURCE_HPP


#include "Platform/XOF_Platform.hpp"
#include "GameplayFoundations/XOF_ReflectionMacros.hpp"


struct ResourceDescriptor;


XOF_REFLECTED_CLASS_BEGIN()
class Resource {
public:
                    Resource() = default;

                    Resource( const Resource& ) = delete;
                    Resource( Resource&& ) = delete;
    Resource&   	operator=( const Resource& ) = delete;
    Resource&   	operator=( Resource&& ) = delete;

    virtual     	~Resource() = default;

    virtual bool    Load( ResourceDescriptor *desc ) = 0;
    inline bool 	IsLoaded() const { return mIsLoaded; }

    inline U32  	GetUID() const { return mUID; }
    virtual CHAR* 	GetName() const = 0;

protected:
    U32         	mUID{0u};
    bool        	mIsLoaded{false};
    bool        	PADDING[3];
};
XOF_REFLECTED_CLASS_END()


#include "CAS_ResourceDescriptors.hpp"


#endif // CAS_RESOURCE_HPP
