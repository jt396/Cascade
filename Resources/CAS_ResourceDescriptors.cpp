#include "CAS_ResourceDescriptors.hpp"


// Base Resource descriptor
ResourceDescriptor::ResourceDescriptor() {}

ResourceDescriptor::ResourceDescriptor( ResourceDirectory dir, ResourceName name ) {
    directory = dir;
    fileName = name;
}

// Base specialization - mesh
MeshDescriptor::MeshDescriptor( ResourceDirectory dir, ResourceName name ) : ResourceDescriptor( dir, name ) {}

TextureDescriptor::TextureDescriptor() {}
TextureDescriptor::TextureDescriptor( ResourceDirectory dir, ResourceName name ) : ResourceDescriptor( dir, name ) {}

// Composites - material
#include "../Rendering/XOF_Material.hpp"
#include "XOF_ResourceManager.hpp"

MaterialDescriptor::MaterialDescriptor() {
    isComposite = true;
}

MaterialDescriptor::MaterialDescriptor( ResourceDirectory dir, ResourceName name ) : ResourceDescriptor( dir, name ) {
    isComposite = true;
}

bool MaterialDescriptor::CompositeLoad( Resource *parent ) {
    Material *mat = reinterpret_cast<Material*>( parent );
    ResourceManager *rm = ResourceManager::Get();

    // Load textures
    for( U32 i=0; i<mat->GetTextureCount( XOF_TEXTURE_TYPE::DIFFUSE ); ++i ) {
        Texture *texture = rm->LoadResource<Texture>( textures[XOF_TEXTURE_TYPE::DIFFUSE][i] );

        if( !texture ) {
            ResourceDescriptor defaultTexture{textures[XOF_TEXTURE_TYPE::DIFFUSE][i].directory, "ERROR_TEXTURE.png"};
            texture = rm->LoadResource<Texture>( defaultTexture );
            XOF_ASSERT( texture );
        }

        mat->SetTexture( XOF_TEXTURE_TYPE::DIFFUSE, i, texture );
    }
    for( U32 i=0; i<mat->GetTextureCount( XOF_TEXTURE_TYPE::BUMP ); ++i ) {
        ResourceDescriptor *rd = &textures[XOF_TEXTURE_TYPE::BUMP][i];
        Texture *texture = rm->LoadResource<Texture>( *rd );

        if( !texture ) {
            ResourceDescriptor defaultTexture{textures[XOF_TEXTURE_TYPE::BUMP][i].directory, "ERROR_TEXTURE.png"};
            texture = rm->LoadResource<Texture>( defaultTexture );
            XOF_ASSERT( texture );
        }

        mat->SetTexture( XOF_TEXTURE_TYPE::BUMP, i, texture );
    }

    for( U32 i=0; i<CAS_MAX_TEXTURE_COUNT; ++i ) {
        if( !cubemaps[i].fileName.IsEmpty() ) {
            CubeMap *cubemap = rm->LoadResource<CubeMap>( cubemaps[i] );

            if( !cubemap ) {
                ResourceDescriptor defaultCubemap{cubemaps[i].directory, "ERROR_TEXTURE.png"};
                cubemap = rm->LoadResource<CubeMap>( defaultCubemap );
                XOF_ASSERT( cubemap );
            }

            mat->SetCubemap( i, cubemap );
        }
    }

    // Load shaders
    Shader *shader = rm->LoadResource<Shader>( shaders );
    if( !shader ) {
        ResourceDescriptor defaultShader{shaders.directory, "DEFAULT_SHADER"};
        shader = rm->LoadResource<Shader>( defaultShader );
        XOF_ASSERT( shader );
    }
    mat->SetShader( shader );

    // Having the ability to fall-back on default/error textures and shaders
    // (which are guaranteed to be present) means composite loading a material will always succeed.
    return true;
}
