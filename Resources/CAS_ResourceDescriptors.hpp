#ifndef CAS_RESOURCEDESCRIPTORS_HPP
#define CAS_RESOURCEDESCRIPTORS_HPP


#include "CAS_Resource.hpp"
#include "Core/XOF_Strings.hpp"
#include "Rendering/XOF_Texture.hpp"


using ResourceDirectory = FixedLengthString<128>;
using ResourceName = FixedLengthString<64>;


// Base resource descriptor
struct ResourceDescriptor {
                        ResourceDescriptor();
                        ResourceDescriptor( ResourceDirectory dir, ResourceName name );

    virtual bool        CompositeLoad( Resource* parent = nullptr ) {
                            XOF_UNUSED_PARAMETER( parent );
                            return false;
                        }

    ResourceDirectory   directory;
    ResourceName        fileName;
    // TODO: Add "fullPath" field, which appends the fileName to the directory
    bool                isComposite{false};
};


struct MeshDescriptor : public ResourceDescriptor {
                        MeshDescriptor( ResourceDirectory dir, ResourceName name );

    bool                loadFromAssimpFile{true};
    bool                retainVertexData{false};
    bool                retainIndexData{false};
};

struct TextureDescriptor : public ResourceDescriptor {
                        TextureDescriptor();
                        TextureDescriptor( ResourceDirectory dir, ResourceName name );

    XOF_TEXTURE_TYPE    type;
};

// Composite resource (resources made up of other resources)
static const U32 TEMP_TEXTURE_TYPE_COUNT = 2;
static const U32 CAS_MAX_TEXTURE_COUNT = 8;
static const U32 CAS_MAX_SHADER_COUNT = 2;

struct MaterialDescriptor : public ResourceDescriptor {
                        MaterialDescriptor();
                        MaterialDescriptor( ResourceDirectory dir, ResourceName name );

    virtual bool        CompositeLoad( Resource* ) override;

                        // Max texture-types x max texture count
    TextureDescriptor   textures[TEMP_TEXTURE_TYPE_COUNT][CAS_MAX_TEXTURE_COUNT];
    ResourceDescriptor  cubemaps[CAS_MAX_TEXTURE_COUNT];

                        // One descriptor is enough for the shaders as they share
                        // the same name, only their extension/file-type differentiates them.
                        // i.e. DiffuseTexture.glvs, DiffuseTexture.glfs etc.
    ResourceDescriptor  shaders;
};


#endif // CAS_RESOURCEDESCRIPTORS_HPP
