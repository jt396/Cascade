#ifndef CAS_RESOURCETYPES_HPP
#define CAS_RESOURCETYPES_HPP


enum class ResourceType : unsigned int {
    texture = 0u,
    cubemap,
    shader,
    material,
    mesh,
    music,
    soundEffect,
    count,
    unknown
};


#endif // CAS_RESOURCETYPES_HPP
