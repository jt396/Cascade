#version 330

#extension GL_ARB_separate_shader_objects : enable

// LIGHTS ===================================================== START
struct BaseLight {
	vec3  color;
	float ambientIntensity;
	float diffuseIntensity;
};

struct DirectionalLight {
	BaseLight base;
	vec3 direction;
};
//uniform DirectionalLight DL;

struct Attenuation {
	float constant;
	float linear;
	float exponential;
};

struct PointLight {
	BaseLight base;
	vec3 position;
	Attenuation attenuation;
};
const int XOF_MAX_POINT_LIGHT_COUNT = 1;
uniform PointLight gPointLight;//s[XOF_MAX_POINT_LIGHT_COUNT];

struct SpotLight {
	PointLight base;
	vec3 direction;
	float cutoff;
};
//const int XOF_MAX_SPOT_LIGHT_COUNT = 0;
//uniform SpotLight gSpotLights[XOF_MAX_SPOT_LIGHT_COUNT];
// LIGHTS ===================================================== END 

uniform sampler2D diffuseMap;
uniform sampler2D normalMap;
// NOTE: ADD ANOTHER SAMPLER FOR SHADOW MAP?

// added for specular light
uniform vec3 eyeWorldPos;
uniform float matSpecularExponent;
uniform float matSpecularColour;

layout (location = 0) in vec3 worldPos0;
layout (location = 1) in vec3 normal0;
layout (location = 2) in vec3 tangent0;
layout (location = 3) in vec2 texCoord0;

// NORMAL-MAPPING ===================================================== START
vec3 CalculateNormalFromMap() {
	vec3 normal = normalize( normal0 );
	vec3 tangent = normalize( tangent0 );

	tangent = normalize( tangent - dot( tangent, normal ) * normal );
	vec3 biTangent = cross( tangent, normal );
	vec3 bumpNormal = texture2D( normalMap, texCoord0 ).xyz;
	bumpNormal = 2.f * bumpNormal - vec3( 1.f, 1.f, 1.f );

	mat3 TBN = mat3( tangent, biTangent, normal );
	return normalize( TBN * bumpNormal );
}
// NORMAL-MAPPING ===================================================== END

vec3 CalcLightInternal(BaseLight light, vec3 direction, vec3 normal) {
	// AMBIENT =====================================================
	vec3 ambientColor = light.ambientIntensity * light.color;

	// DIFFUSE =====================================================
	vec3 diffuseColor = vec3(0.f, 0.f, 0.f);
	vec3 specularColor = vec3( 0.0f, 0.f, 0.f );
	
	float diffuseFactor = dot(normal, -direction);
	if(diffuseFactor > 0) {
		diffuseColor = light.color * light.diffuseIntensity * diffuseFactor;
		
		// SPECULAR ====================================================
		vec3 vertexToEye = normalize(eyeWorldPos - worldPos0);
		// having direction negated here messes with directional/point specular?
		vec3 lightReflect = normalize(reflect(direction, normal));
		float specularFactor = max(dot(vertexToEye, lightReflect), 0.f);
		if(specularFactor > 0) {
			specularFactor = pow(specularFactor, matSpecularExponent);
			specularColor = light.color * matSpecularColour * specularFactor;
		}
	}
	
	return diffuseColor + ambientColor;
}

vec3 CalcPointLight(PointLight pointLight, vec3 normal) {
	vec3 lightDirection = worldPos0 - pointLight.position;
	float distance = length(lightDirection);
	lightDirection = normalize(lightDirection);
	
	vec3 colour = CalcLightInternal(pointLight.base, lightDirection, normal);
	/*
	float attenuation = pointLight.attenuation.constant +
						pointLight.attenuation.linear * distance +
						pointLight.attenuation.exponential * (distance * distance);
	*/
	return colour;// / attenuation;
}

vec3 CalcSpotLight(SpotLight spotLight, vec3 normal) {
	vec3 lightToPixel = vec3(worldPos0 - spotLight.base.position);
	float spotFactor = dot(lightToPixel, spotLight.direction);
	
	if(spotFactor > spotLight.cutoff) {
		return CalcPointLight(spotLight.base, normal) * 
			(1.f - (1.f - spotFactor) * 1.f / (1.f - spotLight.cutoff));
	}
	return vec3(0.f, 0.f, 0.f);
}

vec3 CalcDirectionalLight(DirectionalLight dLight, vec3 normal) {
	return CalcLightInternal(dLight.base, dLight.direction, normal);
}

void main() {
	//vec3 normal = CalculateNormalFromMap();//normalize(normal0);
	// get directional contribution
	vec3 totalLight = vec3(0.f, 0.f, 0.f);//CalcDirectionalLight(DL, normal);
	
	// loop to sum up multiple point light contributions
	//for(int i=0; i<XOF_MAX_POINT_LIGHT_COUNT; ++i) {
		totalLight += CalcPointLight(gPointLight/*s[i]*/, CalculateNormalFromMap());
	//}

	//for(int i=0; i<XOF_MAX_SPOT_LIGHT_COUNT; ++i) {
	//	totalLight += CalcSpotLight(gSpotLights[i], normal);
	//}
	
	gl_FragColor = vec4(totalLight, 1.f) * texture2D(diffuseMap, texCoord0);
}