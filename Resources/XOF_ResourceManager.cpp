/*
===============================================================================

    XOF
    ===
    File	:	XOF_ResourceManager.cpp
    Desc	:	Manage the resrouces used by objects in the game.

===============================================================================
*/
#include "XOF_ResourceManager.hpp"

#include <GL/glew.h>
#include <fstream>
#include <algorithm>


ResourceManager* ResourceManager::Get() {
    static ResourceManager resourceManagerSingleton;
    return &resourceManagerSingleton;
}

bool ResourceManager::StartUp() {
    // Setup memory allocators
    const size_t resourceTypeSizes[] {
        sizeof( Texture ),
        sizeof( CubeMap ),
        sizeof( Shader ),
        sizeof( Material ),
        sizeof( Mesh ),
        sizeof( SoundEffect ),
        sizeof( Music )
    };
    for( U32 i=0; i<Underlying( ResourceType::count ); ++i ) {
        mResourceAllocators[i].Configure( resourceTypeSizes[i] );
    }

    // Load up the default/'on-error' assets
    ResourceDescriptor defaultTextureDesc {"../Resources/Textures/", "CASCADE_ERROR_TEXTURE.png"};
    XOF_ASSERT( LoadResource<Texture>( defaultTextureDesc ) );
    //XOF_ASSERT( LoadResource<CubeMap>( defaultTextureDesc ) );

    MeshDescriptor defaultMeshDesc {"../Resources/Meshes/", "CASCADE_ERROR_MESH.obj"};
    defaultMeshDesc.loadFromAssimpFile = true;
    XOF_ASSERT( LoadResource<Mesh>( defaultMeshDesc ) );

    MaterialDescriptor defaultMaterialDesc {"", "CASCADE_ERROR_MATERIAL"};
    defaultMaterialDesc.shaders = {"../Resources/Shaders/", "CASCADE_ERROR_SHADER"};
    XOF_ASSERT( LoadResource<Material>( defaultMaterialDesc ) );

    // Make use of r-values to move the default assets out of the primary/user asset-map into the default-asset-map
    for( U32 i=0; i<Underlying( ResourceType::count ); ++i ) {
        if( !mResources[i].empty() ) {
            mDefaultResources[i] = std::move( mResources[i] );
            mResources[i].clear();
        }
    }

    // Done
    XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources, "ResourceManager::StartUp() -> Succeeded!" );
    return true;
}

void ResourceManager::ShutDown() {
    // Free user-loaded resources
    for( U32 i=0; i<Underlying( ResourceType::count ); ++i ) {
        for( auto& resource : mResources[i] ) {
            mResourceAllocators[i].DestructAndDeallocate<Resource>( resource.second );
        }
        mResources[i].clear();
    }

    // Free default/'on-error' resources
    for( U32 i=0; i<Underlying( ResourceType::count ); ++i ) {
        if( !mDefaultResources[i].empty() ) {
            mResourceAllocators[i].DestructAndDeallocate<Resource>( mDefaultResources[i].begin()->second );
        }
        mDefaultResources[i].clear();
    }

    XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources, "ResourceManager::ShutDown() -> Succeeded!" );
}
