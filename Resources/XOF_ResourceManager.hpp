/*
===============================================================================

    XOF
    ===
    File	:	XOF_ResourceManager.hpp
    Desc	:	Manage the resources used by entities in game.

===============================================================================
*/
#ifndef XOF_RESOURCE_MANAGER_HPP
#define XOF_RESOURCE_MANAGER_HPP

#include <unordered_map>

#include "CAS_Resource.hpp"
#include "CAS_ResourceDescriptors.hpp"

#include "Core/XOF_LoggingManager.hpp"
#include "Core/XOF_DynamicPoolAllocator.hpp"
#include "Core/XOF_CRC32.hpp"

#include "Rendering/XOF_Mesh.hpp"
#include "Rendering/XOF_Material.hpp"

#include "Audio/XOF_SoundEffect.hpp"
#include "Audio/XOF_Music.hpp"


#include "CAS_ResourceTypes.hpp"
// Base
template<ResourceType type> struct ResourceTypeIndexBase { static const ResourceType val{type}; };
// Specilization base
template<typename type> struct ResourceTypeIndex                : public ResourceTypeIndexBase<ResourceType::unknown>       {};
// Specilizations
template<>              struct ResourceTypeIndex<Texture>       : public ResourceTypeIndexBase<ResourceType::texture>       {};
template<>              struct ResourceTypeIndex<CubeMap>       : public ResourceTypeIndexBase<ResourceType::cubemap>       {};
template<>              struct ResourceTypeIndex<Shader>        : public ResourceTypeIndexBase<ResourceType::shader>        {};
template<>              struct ResourceTypeIndex<Material>      : public ResourceTypeIndexBase<ResourceType::material>      {};
template<>              struct ResourceTypeIndex<Mesh>          : public ResourceTypeIndexBase<ResourceType::mesh>          {};
template<>              struct ResourceTypeIndex<SoundEffect>   : public ResourceTypeIndexBase<ResourceType::soundEffect>   {};


class ResourceManager {
public:
    static ResourceManager*             Get();

    bool                                StartUp();
    void                                ShutDown();

                                        template<typename ResourceType>
    ResourceType*                       LoadResource( ResourceDescriptor& desc );

                                        template<typename ResourceType>
    ResourceType*                       GetResource( const ResourceName &resourceName ) const;

                                        template<typename ResourceType>
    void                                UnloadResource( const ResourceName &resourceName );

private:
    std::unordered_map<U32, Resource*>	mResources[Underlying( ResourceType::count )];
                                        // Default resources are returned when the originall requested resource fails to load
    std::unordered_map<U32, Resource*>	mDefaultResources[Underlying( ResourceType::count )];
    DynamicPoolAllocator                mResourceAllocators[Underlying( ResourceType::count )];
};


template<typename ResourceType>
ResourceType* ResourceManager::LoadResource( ResourceDescriptor& desc ) {
    ResourceType *resource = GetResource<ResourceType>( desc.fileName );
    const U32 typeIndex {Underlying( ResourceTypeIndex<ResourceType>::val )};

    if( resource ) {
        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                         "ResourceManager::LoadResource( %s ) -> already loaded!", desc.fileName.CStr() );
        return resource;
    }

    resource = mResourceAllocators[typeIndex].AllocateAndConstruct<ResourceType>();
    const bool loaded = resource && resource->Load( &desc ) && ( desc.isComposite ? desc.CompositeLoad( resource ) : true );

    if( loaded ) {
        U32 resourceHash {CalculateCRC32( 0, desc.fileName.CStr(), desc.fileName.GetLength() )};
        mResources[typeIndex].insert( std::pair<U32, Resource*>( resourceHash, resource ) );

        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                         "ResourceManager::LoadResource( %s ) -> Succeeded!", desc.fileName.CStr() )
    } else {
        mResourceAllocators[typeIndex].DestructAndDeallocate<ResourceType>( resource );
        resource = reinterpret_cast<ResourceType*>( mDefaultResources[typeIndex].begin()->second );

        XOF_LOG_MESSAGE( LoggingVerbosity::low, LoggingChannels::resources,
                         "ResourceManager::LoadResource( %s ) -> Failed! Using default asset.", desc.fileName.CStr() )
    }

    return resource;
}

template<typename ResourceType>
ResourceType* ResourceManager::GetResource( const ResourceName &resourceName ) const {
    const U32 resourceNameHash {CalculateCRC32( 0, resourceName.CStr(), resourceName.GetLength() )};
    const U32 typeIndex {Underlying( ResourceTypeIndex<ResourceType>::val )};

    auto itr = mResources[typeIndex].find( resourceNameHash );
    return ( itr != mResources[typeIndex].end() ) ? reinterpret_cast<ResourceType*>( itr->second ) : nullptr;
}

template<typename ResourceType>
void ResourceManager::UnloadResource( const ResourceName &resourceName ) {
    const U32 resourceNameHash {CalculateCRC32( 0, resourceName.CStr(), resourceName.GetLength() )};
    const U32 typeIndex {Underlying( ResourceTypeIndex<ResourceType>::val )};

    auto *map = &mResources[typeIndex];

    if( auto itr = map->find( resourceNameHash ) != map->end() ) {
        mResourceAllocators[typeIndex].DestructAndDeallocate<ResourceType>( itr->second );
        map->erase( resourceHash );
    }
}


#endif // XOF_RESOURCE_MANAGER_HPP
