#ifndef ENGINE_PREP_HPP
#define ENGINE_PREP_HPP


#include "XOF_Game.hpp"
// TODO: Replace these implementation-specific includes with the abstract ones
#include "../HIDs/XOF_Keyboard_SFML.hpp"
#include "../HIDs/XOF_Mouse_SFML.hpp"
#include "../Rendering/XOF_FirstPersonCamera.hpp"
class Renderer;


#include "../GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp"


class EnginePrep : public Game {
public:
						EnginePrep();
	virtual				~EnginePrep();

	virtual bool		Initialise( U16 width, U16 height, CHAR* caption ) override;	
	virtual void		Run();
	virtual void		Update( float dt );

private:
    EngineRuntime       mEngine;

	float				mMovementSpeed;
	float				mLookingSpeed;

	U32					mScreenWidth;
	U32					mScreenHeight;

	virtual void		SetupScene();

	virtual void		HandleSystemEvents();
	virtual void		HandleGameEvents();
	virtual void		HandleInput( float dt );
	virtual void		Render( float dt );
};


#endif // ENGINE_PREP_HPP
