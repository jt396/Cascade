/*
===============================================================================

	XOF
	===
	File	:	GDC_FrustumCulling.cpp
	Desc	:	AABB/OOBB-based collision-detection dev/testing as part of the GDC Rapture demo.

===============================================================================
*/
#include "EnginePrep.hpp"

// RESOURCES
#include "../Resources/XOF_ResourceManager.hpp"
ResourceManager *gResourceManager;

// RENDERING
#include <SFML/Graphics.hpp>
#include "../Platform/XOF_Platform.hpp"
#include "../Platform/XOF_PlatformInfo.hpp"
#include "../Platform/XOF_Timer.hpp"
#include "../Rendering/XOF_FirstPersonCamera.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Shader.hpp"

// COLLISION
#include "../Collision&Physics/XOF_CollisionWorld.hpp"
//#include "../Collision&Physics/XOF_AABB.hpp"

// CAMERA
FirstPersonCamera gCamera;
float gYaw = 0.f;
float gPitch = 0.f;
static U32 gWindowWidthOverTwo = 0;
static U32 gWindowHeightOverTwo = 0;

// ENTITIES
#include "TempGameObjects.hpp"
std::vector<TempGameObject*> gTempGameObjects;
TempGameObject *gSelectedTempGameObject;

// COLLISION
#include "../Collision&Physics/XOF_Ray.hpp"
static const float FORWARDS = -1.f; // "forwards" in the scene (normally -1 in GL)
void CreateRayAndTestForCollision( const glm::vec2 &mouseViewSpacePos );
void TestForCollisionBetweenBVs();

// MISC
static double FRAME_TIME = 1000.0 / XOF_TARGET_FRAMERATE;
static U32 ENTITY_COUNT = 1;
U32 gFrustumCulledEntities = 0;


EnginePrep::EnginePrep() {}

EnginePrep::~EnginePrep() {
	mEngine.resourceManager->ShutDown();
	mEngine.renderer->ShutDown();
}

bool EnginePrep::Initialise( U16 width, U16 height, CHAR* caption ) {
	if( !Game::Initialise( width, height, caption ) ) {
		return false;
	}

	mMouse.SetWindow( mWindow.get() );

	mEngine.resourceManager = ResourceManager::Get();
	mEngine.resourceManager->StartUp();

	mEngine.renderer.reset( new Renderer() );
	mEngine.renderer->StartUp();

	SetupScene();

	mMovementSpeed = 0.005f;
	mLookingSpeed = 0.001f;

	mEngine.renderer->Resize( mWindow->getSize().x, mWindow->getSize().y );

	gWindowWidthOverTwo = mScreenHeight / 2;
	gWindowHeightOverTwo = mScreenWidth / 2;

	//mWindow->setMouseCursorVisible( false );
	
	mIsInitialised = true;
	return mIsInitialised;
}

void EnginePrep::Run() {
	if( mIsInitialised ) {
		Timer timer;
		timer.Reset();
		double startTime = timer.GetMilliseconds();
		
		double elapsedTime = 0.0;
		double timeDelta = 0.0;
		double averageTimeDelta = 0.0;
		double unprocessedTime = 0.0;
		U32 frameCount = 0;

		double runningTime = 0.0;

		double lastTime = timer.GetMilliseconds();
		while( mWindow->isOpen() ) {
			double currentTime = timer.GetMilliseconds();
			timeDelta = currentTime - lastTime;
			lastTime = currentTime;

			runningTime = (currentTime - startTime) / 1000.f;

			if( unprocessedTime > FRAME_TIME ) {
				Update( static_cast<float>( runningTime ) );
				Render( static_cast<float>( timeDelta ) );
				++frameCount;
				unprocessedTime -= FRAME_TIME;
			}

			unprocessedTime += timeDelta;
			elapsedTime += timeDelta;
			averageTimeDelta += timeDelta;
			if( elapsedTime >= 1000 ) {
				for( U32 i=0; i<ENTITY_COUNT; ++i ) {
					gCamera.ConstructFrustum( gTempGameObjects[i]->GetTransform().GetModelToWorldMatrix() );
					if( !gTempGameObjects[i]->mTempUsingAABB ) {
						if( !( gTempGameObjects[i]->mIsVisible = gCamera.TEMP_SphereFrustumIntersection( gTempGameObjects[i]->mSphere ) ) ) {
							std::cout << "CULLING ENTITY (SPHERE): " << i << std::endl;
							++gFrustumCulledEntities;
						}
					} else {
						if( !( gTempGameObjects[i]->mIsVisible = gCamera.TEMP_AABBFrustumIntersection( gTempGameObjects[i]->mAABB ) ) ) {
							std::cout << "CULLING ENTITY (AABB): " << i << std::endl;
							++gFrustumCulledEntities;
						}
					}
				}

				std::cout << "FPS: " << frameCount << " (" << ( averageTimeDelta / frameCount ) <<  " MS)" << 
					" | " << gFrustumCulledEntities << "/" << ENTITY_COUNT << " Entities culled." << std::endl;
				gFrustumCulledEntities = 0;
				averageTimeDelta = 0.0;
				frameCount = 0;
				elapsedTime = 0.0;
			}

			HandleInput( static_cast<float>( timeDelta ) );
			HandleEvents();
		} // while( mWindow->isOpen() ) 
	} // if( mIsInitialised )
}

void EnginePrep::Update( float dt ) {
	for( auto &tgo : gTempGameObjects ) {
		tgo->Update( dt );
	}
}

void EnginePrep::SetupScene() {
	// LOAD MESHES
	MeshDescNEW meshDescNEW;
	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
	meshDescNEW.fileName = "./Resources/Meshes/Elizabeth.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.materialName = "material0";
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "mesh0", &meshDescNEW );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
	meshDescNEW.fileName = "./Resources/Meshes/Booker_DeWitt.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.materialName = "material1";
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "mesh1", &meshDescNEW );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
	meshDescNEW.fileName = "./Resources/Meshes/barrel.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.materialName = "material2";
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "./Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "mesh2", &meshDescNEW );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
	meshDescNEW.fileName = "./Resources/Meshes/XOF_UNIT_CUBE.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.materialName = "XOF_DEBUG_DRAW_MATERIAL";
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "./Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "./Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "XOF_UNIT_CUBE", &meshDescNEW );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
	meshDescNEW.fileName = "./Resources/Meshes/XOF_UNIT_SPHERE.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.materialName = "XOF_DEBUG_DRAW_MATERIAL";
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "./Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
	meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "./Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "XOF_UNIT_SPHERE", &meshDescNEW );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	// CREATE TEMP GAME OBJECTS
	for( U32 i=0; i<ENTITY_COUNT; ++i ) {
		TempGameObject *tgo = new TempGameObject( &mEngine, glm::vec3( 0.f, 0.f, 0.f ), 
			reinterpret_cast<Mesh*>( mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MESH, "mesh" + std::to_string( 2 ) ) ),
			reinterpret_cast<Material*>( mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "material" + std::to_string( 2 ) ) ),
			&gCamera );
	
		//tgo->TempTranslate( i * 3, 0.f, 0.f );
		//tgo->Rotate( 0.f, 0.f, 0.f );

		// SETUP AABB/SPHERE - COLLISION		
		Mesh::MeshDimensions &md = const_cast<Mesh::MeshDimensions&>( tgo->GetMesh()->GetDimensions() );
		glm::vec3 min( glm::vec3( md.min.x, md.min.y, md.min.z ) );
		glm::vec3 max( glm::vec3( md.max.x, md.max.y, md.max.z ) );

		//if( i >= 0 ) {
			//tgo->mAABB.originalCenter = tgo->mAABB.center = ( min + max ) / 2.f;
			//tgo->mAABB.originalExtents = tgo->mAABB.extents = ( max - min ) / 2.f;
			GenerateSimpleSphere( tgo->mSphere, min, max, mEngine.resourceManager );
			//GenerateRitterSphere( tgo->mSphere, min, max );
			tgo->mTempUsingAABB = false;
		//} else {
			//GenerateSimpleSphere( tgo->mSphere, min, max );
			//GenerateSimpleSphere( tgo->mSphere, min, max );
			//GenerateRitterSphere( tgo->mSphere, min, max );
			//GenerateAABB
			//GenerateAABB( tgo->mAABB, min, max, mEngine.resourceManager );
			//tgo->mTempUsingAABB = true;
		//}
	
		tgo->TempTranslate( 0.f, 0.f, 3.f );
		//tgo->Scale( 1.f, i * 1.f + 1.f, 1.f );

		gTempGameObjects.push_back( tgo );
	}

	// SETUP SHADER VARIBLES
	Material *mat0 = reinterpret_cast<Material*>( mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "material0" ) );
	Material *debugMat = reinterpret_cast<Material*>( mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "XOF_DEBUG_DRAW_MATERIAL" ) );

	mat0->GetShader()->AddUniform( "wvp" );
	debugMat->GetShader()->AddUniform( "wvp" );
	debugMat->GetShader()->AddUniform( "colour" );
	debugMat->GetShader()->SetUniform( "colour", glm::vec4( 0.f, 1.f, 0.f, 1.f ) );
	debugMat->GetShader()->Bind();


	// SETUP CAMERA
	gCamera.Setup( glm::vec3( 0.f, 0.f, 0.f ), glm::vec3( 0.f, 0.f, 1.f ),
		static_cast<float>( mWindow->getSize().x ),  static_cast<float>( mWindow->getSize().y ),
		static_cast<float>( mWindow->getSize().x ) / static_cast<float>( mWindow->getSize().y ), 
		0.1f, 1000.f );
}

void EnginePrep::HandleEvents() {
	sf::Event event;
	while( mWindow->pollEvent( event ) ) {
		if( event.type == sf::Event::Closed || mKeyboard.IsKeyDown( XOF_KEYBOARD::ESCAPE ) ) {
			mWindow->close();
		}

		if( event.type == sf::Event::Resized ) {
			mEngine.renderer->Resize( event.size.width, event.size.height );
			gCamera.Resize( static_cast<float>( event.size.width ), static_cast<float>( event.size.height ) );

			gWindowWidthOverTwo = event.size.width / 2;
			gWindowHeightOverTwo = event.size.height / 2;
		}

		if( event.type == sf::Event::LostFocus ) {
			do {
				mWindow->pollEvent( event );
				mWindow->setTitle( "XOF [PAUSED]" );
			} while( event.type != sf::Event::GainedFocus );
			mWindow->setTitle( "XOF [RUNNING]" );
		}
	}
}

void EnginePrep::HandleInput( float dt ) {	
	// NOTE: This is causing issues with the camera (turning it upside down and such?)
	//		 Also, switching to keyboard controls while using the mouse to fire rays
	//		 for collision testing.
	//glm::vec2 cameraRelativePos = mMouse.GetRelativePosition();
	//mMouse.SetPosition( gWindowWidthOverTwo, gWindowHeightOverTwo );
	//gYaw += ( gWindowWidthOverTwo - cameraRelativePos.x ) * -mLookingSpeed * dt;
	//gPitch += ( gWindowHeightOverTwo - cameraRelativePos.y ) * -mLookingSpeed * dt;
	//gCamera.SphericalPitchAndYaw( gPitch, gYaw );

	// Movement
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::W ) ) {
		gCamera.SphericalTranslate( glm::vec3( gCamera.GetViewDirection() * dt * mMovementSpeed ) );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::A ) ) {
		gCamera.SphericalTranslate( glm::vec3( gCamera.GetRight() * dt * mMovementSpeed ) );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::S ) ) {
		gCamera.SphericalTranslate( glm::vec3( -gCamera.GetViewDirection() * dt * mMovementSpeed ) );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::D ) ) {
		gCamera.SphericalTranslate( glm::vec3( -gCamera.GetRight() * dt * mMovementSpeed ) );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::Q ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * mMovementSpeed, 0.f ) );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::E ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * -mMovementSpeed, 0.f ) );
	}
	// Looking around
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_UP ) ) {
		gCamera.Pitch( dt * -mLookingSpeed );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_DOWN ) ) {
		gCamera.Pitch( dt * mLookingSpeed );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_LEFT ) ) {
		gCamera.Yaw( dt * mLookingSpeed );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_RIGHT ) ) {
		gCamera.Yaw( dt * -mLookingSpeed );
	}

	// Picking
	if( mMouse.IsButtonDown( XOF_MOUSE::LEFT ) ) {
		std::cout << "MouseX: " << mMouse.GetRelativePosition().x << ", MouseY: " << mMouse.GetRelativePosition().y << std::endl;
		CreateRayAndTestForCollision( mMouse.GetRelativePosition() );
		//TestForCollisionBetweenAABBs();
	}

	// Moving selected temp game object
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::J ) ) {
		gSelectedTempGameObject->TempTranslate( -0.001f, 0.f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::L ) ) {
		gSelectedTempGameObject->TempTranslate( 0.001f, 0.f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::U ) ) {
		gSelectedTempGameObject->TempTranslate( 0.f, -0.001f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::O ) ) {
		gSelectedTempGameObject->TempTranslate( 0.f, 0.001f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::I ) ) {
		gSelectedTempGameObject->TempTranslate( 0.f, 0.f, -0.001f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::K ) ) {
		gSelectedTempGameObject->TempTranslate( 0.f, 0.f, 0.001f );
		TestForCollisionBetweenBVs();
	}
	// Rotate selected temp game object
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::N ) ) {
		gSelectedTempGameObject->TempRotate( -0.001f, 0.f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::M ) ) {
		gSelectedTempGameObject->TempRotate( 0.001f, 0.f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::Y ) ) {
		gSelectedTempGameObject->TempRotate( 0.f, -0.001f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::H ) ) {
		gSelectedTempGameObject->TempRotate( 0.f, 0.001f, 0.f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::Z ) ) {
		gSelectedTempGameObject->TempRotate( 0.f, 0.f, -0.001f );
		TestForCollisionBetweenBVs();
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::X ) ) {
		gSelectedTempGameObject->TempRotate( 0.f, 0.f, 0.001f );
		TestForCollisionBetweenBVs();
	}
	// Scale selected temp game object
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::R ) ) {
		gSelectedTempGameObject->TempScale( 0.001f, 0.f, 0.f );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::F ) ) {
		gSelectedTempGameObject->TempScale( -0.001f, 0.f, 0.f );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::T ) ) {
		gSelectedTempGameObject->TempScale( 0.f, 0.001f, 0.f );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::G ) ) {
		gSelectedTempGameObject->TempScale( 0.f, -0.001f, 0.f );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::C ) ) {
		gSelectedTempGameObject->TempScale( 0.f, 0.f, 0.001f );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::V ) ) {
		gSelectedTempGameObject->TempScale( 0.f, 0.f, -0.001f );
	}
}

void EnginePrep::Render( float dt ) {
	mEngine.renderer->RenderFrame();
	mWindow->display();
	mEngine.renderer->ClearScreen();
}


// ---
static const U32 SCREEN_WIDTH = 640;
static const U32 SCREEN_HEIGHT = 480;
void CreateRayAndTestForCollision( const glm::vec2 &mouseViewSpacePos ) {
	// into 3D NDC space
	float x = ( 2.f * mouseViewSpacePos.x ) / SCREEN_WIDTH - 1.f;
	float y = -( ( 2.f * mouseViewSpacePos.y ) / SCREEN_HEIGHT - 1.f );
	float z = 1.f; // not strictly necessary yet
	glm::vec3 rayNDS( x, y, z );

	// into 4D Homogeneous Clip space
	// NOTE: No need to reverse perspective divide here since this is a ray with no intrinsic depth
	glm::vec4 rayCLIP( rayNDS.x, rayNDS.y, FORWARDS, 1.f );

	// into 4D Camera Coordinates
	// Normally, you use a projection matrix to go from eye/camera space to clip space.
	// In this instance however we're going the other way, so we invert this matrix.
	glm::vec4 rayEYE( glm::inverse( gCamera.GetProjectionMatrix() ) * rayCLIP );
	// zw part = "forwards(FOWARDS)" and "not a point (0.f)"
	rayEYE = glm::vec4( rayEYE.x, rayEYE.y, FORWARDS, 0.f );

	// into 4D World Coordinates
	// -1 for forwards means the ray isn't normalised, normalise the vector here
	glm::vec3 rayWORLD( glm::inverse( gCamera.GetViewMatrix() ) * rayEYE );
	glm::normalize( rayWORLD );

	std::cout << "PickingRay = <" << rayWORLD.x << ", " << rayWORLD.y << ", " << rayWORLD.z << ">" << std::endl;

	//Ray ray( gCamera.GetPosition(), rayWORLD, gCamera.GetPosition().z, gCamera.GetPosition().z + 10.f );
	Ray ray( gCamera.GetPosition(), rayWORLD );
	ray.direction = glm::normalize( ray.direction );

	for( auto &tgo : gTempGameObjects ) {		
		if( tgo->mAABB.IsHit( ray ) || tgo->mSphere.IsHit( ray ) ) {
			gSelectedTempGameObject = tgo;
			break;
		}
	}
}

void TestForCollisionBetweenBVs() {
	if( gSelectedTempGameObject ) {
		for( auto &tgo : gTempGameObjects ) {
			if( tgo != gSelectedTempGameObject ) {
				// AABB
				//tgo->mAABB.IsHit( gSelectedTempGameObject->mAABB );
				//tgo->mAABB.IsHit( gSelectedTempGameObject->mSphere );
				// Sphere
				//tgo->mSphere.IsHit( gSelectedTempGameObject->mSphere );
				//tgo->mSphere.IsHit( gSelectedTempGameObject->mAABB );
				gSelectedTempGameObject->mSphere.isHit = tgo->mAABB.isHit = 
					TestForSphereAABBIntersection( gSelectedTempGameObject->mSphere, tgo->mAABB );
			}
		}
	}
}
