/*
===============================================================================

	XOF
	===
	File	:	TEST_EntityAndComponentSystem.cpp
	Desc	:	Moving from a simple monolithic game-object with fixed members/
				instances of each member to a flexible component and entity
				system.

===============================================================================
*/
#include "EnginePrep.hpp"

// RESOURCES
#include "../Resources/XOF_ResourceManager.hpp"
ResourceManager *gResourceManager;

// RENDERING
#include <SFML/Graphics.hpp>
#include "../Rendering/XOF_Mesh.hpp"
#include "../Platform/XOF_Platform.hpp"
#include "../Platform/XOF_PlatformInfo.hpp"
#include "../Platform/XOF_Timer.hpp"
#include "../Rendering/XOF_FirstPersonCamera.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Shader.hpp"

// COLLISION
#include "../Collision&Physics/XOF_CollisionWorld.hpp"

// CAMERA
FirstPersonCamera gCamera;
float gYaw = 0.f;
float gPitch = 0.f;
static U32 gWindowWidthOverTwo = 0;
static U32 gWindowHeightOverTwo = 0;

// COLLISION
#include "../Collision&Physics/XOF_Ray.hpp"
static const float FORWARDS = -1.f; // "forwards" in the scene (normally -1 in GL)
void CreateRayAndTestForCollision( const glm::vec2 &mouseViewSpacePos );
void TestForCollisionBetweenBVs();

#include  "../Collision&Physics/XOF_AABBNew.hpp"



// GAME-PLAY/GAME-ENTITIES
#include "TempEvents.hpp"
std::vector<std::unique_ptr<Event>> gTempEvents;

#include "../GameplayFoundations/XOF_Entity.hpp"
#include "TempTestEntitiesAndComponents.hpp"
std::vector<std::unique_ptr<Entity>> gTempEntities;


// MISC
static double FRAME_TIME = 1000.0 / XOF_TARGET_FRAMERATE;
static U32 ENTITY_COUNT = 1;
U32 gFrustumCulledEntities = 0;


EnginePrep::EnginePrep() {}

EnginePrep::~EnginePrep() {
	mEngine.resourceManager->ShutDown();
	mEngine.renderer->ShutDown();
}

bool EnginePrep::Initialise( U16 width, U16 height, CHAR* caption ) {
	if( !Game::Initialise( width, height, caption ) ) {
		return false;
	}

    mEngine.inputDevices.mouse.SetWindow( mWindow.get() );

	mEngine.resourceManager = ResourceManager::Get();
	mEngine.resourceManager->StartUp();

	mEngine.renderer.reset( new Renderer() );
	mEngine.renderer->StartUp();

	SetupScene();

	mMovementSpeed = 0.005f;
	mLookingSpeed = 0.001f;

	mEngine.renderer->SetScreenSize( mWindow->getSize().x, mWindow->getSize().y );

	gWindowWidthOverTwo = mScreenHeight / 2;
	gWindowHeightOverTwo = mScreenWidth / 2;

	//mWindow->setMouseCursorVisible( false );
	
	mIsInitialised = true;
	return mIsInitialised;
}

void EnginePrep::Run() {
	if( mIsInitialised ) {
		Timer timer;
		timer.Reset();
		double startTime = timer.GetMilliseconds();
		
		double elapsedTime = 0.0;
		double timeDelta = 0.0;
		double averageTimeDelta = 0.0;
		double unprocessedTime = 0.0;
		U32 frameCount = 0;

		double runningTime = 0.0;

		double lastTime = timer.GetMilliseconds();
		while( mWindow->isOpen() ) {
			double currentTime = timer.GetMilliseconds();
			timeDelta = currentTime - lastTime;
			lastTime = currentTime;

			runningTime = (currentTime - startTime) / 1000.f;

			if( unprocessedTime > FRAME_TIME ) {
				Update( static_cast<float>( runningTime ) );
				Render( static_cast<float>( timeDelta ) );
				++frameCount;
				unprocessedTime -= FRAME_TIME;
			}

			unprocessedTime += timeDelta;
			elapsedTime += timeDelta;
			averageTimeDelta += timeDelta;
			if( elapsedTime >= 1000 ) {
				std::cout << "FPS: " << frameCount << std::endl;
				averageTimeDelta = 0.0;
				frameCount = 0;
				elapsedTime = 0.0;
			}

			HandleInput( static_cast<float>( timeDelta ) );
			HandleSystemEvents();
			HandleGameEvents();
		} // while( mWindow->isOpen() ) 
	} // if( mIsInitialised )
}

void EnginePrep::Update( float dt ) {
	for( auto &tgo : gTempEntities ) {
		tgo->Update( dt );
	}
}

void EnginePrep::SetupScene() {
	// LOAD RESOURCES
	// ---
	MeshDescNEW meshDescNEW;
	meshDescNEW.meshType =  XOF_MESH_TYPE::OBJ;
    meshDescNEW.fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Meshes/XOF_UNIT_CUBE.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.TEMPuseMaterialFromMeshFile = true;
	meshDescNEW.materialName = "platformMaterial";
    meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
    meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	XOF_ASSERT( mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "XOF_UNIT_CUBE", &meshDescNEW ) );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	meshDescNEW.meshType = XOF_MESH_TYPE::OBJ;
    meshDescNEW.fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Meshes/Door.obj";
	meshDescNEW.requiresNewMaterial = true;
	meshDescNEW.TEMPuseMaterialFromMeshFile = true;
	meshDescNEW.materialName = "moverMaterial";
    meshDescNEW.material.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
    meshDescNEW.material.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/DiffuseTexture";
	meshDescNEW.material.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
	XOF_ASSERT( mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MESH, "moverMesh", &meshDescNEW ) );
	memset( &meshDescNEW, 0x00, sizeof( MeshDescNEW ) );

	MaterialDescNEW materialDescNEW;
    materialDescNEW.shaders[XOF_SHADER_TYPE::VERTEX].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	materialDescNEW.shaderCount[XOF_SHADER_TYPE::VERTEX] = 1;
    materialDescNEW.shaders[XOF_SHADER_TYPE::FRAGMENT].fileName = "C:/Users/Jamie/Desktop/XOF/Resources/Shaders/XOF_DEBUG_DRAW_SHADER";
	materialDescNEW.shaderCount[XOF_SHADER_TYPE::FRAGMENT] = 1;
    void *p = mEngine.resourceManager->LoadResource( XOF_RESOURCE_TYPE::MATERIAL, "XOF_DEBUG_DRAW_MATERIAL", &materialDescNEW );
	XOF_ASSERT( p );
	reinterpret_cast<Material*>( p )->GetShader()->AddUniform( "wvp" );
	reinterpret_cast<Material*>( p )->GetShader()->AddUniform( "colour" );

	// CREATE ENTITIES
	// ---
	// Create platform
    Entity *platformEntity = new Entity();
	platformEntity->AddDataComponent<RenderableDC>( 
		(Mesh*)mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MESH, "XOF_UNIT_CUBE" ),
		(Material*)mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "platformMaterial" ),	nullptr, &gCamera );
	platformEntity->AddDataComponent<TransformDC>();
	platformEntity->AddLogicComponent<BasicPassThroughLC>( platformEntity, &mEngine );
	reinterpret_cast<TransformDC*>( platformEntity->GetDataComponent( TEST_COMPONENTS::TRANSFORM_DC ) )->Scale( 10.f, 0.25f, 10.f ); // 0.01f, 0.01f, 0.01f );

	gTempEntities.emplace_back( std::move( platformEntity ) );

	// Create door
	Entity *moverEntity = new Entity();
	moverEntity->AddDataComponent<RenderableDC>(
		(Mesh*)mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MESH, "moverMesh" ),
		(Material*)mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "moverMaterial" ), nullptr, &gCamera );

	moverEntity->AddDataComponent<TransformDC>();
#if UNDEF_ME_FOR_RUNTIME
    TransformDC *tdc = reinterpret_cast<TransformDC*>( moverEntity->GetDataComponent( TEST_COMPONENTS::TRANSFORM_DC ) );
    tdc->Scale( 0.01f, 0.01f, 0.01f );
    tdc->Translate( 0.f, 0.25f, 0.f );
#endif
	Mesh::MeshDimensions md = ((Mesh*)mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MESH, "moverMesh" ))->GetDimensions();
    moverEntity->AddDataComponent<CollidableDC<AABBN*>>(
        ( ( md.min + md.max ) / 2.f ),// * tdc->GetTransform().translation ,
        ( ( md.max - md.min ) / 2.f ),// * tdc->GetTransform().scale,
		mEngine.resourceManager
    );

    moverEntity->AddLogicComponent<MoverLC>( moverEntity, &mEngine, MOVER_DIRECTION::Y_AXIS, 3.f, 1, 0.0005f );
    gTempEntities.emplace_back( std::move( moverEntity ) );

	gTempEvents.emplace_back( std::unique_ptr<Event>( new ScaleEvent( glm::vec3( 0.01f ) ) ) );
    gTempEvents.emplace_back( std::unique_ptr<Event>( new TranslateEvent( glm::vec3( 0.f, 0.15f, 0.f ) ) ) );

	// Create player
	Entity *playerEntity = new Entity();
	//playerEntity->AddDataComponent<CollidableDC<AABB>>( glm::vec3( gCamera.GetPosition() ), glm::vec3( 2.f ), mEngine.resourceManager );
	playerEntity->AddDataComponent<CameraDC>( (Camera*)&gCamera );
	playerEntity->AddLogicComponent<PlayerLC>( playerEntity, &mEngine );//, (CollidableDC<AABB>*)moverEntity->GetDataComponent( COLLIDABLE_AABB_DC ) );
	gTempEntities.emplace_back( std::move( playerEntity ) );

	// SETUP SHADER VARIBLES
	//Material *mat0 = reinterpret_cast<Material*>( mEngine.resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "material2" ) );
	//mat0->GetShader()->AddUniform( "wvp" );

	// SETUP CAMERA
	gCamera.Setup( glm::vec3( 5.f, 1.f, 4.f ), glm::vec3( 0.f, 0.f, -1.f ),
		static_cast<float>( mWindow->getSize().x ),  static_cast<float>( mWindow->getSize().y ),
        4.f / 3.f, //static_cast<float>( mWindow->getSize().x ) / static_cast<float>( mWindow->getSize().y ),
		0.1f, 1000.f );
}

void EnginePrep::HandleSystemEvents() {
	sf::Event event;
	while( mWindow->pollEvent( event ) ) {
        if( event.type == sf::Event::Closed || mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::ESCAPE ) ) {
			mWindow->close();
		}

		if( event.type == sf::Event::Resized ) {
			mEngine.renderer->SetScreenSize( event.size.width, event.size.height );
			gCamera.Resize( static_cast<float>( event.size.width ), static_cast<float>( event.size.height ) );

			gWindowWidthOverTwo = event.size.width / 2;
			gWindowHeightOverTwo = event.size.height / 2;
		}

		if( event.type == sf::Event::LostFocus ) {
			do {
				mWindow->pollEvent( event );
				mWindow->setTitle( "XOF [PAUSED]" );
			} while( event.type != sf::Event::GainedFocus );
			mWindow->setTitle( "XOF [RUNNING]" );
		}
	}
}

void EnginePrep::HandleGameEvents() {
	for( U32 i=0; i<gTempEvents.size(); ++i ) {
		for( U32 j=0; j<gTempEntities.size(); ++j ) {
			gTempEntities[j].get()->HandleEvent( gTempEvents[i].get() );
		}
	}
	gTempEvents.clear();
}

void EnginePrep::HandleInput( float dt ) {	
	// Movement
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::W ) ) {
        gTempEvents.emplace_back( std::unique_ptr<Event>( new TranslateEvent( glm::vec3( gCamera.GetViewDirection() * dt * mMovementSpeed ) ) ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::A ) ) {
        gTempEvents.emplace_back( std::unique_ptr<Event>( new TranslateEvent( glm::vec3( gCamera.GetRight() * dt * mMovementSpeed ) ) ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::S ) ) {
        gTempEvents.emplace_back( std::unique_ptr<Event>( new TranslateEvent( glm::vec3( -gCamera.GetViewDirection() * dt * mMovementSpeed ) ) ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::D ) ) {
        gTempEvents.emplace_back( std::unique_ptr<Event>( new TranslateEvent( glm::vec3( -gCamera.GetRight() * dt * mMovementSpeed ) ) ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::Q ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * mMovementSpeed, 0.f ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::E ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * -mMovementSpeed, 0.f ) );
	}
	// Looking around
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::ARROW_UP ) ) {
		gCamera.Pitch( dt * -mLookingSpeed );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::ARROW_DOWN ) ) {
		gCamera.Pitch( dt * mLookingSpeed );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::ARROW_LEFT ) ) {
        gCamera.Yaw( dt * mLookingSpeed );
        //gTempEvents.emplace_back( std::unique_ptr<Event>( new CameraYawEvent( dt * mLookingSpeed ) ) );
	}
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::ARROW_RIGHT ) ) {
        gCamera.Yaw( -dt * mLookingSpeed );
        //gTempEvents.emplace_back( std::unique_ptr<Event>( new CameraYawEvent( -dt * mLookingSpeed ) ) );
	}
	// Events
    if( mEngine.inputDevices.keyboard.IsKeyDown( XOF_KEYBOARD::SPACE_BAR ) ) {
		gTempEvents.emplace_back( std::unique_ptr<Event>( new PressedSpacebarEvent( gCamera.GetPosition() ) ) );
	}
	// Picking
    if( mEngine.inputDevices.mouse.IsButtonDown( XOF_MOUSE::LEFT ) ) {
		Ray pickingRay;
        CreatePickingRay( mEngine.inputDevices.mouse.GetRelativePosition(), &gCamera, mEngine.renderer->GetScreenDimensions(), FORWARDS, pickingRay );
		gTempEvents.emplace_back( std::unique_ptr<Event>( new FiredPickingRayEvent( pickingRay ) ) );
	}
}

void EnginePrep::Render( float dt ) {
    (void)dt;
	mEngine.renderer->RenderFrame();
	mWindow->display();
	mEngine.renderer->ClearScreen();
}
