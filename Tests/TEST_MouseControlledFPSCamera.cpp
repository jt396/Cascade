#include "EnginePrep.hpp"

#include <stdio.h>

// RENDERING
#include <SFML/Graphics.hpp>
#include "../Platform/XOF_Platform.hpp"
#include "../Platform/XOF_PlatformInfo.hpp"
#include "../Platform/XOF_Timer.hpp"
#include "../Rendering/XOF_Renderer_OpenGL.hpp"
#include "../Rendering/XOF_FirstPersonCamera.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Shader.hpp"

#include "../Rendering/XOF_MaterialManager.hpp"
MaterialManager *materialManager;

static const U32 MAX_BONES = 50;

FirstPersonCamera gCamera;
float gYaw = 0.f;
float gPitch = 0.f;


#include "../Game/TempGameObjects.hpp"
std::unique_ptr<TempGameObject> bookerObject;
std::unique_ptr<TempGameObject> platformObject;


// For camera movement, used to quickly reset the camera back to the middle of the screen
static U32 gWindowWidthOverTwo = 0;
static U32 gWindowHeightOverTwo = 0;


EnginePrep::EnginePrep() {}

EnginePrep::~EnginePrep() {
	mEngine.meshManager->ShutDown();
	mEngine.materialManager->ShutDown();
	mEngine.renderer->ShutDown();
}

bool EnginePrep::Initialise( U16 width, U16 height, CHAR* caption ) {
	if( !Game::Initialise( width, height, caption ) ) {
		return false;
	}

	//mWindow.get()->setFramerateLimit( 60 );

	mMouse.SetWindow( mWindow.get() );

	mEngine.meshManager = MeshManager::Get();
	mEngine.materialManager = MaterialManager::Get();
	mEngine.renderer.reset( new Renderer() );
	
	mEngine.meshManager->StartUp();
	mEngine.renderer->StartUp();
	mEngine.materialManager->StartUp();

	SetupScene();

	mMovementSpeed = 0.005f;
	mLookingSpeed = 0.0005f;

	mEngine.renderer->Resize( mWindow->getSize().x, mWindow->getSize().y );

	gWindowWidthOverTwo = mScreenHeight / 2;
	gWindowHeightOverTwo = mScreenWidth / 2;

	mWindow->setMouseCursorVisible( false );

	mIsInitialised = true;
	return mIsInitialised;
}

// frame-limit
static double FRAME_TIME = 1000.0 / XOF_TARGET_FRAMERATE;
void EnginePrep::Run() {
	if( mIsInitialised ) {
		Timer timer;
		timer.Reset();
		double startTime = timer.GetMilliseconds();
		
		double elapsedTime = 0.0;
		double timeDelta = 0.0;
		double unprocessedTime = 0.0;
		U32 frameCount = 0;

		double runningTime = 0.0;

		double lastTime = timer.GetMilliseconds();
		while( mWindow->isOpen() ) {
			double currentTime = timer.GetMilliseconds();
			timeDelta = currentTime - lastTime;
			lastTime = currentTime;

			runningTime = (currentTime - startTime) / 1000.f;

			HandleInput( static_cast<float>( timeDelta ) );
			HandleEvents();
			if( unprocessedTime > FRAME_TIME ) {
				Update( static_cast<float>( runningTime ) );
				Render( static_cast<float>( timeDelta ) );
				++frameCount;
				unprocessedTime -= FRAME_TIME;
			}

			// ============================== TEMP
			//static float f = 0.01f;
			//gTempSpotLight.direction.z = f;
			//static int sign = 1;
			//f += 0.001f * sign;
			//if( f > 5.f  || f < -5.f ) { sign = -sign; }
			// ============================== TEMP

			unprocessedTime += timeDelta;
			elapsedTime += timeDelta;
			if( elapsedTime >= 1000 ) {
				std::cout << "FPS: " << frameCount << std::endl;
				frameCount = 0;
				elapsedTime = 0.0;
			}
		} // while( mWindow->isOpen() ) 
	} // if( mIsInitialised )
}

void EnginePrep::Update( float dt ) {
	// for the animation, dt is infact the engine/game's run time
	bookerObject->Update( dt );

	// Game objects update/submit engine requests etc...
	//mEngine.renderer->SubmitRenderRequest( RenderRequest( XOF_RENDER_REQUEST_TYPE::RENDER_MESH,
	//	platformObject.get()->GetMaterial(), platformObject.get()->GetMesh(),
	//	nullptr, &gCamera, &platformObject.get()->GetTransform() ) );

	mEngine.renderer->SubmitRenderRequest( RenderRequest( XOF_RENDER_REQUEST_TYPE::RENDER_MESH,
		bookerObject.get()->GetMaterial(), bookerObject.get()->GetMesh(),
		nullptr, &bookerObject.get()->GetTransform(), &gCamera ) );
}

void EnginePrep::SetupScene() {
	// TODO: THIS HAS TO BE MOVED TO THE SHADER/MATERIAL CLASS
	//mEngine.materialManager->GetMaterial( "DEFAULT_MATERIAL" )->GetShader()->AddUniform( "transform" );
	//mEngine.materialManager->GetMaterial( "DEFAULT_MATERIAL" )->GetShader()->Bind();

	// ...
	MeshDesc bookerDesc;
	bookerDesc.type = MeshDesc::MESH_TYPE::MD5;
	bookerDesc.meshName = "bookerMesh";
	bookerDesc.asMeshFile.fileName = "./Resources/Meshes/boblampclean.md5mesh";
	bookerDesc.asMeshFile.materialName = "bookerMaterial";
	bookerDesc.asMeshFile.shaderToUseInMaterial = "./Resources/Shaders/DiffuseTexture_SkeletalAnimation";
	// The mesh manager will poke the material manager with the material details
	// given in the above desc in addition to those gathered from the .obj file
	mEngine.meshManager->AddMesh( bookerDesc );

	bookerObject.reset( new TempGameObject( &mEngine, glm::vec3( 0.f, 0.f, 0.f ), 
		mEngine.meshManager->GetMesh( "bookerMesh" ), 
		mEngine.materialManager->GetMaterial( "bookerMaterial" ), &gCamera ) );
	bookerObject.get()->Rotate( -1.56f, 0.f, 0.f );
	bookerObject.get()->Translate( glm::vec3( 0.f, 1.25f, -3.f ) );
	bookerObject.get()->Scale( 0.05f, 0.05f, 0.05f );

	// SETUP SHADER VARIBLES
	mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->AddUniform( "wvp" );
	mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->AddUniform( "world" );
	for( U32 i=0; i<MAX_BONES; ++i ) {
		I8 name[128];
		memset( name, 0x00, sizeof( name ) );
		_snprintf( name, sizeof( name ), "bones[%d]", i );
		mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->AddUniform( std::string( name ) );
	}
	mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->Bind();

	gCamera.Setup( glm::vec3( 0.f, 3.f, 0.f ), glm::vec3( 0.f, 0.f, 1.f ),
		static_cast<float>( mWindow->getSize().x ),  static_cast<float>( mWindow->getSize().y ),
		static_cast<float>( mWindow->getSize().x ) / static_cast<float>( mWindow->getSize().y ), 
		0.01f, 1000.f );
}

void EnginePrep::HandleEvents() {
	sf::Event event;
	while( mWindow->pollEvent( event ) ) {
		if( event.type == sf::Event::Closed || mKeyboard.IsKeyDown( XOF_KEYBOARD::ESCAPE ) ) {
			mWindow->close();
		}

		if( event.type == sf::Event::Resized ) {
			mEngine.renderer->Resize( event.size.width, event.size.height );
			gCamera.Resize( static_cast<float>( event.size.width ), static_cast<float>( event.size.height ) );

			gWindowWidthOverTwo = event.size.width / 2;
			gWindowHeightOverTwo = event.size.height / 2;
		}

		if( event.type == sf::Event::LostFocus ) {
			do {
				mWindow->pollEvent( event );
				mWindow->setTitle( "XOF_ExplorationGame::EnginePrepStage [PAUSED]" );
			} while( event.type != sf::Event::GainedFocus );
			mWindow->setTitle( "XOF_ExplorationGame::EnginePrepStage [RUNNING]" );
		}
	}
}

void EnginePrep::HandleInput( float dt ) {	
	// Looking
	glm::vec2 cameraRelativePos = mMouse.GetRelativePosition();
	mMouse.SetPosition( gWindowWidthOverTwo, gWindowHeightOverTwo );
	gYaw += ( gWindowWidthOverTwo - cameraRelativePos.x ) * mLookingSpeed * dt;
	gPitch += ( gWindowHeightOverTwo - cameraRelativePos.y ) * -mLookingSpeed * dt;
	gCamera.SphericalPitchAndYaw( gPitch, gYaw );

	// Movement
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::W ) ) {
		gCamera.SphericalTranslate( glm::vec3( gCamera.GetViewDirection() * dt * mMovementSpeed ) );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::A ) ) {
		gCamera.SphericalTranslate( glm::vec3( gCamera.GetRight() * dt * mMovementSpeed ) );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::S ) ) {
		gCamera.SphericalTranslate( glm::vec3( -gCamera.GetViewDirection() * dt * mMovementSpeed ) );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::D ) ) {
		gCamera.SphericalTranslate( glm::vec3( -gCamera.GetRight() * dt * mMovementSpeed ) );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::Q ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * mMovementSpeed, 0.f ) );
	}

	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::E ) ) {
		gCamera.SphericalTranslate( glm::vec3( 0.f, dt * -mMovementSpeed, 0.f ) );
	}
}

void EnginePrep::Render( float dt ) {
	mEngine.renderer->RenderFrame();
	mWindow->display();
}