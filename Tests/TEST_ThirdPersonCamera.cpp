#include "EnginePrep.hpp"

#include <stdio.h>

// RENDERING
#include <SFML/Graphics.hpp>
#include "../Platform/XOF_Platform.hpp"
#include "../Platform/XOF_PlatformInfo.hpp"
#include "../Platform/XOF_Timer.hpp"
#include "../Rendering/XOF_Renderer_OpenGL.hpp"
#include "../Rendering/XOF_FirstPersonCamera.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Shader.hpp"

#include "../Rendering/XOF_MaterialManager.hpp"
MaterialManager *materialManager;

static const U32 MAX_BONES = 50;

FirstPersonCamera gCamera;
float gYaw = 0.f;
float gPitch = 0.f;


//#include "../GameplayFoundations/XOF_GameObject.hpp"
#include "../Game/TempGameObjects.hpp"
std::unique_ptr<TempGameObject> bookerObject;
std::unique_ptr<TempGameObject> platformObject;


// For camera movement, used to quickly reset the camera back to the middle of the screen
static U32 gWindowWidthOverTwo = 0;
static U32 gWindowHeightOverTwo = 0;


#include "../Rendering/XOF_ThirdPersonCamera.hpp"
ThirdPersonCamera gTempThirdPersonCameraD;


EnginePrep::EnginePrep() {}

EnginePrep::~EnginePrep() {
	mEngine.meshManager->ShutDown();
	mEngine.materialManager->ShutDown();
	mEngine.renderer->ShutDown();
}

bool EnginePrep::Initialise( U16 width, U16 height, CHAR* caption ) {
	if( !Game::Initialise( width, height, caption ) ) {
		return false;
	}

	mMouse.SetWindow( mWindow.get() );

	mEngine.meshManager = MeshManager::Get();
	mEngine.materialManager = MaterialManager::Get();
	mEngine.renderer.reset( new Renderer() );
	
	mEngine.meshManager->StartUp();
	mEngine.renderer->StartUp();
	mEngine.materialManager->StartUp();

	SetupScene();

	mMovementSpeed = 0.005f;
	mLookingSpeed = 0.005f;

	mEngine.renderer->Resize( mWindow->getSize().x, mWindow->getSize().y );

	gWindowWidthOverTwo = mScreenHeight / 2;
	gWindowHeightOverTwo = mScreenWidth / 2;

	mWindow->setMouseCursorVisible( false );

	mIsInitialised = true;
	return mIsInitialised;
}

// frame-limit
static double FRAME_TIME = 1000.0 / XOF_TARGET_FRAMERATE;
void EnginePrep::Run() {
	if( mIsInitialised ) {
		Timer timer;
		timer.Reset();
		double startTime = timer.GetMilliseconds();
		
		double elapsedTime = 0.0;
		double timeDelta = 0.0;
		double unprocessedTime = 0.0;
		U32 frameCount = 0;

		double runningTime = 0.0;

		double lastTime = timer.GetMilliseconds();
		while( mWindow->isOpen() ) {
			double timeTakenThisFrame = timer.GetMilliseconds();

			double currentTime = timer.GetMilliseconds();
			timeDelta = currentTime - lastTime;
			lastTime = currentTime;

			runningTime = (currentTime - startTime) / 1000.f;

			HandleInput( static_cast<float>( timeDelta ) );
			HandleEvents();
			if( unprocessedTime > FRAME_TIME ) {
				Update( static_cast<float>( runningTime ) );
				Render( static_cast<float>( timeDelta ) );
				++frameCount;
				unprocessedTime -= FRAME_TIME;
			}

			unprocessedTime += timeDelta;
			elapsedTime += timeDelta;
			if( elapsedTime >= 1000 ) {
				std::cout << "FPS: " << frameCount << std::endl;
				frameCount = 0;
				elapsedTime = 0.0;
			}
			//std::cout << "FRAME-TIME: " << ( timer.GetMilliseconds() - timeTakenThisFrame ) << std::endl;
		} // while( mWindow->isOpen() ) 
	} // if( mIsInitialised )
}

// For the animation, dt is infact the engine/game's run time
void EnginePrep::Update( float dt ) {
	// Game objects update/submit engine requests etc...
	//platformObject->Update( dt );
	//mEngine.renderer->SubmitRenderRequest( RenderRequest( XOF_RENDER_REQUEST_TYPE::RENDER_MESH,
	//	platformObject.get()->GetMaterial(), platformObject.get()->GetMesh(),
	//	nullptr, &platformObject.get()->GetTransform(), &gTempThirdPersonCameraD ) );
	
	bookerObject->Update( dt );
	mEngine.renderer->SubmitRenderRequest( RenderRequest( XOF_RENDER_REQUEST_TYPE::RENDER_MESH,
		bookerObject.get()->GetMaterial(), bookerObject.get()->GetMesh(),
		nullptr, &bookerObject.get()->GetTransform(), &gTempThirdPersonCameraD ) );
}

void EnginePrep::SetupScene() {
	MeshDesc bookerDesc;
	bookerDesc.type = MeshDesc::MESH_TYPE::MD5;
	bookerDesc.meshName = "bookerMesh";
	bookerDesc.asMeshFile.fileName = "./Resources/Meshes/boblampclean.md5mesh";
	bookerDesc.asMeshFile.materialName = "material0";
	bookerDesc.asMeshFile.shaderToUseInMaterial = "./Resources/Shaders/DiffuseTexture";
	// The mesh manager will poke the material manager with the material details
	// given in the above desc in addition to those gathered from the .obj file
	mEngine.meshManager->AddMesh( bookerDesc );

	bookerObject.reset( new TempGameObject( &mEngine, glm::vec3( 0.f, 0.f, 0.f ), 
		mEngine.meshManager->GetMesh( "bookerMesh" ), mEngine.materialManager->GetMaterial( "material0" ), 
		&gTempThirdPersonCameraD ) );
	bookerObject.get()->Rotate( -1.56f, 0.f, 0.f );
	bookerObject.get()->Translate( glm::vec3( 0.f, 1.25f, -3.f ) );
	bookerObject.get()->Scale( 0.05f, 0.05f, 0.05f );

	// SETUP SHADER VARIBLES
	mEngine.materialManager->GetMaterial( "material0" )->GetShader()->AddUniform( "wvp" );
	//mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->AddUniform( "world" );
	//for( U32 i=0; i<MAX_BONES; ++i ) {
	//	I8 name[128];
	//	memset( name, 0x00, sizeof( name ) );
	//	_snprintf( name, sizeof( name ), "bones[%d]", i );
	//	mEngine.materialManager->GetMaterial( "bookerMaterial" )->GetShader()->AddUniform( std::string( name ) );
	//}
	mEngine.materialManager->GetMaterial( "material0" )->GetShader()->Bind();


	MeshDesc platformDesc;
	platformDesc.type = MeshDesc::MESH_TYPE::OBJ;
	platformDesc.meshName = "platformMesh";
	platformDesc.asMeshFile.fileName = "./Resources/Meshes/barrel.obj";
	platformDesc.asMeshFile.materialName = "platformMaterial";
	platformDesc.asMeshFile.shaderToUseInMaterial = "./Resources/Shaders/DiffuseTexture";
	// The mesh manager will poke the material manager with the material details
	// given in the above desc in addition to those gathered from the .obj file
	mEngine.meshManager->AddMesh( platformDesc );

	platformObject.reset( new TempGameObject( &mEngine, glm::vec3( 0.f, -2.f, 0.f ), 
		mEngine.meshManager->GetMesh( "platformMesh" ), mEngine.materialManager->GetMaterial( "XOF_DEFAULT_MATERIAL" ), 
		&gTempThirdPersonCameraD ) );

	gTempThirdPersonCameraD.Setup( glm::vec3( 0.f, 3.5f, 0.5f ), bookerObject->GetTransform().translation,
		static_cast<float>( mWindow->getSize().x ),  static_cast<float>( mWindow->getSize().y ),
		static_cast<float>( mWindow->getSize().x ) / static_cast<float>( mWindow->getSize().y ), 
		0.01f, 1000.f );
}

void EnginePrep::HandleEvents() {
	sf::Event event;
	while( mWindow->pollEvent( event ) ) {
		if( event.type == sf::Event::Closed || mKeyboard.IsKeyDown( XOF_KEYBOARD::ESCAPE ) ) {
			mWindow->close();
		}

		if( event.type == sf::Event::Resized ) {
			mEngine.renderer->Resize( event.size.width, event.size.height );
			gTempThirdPersonCameraD.Resize( static_cast<float>( event.size.width ), static_cast<float>( event.size.height ) );

			gWindowWidthOverTwo = event.size.width / 2;
			gWindowHeightOverTwo = event.size.height / 2;
		}

		if( event.type == sf::Event::LostFocus ) {
			do {
				mWindow->pollEvent( event );
				mWindow->setTitle( "XOF::GDC_Demo [PAUSED]" );
			} while( event.type != sf::Event::GainedFocus );
			mWindow->setTitle( "XOF::GDC_Demo [RUNNING]" );
		}
	}
}

void EnginePrep::HandleInput( float dt ) {	
	// Looking
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_LEFT ) ) {
		gTempThirdPersonCameraD.Yaw( -dt * mLookingSpeed );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_RIGHT ) ) {
		gTempThirdPersonCameraD.Yaw( dt * mLookingSpeed );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_UP ) ) {
		gTempThirdPersonCameraD.Pitch( -dt * mLookingSpeed );
	}
	if( mKeyboard.IsKeyDown( XOF_KEYBOARD::ARROW_DOWN ) ) {
		gTempThirdPersonCameraD.Pitch( dt * mLookingSpeed );
	}
}

void EnginePrep::Render( float dt ) {
	mEngine.renderer->RenderFrame();
	mWindow->display();
}