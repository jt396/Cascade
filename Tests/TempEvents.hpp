#ifndef TEMP_EVENTS_HPP
#define TEMP_EVENTS_HPP


#include "../GameplayFoundations/XOF_Event.hpp"
#include <glm/vec3.hpp>


enum TEMP_EVENTS {
	PRESSED_SPACE_BAR = 0,
	FIRED_PICKING_RAY,
	TRANSLATE,
	SCALE,
	CAM_PITCH,
	CAM_YAW,
};


struct PressedSpacebarEvent : public Event {
	enum ARGS {
		PLAYER_POS = 0,
	};

	PressedSpacebarEvent( const glm::vec3& playerPos ) { 
		type = TEMP_EVENTS::PRESSED_SPACE_BAR;

		arguments[PLAYER_POS].type = XOF_EVENT_ARG_TYPE::XEAT_VEC3F;
		arguments[PLAYER_POS].asVec3f[0] = playerPos.x;
		arguments[PLAYER_POS].asVec3f[1] = 0.f;
		arguments[PLAYER_POS].asVec3f[2] = playerPos.z;
	}
};

struct FiredPickingRayEvent : public Event {
	enum ARGS {
		RAY_ORIGIN = 0,
		RAY_DIRECTION,
	};

	FiredPickingRayEvent( const Ray& pickingRay ) { 
		type = TEMP_EVENTS::FIRED_PICKING_RAY;

		arguments[RAY_ORIGIN].asVec3f[0] = pickingRay.origin.x;
		arguments[RAY_ORIGIN].asVec3f[1] = pickingRay.origin.y;
		arguments[RAY_ORIGIN].asVec3f[2] = pickingRay.origin.z;
		arguments[RAY_DIRECTION].asVec3f[0] = pickingRay.direction.x;
		arguments[RAY_DIRECTION].asVec3f[1] = pickingRay.direction.y;
		arguments[RAY_DIRECTION].asVec3f[2] = pickingRay.direction.z;
	}
};

struct TranslateEvent : public Event {
	enum ARGS {
		TRANSLATE_AMOUNT = 0,
	};

	TranslateEvent( const glm::vec3& translation ) { 
		type = TEMP_EVENTS::TRANSLATE;

		arguments[TRANSLATE_AMOUNT].asVec3f[0] = translation.x;
		arguments[TRANSLATE_AMOUNT].asVec3f[1] = translation.y;
		arguments[TRANSLATE_AMOUNT].asVec3f[2] = translation.z;
	}
};

struct ScaleEvent : public Event {
	enum ARGS {
		SCALE_AMOUNT = 0,
	};

	ScaleEvent( const glm::vec3& scale ) { 
		type = TEMP_EVENTS::SCALE;

		arguments[SCALE_AMOUNT].asVec3f[0] = scale.x;
		arguments[SCALE_AMOUNT].asVec3f[1] = scale.y;
		arguments[SCALE_AMOUNT].asVec3f[2] = scale.z;
	}
};

struct CameraYawEvent : public Event {
	enum ARGS {
		YAW_AMOUNT = 0,
	};

	CameraYawEvent( float yaw ) { 
		type = TEMP_EVENTS::CAM_YAW;

		arguments[YAW_AMOUNT].asFloat = yaw;
	}
};



#endif // TEMP_EVENTS_HPP