#ifndef TEMP_TEST_ENTITIES_AND_COMPONENTS
#define TEMP_TEST_ENTITIES_AND_COMPONENTS


#include "../Platform/XOF_Platform.hpp"

#include "../GameplayFoundations/XOF_Entity.hpp"
#include "../GameplayFoundations/XOF_EngineRuntimeFrontEnd.hpp"

#include "../GameplayFoundations/XOF_Event.hpp"

#include "../Rendering/XOF_Mesh.hpp"
#include "../Rendering/XOF_Material.hpp"
#include "../Rendering/XOF_Transform.hpp"
#include "../Rendering/XOF_Camera.hpp"


//#include "../Collision&Physics/XOF_Sphere.hpp"
//#include "../Collision&Physics/XOF_AABB.hpp"
// TESTING VIRTUAL/TEMPLATED COMPONENTS - 29/02
//#include "../Collision&Physics/XOF_AABBNew.hpp"

// Turn certain stuff off for now for the editor
//#define ENABLE_FOR_RUNTIME_ONLY 1


enum TEST_COMPONENTS {
	// logic
	PASS_THROUGH_LC = 0,
	PLAYER_LC,
	MOVER_LC,
	// data
	CAMERA_DC,
	RENDERABLE_DC,
	TRANSFORM_DC,
	COLLIDABLE_SPHERE_DC,
	COLLIDABLE_AABB_DC,
	//
	UNASSIGNED,
};


// ------------------------------------------------------------------------------------------------------------------------------------------------- DATA
#include "../GameplayFoundations/XOF_ReflectionMacros.hpp"

XOF_REFLECTED_CLASS_BEGIN()
class RenderableDC : public Component {
public:
				RenderableDC() = delete;

                RenderableDC( const std::string& name, Mesh *mesh, Material *material, Transform *transform, Camera *camera ) :
                    Component( name ), mMesh( mesh ), mMaterial( material ), mCamera( camera ) {
					(void)transform;
                    mTypeID = RENDERABLE_DC;
                    memset( DEBUG_mTypeString, 0x00, 32 );
                    memcpy( DEBUG_mTypeString, "RenderableDC", sizeof("RenderableDC")-1 );
                }
				RenderableDC( Mesh *mesh, Material *material, Transform *transform, Camera *camera ) : 
					mMesh( mesh ), mMaterial( material ), mCamera( camera ) { 
					(void)transform;
					mTypeID = RENDERABLE_DC;
                    memset( DEBUG_mTypeString, 0x00, 32 );
                    memcpy( DEBUG_mTypeString, "RenderableDC", sizeof("RenderableDC")-1 );
				}

				~RenderableDC() override {}

//private: TESTING
    Mesh*       mMesh;
                XOF_REFLECTED_VARIABLE()
    Material*   mMaterial;
    Camera*     mCamera;
};
XOF_REFLECTED_CLASS_END()

XOF_REFLECTED_CLASS_BEGIN()
class TransformDC : public Component {
public:
                TransformDC() {
                    mTypeID = TEST_COMPONENTS::TRANSFORM_DC;
                    memset( DEBUG_mTypeString, 0x00, 32 );
                    memcpy( DEBUG_mTypeString, "TransformDC", sizeof("TransformDC")-1 );
                }

                TransformDC( const std::string& name ) : Component( name ) {
                    mTypeID = TEST_COMPONENTS::TRANSFORM_DC;
                    memset( DEBUG_mTypeString, 0x00, 32 );
                    memcpy( DEBUG_mTypeString, "TransformDC", sizeof("TransformDC")-1 );
                }

	void		Translate( const glm::vec3& v ) { mTransform.translation = v; }
	void		Translate( float x, float y, float z ) { mTransform.translation = glm::vec3( x, y, z ); }
	void		Rotate( float x, float y, float z ) { mTransform.rotation = glm::vec3( x, y, z ); }
	void		Scale( float x, float y, float z ) { mTransform.scale = glm::vec3( x, y, z ); }

	Transform&	GetTransform() { return mTransform; }

	glm::mat4x4	GetModelToWorldMatrix() { return mTransform.GetModelToWorldMatrix(); }

//private:

                XOF_REFLECTED_VARIABLE()
	Transform	mTransform;
};
XOF_REFLECTED_CLASS_END()


template<typename T>
class CollidableDC : public Component {
public:
			CollidableDC() = delete;
			~CollidableDC() { delete mCollisionVolume; }

			// AABB
			CollidableDC( const glm::vec3& center, const glm::vec3& halfExtents, ResourceManager *rm ) {
				mCollisionVolume = new AABBN( center, halfExtents, reinterpret_cast<Mesh*>( rm->GetResource( XOF_RESOURCE_TYPE::MESH, "XOF_UNIT_CUBE" ) ) );

				mTypeID = TEST_COMPONENTS::COLLIDABLE_AABB_DC;
				mIsHit = false;
			}

            // SPHERE
            //CollidableDC( const glm::vec3& center, const glm::vec3& radius, ResourceManager *rm ) {
                //mCollisionVolume = new SphereN
            //}

	T		GetCollisionVolume() const { return mCollisionVolume; }

	void	SetHitStatus( bool isHit ) { mIsHit = isHit; }
	bool	IsHit() const { return mIsHit; }

private:
	T		mCollisionVolume;
	bool	mIsHit;
};

/*#define AsCollidableAABB( cp ) reinterpret_cast<CollidableDC<AABB>*>( cp )
#define AsCollidableSphere( cp ) reinterpret_cast<CollidableDC<Sphere>*>( cp )

// NOTE: Making DataComponent::mTypeID const may allow this approach to work

#define TEST( p ) ((const CollidableDC<AABB*>*)( p ))->GetType()

const U32 GetCollidableType( const void *p ) { return reinterpret_cast<const CollidableDC<AABB>*>( p )->GetType(); }
template<TEST_COMPONENTS collidableTypeID> struct ToSpecificCollidable {};
template<> struct ToSpecificCollidable<TEST_COMPONENTS::COLLIDABLE_AABB_DC> { static CollidableDC<AABB>* foo( void *p ) { return reinterpret_cast<CollidableDC<AABB>*>( p ); } };*/


// Quick/temp wrapper around a camera so they can used as components
#include "../Rendering/XOF_FirstPersonCamera.hpp"
class CameraDC : public Component {
public:
		CameraDC( Camera *camera ) { 
			mTypeID = TEST_COMPONENTS::CAMERA_DC;
			mCamera = (FirstPersonCamera*)camera;
			//mCamera.reset( std::move( (FirstPersonCamera*)camera ) );
		}

//private:
	FirstPersonCamera *mCamera;
	//std::unique_ptr<FirstPersonCamera>	mCamera;
};


// ------------------------------------------------------------------------------------------------------------------------------------------------- LOGIC
#include "../Collision&Physics/XOF_CollisionWorld.hpp"

// TODO: ADD MOVER STARTING STATE (OPEN/CLOSED)
enum MOVER_LC_STATE	{
	OPEN = 0,
	OPENING,
	CLOSING,
	CLOSED,
};

enum MOVER_DIRECTION {
	X_AXIS = 0,
	Y_AXIS,
	Z_AXIS,
};
#if 1
//template<typename CollisionVolumeType>
class MoverLC : public Component {
public:
						MoverLC() = delete;
                        MoverLC( Entity *parentEntity, EngineRuntime *engine,                                                   // CORE ENGINE/ENTITY
							MOVER_DIRECTION directionToMove, float amountToMoveBy, I8 movementStepSign, float movementSpeed	)	// MOVER SPECIFIC
							: mParentEntity( parentEntity ), mEngine( engine ) {
							// CORE ENGINE/ENTITY
							mTypeID = TEST_COMPONENTS::MOVER_LC;

							mRenderableDC = reinterpret_cast<RenderableDC*>( mParentEntity->GetDataComponent( TEST_COMPONENTS::RENDERABLE_DC ) );
							XOF_ASSERT( mRenderableDC );
							mTransformDC = reinterpret_cast<TransformDC*>( mParentEntity->GetDataComponent( TEST_COMPONENTS::TRANSFORM_DC ) );
							XOF_ASSERT( mTransformDC );

							// CDC template param is used for the collision volume, so it must be a valid collision volume type
							mCollidableDC = reinterpret_cast<CollidableDC<BoundingVolume*>*>( mParentEntity->GetDataComponent( TEST_COMPONENTS::COLLIDABLE_AABB_DC ) );
							XOF_ASSERT( mCollidableDC );
							//const void *p = const_cast<void*>( mCollidableDC );
							//CollidableDC<AABB> *caabb = ToSpecificCollidable<GetCollidableType( p )>::foo( mCollidableDC );
							//XOF_ASSERT( AsCollidableAABB( mCollidableDC ) );


							// MOVER SPECIFIC
							mDirectionToMoveAlong = directionToMove;
							mAmountToMoveBy = amountToMoveBy;
							mMovementSpeed = movementSpeed;
							mMovementStepSign = movementStepSign;
							mAmountMoved = 0.f;
							mMoverState = CLOSED;
						}

	virtual void		Update( float dt ) override {
							// Update mover logic
							float absMoved = 0.f, absMoveBy = 0.f, step = 0.f;

							if( mMoverState == OPENING || mMoverState == CLOSING ) {
								absMoved = std::abs( mAmountMoved );
								absMoveBy = std::abs( mAmountToMoveBy );
								step = mMovementStepSign * ( mMovementSpeed * dt );
								switch( mMoverState ) {
									case OPENING:	if( absMoved > absMoveBy ) {
														mMoverState = OPEN;
														mAmountMoved = 0.f;	
													} else {														
														mAmountMoved += step;
														mTransformDC->mTransform.translation[mDirectionToMoveAlong] += step;
														mCollidableDC->GetCollisionVolume()->Transform( glm::vec3( 0.f, step, 0.f ), nullptr );
													}
													break;

									case CLOSING:	if( absMoved > absMoveBy ) {
														mMoverState = CLOSED;
														mAmountMoved = 0.f;
													} else {
														mAmountMoved -= step;
														mTransformDC->mTransform.translation[mDirectionToMoveAlong] -= step;
														mCollidableDC->GetCollisionVolume()->Transform( glm::vec3( 0.f, -step, 0.f ), nullptr );
													}
													break;
								}
							}

							
							// Set WVP matrix
							glm::mat4x4 MVP = mRenderableDC->mCamera->GetProjectionMatrix() * mRenderableDC->mCamera->GetViewMatrix() *
								mCollidableDC->GetCollisionVolume()->GetVisualRepresentationTransform();
								//mTransformDC->GetModelToWorldMatrix();

							// Draw collision volume - TEMP?
							Material *debugMat = reinterpret_cast<Material*>( 
								mEngine->resourceManager->GetResource( XOF_RESOURCE_TYPE::MATERIAL, "XOF_DEBUG_DRAW_MATERIAL" ) );

							debugMat->GetShader()->Bind();
							debugMat->GetShader()->SetUniform( "colour", mCollidableDC->IsHit()? glm::vec4( 1.f, 0.f, 0.f, 1.f ) : glm::vec4( 0.f, 1.f, 0.f, 1.f ) );
							debugMat->GetShader()->SetUniform( "wvp", MVP );

							mEngine->renderer->DEBUG_DrawBoundingVolume( mCollidableDC->GetCollisionVolume()->GetVisualRepresentation() ); //visualRepresentation );
							
							// Submit render request
							RenderRequest rr( XOF_RENDER_REQUEST_TYPE::RENDER_MESH, mRenderableDC->mMaterial,
								mRenderableDC->mMesh, &mTransformDC->GetTransform(), mRenderableDC->mCamera );

							MVP = mRenderableDC->mCamera->GetProjectionMatrix() * mRenderableDC->mCamera->GetViewMatrix() * mTransformDC->GetModelToWorldMatrix();

							std::string rrpn( "wvp" );
							rr.shaderArgs[0].type = XOF_SHADER_PARAM_TYPE::MAT4x4F;
							memcpy( rr.shaderArgs[0].name, rrpn.c_str(), rrpn.length() );
							memcpy( rr.shaderArgs[0].value, &MVP, sizeof( glm::mat4x4 ) );

							mEngine->renderer->SubmitRenderRequest( rr );
						}

	virtual void		HandleEvent( const Event *e ) override {
							(void)e;
#if 0
							switch( e->type ) {
								case TEMP_EVENTS::PRESSED_SPACE_BAR: Move(); break;

								case TEMP_EVENTS::FIRED_PICKING_RAY: mCollidableDC->SetHitStatus( TestForIntersection( Ray( glm::vec3( 
																									e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] ),
																									glm::vec3( e->arguments[1].asVec3f[0], e->arguments[1].asVec3f[1], e->arguments[1].asVec3f[2] ) ),
																									reinterpret_cast<AABBN&>( *mCollidableDC->GetCollisionVolume() ) ) ); break;

								case TEMP_EVENTS::SCALE:	mTransformDC->GetTransform().scale *= glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] );
															mCollidableDC->GetCollisionVolume()->Transform( glm::vec3( 0.f ), 
																&( glm::scale( glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] ) ) ) );
															break;
                                case TEMP_EVENTS::TRANSLATE:	mTransformDC->GetTransform().translation = glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] );
                                                                //mCollidableDC->GetCollisionVolume()->Transform( glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] ), nullptr );
																break;
							}
#endif
						}

private:
	Entity				*mParentEntity;
    EngineRuntime   	*mEngine;

	RenderableDC		*mRenderableDC;
	TransformDC			*mTransformDC;

	//CollidableDC<AABB>	*mCollidableDC;
    CollidableDC<BoundingVolume*>	*mCollidableDC;

	MOVER_LC_STATE		mMoverState;
	MOVER_DIRECTION		mDirectionToMoveAlong;
	float				mAmountToMoveBy;
	float				mAmountMoved;
	float				mMovementSpeed;
	I8					mMovementStepSign;

	void				Move() {
							switch( mMoverState ) {
								case OPEN: mMoverState = CLOSING;
								case OPENING:
								case CLOSING: break;
								case CLOSED: mMoverState = OPENING;
							}
						}
};
#endif

#include "../Collision&Physics/XOF_CollisionWorld.hpp"
class PlayerLC : public Component {
public:
						PlayerLC() = delete;

                        PlayerLC( Entity *entity, EngineRuntime *engine ) { //CollidableDC<AABB> *tempMoverCollidable ) {
							XOF_ASSERT( entity ); 
							mTypeID = PLAYER_LC;
							mEngine = engine;

							mCameraDC = (CameraDC*)entity->GetDataComponent( TEST_COMPONENTS::CAMERA_DC );
							XOF_ASSERT( mCameraDC );
							//mCollidableDC = (CollidableDC<AABB>*)entity->GetDataComponent( TEST_COMPONENTS::COLLIDABLE_AABB_DC );
							//XOF_ASSERT( mCollidableDC );

							//TEMP_MoverCollidableDC = tempMoverCollidable;
						}

	virtual void		Update( float dt ) override { (void)dt; }

	virtual void		HandleEvent( const Event *e ) override {
							(void)e;
#ifdef ENABLE_FOR_RUNTIME_ONLY
							switch( e->type ) {
                                case TEMP_EVENTS::TRANSLATE: //if( !TestForCollision( *mCollidableDC->GetCollisionVolume(), *TEMP_MoverCollidableDC->GetCollisionVolume() ) ) {
																	mCameraDC->mCamera->SphericalTranslate( 
																		glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] ) );
                                                                    //mCollidableDC->GetCollisionVolume()->Transform(
                                                                    //	glm::vec3( e->arguments[0].asVec3f[0], e->arguments[0].asVec3f[1], e->arguments[0].asVec3f[2] ), nullptr );
                                                                //}
                                                             break;
								case TEMP_EVENTS::CAM_PITCH: mCameraDC->mCamera->Pitch( e->arguments[0].asFloat ); break;
								case TEMP_EVENTS::CAM_YAW: mCameraDC->mCamera->Yaw( e->arguments[0].asFloat ); break;
							}
#endif
						}

private:
    EngineRuntime       *mEngine;

	CameraDC			*mCameraDC;
	//CollidableDC<AABB>	*mCollidableDC;
	//CollidableDC<AABB>	*TEMP_MoverCollidableDC;
};

XOF_REFLECTED_CLASS_BEGIN()
class BasicPassThroughLC : public Component {
public:
						BasicPassThroughLC() = delete;

                        BasicPassThroughLC( Entity *entity, EngineRuntime *engine  ) {
                            XOF_ASSERT( entity && engine );
							mTypeID = PASS_THROUGH_LC;
							mEngine = engine;

							mRenderable = (RenderableDC*)entity->GetDataComponent( TEST_COMPONENTS::RENDERABLE_DC );
							mTransform = (TransformDC*)entity->GetDataComponent( TEST_COMPONENTS::TRANSFORM_DC );
                            XOF_ASSERT( mRenderable && mTransform );

                            memset( DEBUG_mTypeString, 0x00, 32 );
                            memcpy( DEBUG_mTypeString, "BasicPassThroughLC", sizeof("BasicPassThroughLC")-1 );
						}

	virtual void		Update( float dt ) override {
							(void)dt;

							RenderRequest rr( XOF_RENDER_REQUEST_TYPE::RENDER_MESH, mRenderable->mMaterial,
								mRenderable->mMesh, &mTransform->GetTransform(), mRenderable->mCamera );

                            // Set WVP matrix - BROKEN HERE
                            glm::mat4x4 MVP = mRenderable->mCamera->GetProjectionMatrix() *
                                mRenderable->mCamera->GetViewMatrix() * mTransform->GetModelToWorldMatrix();

							std::string rrpn( "wvp" );
							rr.shaderArgs[0].type = XOF_SHADER_PARAM_TYPE::MAT4x4F;
							memcpy( rr.shaderArgs[0].name, rrpn.c_str(), rrpn.length() );
							memcpy( rr.shaderArgs[0].value, &MVP, sizeof( glm::mat4x4 ) );

							mEngine->renderer->SubmitRenderRequest( rr );
						}

	virtual void		HandleEvent( const Event *e ) override { (void)e; }

private:
	RenderableDC		*mRenderable;
	TransformDC			*mTransform;
    EngineRuntime       *mEngine;
};
XOF_REFLECTED_CLASS_END()


#endif // TEMP_TEST_ENTITIES_AND_COMPONENTS
