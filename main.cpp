/*
===============================================================================

	XOF
	===
	File	:	Main.cpp
	Desc	:	Program start point.

===============================================================================
*/
#include "Tests/EnginePrep.hpp"
#include <iostream>

/*int main() {
    return 0;
    system( "PAUSE" );
}*/
#if 1
int main( int argc, char **argv ) {
    XOF_UNUSED_PARAMETER( argc );
    XOF_UNUSED_PARAMETER( argv );

	std::cout << "XOF\n---" << std::endl;

	EnginePrep testHarness;
	if( testHarness.Initialise( 640, 480, "XOF [RUNNING]" ) ) {
		testHarness.Run();
	} else {
		std::cout << "XOF Could not initialise." << std::endl;
	}

	return 0;
}
#endif
